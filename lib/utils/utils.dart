import 'dart:io';
import 'dart:math';

import 'package:Heals/config/config.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/model/user_info_model.dart';
import 'package:Heals/utils/mapUtil.dart';
import 'package:Heals/widgets/dialog_btn_widget.dart';
import 'package:Heals/widgets/dialog_content_widget.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:uuid/uuid.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

String getSize(size) {
  var k = 1024;
  var m = 1024 * 1024;
  var g = 1024 * 1024 * 1024;
  String result = size == null
      ? '0'
      : size > g
          ? (size / g).toStringAsFixed(2) + 'G'
          : size > m
              ? (size / m).toStringAsFixed(2) + 'M'
              : (size / k).toStringAsFixed(2) + 'Kb';
  return result;
}

// 超过千或万显示方式
String getNum(int? count) {
  var k = 1000;
  var w = 10000;
  String result = (count == null || count == 0
      ? ''
      : count > w
          ? (count / w).toStringAsFixed(1).replaceAll('.0', '') + 'w'
          : count > k
              ? (count / k).toStringAsFixed(1).replaceAll('.0', '') + 'k'
              : count.toString());
  return result;
}

void showBiuDialog(int typ, Function onConfirm) {
  Get.defaultDialog(
    title: "",
    titleStyle: const TextStyle(fontSize: 0),
    content: Column(
      children: [
        Text(typ == 1 ? "ad_display_tips".tr : "cancel_biu_tips".tr,
            style: const TextStyle(color: Colors.white, fontSize: 16.0)),
        Text("is_go_on".tr, style: const TextStyle(color: Colors.white, fontSize: 16.0)),
        typ == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Text("gift_for_no_ad".tr,
                    style: const TextStyle(color: HColors.secondaryText, fontSize: HFontSizes.miner)),
              )
            : const SizedBox(),
      ],
    ),
    radius: 16.0,
    backgroundColor: const Color(0xFF2E2E33),
    // cancel: DialogBtnWidget(typ: 1, onTap: () => Get.back()),
    // confirm: DialogBtnWidget(
    //     typ: 2,
    //     onTap: () {
    //       onConfirm();
    //       Get.back();
    //     }),
  );
}

Future<List<String>> getImagePathList(List<AssetEntity> pickedFileList) async {
  List<String> imgPathList = [];
  bool isExitOverSize = false;
  for (var pickedFile in pickedFileList) {
    // if (pickedFile.relativePath != null && pickedFile.title != null) {
    File? file = await pickedFile.file;

    final dir = await path_provider.getTemporaryDirectory();

    final targetPath = dir.absolute.path + "/temp.jpg";

    File? result = await FlutterImageCompress.compressAndGetFile(
      file!.absolute.path,
      targetPath,
      quality: 50,
      rotate: 0,
    );

    int size = await file.length();
    if (size > 20971520) {
      isExitOverSize = true;
      continue;
    }
    imgPathList.add(result!.absolute.path);
    // }
  }
  if (isExitOverSize) {
    EasyLoading.showToast("failed_to_send_to_big".tr);
  }
  return imgPathList;
}

String timeFormatter(int time) {
  DateTime now = DateTime.now();
  DateTime fTime = DateTime.fromMillisecondsSinceEpoch(time);
  Duration difference = now.difference(fTime);
  if (difference.inDays == 0) {
    return "HH:mm";
  } else if (difference.inDays <= 7) {
    return "EEE HH:mm";
  } else if (difference.inDays <= 365) {
    return "MM-dd HH:mm";
  }

  return "yyy-MM-dd HH:mm";
}

String timeFormatterSeconds(int time) {
  return "${NumberFormat("00").format(((time / 60).floor() % 60).ceil())}:${NumberFormat("00").format((time % 60).ceil())}";
}

String formatterHours(int time) {
  return "${NumberFormat("00").format((time / (60 * 60)).floor())}:${NumberFormat("00").format(((time / 60).floor() % 60).ceil())}:${NumberFormat("00").format((time % 60).ceil())}";
}

String skipLastChar(String text) {
  return text.characters.skipLast(1).toString();
}

String namedTime(int seconds) {
  if (seconds < 60) {
    return "$seconds秒";
  } else if (seconds >= 60 && seconds < 3600) {
    return "${seconds ~/ 60}分钟";
  } else if (seconds >= 3600 && seconds < 86400) {
    return "${seconds ~/ 3600}小时";
  } else {
    return "${seconds ~/ 86400}天";
  }
}

// String nearTime(int time) {
//   int now = nowTimestamp();
//   int sendTime = time * 1000;
//   return now - sendTime >= 86400000
//       ? DateFormat(DateTime.now().year != DateTime.fromMillisecondsSinceEpoch(sendTime).year ? 'yyy-MM-dd' : 'MM-dd')
//           .format(DateTime.fromMillisecondsSinceEpoch(sendTime))
//       : timeago.format(DateTime.fromMillisecondsSinceEpoch(sendTime), locale: "zh_CN");
// }

bool isEmpty(String str) {
  if (str.isEmpty) {
    return true;
  }
  return false;
}

String getFileType(fileName) {
  return fileName.split('.')[fileName.split('.').length - 1];
}

void showCuzDialog(String title,
    {titleWidget,
    textAlign,
    barrierDismissible,
    textCancel,
    textConfirm,
    onlySubmitBtn = false,
    onCancel,
    onConfirm,
    autoBack = true}) {
  Get.defaultDialog(
    title: "",
    titleStyle: const TextStyle(fontSize: 0),
    content: titleWidget ??
        DialogContentWidget(
            child: Text(title,
                textAlign: textAlign ?? TextAlign.center,
                style: const TextStyle(color: HColors.mainColorText, fontSize: 16.0))),
    radius: 16.0,
    backgroundColor: Colors.white,
    barrierDismissible: barrierDismissible ?? true,
    cancel: onlySubmitBtn
        ? null
        : DialogBtnWidget(
            typ: 1,
            textCancel: textCancel ?? "cancel".tr,
            onTap: () {
              Get.back();
              if (onCancel != null) {
                onCancel();
              }
            }),
    confirm: DialogBtnWidget(
      onlySubmitBtn: onlySubmitBtn,
      typ: 2,
      textConfirm: textConfirm ?? "confirm".tr,
      onTap: () {
        if (autoBack) {
          Get.back();
        }
        if (onConfirm != null) {
          onConfirm();
        }
      },
    ),
  );
}

void beautyToast() {
  EasyLoading.instance
    ..radius = 12.0
    ..backgroundColor = HColors.mainColor;
}

String timeFormat(int seconds) {
  if (seconds == 0) {
    return "00:00";
  }

  int theHours = 0;
  int theMins = 0;
  int theSeconds = 0;
  theHours = seconds ~/ 3600;
  theMins = (seconds - (theHours * 3600)) ~/ 60;
  theSeconds = (seconds - (theHours * 3600) - (theMins * 60));

  NumberFormat formater = NumberFormat("00");
  if (theHours > 0) {
    return "${formater.format(theHours)}:${formater.format(theMins)}:${formater.format(theSeconds)}";
  }

  return "${formater.format(theMins)}:${formater.format(theSeconds)}";
}

Future<double> getTotalSizeOfFilesInDir(final FileSystemEntity file) async {
  if (file is File) {
    int length = await file.length();
    return double.parse(length.toString());
  }
  if (file is Directory) {
    final List<FileSystemEntity> children = file.listSync();
    double total = 0;
    for (final FileSystemEntity child in children) {
      total += await getTotalSizeOfFilesInDir(child);
    }
    return total;
  }
  return 0;
}

Future<void> delDir(FileSystemEntity directory) async {
  if (directory is Directory) {
    final List<FileSystemEntity> children = directory.listSync();
    for (final FileSystemEntity child in children) {
      await delDir(child);
    }
  } else {
    await directory.delete(recursive: true);
  }
}

String getImageNameByPath(String filePath) {
  return filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length);
}

String getSavePath({required String path, required String fileFirType, required String id}) {
  String fileName = getImageNameByPath(path);
  String fileType = getFileType(fileName);
  String uuid1 = const Uuid().v1();
  String key = '$fileFirType/$id/$uuid1.' + fileType;
  return key;
}

int nowTimestamp() {
  return DateTime.now().millisecondsSinceEpoch ~/ 1000;
}

String phoneTextJudge(String? text) {
  return text == null || text.isEmpty ? '' : "+$text";
}

String textJudge(String? text) {
  return text == null || text.isEmpty ? '' : text;
}

String getStatusTxt(BookingModel bookingModel) {
  String status = bookingModel.state ?? '';
  Map<String, String> appointmentStatus = {
    "PENDING": "status_pendding".tr,
    "PENDING_QUESTIONNAIRE": "status_que".tr,
    "PENDING_DEPOSIT": "status_deposit".tr,
    "HOLD": "status_comfirm".tr,
    "SUCCESS": "status_upcoming".tr,
    "FINISH": "status_do".tr,
    // "CANCELLED": "Cancelled",
    "CANCEL": "Cancelled",
    "REPLIED_FAIL": "Failed reply",
    "FAIL": "Failed",
  };
  // print("status===$status");
  if (isClinicCancelled(bookingModel)) {
    return "clinic_cancel_tip".tr;
  }
  if (isClinicRejected(bookingModel)) {
    return "clinic_rejected_tip".tr;
  }
  if (isPatientCancelled(bookingModel)) {
    return "patient_cancel_tip".tr;
  }
  if (isPatientRejected(bookingModel)) {
    return "patient_rejected_tip".tr;
  }
  return appointmentStatus[status] ?? 'NoFound';
}

bool isUpcoming(BookingModel bookingModel) {
  return isSuccess(bookingModel.state!) && !isExpried(bookingModel.appointmentDate, bookingModel.appointmentTime);
}

bool isPenddding(BookingModel bookingModel) {
  return bookingModel.state == "PENDING" || isUnfinish(bookingModel.state!) || isHold(bookingModel.state!);
}

bool isPast(BookingModel bookingModel) {
  // 失败或 成功  或过期都为过去
  return (isCancel(bookingModel.state!) ||
      isFinish(bookingModel.state!) ||
      isFailed(bookingModel) ||
      isExpried(bookingModel.appointmentDate, bookingModel.appointmentTime));
}

bool isFailed(BookingModel bookingModel) {
  return isClinicRejected(bookingModel) ||
      isPatientRejected(bookingModel) ||
      isPatientCancelled(bookingModel) ||
      isClinicCancelled(bookingModel);
}

bool isPastCanAgain(BookingModel bookingModel) {
  return (isFinish(bookingModel.state!) ||
      (isExpried(bookingModel.appointmentDate, bookingModel.appointmentTime) && isSuccess(bookingModel.state!)));
}

bool isUnfinish(String state) {
  return isQuestionnaire(state) || isDeposit(state);
}

bool isQuestionnaire(String state) {
  return state == "PENDING_QUESTIONNAIRE";
}

bool isDeposit(String state) {
  return state == "PENDING_DEPOSIT";
}

bool isSuccess(String status) {
  return status == "SUCCESS";
}

bool isCancel(String status) {
  return status == "CANCEL";
}

bool isFinish(String status) {
  return status == "FINISH";
}

bool isFailState(String status) {
  return status == "FAIL";
}

bool isHold(String status) {
  return status == "HOLD";
}

// 当状态=FAIL 并且 userCancel 则前端显示Clinic rejected
bool isClinicRejected(BookingModel bookingModel) {
  return isFailState(bookingModel.state!) && bookingModel.userCancel == null;
}

// 当状态=FAIL 并且 user_reply不为空， 则前端显示Patient rejected
bool isPatientRejected(BookingModel bookingModel) {
  return isFailState(bookingModel.state!) && bookingModel.userReply != null;
}

// 当 user_cancel字段不为空， 则 前端显示 Patient cancelled
bool isPatientCancelled(BookingModel bookingModel) {
  return isFailState(bookingModel.state!) && bookingModel.userCancel != null;
}

// 当 clinic_cancel字段不为空， 则 前端显示 Clinic cancelled
bool isClinicCancelled(BookingModel bookingModel) {
  return bookingModel.clinicCancel != null;
}

String? getotherStatus(BookingModel bookingModel) {
  String? result;
  if (isClinicCancelled(bookingModel)) {
    result = "clinic_status_cancel_tip".tr;
  }
  if (isClinicRejected(bookingModel)) {
    result = "clinic_status_rejected_tip".tr;
  }
  if (isPatientCancelled(bookingModel)) {
    result = "patient_status_cancel_tip".tr;
  }
  if (isPatientRejected(bookingModel)) {
    result = "patient_status_rejected_tip".tr;
  }
  return result;
}

int timeNowStamp() {
  DateTime dateNow = DateTime.now();
  return (dateNow.hour * 60 * 60 + dateNow.minute * 60 + dateNow.second);
}

int dateToStamp(date) {
  DateTime dateTemp = DateTime.parse(date);
  int dateStr = dateTemp.millisecondsSinceEpoch ~/ 1000;
  return dateStr;
}

bool isExpried(String? date, time) {
  if (date == null) return false;
  int dateStr = dateToStamp(date);
  int midNight = nowTimestamp() - timeNowStamp();
  if ((dateStr - midNight) < 0) {
    return true;
  }
  return false;
}

bool isToday(date) {
  int dateStr = dateToStamp(date);
  int midNight = nowTimestamp() - timeNowStamp();
  if ((dateStr - midNight) == 0) {
    return true;
  }
  return false;
}

String formatterNum(String number) {
  var formatter = NumberFormat('# ## 000');
  return formatter.format(number);
}

bool userinfoIsFull(UserInfoModel userInfoModel) {
  bool result = true;
  Map<String, dynamic> map = userInfoModel.toJson((value) => {});
  try {
    map.forEach((key, value) {
      if (Constants.profileRequired.contains(key) && value.isEmpty) {
        result = false;
        throw Exception();
      }
    });
  } catch (e) {}
  return result;
}

String getDistance(double? lat1, double? lng1, double? lat2, double? lng2) {
  /// 单位：米
  /// def ：地球半径
  if (lat1 == null || lng1 == null || lat2 == null || lng2 == null) {
    return '';
  }
  double def = 6378137.0;
  double radLat1 = _rad(lat1);
  double radLat2 = _rad(lat2);
  double a = radLat1 - radLat2;
  double b = _rad(lng1) - _rad(lng2);
  double s = 2 * asin(sqrt(pow(sin(a / 2), 2) + cos(radLat1) * cos(radLat2) * pow(sin(b / 2), 2)));
  double distance = (s * def).roundToDouble();
  String result = '';
  if (distance > 1000) {
    result = (distance / 1000).toStringAsFixed(2) + ' km';
  } else {
    result = distance.toStringAsFixed(2) + ' m';
  }
  return result;
}

double _rad(double d) {
  return d * pi / 180.0;
}

String phoneFormat(String? phone) {
  if (phone == null) return '';
  var phoneResult = phone;
  if (phone.isNotEmpty && (phone.startsWith('86_') || phone.startsWith('853_') || phone.startsWith('852_'))) {
    phoneResult = '+' + phone.split("_")[0] + ' ' + phone.split("_")[1];
  }
  return phoneResult;
}

void selectMap(clinic) {
  List<SelectItemModel> mapList = [
    SelectItemModel(id: 1, title: 'google_map'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 3, title: 'autoNavi_map'.tr, cancel: false, isSelected: false),
  ];
  if (Platform.isIOS) {
    mapList.add(SelectItemModel(id: 5, title: 'apple_map'.tr, cancel: false, isSelected: false));
  }
  Get.bottomSheet(
    SelectDialog(
      contentList: mapList,
      isBlackBg: true,
      callback: (item) {
        Get.back();
        if (item.id == -1) return;
        switch (item.id) {
          case 1:
            MapUtil.gotoGoogleMap(clinic!.longitude, clinic!.latitude);
            break;
          case 3:
            MapUtil.gotoAMap(clinic!.longitude, clinic!.latitude);
            break;
          case 5:
            MapUtil.gotoAppleMap(clinic!.longitude, clinic!.latitude);
            break;
          default:
            MapUtil.gotoAMap(clinic!.longitude, clinic!.latitude);
        }
      },
    ),
  );
}

void addEventtoCalender({title, description, location, required String startDate, required String endDate}) {
  int year = int.parse(startDate.split("-")[0]);
  int month = int.parse(startDate.split("-")[1]);
  int day = int.parse(startDate.split("-")[2]);
  final Event event = Event(
    title: 'Pill Reminder 服藥提醒',
    description: description ?? '',
    location: location ?? '',
    startDate: DateTime(year, month, day),
    endDate: DateTime(year, month, day),
    iosParams: const IOSParams(
      reminder: Duration(/* Ex. hours:1 */), // on iOS, you can set alarm notification after your event.
    ),
    androidParams: const AndroidParams(
      emailInvites: [], // on Android, you can add invite emails to your event.
    ),
  );
  Add2Calendar.addEvent2Cal(event);
}

// 动态获取权限
// 默认获取phones, camera, microphone, storage
Future<Map<Permission, PermissionStatus>> requestPermissions({List<Permission>? permissions}) async {
  if (permissions == null || permissions.isEmpty) {
    permissions = [
      Permission.photos,
      Permission.camera,
      Platform.isAndroid ? Permission.microphone : Permission.speech,
      Permission.storage,
    ];
  }

  Map<Permission, PermissionStatus> status = await permissions.request();

  return status;
}

String getCountryCode() {
  String countryCode = Get.deviceLocale?.countryCode!.toUpperCase() ?? '';

  if (!Constants.codes.contains(countryCode)) {
    countryCode = 'TW';
  }
  if (Get.deviceLocale!.languageCode.toUpperCase() == 'EN') {
    countryCode = 'EN';
  }
  return countryCode;
}
