// typedef UnknownException = Exception;
// typedef BiuException = ApiException;
// typedef FriendException = ApiException;
// typedef SwapException = ApiException;

class ApiException implements Exception {
  late int _code;
  late String _message;

  int get code => _code;
  String get message => _message;

  ApiException(cause) {
    _code = 0;
    _message = "";

    if (cause is List && cause.length > 1) {
      _code = cause[0] as int;
      _message = cause[1] as String;
    }
  }
}
