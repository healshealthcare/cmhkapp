import 'dart:io';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';

class MapUtil {
  /// 高德地图
  static Future<void> gotoAMap(longitude, latitude) async {
    var url =
        '${Platform.isAndroid ? 'android' : 'ios'}amap://navi?sourceApplication=amap&lat=$latitude&lon=$longitude&dev=0&style=2';

    // bool canLaunchUrl = await canLaunch(url);
    try {
      bool canLaunchUrl = await launch(url);
      if (!canLaunchUrl) {
        EasyLoading.showToast("unfound_amap".tr);
        // return false;
      }
    } catch (e) {
      EasyLoading.showToast("unfound_amap".tr);
      print(e);
    }
    // if (!canLaunchUrl) {
    //   EasyLoading.showToast("unfound_amap".tr);
    //   return false;
    // }

    // await launch(url);

    // return true;
  }

  /// 腾讯地图
  static Future<bool> gotoTencentMap(longitude, latitude) async {
    var url =
        'qqmap://map/routeplan?type=drive&fromcoord=CurrentLocation&tocoord=$latitude,$longitude&referer=IXHBZ-QIZE4-ZQ6UP-DJYEO-HC2K2-EZBXJ';
    bool canLaunchUrl = await canLaunch(url);
    try {
      launch(url);
    } catch (e) {
      print(e);
    }
    if (!canLaunchUrl) {
      EasyLoading.showToast('未检测到腾讯地图~');
      return false;
    }

    await launch(url);

    return canLaunchUrl;
  }

  /// 百度地图
  static Future<bool> gotoBaiduMap(longitude, latitude) async {
    var url = 'baidumap://map/direction?destination=$latitude,$longitude&coord_type=bd09ll&mode=driving';

    bool canLaunchUrl = await canLaunch(url);

    if (!canLaunchUrl) {
      EasyLoading.showToast("unfound_baidu_map".tr);
      return false;
    }

    await launch(url);

    return canLaunchUrl;
  }

  /// 苹果地图
  static Future<bool> gotoAppleMap(longitude, latitude) async {
    var url = 'https://maps.apple.com/?q=$latitude,$longitude';

    bool canLaunchUrl = await canLaunch(url);

    if (!canLaunchUrl) {
      EasyLoading.showToast('fail');
      return false;
    }

    return await launch(url);
  }

  /// 谷歌地图
  static Future<void> gotoGoogleMap(longitude, latitude) async {
    var url = Platform.isAndroid ? 'geo:$latitude,$longitude' : 'comgooglemaps://?center=$latitude,$longitude';
    // var url = 'https://www.google.com/maps/search/?api=1&destination=$latitude,$longitude';
    try {
      bool canLaunchUrl = await launch(url);
      if (!canLaunchUrl) {
        EasyLoading.showToast("unfound_google_map".tr);
        // return false;
      }
    } catch (e) {
      EasyLoading.showToast("unfound_google_map".tr);
      print(e);
    }
    // bool canLaunchUrl = await canLaunch(url);

    // return await launch(url);
  }
}
