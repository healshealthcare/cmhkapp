// 主题配置类

import 'package:flutter/material.dart';

/// 配置项目中可能用到的主题颜色等内容
/// 默认首先显示的系统样式为 0 位置
List<ThemeData> themes = [
  ThemeData(
    // theme : light 模式
    fontFamily: "PingFang",
    scaffoldBackgroundColor: Colors.white,

    ///作为Scaffold下的Material默认颜色，用于materia应用程序或app内页面的背景色。
    primaryColor: const Color(0xFFFFD24D),

    ///App主要部分的背景色（ToolBar,Tabbar等）
    primaryColorDark: const Color(0xFFFE014D),

    ///primaryColor的较深版本 -主要色块的渐变较深版
    primaryColorLight: const Color(0xFFFFD033),

    ///前景色(按钮、文本、覆盖边缘效果等)
    cardColor: Colors.white,

    ///用在卡片(Card)上的Material的颜色。
    backgroundColor: const Color(0xFFE0E3E8),

    ///与primaryColor对比的颜色(例如 用作进度条的剩余部分)。
    dividerColor: const Color(0xFFEDEFF2),

    ///分隔符(Dividers)和弹窗分隔符(PopupMenuDividers)的颜色，也用于ListTiles和DataTables的行之间。
    indicatorColor: const Color(0xffffad33),

    ///TabBar中选项选中的指示器颜色
    dialogBackgroundColor: Colors.white,

    ///Dialog元素的背景色
    secondaryHeaderColor: const Color(0xFFFAFBFD),

    ///有选定行时PaginatedDataTable标题的颜色
    splashColor: Colors.white.withOpacity(0.95),

    ///墨水溅出的颜色
    toggleableActiveColor: const Color(0xFFFFAD33),

    ///用于突出显示切换Widget（如Switch，Radio和Checkbox）的活动状态的颜色。
    brightness: Brightness.light, // statusBar颜色
    hintColor: const Color(0xFFA1A7B3),

    ///用于提示文本或占位符文本的颜色，例如在TextField中。
    disabledColor: const Color(0xFFEDEFF2),

    ///无效的部件(widget)的颜色，不管它们的状态如何。例如，一个禁用的复选框(可以选中或不选中)。
    highlightColor: Colors.black.withOpacity(0.05),

    ///用于墨水喷溅动画或指示菜单被选中时的高亮颜色
    selectedRowColor: const Color(0xFFFFAD33), // 选择颜色      ///用于高亮选定行的颜色。
    unselectedWidgetColor:
        const Color(0xFFE8EAF2), // 未选择组件颜色     ///小部件处于非活动(但启用)状态时使用的颜色。例如，未选中的复选框。通常与accentColor形成对比
    errorColor: const Color(0xFFFF3333),

    ///Material中RaisedButtons使用的默认填充色。
    focusColor: const Color(0xFFFFD033),

    /// 输入框focus的竖线颜色
    canvasColor: Colors.white,

    ///MaterialType.canvas Material的默认颜色。
    // textSelectionColor: Color(0xFFFFD033), ///文本字段(如TextField)中文本被选中的颜色。
    shadowColor: const Color(0xB5FFC912),

    /// 按钮内阴影颜色
    appBarTheme: const AppBarTheme(color: Colors.white),
    colorScheme: const ColorScheme(
      // 颜色
      primary: Color(0xFF292C33), // 主要字体颜色
      primaryVariant: Color(0xFF45474D), // 二级字体颜色
      secondary: Color(0xFF666F80), // 三级字体颜色
      secondaryVariant: Color(0xFF7B8499), // placeholder 字体颜色
      surface: Color(0xFFA1A7B3), //
      onSurface: Color(0xFFDCDFE6),
      background: Colors.white,
      error: Color(0xFFFF3333),
      onSecondary: Color(0xFF4080FF),
      onPrimary: Color(0xFF58CF5A),
      onBackground: Color(0xFF7169D8),
      onError: Color(0xFFF24F4F),
      brightness: Brightness.light,
    ),
    buttonTheme: const ButtonThemeData(
        colorScheme: ColorScheme(
            primary: Color(0xFFFFD033),
            primaryVariant: Color(0xFFFFAD33),
            secondary: Color(0xFF41D454),
            secondaryVariant: Color(0xFF22AA7D),
            surface: Color(0x80E4EDFF), // 按钮外阴影颜色
            background: Color(0xFFE933FF),
            error: Color(0xFFFF3333),
            onPrimary: Color(0xFFEDEFF2), // 按钮未活动背景色
            onSecondary: Color(0xFFFF7433), // 按钮渐变重色
            onSurface: Color(0xFFFFB240), // 按钮渐变轻色
            onBackground: Color(0xFF7169D8),
            onError: Color(0xFFF24F4F),
            brightness: Brightness.light)),
    toggleButtonsTheme: const ToggleButtonsThemeData(
      color: Color(0xFFFFE284),
      disabledColor: Color(0xFFDCDFE6),
      selectedColor: Color(0xFFFF7433),
    ),
    textTheme: const TextTheme(
      headline1: TextStyle(
          // 块级大标题
          fontSize: 18,
          color: Color(0xFF292C33),
          fontWeight: FontWeight.bold),
      headline2: TextStyle(
          // list 大标题
          fontSize: 16,
          color: Color(0xFF45474D),
          fontWeight: FontWeight.bold),
      headline3: TextStyle(fontSize: 16, color: Color(0xFF292C33), fontWeight: FontWeight.bold),
      headline4: TextStyle(
          // 入口 主标题
          fontSize: 12,
          color: Color(0xFF292C33)),
      subtitle1: TextStyle(
          // list 副标题
          fontSize: 12,
          color: Color(0xFF45474D)),
      subtitle2: TextStyle(
        fontSize: 14,
        color: Color(0xFF737780),
      ),
      caption: TextStyle(
          // 缺省页等文案
          fontSize: 12,
          color: Color(0xFF7B8499)),
    ),
    primaryTextTheme: const TextTheme(

        ///与primary color形成对比的文本主题。
        // 主要字体主题
        // title: TextStyle(
        //     // 主要标题样式
        //     fontSize: 14,
        //     color: Color(0xFF1D1D1E),
        //     fontWeight: FontWeight.bold),
        // subhead: TextStyle(fontSize: 10, color: Color(0xFFB0B2BF)), // 副标题样式
        ),
    tabBarTheme: const TabBarTheme(
      // tabBar 字体颜色
      // 导航tab主题
      labelStyle: TextStyle(
        color: Color(0xFF45474D),
        fontSize: 10,
      ),
      unselectedLabelStyle: TextStyle(
        color: Color(0xFFDCDFE6),
        fontSize: 10,
      ),
      labelColor: Color(0xFFFFB933),
      unselectedLabelColor: Color(0xFF8C8F9D),
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      // 底部导航主题
      color: Colors.white.withOpacity(.55),
    ),
    inputDecorationTheme: InputDecorationTheme(
      fillColor: const Color(0xFF5A5C63).withOpacity(.05),
    ),
  ),
  ThemeData(
    // 默认theme 效果(dark模式)
    fontFamily: "PingFang",
    scaffoldBackgroundColor: const Color(0xFF000000),
    backgroundColor: const Color(0xFF0D0D0D), //
    primaryColor: const Color(0xFF272a3f), //
    primaryColorDark: const Color(0xFF121212),
    primaryColorLight: const Color(0xFF000000),
    cardColor: Colors.white.withOpacity(.06),
    dividerColor: const Color(0xFF454860),
    indicatorColor: const Color(0xFF8144E5),
    dialogBackgroundColor: const Color(0x80303030),
    secondaryHeaderColor: const Color(0xFF121212), // 次要头部颜色
    splashColor: const Color(0xE6101010),
    toggleableActiveColor: const Color(0xFFFFB933), // active 颜色
    brightness: Brightness.dark,
    hintColor: Colors.white.withOpacity(.05), // 小标签颜色
    disabledColor: const Color(0xFFD1D6DC), // 失效颜色
    // highlightColor: Color(0xFFFFB933), // 高亮色
    selectedRowColor: const Color(0xFFFFB933),
    unselectedWidgetColor: Colors.white.withOpacity(.10), // 未选择组件颜色
    errorColor: const Color(0xFFF43745),
    focusColor: Colors.white.withOpacity(.04), // 头部渐变色
    hoverColor: Colors.white.withOpacity(.04), // 头��渐变色
    canvasColor: const Color(0xE6000000),
    // textSelectionColor: Colors.white,
    shadowColor: const Color(0xB5FFC912),
    colorScheme: ColorScheme(
      primary: Colors.white, // 主要字体颜色
      primaryVariant: Colors.white.withOpacity(.8), // 二级字体颜色
      secondary: Colors.white.withOpacity(.6), // 三级字体颜色
      secondaryVariant: Colors.white.withOpacity(.4), // placeholder 字体颜色
      surface: Colors.black,
      background: Colors.white.withOpacity(.1),
      error: const Color(0xFFF43745),
      onPrimary: Colors.white.withOpacity(.6),
      onSecondary: Colors.white.withOpacity(.7),
      onSurface: Colors.white.withOpacity(.8),
      onBackground: Colors.white.withOpacity(.95),
      onError: const Color(0xFFF43745),
      brightness: Brightness.dark,
    ),
    buttonTheme: ButtonThemeData(
        buttonColor: const Color(0xFFFFE284), //登录注册页面未输入状态的按钮颜色
        splashColor: Colors.white.withOpacity(0.95),
        disabledColor: const Color(0xFFDCDFE6),

        ///按钮不重要或失效背景色
        highlightColor: const Color(0xFFFF7433),

        ///按钮活动渐变重色
        hoverColor: const Color(0xFFFFB240),

        ///按钮活动轻色7
        colorScheme: const ColorScheme(
          primary: Color(0xFFFFD033),
          primaryVariant: Color(0xFFFFAD33),
          secondary: Color(0xFF41D454),
          secondaryVariant: Color(0xFF22AA7D),
          surface: Color(0xFFFF3333),
          background: Color(0xFFE933FF),
          error: Color(0xFFFF3333),
          onPrimary: Color(0xFFEDEFF2), // 按钮未活动背景色
          onSecondary: Colors.white,
          onSurface: Colors.white,
          onBackground: Colors.white,
          onError: Color(0xFFF24F4F),
          brightness: Brightness.dark,
        )),
    textTheme: const TextTheme(
      headline1: TextStyle(
          // 块级大标题
          fontSize: 18,
          color: Color(0xFF292C33),
          fontWeight: FontWeight.bold),
      headline2: TextStyle(
          // list 大标题
          fontSize: 16,
          color: Color(0xFF45474D),
          fontWeight: FontWeight.bold),
      headline4: TextStyle(
          // 入口 主标题
          fontSize: 12,
          color: Color(0xFF292C33)),
      subtitle1: TextStyle(
          // list 副标题
          fontSize: 12,
          color: Color(0xFF45474D)),
      caption: TextStyle(
          // 缺省页等文案
          fontSize: 12,
          color: Color(0xFF7B8499)),
    ),
    primaryTextTheme: const TextTheme(
        // title: TextStyle(
        //     fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold),
        // subhead: TextStyle(fontSize: 10, color: Colors.white.withOpacity(.4)),
        ),
    tabBarTheme: const TabBarTheme(
      // 导航tab主题
      labelStyle: TextStyle(
        color: Color(0xFF45474D),
        fontSize: 10,
      ),
      unselectedLabelStyle: TextStyle(
        color: Color(0xFFDCDFE6),
        fontSize: 10,
      ),
      labelColor: Color(0xFFFFB933),
      unselectedLabelColor: Color(0xFF8C8F9D),
    ),
    bottomAppBarTheme: const BottomAppBarTheme(
      color: Color(0xE6101010),
    ),
    inputDecorationTheme: InputDecorationTheme(
      fillColor: Colors.white.withOpacity(.1),
    ),
  ),
];

///记录当前被选中的主题
