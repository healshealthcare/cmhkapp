import 'package:flutter/material.dart';

class HStyles {
  static const positiveDialogActionStyle = TextStyle(fontSize: HFontSizes.small, color: HColors.primary);
  static const titleBarHeight = 56.0;
  static const itemHeight = 44.0;
}

class HIconSizes {
  static const large = 56.0;
  static const medium = 24.0;
  static const normal = 18.0;
  static const small = 12.0;
}

class HFontSizes {
  static const aboutTtile = 44.0;
  static const particular = 32.0;
  static const hugest = 28.0;
  static const huger = 26.0;
  static const huge = 24.0;
  static const larger = 22.0;
  static const large = 20.0;
  static const medium = 18.0;
  static const normal = 16.0;
  static const small = 14.0;
  static const smaller = 13.0;
  static const smallest = 12.0;
  static const min = 11.0;
  static const miner = 10.0;
  static const minest = 9.0;
  static const limit = 8.0;
}

class HColors {
  static const Color primary = Color(0xFFF9F9F9);
  static const Color lightPrimary = Color(0xFFF8F9F9);
  static const Color lighterPrimary = Color(0xFF2E2E33);
  static const Color darkPrimary = Color(0xFF227CC8);

  static const Color mainColor = Color(0xFF227CC8);
  static const Color minorColor = Color(0xFFFE014D);
  static const Color btnBg = Color(0xFFE6F4FF);

  static const Color border = Color(0xFFEEEEEE);
  static const Color btnBorder = Color(0xFF4CADFF);
  static const Color btnClose = Color(0xFFF6F6F6);
  static const Color borderClose = Color(0xFF707070);
  static const Color txtClose = Color(0xFF707070);
  static const Color borderHr = Color(0xFFEBEBEB);
  static const Color bgLight = Color(0xFFF2F3F6);
  static const Color disabled = Color(0xFFD1D6DC);

  static const Color success = Color(0xFF00C86D);
  static const Color warning = Color(0xFFDF471F);
  static const Color danger = Color(0xFFFF3333);
  static const Color info = Color(0xFFA3A3A3);
  static const Color hightLight = Color(0xFFFF8213);
  static const Color hightLightOp = Color(0xFFFFFAF6);

  static const Color mainText = Color(0xFF07333C);
  static const Color regularText = Color(0xCC009888); // 80%
  static const Color secondaryText = Color(0xFF9E9E9E); // 60%
  static const Color timeText = Color(0x66009888); // 40%
  static const Color thirdText = Color(0x4D009888); // 30%
  static const Color unactiveText = Color(0xFF73BAB1); // 8%
  static const Color greyText = Color(0xFFBABABA); // 8%
  static const Color statusText = Color(0xFFEF9B2E); // 8%
  static const Color notColor = Color(0x2B997546); // 8%
  static const Color notBg = Color(0xFFFFFBF9); // 8%

  static const Color heiLightText = Color(0xFFFF8213); // 8%

  static const Color placeholderColor = Color(0xFF314A9C); // 70%
  static const Color fifthColor = Color(0x1A1BBC9B); // 10%
  static const Color sixthColor = Color(0x14009888); // 8%

  static const Color chatLeftColor = Color(0x29009888); // 16%
  static const Color chatRightColor = Color(0x2EFFD24D); // 18%

  static const Color mainTextRes = Color(0xFF0B0B0D);

  static const Color mainColorText = Color(0xFF227CC8);
  static const Color subColorText = Color(0xFF07333C);
  static const Color dialogBgBlack = Color(0xFFFFFFFF);

  static const Color tabbarBg = Color(0xFFFFFFFF);
  static const Color tagBg = Color(0x2B46998C);
  static const Color tipBg = Color(0xFF176EB7);
  static const Color inputBg = Color(0x0A009888);
  static const Color dotBg = Color(0xFFC5C5C5);
  static const Color statusTagBg = Color(0xFFFEF4E5);
  static const Color dateCardBg = Color(0xFFE6F4FF);

  static const Color shadowColor = Color(0x2B467D99);
  static const Color timeLineColor = Color(0xFFCFCFCF);
  static const Color smallImportantColor = Color(0xFF021221);
}

// 100%	0%	00
// 99%	1%	03
// 98%	2%	05
// 97%	3%	07
// 96%	4%	0A
// 95%	5%	0D
// 94%	6%	0F
// 93%	7%	12
// 92%	8%	14
// 91%	9%	17
// 90%	10%	1A
// 89%	11%	1C
// 88%	12%	1E
// 87%	13%	21
// 86%	14%	24
// 85%	15%	26
// 84%	16%	29
// 83%	17%	2B
// 82%	18%	2E
// 81%	19%	30
// 80%	20%	33
// 79%	21%	36
// 78%	22%	38
// 77%	23%	3B
// 76%	24%	3D
// 75%	25%	40
// 74%	26%	42
// 73%	27%	45
// 72%	28%	47
// 71%	29%	4A
// 70%	30%	4D
// 69%	31%	4F
// 68%	32%	52
// 67%	33%	54
// 66%	34%	57
// 65%	35%	59
// 64%	36%	5C
// 63%	37%	5E
// 62%	38%	61
// 61%	39%	63
// 60%	40%	66
// 59%	41%	69
// 58%	42%	6B
// 57%	43%	6E
// 56%	44%	70
// 55%	45%	73
// 54%	46%	75
// 53%	47%	78
// 52%	48%	7A
// 51%	49%	7D
// 50%	50%	80
// 49%	51%	82
// 48%	52%	85
// 47%	53%	87
// 46%	54%	8A
// 45%	55%	8C
// 44%	56%	8F
// 43%	57%	91
// 42%	58%	94
// 41%	59%	96
// 40%	60%	99
// 39%	61%	9C
// 38%	62%	9E
// 37%	63%	A1
// 36%	64%	A3
// 35%	65%	A6
// 34%	66%	A8
// 33%	67%	AB
// 32%	68%	AD
// 31%	69%	B0
// 30%	70%	B3
// 29%	71%	B5
// 28%	72%	B8
// 27%	73%	BA
// 26%	74%	BD
// 25%	75%	BF
// 24%	76%	C2
// 23%	77%	C4
// 22%	78%	C7
// 21%	79%	C9
// 20%	80%	CC
// 19%	81%	CF
// 18%	82%	D1
// 17%	83%	D4
// 16%	84%	D6
// 15%	85%	D9
// 14%	86%	DB
// 13%	87%	DE
// 12%	88%	E0
// 11%	89%	E3
// 10%	90%	E6
// 9%	91%	E8
// 8%	92%	EB
// 7%	93%	ED
// 6%	94%	F0
// 5%	95%	F2
// 4%	96%	F5
// 3%	97%	F7
// 2%	98%	FA
// 1%	99%	FC
// 0%	100%	FF
