class Constants {
  static String app = 'Heals';
  static Map<String, dynamic> lang = {"en": "en", "cn": "zh-Hans", "tw": "zh-Hant"};
  static Map<String, dynamic> month = {
    '1': 'Jan',
    '2': 'Feb',
    '3': 'Mar',
    '4': 'Apr',
    '5': 'May',
    '6': 'Jun',
    '7': 'Jul',
    '8': 'Aug',
    '9': 'Sep',
    '10': 'Oct',
    '11': 'Nov',
    '12': 'Dec',
  };
  static Map<String, dynamic> week = {
    'Mon': 'Monday',
    'Tue': 'Tuesday',
    'Wed': 'Wednesday',
    'Thu': 'Thursday',
    'Fri': 'Friday',
    'Sat': 'Saturday',
    'Sun': 'Sunday',
  };
  // static Map<String, dynamic> profile = {
  //   'id_num': 'ID Number',
  //   'chi_name': 'Chinese Name',
  //   'eng_name': 'English Name',
  //   'relationship': 'Relationship',
  //   'telephone': 'Mobile No.',
  //   // 'gender': 'May',
  //   'email': 'Email',
  //   'address': 'Jul',
  //   'emerg_name': 'Emergency Contact',
  //   'emerg_phone': 'Contact Phone',
  //   'nickname': 'Nick Name',
  // };
  static List<String> profileRequired = [
    'id_num',
    'chi_name',
    'eng_name',
    // 'dob',
    'telephone',
    'gender',
    // 'email',
    // 'address',
    // 'emerg_name',
    // 'emerg_phone',
    // 'nickname'
  ];
  static String formText = "TEXT";
  static String formSelect = "SELECT";
  static String formDate = "DATE";
  static String formPhone = "PHONE";
  static String formAddr = "ADDRESS";
  static String formRadio = "RADIO";
  static String formCheckbox = "CHECKBOX";
  static List<String> codes = ['EN', 'CN', 'TW'];

  static String phoneRegex = r"(852)?((2[1-9]|3[145679]|5[1234569]9[0-8])[0-9]{7}|6[0-9]{8})$";
  static String passRegex = "^(?:(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).{6,12}";
  static String idRegex = r"[A-Z][0-9]{6}\([0-9]|[A-Z]\)$";
}
