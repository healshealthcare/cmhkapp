import 'dart:io';

import 'package:dio/dio.dart';

class Config {
  static const bool productMode = bool.fromEnvironment("dart.vm.product");
//   dev：
// hostname=https://api-dev.healshealthcare.com/api/v1
// uat：
// hostname=https://api-uat.healshealthcare.com/api/v1
// prod：
// hostname=https://api.healshealthcare.com/api/v1
  static const String baseURL =
      // productMode ? "https://api-dev.healshealthcare.com/api/v1" : "https://api-dev.healshealthcare.com/api/v1"; // 服务器
      productMode ? "https://api.healshealthcare.com/api/v1" : "https://api-dev.healshealthcare.com/api/v1"; // 服务器

  static const String uploadUrl = "https://heals-nc1.s3-ap-east-1.amazonaws.com";
  static const int notifyPeriod = 30;
  static const int connTO = 30000;
  static const int receiveTO = 30000;
  static const int appointmentimer = 10;
  static const int queuetimer = 1;
  static const ResponseType resp = ResponseType.json;

  static String channel = "offical"; // 友盟渠道 offical-官方 hw-华为 op-oppo vv-vivo xm-小米 mz-魅族 yyb-应用宝 cc-虫虫助手 ios-apple
  static String androidVersion = "1.0.0+1"; // 安卓更新版本号
  static String androidDownloadUrl = ""; // 安卓更新版本号
  static String deliveryUrl = "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/delivery.html";
  static Map<String, dynamic> paymentUrl = {
    'EN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/terms-of-service.html",
    'CN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/tcsc.html",
    'TW': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/tctc.html",
  };
  static Map<String, dynamic> privacyUrl = {
    'EN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/privacy-policy.html",
    'CN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/privacy-policysc.html",
    'TW': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/privacy-policytc.html",
  };
  static Map<String, dynamic> termUrl = {
    'EN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/terms-of-service.html",
    'CN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/tcsc.html",
    'TW': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/tctc.html",
  };
  static Map<String, dynamic> helpUrl = {
    'EN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/help.html",
    'CN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/helpsc.html",
    'TW': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/helptc.html",
  };
  static Map<String, dynamic> commercialUrl = {
    'EN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/commercial.html",
    'CN': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/commercial.html",
    'TW': "https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/commercial.html",
  };

  static Map<String, dynamic> headers = {
    // "os": Platform.isAndroid ? "android" : (Platform.isIOS ? "ios" : "other"),
    // "channel": "1",
  };
  static String passRule = "[a-zA-Z]|[0-9]";
  static String contentRule = "[a-zA-Z]|[\u4e00-\u9fa5]|[0-9]";
  static String phoneRule = "[0-9]";
  static String androidAppKey = "61d1d3aee0f9bb492bb61c36"; // 友盟安卓 key
  static String iosAppKey = "61d1d442e014255fcbd41b1a"; // 友盟ios key

  static Map<String, Map<String, String>> get thirdLogin => Platform.isIOS
      ? {
          "wx": {
            "appID": "",
            "appSecret": "",
            "universalLink": "",
            "minAppID": "",
          },
          "qq": {
            "appID": "",
            "appSecret": "",
          },
          "apple": {},
        }
      : {
          "wx": {
            "appID": "",
            "appSecret": "",
            "universalLink": "",
            "minAppID": "",
          },
          "qq": {
            "appID": "",
            "appSecret": "",
          },
        };
}
