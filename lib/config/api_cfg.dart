class ApiCfg {
  static const userInfo = "/user/profile";
  static const userSmsReq = "/user/sms/request";
  static const userRegister = "/user/register_v2";
  static const userAuth = "/user/auth";
  static const userAvatar = "/user/get_icon";
  static const forgetPswUrl = "/user/forget_password";
  static const logoutURL = "/user/logout";
  static const deleteAccURL = "/user/unregister";
  static const userAuthOtp = "/user/auth_otp";
  static const uploadTokenUrl = "/user/create_upload_token";
  static const languageUrl = "/user/language";

  static const homeUrl = "/home/feeds";
  static const doctorListUrl = "/doctor_clinic/search";
  static const appointmentBookUrl = "/appointments/book";
  static const specialtiesUrl = "/doctor/specialties";
  static const relationshipUrl = "/user/dependents";
  static const relationshipAddUrl = "/user/dependents";
  static const relationshipUpdateUrl = "/user/dependents/update";
  static const deleteFamilyUrl = "/user/dependents/delete";
  static const questionnaireUrl = "/appointments/questionnaire";
  static const questionnaireAnswerUrl = "/appointments/questionnaire_answer";
  static const favoriteDoctorUrl = "/my/favorite_doctor";
  static const doctorDetailUrl = "/my/favorite_doctor/status";
  static const delFavoriteUrl = "/my/favorite_doctor/delete";
  static const districtsUrl = "/districts";

  static const myBookingUrl = "/appointments";
  static const replyAppointUrl = "/appointments/reply";
  static const cancelAppointUrl = "/appointments/cancel";
  static const depositSettingUrl = "/appointments/deposit_settings";
  static const referralLetterUrl = "/appointments/referral_letter_images";
  static const formTreeUrl = "/visit/form_address_tree";
  static const visitFormUrl = "/visit/registration_form";
  static const visitDetailUrl = "/visit/detail";
  static const visitCheckInFormUrl = "/visit/check_in_with_form";
  static const appointmentNotesUrl = "/appointments/detail_notes";

  static const unionpayUrl = "/unionpay";
  static const unionpayAddUrl = "/unionpay/add";
  static const unionpayPayUrl = "/unionpay/pay";
  static const editUserUrl = "/user/profile";
  static const medicalHisUrl = "/medical_histories";
  static const relativeUrl = "/relationship";
  static const visitRemoteUrl = "/visit/remote";
  static const visitCheckinUrl = "/visit/check_in";
  static const visitRemoteCancelUrl = "/visit/remote/cancel";
  static const visitStatusUrl = "/visit/status_v2201";

  static const changePassUrl = "/user/change_password";
  static const changeIconUrl = "/user/change_icon";
  static const resetPassUrl = "/reset_pw";
  static const messageUrl = "/user/message";
  static const messageUnreadUrl = "/user/message/count_unread";
  static const markReadUrl = "/user/message/mark_read";
  static const invoiceUrl = "/invoices";
  static const addressUrl = "/address_book/address_tree";
  static const myAddressUrl = "/address_book/get";
  static const addAddressUrl = "/address_book/add";
  static const updateAddressUrl = "/address_book/update";
  static const deleteAddressUrl = "/address_book/del";

  static const payUrl = "/cmhk_pay/pay_transaction";
  // static const couponListUrl = "/cmhk_pay/coupon_list";
  static const couponGroupListUrl = "/cmhk_pay/coupon_group_list";
  static const couponDetailUrl = "/cmhk_pay/coupon_detail";
}
