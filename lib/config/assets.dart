class AssetsInfo {
  static const tabHomeActive = "assets/images/tab_home_active.png";
  static const tabHome = "assets/images/tab_home.png";
  static const tabCheckInActive = "assets/images/tab_check_in_active.png";
  static const tabCheckIn = "assets/images/tab_check_in.png";
  static const tabHealthSupActive = "assets/images/tab_health_passport_active.png";
  static const tabHealthSup = "assets/images/tab_health_passport.png";
  static const tabAccountActive = "assets/images/tab_account_active.png";
  static const tabAccount = "assets/images/tab_account.png";

  static const headerBg = "assets/images/header_bg.png";
  static const doctorBg = "assets/images/doctor_bg.png";
  static const backIcon = "assets/images/back_icon.png";
  static const backWhiteIcon = "assets/images/back_white_icon.png";
  static const selectIconHlIcon = "assets/images/select_icon_hl.png";
  static const selectIconNlIcon = "assets/images/select_icon_nl.png";
  static const arrorSmallIcon = "assets/images/arror_small_icon.png";
  static const plusIcon = "assets/images/plus_icon.png";
  static const noAppointment = "assets/images/no_appointment.png";

  // home
  static const homeTopBg = "assets/images/top_bg.png";
  static const notification = "assets/images/notification.png";
  static const search = "assets/images/search.png";
  static const findDoctor = "assets/images/find_doctor.png";
  static const myBooking = "assets/images/my_booking.png";
  static const drugDelivery = "assets/images/drug_delivery.png";

  static const userIcon = "assets/images/user.png";
  static const doctorIcon = "assets/images/doctor.png";
  static const houseIcon = "assets/images/house.png";
  static const callingIcon = "assets/images/calling.png";
  static const addressIcon = "assets/images/address.png";
  static const deleteIcon = "assets/images/delete.png";
  static const editIcon = "assets/images/edit.png";
  static const navigatorIcon = "assets/images/navigator.png";
  static const navigatorOutline = "assets/images/navigator_outline.png";

  // func
  static const arrowRight = "assets/images/view_all.png";

  // account
  static const defaultAvatar = "assets/images/avatar.png";
  static const doctorDefaultAvatar = "assets/images/doctor_default_avatar.png";
  static const settingIcon = "assets/images/setting.png";
  static const favoriteDoctorIcon = "assets/images/favorite_doctors.png";
  static const healthPassportIcon = "assets/images/health_passport.png";
  static const manageAddressIcon = "assets/images/manage_address.png";
  static const medicineBoxIcon = "assets/images/medicine_box.png";
  static const myFamilyIcon = "assets/images/my_family.png";
  static const myWalletIcon = "assets/images/my_wallet.png";
  static const listArrowRightIcon = "assets/images/list_arrow_right.png";

  // search
  static const triTopIcon = "assets/images/tri_top.png";
  static const triBottomIcon = "assets/images/tri_bottom.png";
  static const shiftIcon = "assets/images/shift_icon.png";
  static const likeIcon = "assets/images/like.png";
  static const likeActiveIcon = "assets/images/like_active.png";
  static const clockIcon = "assets/images/clock_icon.png";

  // setting
  static const languageIcon = "assets/images/change_language.png";
  static const passwordIcon = "assets/images/change_password.png";
  static const checkUpdateIcon = "assets/images/check_update.png";
  static const clearCacheIcon = "assets/images/clear_cache.png";
  static const privacyPolicyIcon = "assets/images/privacy_policy.png";
  static const termServiceIcon = "assets/images/terms_service.png";

  // doctor
  static const likeWhiteIcon = "assets/images/like_white.png";
  static const timeIcon = "assets/images/time.png";

  // login
  static const eyeCloseIcon = "assets/images/eye_close_icon.png";
  static const eyeIcon = "assets/images/eye_icon.png";
  static const lockIcon = "assets/images/lock_icon.png";
  static const loginTopBg = "assets/images/login_top_bg.png";
  static const inputClearBtn = "assets/images/input_clear_icon.png";
  static const verifyIcon = "assets/images/verify_icon.png";
  static const selectIconHl = "assets/images/select_box_hl.png";
  static const selectIconNl = "assets/images/select_box_nl.png";
  static const wechatIcon = "assets/images/wechat_icon.png";
  static const arrorBottomBtn = "assets/images/arror_bottom_btn.png";

  // clinic
  static const clinicIcon = "assets/images/clinic_icon.png";
  static const phoneIcon = "assets/images/phone_icon.png";
  static const clinicWhiteIcon = "assets/images/clinic_white_icon.png";
  static const phoneWhiteIcon = "assets/images/phone_white_icon.png";
  static const addressWhiteIcon = "assets/images/address_white_icon.png";

  // appointment
  static const videoActiveIcon = "assets/images/video_active_icon.png";
  static const videoIcon = "assets/images/video_icon.png";
  static const faceActiveIcon = "assets/images/face_active_icon.png";
  static const faceIcon = "assets/images/face_icon.png";
  static const appointmentSuccess = "assets/images/appointment_success.png";
  static const checkSuccessIcon = "assets/images/check_success_icon.png";
  static const closeErrorIcon = "assets/images/close_error_icon.png";
  static const successAppointment = "assets/images/success_appointment.png";
  static const uploadIcon = "assets/images/upload_icon.png";
  static const questionIcon = "assets/images/question_icon.png";
  static const detailPic = "assets/images/detail_pic.png";
  static const payCardIcon = "assets/images/pay_card_icon.png";
  static const unionIcon = "assets/images/union.png";
  static const infoIcon = "assets/images/info.png";

  // checkIn
  static const stepNl = "assets/images/step_nl.png";
  static const stepHl = "assets/images/step_hl.png";
  static const formTop = "assets/images/form_top.png";
  static const calenderIcon = "assets/images/calender_icon.png";
  static const arrowDownIcon = "assets/images/arrow_down_icon.png";

  // 设置
  static const addMedicineIcon = "assets/images/add_medicine_icon.png";
  static const notificationIcon = "assets/images/notification_icon.png";
  static const editBlueIcon = "assets/images/edit_icon.png";
  static const medicineEmpty = "assets/images/medicine_empty.png";
  static const timeLineIcon = "assets/images/time_line_icon.png";

  // message
  static const checkIcon = "assets/images/check_icon.png";
  static const tabCheckIcon = "assets/images/big_check_icon.png";
  static const tabMsgIcon = "assets/images/message_icon.png";
  static const emptyTodo = "assets/images/empty_todo.png";
  static const emptyMsg = "assets/images/empty_msg.png";

  static const guide1 = "assets/images/guide1.png";
  static const guide2 = "assets/images/guide2.png";
  static const guide3 = "assets/images/guide3.png";

  // 重置密码
  static const topResetBg = "assets/images/top_reset_bg.png";
  static const passwordBg = "assets/images/password_bg.png";

  static const payment = "assets/images/payment.png";

  // 视讯
  static const notice = "assets/images/notice.png";
  static const videoBottom = "assets/images/video_bottom.png";
  static const videoClose = "assets/images/video_close.png";
  static const videoConsultation = "assets/images/video_consultation.png";
  static const videoOff = "assets/images/video_off.png";
  static const videoOpen = "assets/images/video_open.png";
  static const vioceOpen = "assets/images/vioce_open.png";
  static const voiceClose = "assets/images/voice_close.png";
  static const switchCamera = "assets/images/switch_camera.png";

  static const waitSuccess = "assets/images/wait_success.png";
  static const waitFail = "assets/images/crying_face.png";
  static const speakerOnIcon = "assets/images/calling_hands_free_sel_icon.webp";
  static const speakerOffIcon = "assets/images/calling_hands_free_nor_icon.webp";

  // 權益
  static const couponIcon = "assets/images/icon_coupon_coupon.png";
  static const couponBgIcon = "assets/images/icon_bottom_frame_coupon.png";
  static const iconExpandCoupon = "assets/images/icon_expand_coupon.png";

  static home(bool isHigh) => isHigh ? "assets/images/tab_home_active.png" : "assets/images/tab_home.png";

  static const loadingRiv = "assets/images/loading.riv";
}
