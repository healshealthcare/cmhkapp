final Map<String, String> zhCN = {
  "app": "CMHK",
  "search_doctor_tip": "搜索医生",
  "search_tip": "搜索",
  "logout": "确认退出吗？",

  // guide
  "guide_title_1": "名医预约",
  "guide_title_2": "即时挂号",
  "guide_title_3": "健康管理",
  "guide_content_1": "根据自身需要预约合适您自己的名医",
  "guide_content_2": "远程取号，实时查看排队状态",
  "guide_content_3": "一站式管理你的健康档案，贴心服药提醒",
  "start": "开始使用",

  // login
  "login_tab": "登录",
  "register_tab": "注册",
  "phone_hint_text": "手机号码",
  "pass_old_hint_text": "旧密码",
  "pass_new_hint_text": "新密码",
  "pass_repeat_hint_text": "再次输入新密码",
  "pass_hint_text": "密码",
  "veri_hint_text": "验证",
  "log_in_btn": "登录",
  "register_btn": "注册",
  "pass_change_tip": "密码登录",
  "otp_change_tip": "验证码登录",
  "forget_pass_tip": "忘记密码",
  "veri_code_tip": "验证码",
  "cound_down_tip": "%s s", // %s 占位符，不可删除
  "log_way_chagne_tip": "其他登录方式",
  "login_agree_tip": "我同意接受",
  "login_agree_tip_1": "服务条款",
  "login_agree_tip_2": "及",
  "login_agree_tip_3": "私隐政策。",

  "cancel": "取消",
  "confirm": "确认",
  "gender_male": "男",
  "gender_female": "女",
  "gender_other": "其他",
  "google_map": "谷歌地图",
  "autoNavi_map": "高德地图",
  "apple_map": "Apple地圖",
  "check_in_btn": "签到",
  "check_in_space_btn": "签到",
  "join_video_room": "进入就诊室",

  // page
  "page_payment_title": "支付",
  "page_payment_detail_title": "付款详情",
  "page_statement_title": "服务条款",
  "page_commercial_title": "最新消息",
  "page_help_title": "帮助",
  "page_delivery_title": "Delivery",
  "page_privacy_title": "隐私政策",
  "page_profile_title": "我的资料",
  "page_appointment_detail": "预约详情",
  "page_appointment_history": "预约记录",
  "page_appointment_questions": "健康问卷",
  "page_appointment_success": "预约成功",
  "page_check_form": "登记表",
  "page_check_in": "签到",
  "page_sent_form": "发送登记表",
  "page_favorite_doctor": "关注的医生",
  "page_medicine_box": "药物记录",
  "page_health_history": "就诊记录",
  "page_queue_detail": "排队详情",
  "page_queue_quccess": "排队成功",
  "page_setting": "设置",
  "page_reset_password": "更改密码",
  "page_payment_history_title": "付款记录",
  "page_address_title": "地址",
  "page_queue_success_title": "排队详情",
  "page_message_title": "消息",
  "page_family_title": "我的家庭成员",
  "page_family_add_title": "%s的资料",
  "page_family_update_title": "添加家庭成员",
  "page_coupon_title": "我的卡券",
  "page_coupon_detail_title": "权益详情",

  // home tab
  "tab_home": "首頁",
  "tab_check": "签到",
  "tab_msg": "待办",
  "tab_account": "我的",

  // setting
  "clean_cache": "清理緩存?",
  "language_setting": "更改語言",
  "pass_setting": "更改密码",
  "clean_setting": "清理緩存",
  "term_setting": "服务条款",
  "privacy_setting": "隐私条款",
  "update_setting": "版本更新",
  "delete_account_setting": "删除账号",
  "delete_account_dialog": "删除账号将令您无法登录及使用医聊通所有功能，请谨慎选择！",
  "delete_account_again_dialog": "本次操作将删除您的账号，请确认！",
  "out_setting": "退出",
  "language_en": "英文",
  "language_cn": "简体中文",
  "language_tw": "繁体中文",
  "local_change_tip": "请重启应用以使修改生效！",

  // profile
  "profile_tip": "请填写个人资料 (姓名，性別，证件号码和联络电话为必填项)",

  // account
  "health_history_title": "就诊记录",
  "medicine_box_title": "药物记录",
  "favorite_doctor_title": "我关注的医生",
  "my_wallet_title": "账单记录",
  "manage_address_title": "地址管理",
  "manage_coupon_title": "我的卡券",

  // appointment detail
  "appointment_hold_descripe": "医生分配了以下就诊时间，请尽快于两小时内确认接受或取消",
  "appointment_unhold_descripe": "预约请求已经发送，请等候诊所回复",
  "appointment_confirm_title": "预约成功啦! ",
  "appointment_confirm_title_tip": "下一步要做什么？",
  "appointment_accept_title": "確定预约",
  "appointment_reschedule_title": "更改预约",
  "appointment_cancel_title": "取消预约",
  "appointment_accept_note_title": "重要提醒",
  "appointment_accept_note": "你已成功预约了医生.\n如果你无法如期看诊，请立即登入本程式取消你的预约。",
  "pay_agree_tip": "继续下一步代表你已阅读并同意",
  "pay_agree_tip_1": "服务条款",

  // appointment list
  "tab_all": "全部",
  "tab_upcoming": "待就诊",
  "tab_pending": "待确认",
  "tab_past": "过往预约",
  "filter_specialty": "全部科室",
  "filter_service": "全部服务",
  "filter_district": "全部区域",
  "filter_distance": "距离最近",
  "filter_favorite": "我的关注",
  "filter_open": "营业中",
  "tag_ticket": "排队",
  "tag_appointment": "预约",
  "tag_tele": "线上诊症",
  "status_done": "已确认", // 预约列表状态文案
  "status_close": "已完成", // 预约列表状态文案
  "status_pending": "待付款",
  "status_void": "取消",
  "appointment_accept_btn": "确认预约",
  "appointment_reschedule_btn": "重新预约",
  "appointment_cancel_btn": "取消预约",
  "appointment_book_again_btn": "再次预约",
  "appointment_date_title": "预约日期:",
  "queue_title": "排队号码",
  "est_title": "预计看诊时间",
  "clinic_cancel_tip": "诊所取消",
  "clinic_rejected_tip": "诊所拒絕",
  "patient_cancel_tip": "就诊者取消",
  "patient_rejected_tip": "就诊者拒絕",
  "clinic_status_cancel_tip": "预约状态: 医生取消了你的预约",
  "clinic_status_rejected_tip": "预约状态: 医生拒绝了你的预约",
  "patient_status_cancel_tip": "预约状态: 你已取消了预约",
  "patient_status_rejected_tip": "预约状态: 你已取消了预约",

  // appointment
  "appointment_added_favorite": "已关注",
  "appointment_add_favorite": "关注",
  "appointment_queue_btn": "立即排队",
  "appointment_continue_btn": "下一步",
  "appointment_queue_tip": "前面等待人数: ",
  "appointment_in_clinic_tip": "诊所就诊",
  "appointment_video_visit_tip": "线上诊症",
  "appointment_contact_phone_tip": "手机号码",
  "appointment_pleose_input_tip": "请输入",
  "appointment_choose_date_title": "请选择就诊日期",
  "appointment_note_tip": "请留意: \n每次排队均需收取按金。就诊完成后，押金将用于抵扣就诊费用。",
  "appointment_earlist_date_tip": "最早时段",
  "appointment_choose_date_tip": "期望日期",
  "appointment_select_date_tip": "选择日期",
  "appointment_choice_pri": "选择",
  "appointment_choice_suf": "(选填)",
  "appt_At": "预约请求于：",
  "clinic_reply_At": "诊所回复于：",
  "payment_tip": "继续下一步代表你已阅读并同意 ",
  "payment_tip_content": "服务条款",
  "appointment_home_delivery": "上门派送",
  "appointment_drug_delivery": "药物派送",
  "appointment_pick_up": "服务点自取",
  "appointment_select_service": "派送服务",
  "appointment_standard_delivery": "标准派送",
  "appointment_express_delivery": "特快派送",
  "appointment_select_address": "派送地址",
  "appointment_select_family": "为家庭成员预约",
  "appointment_select_coupon": "我的卡券",
  "appointment_choose_tip": "选择",
  "appointment_choose_address_tip": "请选择派送地址",
  "address_delete_btn": "删除",

  // appointment questions
  "appointment_question_tip": "应诊所要求，请描述你的症状和问题，帮助医生提前了解你的病情",
  "appointment_or": "病情描述:",
  "appointment_question_limit": "提前告知医生你的病情，有助于提高诊疗效率。请输入简明扼要的10-200字",

  // appointment sucess
  "appointment_success_tip": "发给医生... ",
  "appointment_success_discripe": "你的请求已经发送给医生.\n\n请耐心等候医生的回复.",
  "appointment_success_btn": "查看预约详情",

  // check in form
  "check_in_back": "上一步",
  "check_in_cancel": "取消",
  "check_in_submit": "提交",
  "check_in_continue": "下一步",

  // check in success
  "check_in_success": "签到成功啦!",
  "check_in_descripe": "你的签到登记表已经发送到诊所，请耐心等候",

  // doctor detail
  "doctor_appointment_btn": "立即预约",

  // favorite doctor
  "doctor_tag": "预约",
  "telehealth_tag": "线上诊症",

  // home
  "hello": "你好！", //
  "help_center": "帮助", //
  "find_doctor": "查看\n所有医生", // \n 代表换行
  "my_booking": "预约记录",
  "video_consultation": "线上诊症",
  "today_schedule": "今日行程",
  "today_schedule_tip": "到达诊所后请按签到键",
  "view_all": "查看所有",
  "view_btn": "排队详情",
  "no_appointment_tip": "今日沒有预约/排队哦～",
  "queue_poi_tip": "排队号码",
  "home_queue_status": "前面还有等待人数:",
  "video_title": "重要提醒",
  "video_tip": "线上诊症不适用于急诊，如遇紧急情况，请前往就近的诊所或医院作进一步检查或治疗。",
  "video_check": "By click on Next button, I have read and hereby agree to the terms and conditions,",
  "video_must_check_tip": "Please agree to the terms and condition before moving to next step.",
  "home_tip": "你有新的待办事项，请立即处理",

  // medicine box
  "medication_tip": "暂无药物记录 ",
  "clinic_tip": "诊所: ",
  "doctor_tip": "医生: ",
  "user_name_tip": "名称: ",
  "prescription_tip": "药物: ",
  "diagnosis_tip": "诊症: ",
  "waybill_tip": "运单编号: ",

  // profile
  "avatar_title": "头像",
  "eng_name_title": "英文全名",
  "chi_name_title": "中文全名",
  "nick_title": "昵称",
  "birth_title": "生日",
  "gender_title": "性別",
  "id_title": "证件号码",
  "mobile_title": "手机号码",
  "email_title": "电子邮箱",
  "emer_con_title": "紧急联系人",
  "rel_title": "关系",
  "con_phone_title": "手机号码",
  "save_btn": "保存",

  // queue detail
  "queue_tip": "感谢你的等候.\n预计每位病人需时约5-10分钟，\n你前面还有等候人数:",
  "queue_detail_success_tip": "你已成功签到。如需取消就诊，请告知护士. 一经取消，将不退还按金。 详情请参阅服务条款。",
  // "wait_time_tip": "预计看诊时间:",
  "leave_queue_btn": "取消排队",
  "leave_queue_sure": "取消排队，押金将不获退还，详情请参阅服务条款。你确定要取消排队吗？", // 取消排队确定
  "queue_detail_note": "线上诊症排队，当还有2位等候就轮到你时，我们将会开放线上就诊室以便你提前进入准备。 \n\n请准时就诊，并确保你的网络通讯良好。",

  // queue success
  "queue_success_tip": "签到成功啦!\n请耐心等候",

  // search
  "search_cancel": "取消",
  "search_spec": "全科",
  "search_ser": "就诊类型",
  "search_dist": "区域",
  "search_clin_close": "诊所休息中",
  "search_cur_queue": "排队人数:",
  "search_eTicket": "排队",
  "search_booking": "预约",
  "search_tele": "线上诊症",

  // reset password
  "reset_pass_title": "重置密码",
  "reset_pass_content": "请输入注册号码以接收验证码",
  "reset_pass_btn": "提交",

  // forget password
  "forget_pass_title": "忘记密码",
  "forget_pass_content": "请输入注册号码以接收验证码",
  "forget_send_code_title": "输入验证码 ",
  "forget_send_code_content": "验证码已发送  ",
  "forget_pass_btn": "下一步",
  "forget_pass_submit_btn": "提交",
  "forget_resend_1": "",
  "forget_resend_2": "再次发送验证码",

  // payment detail
  "payment_invoce": "账单",
  "payment_invoce_item": "账单详情",
  "payment_btn": "立即付款",
  "payment_patient": "就诊者",
  "payment_doctor": "医生",
  "payment_clinic": "诊所",
  "payment_visit_date": "就诊日期",
  "payment_incoice_date": "账单日期",
  "payment_incoice_code": "账单号码",
  "payment_incoice_status": "付款状态",
  "payment_deposit": "押金",
  "payment_total": "总支付金额",
  "payment_status_done": "已付款",
  "payment_status_pending": "待付款",
  "payment_status_void": "已取消",

  // health History
  "dia_name": "药物名称：",
  "usg_name": "用法：",
  "start_date_name": "开始日期：",
  "fre_name": "次数：",
  "dur_name": "日数：",
  "dos_name": "数量：",

  // action
  "more": "查看详情",
  "back_home": "返回首页",
  "back_page": "返回",

  // message
  "tab_todo": "待办",
  "tab_notice": "通知",
  "message_todo_empty": "暂无待办事项",
  "message_empty": "暂无消息",
  "message_title_appointment": "预约通知",
  "message_title_payment": "付款通知",
  "message_title_checkin": "签到提醒",

  // queue toast
  "wrong_account": "登录账号或密码错误",
  "loading": "加载中...", // 各种数据加载中
  "leave_tip": "排队已取消",

  // appointment toast
  "phone_input": "请输入正确的手机号码",
  "contact_input": "请输入手机号码", // 正确的电话号码
  "contact_input_title": "手机号码", //
  "appointment_again_tip": "请完成当前就诊再重新排队或签到",

  // appointment question toast
  "answer_all_tip": "请完成所有问题",

  // checkin toast
  "eticket_again_tip": "请完成当前就诊再重新排队或签到",

  // login
  "statement_tip": "请同意服务条款和私隐政策。",
  "login_phone_input_tip": "请输入正确的电话号码",
  "login_phone_correct_tip": "电话号码格式无效，请重新输入",
  "login_msg_input_tip": "请输入验证码",
  "login_pwd_input_tip": "请输入8-16位密码",
  "login_pwd_correct_tip": "密码请使用数字和大小写字母的组合",
  "register_success_tip": "注册成功，请重新登录",
  "login_msg_sent_tip": "发送成功",

  // address
  "address_add_btn": "增加地址",
  "address_receipient_title": "收件人",
  "address_receipient_phone_title": "联络电话",
  "address_street_1_title": "街道地址1",
  "address_street_2_title": "街道地址2",
  "address_area_title": "区域",
  "address_region_title": "分区",
  "address_district_title": "地区",
  "address_fill_tip": "请填写所有必填项",
  "address_save_btn": "保存",
  "delete_address_tip": "确定删除地址吗？",

  // chat
  "doctor_late_title": "医生很快就会进入会议室，请耐心等候",
  "doctor_late_content": "如果医生超时五分钟仍未进入会议室，我们的客户服务员会主动联络你.",
  "patient_late_title": "你迟到了",
  "patient_late_content": "医生正在进行其他问诊，请耐心等候.",
  "chat_quit": "你确定要离开就诊室吗？",

  // profile
  "finish_profile_tip": "请填写个人资料 (姓名，性別，证件号码和联络电话为必填项)",
  "profile_hkid_tip": "身份证号码格式无效，请重新输入",

  // Consultation Completed
  "consultation_completed": "诊症完成!",
  "consultation_next": "请留步!",
  "consultation_payment": "切勿离开App \n姑娘现正为你准备账单及药物, 预计需时几分钟,请耐心等候...",
  "consultation_payment_content": " \n[待办]页面可查看等待付款的账单",

  // queue success
  "queue_success_content": "请稍等, 我们正在获取诊所最新的排队状况...",
  "queue_fail_title": "取号不成功",
  "queue_fail_content": "请嘗試以下解決方案：\n\n- 確保手機網絡通訊良好, 之後重新排隊；\n\n- 聯絡我們的客服專員處理退款",
  "queue_checkin_btn": "签到",

  //  coupon
  "coupon_pre": "卡券",
  "coupon_expired_at": "有效期至: ",
  "coupon_detail": "详情",
  "coupon_all": "全部",
  "coupon_valid": "尚未使用",
  "coupon_used": "已使用",
  "coupon_expired": "已失效",
  "coupon_empty": "您还没有优惠券",

  // family
  "delete_family_tip": "确定移除该成员？",
  "family_add_btn": "添加成员",

  // map
  "unfound_amap": "未有高德地圖",
  "unfound_baidu_map": "未有百度地圖",
  "unfound_google_map": "未有谷歌地圖",

  // status
  "status_pendding": "待诊所回复",
  "status_do": "已完成",
  "status_que": "待完成问卷",
  "status_upcoming": "待就诊",
  "status_deposit": "待支付按金",
  "status_comfirm": "待你的回复",

  "no_data": "暂无数据",

  "id_num": "证件号码",
  "chi_name": "中文全名",
  "eng_name": "英文全名",
  "relationship": "关系",
  "telephone": "联络电话",
  "email": "电邮",
  "address": "地址",
  "emerg_name": " 紧急联络人",
  "emerg_phone": "联络人电话",
  "nickname": "昵称",

  "permission_ungrant": "未授权",
  "use": "使用",
};
