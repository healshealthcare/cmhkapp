final Map<String, String> zhEN = {
  "app": "Heals",
  "search_doctor_tip": "Search for doctors",
  "search_tip": "Search",
  "logout": "Are you sure to log out?",

  // guide
  "guide_title_1": "Schedule\nappointment with\nexpert doctors",
  "guide_title_2": "Easy and quick\nqueueing",
  "guide_title_3": "Manage health\nhistory in one place",
  "guide_content_1": "Find experienced specialist doctors and book appointment online hassle-free",
  "guide_content_2": "Get eTicket remotely and manage your time to visit the doctor",
  "guide_content_3": "Keep track of your consultation history, set pill reminder in one click",
  "start": "START",

  // login
  "login_tab": "Log In",
  "register_tab": "Sign up",
  "phone_hint_text": "phone number",
  "pass_old_hint_text": "old password",
  "pass_new_hint_text": "new password",
  "pass_repeat_hint_text": "repeat new password",
  "pass_hint_text": "Password",
  "veri_hint_text": "Verification",
  "log_in_btn": "Log In",
  "register_btn": "Register",
  "pass_change_tip": "Password login",
  "otp_change_tip": "OTP login",
  "forget_pass_tip": "Forgot password",
  "veri_code_tip": "Verification Code",
  "cound_down_tip": "%s s", // %s 占位符，不可删除
  "log_way_chagne_tip": "Or log in with",
  "login_agree_tip": "I agree to the",
  "login_agree_tip_1": "Terms of Service ",
  "login_agree_tip_2": "and",
  "login_agree_tip_3": " Privacy Policy",

  "cancel": "Cancel",
  "confirm": "Confirm",
  "gender_male": "Male",
  "gender_female": "Female",
  "gender_other": "Other",
  "google_map": "Navigate by Google Map",
  "autoNavi_map": "Navigate by AutoNavi Map",
  "apple_map": "Navigate by Apple Maps",
  "check_in_btn": "Check In",
  "check_in_space_btn": "Check In",
  "join_video_room": "Start Video Call",

  // page
  "page_payment_title": "Payment",
  "page_payment_detail_title": "Payment Details",
  "page_statement_title": "Terms of service",
  "page_commercial_title": "What is new",
  "page_help_title": "Help",
  "page_delivery_title": "Delivery",
  "page_privacy_title": "Privacy policy",
  "page_profile_title": "My Profile",
  "page_appointment_detail": "Appointment Details",
  "page_appointment_history": "Appointment History",
  "page_appointment_questions": "Health Questions",
  "page_appointment_success": "Health Success",
  "page_check_form": "Registration Form",
  "page_check_in": "Check In",
  "page_sent_form": "Sent form",
  "page_favorite_doctor": "Favorite Doctor",
  "page_medicine_box": "Medicine Record",
  "page_health_history": "Health History",
  "page_queue_detail": "Queue Details",
  "page_queue_quccess": "Queue Success",
  "page_setting": "setting",
  "page_reset_password": "Change password",
  "page_payment_history_title": "Payment History",
  "page_address_title": "Address",
  "page_queue_success_title": "Queue detail",
  "page_message_title": "Message",
  "page_family_title": "My family member",
  "page_family_add_title": "%s Profile",
  "page_family_update_title": "Add family member",
  "page_coupon_title": "e-Coupon",
  "page_coupon_detail_title": "Coupon Detail",

  // home tab
  "tab_home": "Home",
  "tab_check": "Check In",
  "tab_msg": "To-do",
  "tab_account": "Account",

  // setting
  "clean_cache": "Clean cache?",
  "language_setting": "Change language",
  "pass_setting": "Change password",
  "clean_setting": "Clear cache",
  "term_setting": "Terms of service",
  "privacy_setting": "Privacy policy",
  "update_setting": "Check update",
  "delete_account_setting": "Delete Account",
  "delete_account_dialog":
      "Deleting your account will prevent you from logging in and using all the features of Dr.HK, please choose carefully!",
  "delete_account_again_dialog": "This operation is going to delete your account, please confirm!",
  "out_setting": "Log Out",
  "language_en": "English",
  "language_cn": "Simple Chinese",
  "language_tw": "Traditional Chinese",
  "local_change_tip": "The changes will be effective when restart the app!",

  // profile
  "profile_tip": "Please complete your profile (name, gender phone number, ID number are required to fill out)",

  // account
  "health_history_title": "Health History",
  "medicine_box_title": "Medicine Record",
  "favorite_doctor_title": "Favorite Doctors",
  "my_wallet_title": "Transaction History",
  "manage_address_title": "Manage Address",
  "manage_coupon_title": "My eCoupon",

  // appointment detail
  "appointment_hold_descripe":
      "Doctor has offered you the following appointment date and time, this offer will be cancelled automatically if no response from you in 2 hours. ",
  "appointment_unhold_descripe":
      "Your request has been sent to clinic. Doctor will reply soon with appointment date and time :",
  "appointment_confirm_title": "Appointment Confirmed! ",
  "appointment_confirm_title_tip": "Click here to see the next step",
  "appointment_accept_title": "Accept Appointment",
  "appointment_reschedule_title": "Reschedule Booking",
  "appointment_cancel_title": "Cancel Booking",
  "appointment_accept_note_title": "Important Notice",
  "appointment_accept_note":
      "You are holding a space on doctor’s calendar that is no longer available to other patients.\n\nTo be respectful of other patients and the doctor, please cancel your appointment in App as soon as you know you will not be able to visit the doctor on schedule.",

  // appointment list
  "tab_all": "All",
  "tab_upcoming": "Upcoming",
  "tab_pending": "Pending",
  "tab_past": "Past",
  "filter_specialty": "All Specialties",
  "filter_service": "All Services",
  "filter_district": "All Districts",
  "filter_distance": "Sort by distance",
  "filter_favorite": "My favorite",
  "filter_open": "Open only",
  "tag_ticket": "e-Ticket",
  "tag_appointment": "Appointment",
  "tag_tele": "Video Call",
  "status_done": "Done", // 预约列表状态文案
  "status_close": "Closed", // 预约列表状态文案
  "status_pending": "Pending for payment",
  "status_void": "Void",
  "appointment_accept_btn": "Accept",
  "appointment_reschedule_btn": "Reschedule",
  "appointment_cancel_btn": "Cancel",
  "appointment_book_again_btn": "Book Again",
  "appointment_date_title": "Appointment schedule:",
  "queue_title": "Queue Position",
  "est_title": "Est. Consultation",
  "clinic_cancel_tip": "Clinic cancelled",
  "clinic_rejected_tip": "Clinic rejected",
  "patient_cancel_tip": "Patient cancelled",
  "patient_rejected_tip": "Patient rejected",
  "clinic_status_cancel_tip": "Status: Doctor cancelled",
  "clinic_status_rejected_tip": "Status: Clinic rejected",
  "patient_status_cancel_tip": "Status: Patient cancelled appointment",
  "patient_status_rejected_tip": "Status: Patient rejected appointment",

  // appointment
  "appointment_added_favorite": "Added to favorite",
  "appointment_add_favorite": "Add to favorite",
  "appointment_queue_btn": "Queue Now",
  "appointment_continue_btn": "Continue",
  "appointment_queue_tip": "People ahead of you : ",
  "appointment_in_clinic_tip": "Clinic Visit",
  "appointment_video_visit_tip": "Video Call",
  "appointment_contact_phone_tip": "Contact phone",
  "appointment_pleose_input_tip": "please input",
  "appointment_choose_date_title": "Choose your appointment date",
  "appointment_note_tip":
      "Please note: \neTicket service requires deposit. It will be charged to your payment account and deducted from your total due after the consultation. ",
  "appointment_earlist_date_tip": "Earliest Available Date ",
  "appointment_choose_date_tip": "Choose Your Own Date",
  "appointment_select_date_tip": "Select Date",
  "appointment_choice_pri": "Choice",
  "appointment_choice_suf": "(optional)",
  "appt_At": "Appt created at：",
  "clinic_reply_At": "Clinic replied at：",
  "payment_tip": "By clicking below, you agree to ",
  "payment_tip_content": "Terms of Service",
  "appointment_home_delivery": "Home Delivery",
  "appointment_drug_delivery": "Delivery Method",
  "appointment_pick_up": "Pick Up",
  "appointment_select_service": "Delivery Service",
  "appointment_standard_delivery": "Standard Delivery",
  "appointment_express_delivery": "Express Delivery",
  "appointment_select_address": "Delivery Address",
  "appointment_select_family": "Ticket for family member",
  "appointment_select_coupon": "e-Coupon",
  "appointment_choose_tip": "choose",
  "appointment_choose_address_tip": "Please choose delivery address",
  "address_delete_btn": "Delete",

  // appointment questions
  "appointment_question_tip": "Please answer the following questions for us to create appointment request.",
  "appointment_or": "Or describe your sympthoms :",
  "appointment_question_limit": "limits to 200 characters",

  // appointment sucess
  "appointment_success_tip": "Almost there... ",
  "appointment_success_discripe":
      "Your request has been sent to doctor successfully.\n\nYou will receive a notification later to confirm the details of the appointment.",
  "appointment_success_btn": "View Details",

  // check in form
  "check_in_back": "Back",
  "check_in_cancel": "Cancel",
  "check_in_submit": "Submit",
  "check_in_continue": "Continue",

  // check in success
  "check_in_success": "Check in successfully!",
  "check_in_descripe": "Your registration form has been sent to clinic, doctor will see you soon.",

  // doctor detail
  "doctor_appointment_btn": "Book Appointment",

  // favorite doctor
  "doctor_tag": "Booking",
  "telehealth_tag": "Video Call",

  // home
  "hello": "Hello", //
  "help_center": "Help Center", //
  "find_doctor": "All\nDoctors", // \n 代表换行
  "my_booking": "My\nBooking",
  "video_consultation": "Video\nConsultation",
  "today_schedule": "Today’s schedule",
  "today_schedule_tip": "Click \"Check-in\" to notify clinic upon arrival.",
  "view_all": "View all",
  "view_btn": "View Queue",
  "no_appointment_tip": "No appointment planned today",
  "queue_poi_tip": "Queue Position ",
  "home_queue_status": "People ahead of you:",
  "video_title": "Important Notice",
  "video_tip":
      "Please do not use the Telemedicine Services for emergency or urgent medical matters. For all urgent or emergency matters that you believe may immediately affect your health, you must immediately go to the nearest urgent care facility.",
  "video_check": "By click on Confirm button, I've read and hereby agree to the terms and conditions.",
  "video_must_check_tip": "Please agree to the terms and condition before moving to next step.",
  "home_tip": "You have new notification, click to check detail.",

  // medicine box
  "medication_tip": "You don't have any medication yet. ",
  "clinic_tip": "Clinic: ",
  "doctor_tip": "Doctor: ",
  "user_name_tip": "Name: ",
  "prescription_tip": "Prescription: ",
  "diagnosis_tip": "Diagnosis: ",
  "waybill_tip": "Waybill No.: ",

  // profile
  "avatar_title": "Profile image",
  "eng_name_title": "English Name",
  "chi_name_title": "Chinese Name",
  "nick_title": "Nick Name",
  "birth_title": "Date of Birth",
  "gender_title": "Gender",
  "id_title": "ID Number",
  "mobile_title": "Mobile No.",
  "email_title": "Email",
  "emer_con_title": "Emergency Contact",
  "rel_title": "Relationship",
  "con_phone_title": "Contact Phone",
  "save_btn": "Save",

  // queue detail
  "queue_tip": "Thank you for waiting!\nEstimated 5-10 minutes per person.\nPeople ahead of you is:",
  "queue_detail_success_tip":
      "You’ve checked in successfully. \nPlease inform nurse to cancel your eTicket if you have to leave the queue. Please note that your deposit will not be refunded.",
  // "wait_time_tip": "Your estimated wait time is:",
  "leave_queue_btn": "Leave Queue",
  "leave_queue_sure":
      "Are you sure to cancel? You may not get back the deposit. Please refer to Terms of Service for details.", // 取消排队确定
  "queue_detail_note":
      "For video consultation, the meeting room will be available when there are 2 people ahead of you. \nPlease get prepared and make sure your have good network connection.",

  // queue success
  "queue_success_tip": "Check in successfully!\nDoctor will see you soon",

  // search
  "search_cancel": "Cancel",
  "search_spec": "Specialty",
  "search_ser": "Service",
  "search_dist": "District",
  "search_clin_close": "Clinic closed",
  "search_cur_queue": "People ahead:",
  "search_eTicket": "eTicket",
  "search_booking": "Appointment",
  "search_tele": "Video Call",

  // reset password
  "reset_pass_title": "Reset password",
  "reset_pass_content": "Enter your register phone number to receive verification code",
  "reset_pass_btn": "Submit",

  // forget password
  "forget_pass_title": "Forgot password",
  "forget_pass_content": "Enter your register phone number to receive verification code",
  "forget_send_code_title": "Enter verification code ",
  "forget_send_code_content": "verification code has been sent to  ",
  "forget_pass_btn": "Next",
  "forget_pass_submit_btn": "Submit",
  "forget_resend_1": "Not received? ",
  "forget_resend_2": " Resend code",

  // payment detail
  "payment_invoce": "Invoice",
  "payment_invoce_item": "Invoice Item",
  "payment_btn": "Pay Now",
  "payment_patient": "Patient",
  "payment_doctor": "Doctor",
  "payment_clinic": "Clinic",
  "payment_visit_date": "Visit Date",
  "payment_incoice_date": "Invoice Date",
  "payment_incoice_code": "Invoice Code",
  "payment_incoice_status": "Invoice status",
  "payment_deposit": "Refund",
  "payment_total": "Total Cost",
  "payment_status_done": "Done",
  "payment_status_pending": "Pending for payment",
  "payment_status_void": "Void",

  // health History
  "dia_name": "Drug Name：",
  "usg_name": "Usage：",
  "start_date_name": "Start date：",
  "fre_name": "Frequency：",
  "dur_name": "Duration：",
  "dos_name": "Dosage：",

  // action
  "more": "View more",
  "back_home": "Back to Home Page",
  "back_page": "Back",

  // message
  "tab_todo": "To-do",
  "tab_notice": "Message",
  "message_todo_empty": "No to-do",
  "message_empty": "No message",
  "message_title_appointment": "Appointment notification",
  "message_title_payment": "Payment notification",
  "message_title_checkin": "Check in notification",

  // queue toast
  "wrong_account": "Incorrect password",
  "loading": "Loading...", // 各种数据加载中
  "leave_tip": "Your queue has been cancelled",

  // appointment toast
  "phone_input": "Please input correct phone number",
  "contact_input": "Please input contact phone", // 正确的电话号码
  "contact_input_title": "Contact phone", //
  "appointment_again_tip": "You have eTicket/Check-in already, please finish that one first",

  // appointment question toast
  "answer_all_tip": "Please answer all questions",

  // checkin toast
  "eticket_again_tip": "You have eTicket/Check-in already, please finish that one first",

  // login
  "statement_tip": "Please read and agree to all terms before logging in.",
  "login_phone_input_tip": "Please input correct phone number",
  "login_phone_correct_tip": "The phone number is not valid",
  "login_msg_input_tip": "Please input verification code",
  "login_pwd_input_tip": "Use a strong password that contains the symbols, numbers and letters.",
  "login_pwd_correct_tip": "password must contain upper case letter, lower case letter and number.",
  "register_success_tip": "Sign up successfully, please log in",
  "login_msg_sent_tip": "Verification code sent",

  // address
  "address_add_btn": "Add Address",
  "address_receipient_title": "Recipient Name",
  "address_receipient_phone_title": "Recipient Phone No",
  "address_street_1_title": "Street Address Line1",
  "address_street_2_title": "Street Address Line2",
  "address_area_title": "Area",
  "address_region_title": "Region",
  "address_district_title": "District",
  "address_fill_tip": "please fill out all required field",
  "address_save_btn": "Save",
  "delete_address_tip": "Are you sure to delete the address?",

  // chat
  "doctor_late_title": "Thank you for waiting, doctor will come in soon",
  "doctor_late_content": "Customer service will meet you if the doctor does not show up upon the scheduled time.",
  "patient_late_title": "You are late to the consultation",
  "patient_late_content": "Doctor is doing another consultation. Please stay online and doctor will be here soon.",
  "chat_quit": "Are you sure to leave the consultation room?",

  // profile
  "finish_profile_tip": "Please complete your profile (name, gender, phone number, ID number are required to fill out)",
  "profile_hkid_tip": "The HKID is not valid",

  // Consultation Completed
  "consultation_completed": "Consultation Completed!",
  "consultation_next": "WAIT!",
  "consultation_payment": "DO NOT LEAVE THIS APP yet \nas the nurse is preparing your invoice and medication etc.",
  "consultation_payment_content": " \nYou will find the pending invoice on To-Do list.",

  // queue success
  "queue_success_content": " Please stay tuned, \n\nwe are getting the latest queue status from clinic...",
  "queue_fail_title": "We're sorry! ",
  "queue_fail_content":
      "Please try below solution:\n\n- check your network connection and then queue again;\n\n- contact our customer service to arrange refund.",
  "queue_checkin_btn": "Check in clinic",

  //  coupon
  "coupon_pre": "Coupon",
  "coupon_expired_at": "Valid until: ",
  "coupon_detail": "Details",
  "coupon_all": "ALL",
  "coupon_valid": "VALID",
  "coupon_used": "USED",
  "coupon_expired": "INVALID",
  "coupon_empty": "No eCoupon yet",

  // family
  "delete_family_tip": "Comfirm to remove this member?",
  "family_add_btn": "Add family member",

  // map
  "unfound_amap": "Map is not found",
  "unfound_baidu_map": "Map is not found",
  "unfound_google_map": "Map is not found",

  // status
  "status_pendding": "Waiting for clinic reply",
  "status_do": "Done",
  "status_que": "Pending for health question",
  "status_upcoming": "Upcoming",
  "status_deposit": "Pending for deposit",
  "status_comfirm": "Waiting for your confirmation",

  "no_data": "No record yet",

  "id_num": "ID Number",
  "chi_name": "Chinese Name",
  "eng_name": "English Name",
  "relationship": "Relationship",
  "telephone": "Mobile No.",
  "email": "Email",
  "address": "Address",
  "emerg_name": "Emergency Contact",
  "emerg_phone": "Contact Phone",
  "nickname": "Nick Name",

  "permission_ungrant": "Untranted",
  "use": "Use",
};
