final Map<String, String> zhTW = {
  "app": "CMHK",
  "search_doctor_tip": "搜尋醫生",
  "search_tip": "搜尋",
  "logout": "確認退出嗎？",

  // guide
  "guide_title_1": "名醫預約",
  "guide_title_2": "即時掛號",
  "guide_title_3": "健康管理",
  "guide_content_1": "根據自身需求預約合適你的名醫",
  "guide_content_2": "線上排隊，實時查詢排隊進程",
  "guide_content_3": "一站式管理你的健康檔案，貼心服藥提醒",
  "start": "開始使用",

  // login
  "login_tab": "登入",
  "register_tab": "註冊",
  "phone_hint_text": "電話號碼",
  "pass_old_hint_text": "舊密碼",
  "pass_new_hint_text": "新密碼",
  "pass_repeat_hint_text": "再次填寫新密碼",
  "pass_hint_text": "密碼",
  "veri_hint_text": "驗證",
  "log_in_btn": "登入",
  "register_btn": "註冊",
  "pass_change_tip": "賬號登錄",
  "otp_change_tip": "驗證碼登錄",
  "forget_pass_tip": "忘記密碼",
  "veri_code_tip": "驗證碼",
  "cound_down_tip": "%s s", // %s 占位符，不可删除
  "log_way_chagne_tip": "其他登錄方式",
  "login_agree_tip": "我同意接受",
  "login_agree_tip_1": "服务條款",
  "login_agree_tip_2": "及",
  "login_agree_tip_3": "私隱政策。",

  "cancel": "取消",
  "confirm": "確認",
  "gender_male": "男",
  "gender_female": "女",
  "gender_other": "其他",
  "google_map": "使用谷歌地圖",
  "autoNavi_map": "使用高德地圖",
  "apple_map": "使用Apple地圖",
  "check_in_btn": "簽到",
  "check_in_space_btn": "簽到",
  "join_video_room": "進入就診室",

  // page
  "page_payment_title": "支付",
  "page_payment_detail_title": "付款詳情",
  "page_statement_title": "服務條款",
  "page_commercial_title": "最新消息",
  "page_help_title": "幫助",
  "page_delivery_title": "藥物派送",
  "page_privacy_title": "私隱政策",
  "page_profile_title": "我的資料",
  "page_appointment_detail": "預約詳情",
  "page_appointment_history": "預約記錄",
  "page_appointment_questions": "健康問卷",
  "page_appointment_success": "預約成功",
  "page_check_form": "登記表",
  "page_check_in": "簽到",
  "page_sent_form": "發送登記表",
  "page_favorite_doctor": "關注的醫生",
  "page_medicine_box": "藥物記錄",
  "page_health_history": "就診記錄",
  "page_queue_detail": "排隊詳情",
  "page_queue_quccess": "排隊成功",
  "page_setting": "設置",
  "page_reset_password": "更改密碼",
  "page_payment_history_title": "付款記錄",
  "page_address_title": "地址",
  "page_queue_success_title": "排隊詳情",
  "page_message_title": "消息",
  "page_family_title": "我的家庭成員",
  "page_family_add_title": "%s的資料",
  "page_family_update_title": "添加家庭成員",
  "page_coupon_title": "我的電子券",
  "page_coupon_detail_title": "權益詳情",

  // home tab
  "tab_home": "首頁",
  "tab_check": "簽到",
  "tab_msg": "待辦",
  "tab_account": "我的",

  // setting
  "clean_cache": "清理緩存?",
  "language_setting": "更改語言",
  "pass_setting": "更改密碼",
  "clean_setting": "清理緩存",
  "term_setting": "服務條款",
  "privacy_setting": "私隱政策",
  "update_setting": "版本更新",
  "delete_account_setting": "刪除賬號",
  "delete_account_dialog": "刪除賬號將令您無法登陸及使用醫療所有功能，請謹慎選擇！",
  "delete_account_again_dialog": "本次操作將刪除您的賬號，請確認！",
  "out_setting": "退出",
  "language_en": "英文",
  "language_cn": "簡體中文",
  "language_tw": "繁體中文",
  "local_change_tip": "請重啟應用以使修改生效!",

  // profile
  "profile_tip": "請完成個人信息 (姓名，性別，證件號碼和聯絡電話是必填項目)",

  // account
  "health_history_title": "就診記錄",
  "medicine_box_title": "藥物記錄",
  "favorite_doctor_title": "我關注的醫生",
  "my_wallet_title": "賬單記錄",
  "manage_address_title": "地址管理",
  "manage_coupon_title": "我的電子券",

  // appointment detail
  "appointment_hold_descripe": "醫生分配你的就診日期和時間如下，請於兩個小時之內確認接受或取消",
  "appointment_unhold_descripe": "預約請求已經發送，請等待診所回復",
  "appointment_confirm_title": "預約成功啦! ",
  "appointment_confirm_title_tip": "下一步要做什麼？",
  "appointment_accept_title": "確定預約",
  "appointment_reschedule_title": "更改預約",
  "appointment_cancel_title": "取消預約",
  "appointment_accept_note_title": "重要提醒",
  "appointment_accept_note": "你已經成功預約了醫生.\n如果你無法如期就診，請立即登入本程式取消預約。",
  "pay_agree_tip": "繼續下一步代表你已閱讀並同意",
  "pay_agree_tip_1": "服務條款",

  // appointment list
  "tab_all": "全部",
  "tab_upcoming": "待就診",
  "tab_pending": "待確認",
  "tab_past": "過往預約",
  "filter_specialty": "所有",
  "filter_service": "所有",
  "filter_district": "所有",
  "filter_distance": "距離最近",
  "filter_favorite": "我的關注",
  "filter_open": "營業中",
  "tag_ticket": "排隊",
  "tag_appointment": "预约",
  "tag_tele": "線上診症",
  "status_done": "已確認", // 预约列表状态文案
  "status_close": "已完成", // 预约列表状态文案
  "status_pending": "待付款",
  "status_void": "取消",
  "appointment_accept_btn": "確認預約",
  "appointment_reschedule_btn": "重新預約",
  "appointment_cancel_btn": "取消預約",
  "appointment_book_again_btn": "再次預約",
  "appointment_date_title": "預約日期:",
  "queue_title": "排隊號碼",
  "est_title": "預計看診時間",
  "clinic_cancel_tip": "診所取消",
  "clinic_rejected_tip": "診所拒絕",
  "patient_cancel_tip": "就診人取消",
  "patient_rejected_tip": "就診人拒絕",
  "clinic_status_cancel_tip": "預約狀態: 醫生取消了你的預約",
  "clinic_status_rejected_tip": "預約狀態: 醫生拒絕了你的預約請求",
  "patient_status_cancel_tip": "預約狀態: 你已取消了預約",
  "patient_status_rejected_tip": "預約狀態: 你已取消了預約",

  // appointment
  "appointment_added_favorite": "已關注",
  "appointment_add_favorite": "關注",
  "appointment_queue_btn": "馬上排隊",
  "appointment_continue_btn": "下一步",
  "appointment_queue_tip": "前面等候人數: ",
  "appointment_in_clinic_tip": "診所就診",
  "appointment_video_visit_tip": "線上診症",
  "appointment_contact_phone_tip": "聯絡電話",
  "appointment_pleose_input_tip": "請輸入",
  "appointment_choose_date_title": "請選擇就診日期",
  "appointment_note_tip": "請留意: \n每次排隊均需收取按金。就診完成後，按金將會用於抵扣就診總費用。",
  "appointment_earlist_date_tip": "最早時段",
  "appointment_choose_date_tip": "期望日期",
  "appointment_select_date_tip": "選擇日期",
  "appointment_choice_pri": "選擇",
  "appointment_choice_suf": "(選填)",
  "appt_At": "預約創建：",
  "clinic_reply_At": "診所回復：",
  "payment_tip": "繼續下一步代表你已閱讀並同意 ",
  "payment_tip_content": "服務條款",
  "appointment_home_delivery": "上門派送",
  "appointment_drug_delivery": "藥物派送",
  "appointment_pick_up": "服務點自取",
  "appointment_select_service": "派送服務",
  "appointment_standard_delivery": "標準派送",
  "appointment_express_delivery": "特快派送",
  "appointment_select_address": "派送地址",
  "appointment_select_family": "為家庭成員預約",
  "appointment_select_coupon": "我的電子券",
  "appointment_choose_tip": "選擇",
  "appointment_choose_address_tip": "請選擇派送地址",
  "address_delete_btn": "刪除",

  // appointment questions
  "appointment_question_tip": "應診所要求，請描述你的症狀和問題，幫助醫生提前了解你的病情",
  "appointment_or": "病情描述:",
  "appointment_question_limit": "提前告知醫生你的病情，有助於提升診療效率。請輸入簡明扼要的10-200字",

  // appointment sucess
  "appointment_success_tip": "已發給醫生... ",
  "appointment_success_discripe": "你的預約請求已經發給醫生.\n請耐心等候醫生的回復.",
  "appointment_success_btn": "查看預約詳情",

  // check in form
  "check_in_back": "上一步",
  "check_in_cancel": "取消",
  "check_in_submit": "提交",
  "check_in_continue": "下一步",

  // check in success
  "check_in_success": "簽到成功啦!",
  "check_in_descripe": "你的簽到登記表已經發送至診所，請耐心等候",

  // doctor detail
  "doctor_appointment_btn": "立即預約",

  // favorite doctor
  "doctor_tag": "預約",
  "telehealth_tag": "線上診症",

  // home
  "hello": "你好！", //
  "help_center": "幫助", //
  "find_doctor": "查看\n所有醫生", // \n 代表换行
  "my_booking": "預約記錄",
  "video_consultation": "線上診症",
  "today_schedule": "今日行程",
  "today_schedule_tip": "抵達診所後請按簽到鍵",
  "view_all": "查看所有",
  "view_btn": "排隊詳情",
  "no_appointment_tip": "今日沒有預約/排隊記錄哦～",
  "queue_poi_tip": "你的號碼 ",
  "home_queue_status": "你前面還有等待人數:",
  "video_title": "重要提示",
  "video_tip": "線上診症不適用於急診，如遇緊急情況，請前往就近的診所或醫院作進一步檢查或治療。",
  "video_check": "By click on Next button, I have read and hereby agree to the terms and conditions,",
  "video_must_check_tip": "Please agree to the terms and condition before moving to next step.",
  "home_tip": "你有新的待辦事项，請立即處理",

  // medicine box
  "medication_tip": "暫無藥物記錄 ",
  "clinic_tip": "診所: ",
  "doctor_tip": "醫生: ",
  "user_name_tip": "名稱: ",
  "prescription_tip": "藥物: ",
  "diagnosis_tip": "診症: ",
  "waybill_tip": "運單編號:",

  // profile
  "avatar_title": "個人圖片",
  "eng_name_title": "英文全名",
  "chi_name_title": "中文全名",
  "nick_title": "暱稱",
  "birth_title": "生日",
  "gender_title": "性別",
  "id_title": "證件號碼",
  "mobile_title": "電話號碼",
  "email_title": "電子郵箱",
  "emer_con_title": "緊急聯絡人",
  "rel_title": "關係",
  "con_phone_title": "聯絡電話",
  "save_btn": "保存",

  // queue detail
  "queue_tip": "感謝你的等候！ \n預計每位病人大約5-10分鐘，\n你前面還有等候人数:",
  "queue_detail_success_tip": "你已成功簽到。如需取消就診，請告知診所護士. 一經取消，按金將不獲退還， 詳情請參閱服務條款。",
  // "wait_time_tip": "預計看診時間:",
  "leave_queue_btn": "取消排隊",
  "leave_queue_sure": "取消排隊，按金將不獲退還，详情请参阅服务条款。你確定要取消排隊嗎？", // 取消排队确定
  "queue_detail_note": "排隊線上診症，當還有2位等候就輪到你時，我們會開放線上就診室，以便你提前进入准备。\n請準時到達，並確保你的網絡通訊良好。",

  // queue success
  "queue_success_tip": "簽到成功!\n請耐心等候",

  // search
  "search_cancel": "取消",
  "search_spec": "全科",
  "search_ser": "就診類型",
  "search_dist": "區域",
  "search_clin_close": "診所休息中",
  "search_cur_queue": "排隊人數:",
  "search_eTicket": "排隊",
  "search_booking": "預約",
  "search_tele": "線上診症",

  // reset password
  "reset_pass_title": "重置密碼",
  "reset_pass_content": "請輸入賬號電話號碼以接收驗證碼",
  "reset_pass_btn": "提交",

  // forget password
  "forget_pass_title": "忘記密碼",
  "forget_pass_content": "請輸入賬號電話號碼以接收驗證碼",
  "forget_send_code_title": "輸入驗證碼 ",
  "forget_send_code_content": "驗證碼已發送  ",
  "forget_pass_btn": "下一步",
  "forget_pass_submit_btn": "提交",
  "forget_resend_1": "",
  "forget_resend_2": "再次發送驗證碼",

  // payment detail
  "payment_invoce": "賬單",
  "payment_invoce_item": "賬單詳情",
  "payment_btn": "馬上付款",
  "payment_patient": "就診者",
  "payment_doctor": "醫生",
  "payment_clinic": "診所",
  "payment_visit_date": "就診日期",
  "payment_incoice_date": "賬單日期",
  "payment_incoice_code": "賬單號碼",
  "payment_incoice_status": "付款狀態",
  "payment_deposit": "按金",
  "payment_total": "總支付金額",
  "payment_status_done": "已付款",
  "payment_status_pending": "待付款",
  "payment_status_void": "已取消",

  // health History
  "dia_name": "藥物名稱：",
  "usg_name": "用法：",
  "start_date_name": "開始日期：",
  "fre_name": "次數：",
  "dur_name": "日數：",
  "dos_name": "數量：",

  // action
  "more": "查看詳情",
  "back_home": "返回首頁",
  "back_page": "返回",

  // message
  "tab_todo": "待辦",
  "tab_notice": "通知",
  "message_todo_empty": "暫無待辦事項",
  "message_empty": "暂无消息",
  "message_title_appointment": "預約提醒",
  "message_title_payment": "付款提醒",
  "message_title_checkin": "簽到提醒",

  // queue toast
  "wrong_account": "登录密码错误",
  "loading": "更新中...", // 各种数据加载中
  "leave_tip": "排隊已取消",

  // appointment toast
  "phone_input": "請輸入正確的電話號碼",
  "contact_input": "請輸入電話號碼", // 正确的电话号码
  "contact_input_title": "聯絡電話", //
  "appointment_again_tip": "請完成當前就診再重新排隊或簽到",

  // appointment question toast
  "answer_all_tip": "請完成所有問題",

  // checkin toast
  "eticket_again_tip": "請完成當前就診再排新隊或簽到",

  // login
  "statement_tip": "請同意服務條款和私隱政策。",
  "login_phone_input_tip": "請輸入正確的電話號碼",
  "login_phone_correct_tip": "電話號碼格式無效，請重新輸入",
  "login_msg_input_tip": "請輸入驗證碼",
  "login_pwd_input_tip": "請輸入8-16位密碼",
  "login_pwd_correct_tip": "密碼請使用數字和大小寫字母的組合",
  "register_success_tip": "註冊成功，請重新登錄",
  "login_msg_sent_tip": "發送成功",

  // address
  "address_add_btn": "增加地址",
  "address_receipient_title": "收件人",
  "address_receipient_phone_title": "聯絡電話",
  "address_street_1_title": "街道地址1",
  "address_street_2_title": "街道地址2",
  "address_area_title": "區域",
  "address_region_title": "分區",
  "address_district_title": "地區",
  "address_fill_tip": "請填寫所有必填欄位",
  "address_save_btn": "保存",
  "delete_address_tip": "確定刪除地址嗎？",

  // chat
  "doctor_late_title": "醫生很快就會進入會議室，請耐心等候",
  "doctor_late_content": "如果醫生超時五分鐘仍未進入會議室，我們的客戶服務員會主動聯絡你。",
  "patient_late_title": "你遲到了",
  "patient_late_content": "醫生正在進行其他問診，請耐心等候。",
  "chat_quit": "你確定要離開就診室嗎？",

  // profile
  "finish_profile_tip": "請完成個人信息 (姓名，性別，證件號碼和聯絡電話是必填項目)",
  "profile_hkid_tip": "身份證號碼格式無效，請重新輸入",

  // Consultation Completed
  "consultation_completed": "就診完成!",
  "consultation_next": "請留步!",
  "consultation_payment": "切勿離開APP \n姑娘現正為你準備帳單及藥物,預計需時幾分鐘,請耐心等候….",
  "consultation_payment_content": " \n「待辦」頁面可查看等待付款的賬單",

  // queue success

  "queue_success_content": "請稍等, 我們正在獲取診所最新的排隊狀況...",
  "queue_fail_title": "取號不成功",
  "queue_fail_content": "请嘗試以下解決方案：\n\n- 確保手機網絡通訊良好, 之後重新排隊；\n\n- 聯絡我們的客服專員處理退款",
  "queue_checkin_btn": "簽到",

  //  coupon
  "coupon_pre": "卡券",
  "coupon_expired_at": "有效期至: ",
  "coupon_detail": "詳情",
  "coupon_all": "全部",
  "coupon_valid": "尚未使用",
  "coupon_used": "已使用",
  "coupon_expired": "已失效",
  "coupon_empty": " 仲未有電子券",

  // family
  "delete_family_tip": "確定移除該成員？",
  "family_add_btn": "添加家庭成員",

  // map
  "unfound_amap": "未有高德地圖",
  "unfound_baidu_map": "未有百度地圖",
  "unfound_google_map": "未有谷歌地圖",

  // status
  "status_pendding": "待診所回復",
  "status_do": "已完成",
  "status_que": "待完成問卷",
  "status_upcoming": "待就診",
  "status_deposit": "待支付按金",
  "status_comfirm": "待你的回復",

  "no_data": "暫無數據",

  "id_num": "證件號碼",
  "chi_name": "中文全名",
  "eng_name": "英文全名",
  "relationship": "關係",
  "telephone": "聯絡電話",
  "email": "電郵",
  "address": "地址",
  "emerg_name": "緊急聯絡人",
  "emerg_phone": "聯絡人電話",
  "nickname": "暱稱",

  "permission_ungrant": "未授權",
  "use": "使用",
};
