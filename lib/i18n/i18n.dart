import 'package:Heals/i18n/zh_cn.dart';
import 'package:Heals/i18n/zh_en.dart';
import 'package:Heals/i18n/zh_tw.dart';
import 'package:get/get_navigation/src/root/internacionalization.dart';

class I18N extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        "en_EN": zhEN,
        "zh_CN": zhCN,
        "zh_TW": zhTW,
      };
}
