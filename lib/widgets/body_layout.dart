import 'package:flutter/material.dart';

class BodyLayout extends StatelessWidget {
  final Widget? child;
  const BodyLayout({Key? key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 88.0), child: SafeArea(top: false, child: child ?? const SizedBox()));
  }
}
