import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// tab文字
Widget tabTxt(text, {color}) {
  return Text(
    text,
    style: TextStyle(
      fontSize: HFontSizes.minest,
      color: color,
      fontFamily: 'Comfortaa',
    ),
  );
}

// 大文字
Widget title33Txt(text, {color}) {
  return Text(
    text,
    style: TextStyle(
      color: color ?? HColors.mainText,
      fontSize: 33.0,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 大文字
Widget titleBigTxt(text) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.mainColorText,
      fontSize: HFontSizes.particular,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 大文字28
Widget titleHugestTxt(text) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.mainText,
      fontSize: HFontSizes.hugest,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 大文字28
Widget titleHugestTxtActive(text, {color, textAlign}) {
  return Text(
    text.toString(),
    textAlign: textAlign ?? TextAlign.left,
    style: TextStyle(
      color: color ?? HColors.mainColorText,
      fontSize: HFontSizes.hugest,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 大文字26
Widget titleHugeTxt(text, {color, textAlign}) {
  return Text(
    text.toString(),
    textAlign: textAlign ?? TextAlign.left,
    style: TextStyle(
      color: color ?? HColors.mainText,
      fontSize: HFontSizes.huger,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 标题22
Widget titleLargerTxt(text, {color}) {
  return Text(
    text,
    style: TextStyle(
      color: color ?? HColors.mainText,
      fontSize: HFontSizes.larger,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 标题22
Widget titleLargerTxtActive(text, {color}) {
  return Text(
    text,
    style: TextStyle(
      color: color ?? HColors.mainColorText,
      fontSize: HFontSizes.larger,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 标题
Widget titleLargeTxt(text, {color, textAlign}) {
  return Text(
    text,
    textAlign: textAlign,
    style: TextStyle(
      color: color ?? HColors.mainText,
      fontSize: HFontSizes.large,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 标题
Widget titleLargeMainTxt(text, {maxLines, fontSize, double? highLine, color, textAlign}) {
  return Text(
    "$text",
    style: TextStyle(
      color: color ?? HColors.mainColorText,
      fontSize: fontSize ?? HFontSizes.large,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
    maxLines: maxLines,
    overflow: TextOverflow.ellipsis,
  );
}

// 标题
Widget titleTxt(text, {maxLines, textAlign, fontSize, double? highLine}) {
  return Text(
    text ?? '',
    style: TextStyle(
      color: HColors.mainText,
      fontSize: fontSize ?? HFontSizes.medium,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    maxLines: maxLines,
    textAlign: textAlign,
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
    overflow: TextOverflow.ellipsis,
  );
}

// 标题
Widget titleActiveTxt(text, {textAlign, fontSize}) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.mainColorText,
      fontSize: HFontSizes.medium,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    textAlign: textAlign,
  );
}

// 标题
Widget titleTxtRes(text) {
  return Text(
    text,
    style: const TextStyle(
      color: Colors.white,
      fontSize: HFontSizes.medium,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

// 高亮标题
Widget titleMediumMainTxt(text, {maxLines, fontSize, double? highLine}) {
  return Text(
    text,
    style: TextStyle(
      color: HColors.mainColorText,
      fontSize: fontSize ?? HFontSizes.medium,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
    maxLines: maxLines,
    overflow: TextOverflow.ellipsis,
  );
}

// 高亮标题
Widget titleMediumTxt(text, {maxLines, textAlign, color, fontSize, double? highLine}) {
  return Text(
    text ?? '',
    textAlign: textAlign,
    style: TextStyle(
      color: color ?? HColors.mainText,
      fontSize: fontSize ?? HFontSizes.medium,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    maxLines: maxLines ?? 10,
    overflow: TextOverflow.ellipsis,
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
  );
}

// 高亮标题
Widget titleNormalMainTxt(text, {maxLines, fontSize, double? highLine}) {
  return Text(
    text,
    style: TextStyle(
      color: HColors.mainColorText,
      fontSize: fontSize ?? HFontSizes.normal,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
    maxLines: maxLines,
    overflow: TextOverflow.ellipsis,
  );
}

// 高亮标题
Widget titleNormalTxt(text, {maxLines, textAlign, color, fontSize, double? highLine}) {
  return Text(
    text ?? '',
    textAlign: textAlign,
    style: TextStyle(
      color: color ?? HColors.mainText,
      fontSize: fontSize ?? HFontSizes.normal,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    maxLines: maxLines ?? 10,
    overflow: TextOverflow.ellipsis,
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
  );
}

Widget titleNormalTxtRes(text, {fontSize}) {
  return Text(
    text,
    style: TextStyle(
      color: Colors.white,
      fontSize: fontSize ?? HFontSizes.normal,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget titleNormal80Txt(text) {
  return Text(
    text ?? '',
    style: TextStyle(
      color: HColors.mainText.withOpacity(.8),
      fontSize: HFontSizes.normal,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget titleNormalMain70Txt(text) {
  return Text(
    text,
    style: TextStyle(
      color: HColors.mainColorText.withOpacity(.7),
      fontSize: HFontSizes.normal,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget subTitleTxt(text, {color, fontSize, maxLines, highLine, textAlign}) {
  return Text(
    text ?? '',
    textAlign: textAlign ?? TextAlign.left,
    style: TextStyle(
      color: color ?? HColors.subColorText,
      fontSize: fontSize ?? HFontSizes.small,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
    maxLines: maxLines,
    overflow: TextOverflow.ellipsis,
  );
}

Widget subTitleGreyTxt(text, {textAlign, color, maxLines}) {
  return Text(
    text,
    style: TextStyle(
      color: color ?? HColors.greyText,
      fontSize: HFontSizes.small,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    textAlign: textAlign,
  );
}

Widget thirdTitle(text, {textAlign, color, maxLines}) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.subColorText,
      fontSize: HFontSizes.smaller,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    maxLines: maxLines,
    overflow: TextOverflow.ellipsis,
  );
}

Widget thirdTitleActive(text, {highLine, maxLines, color}) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.mainColorText,
      fontSize: HFontSizes.smaller,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    maxLines: maxLines,
    overflow: TextOverflow.ellipsis,
  );
}

Widget smallestTitle(text, {highLine, maxLines, color}) {
  return Text(
    text,
    style: TextStyle(
      color: color ?? HColors.mainText,
      fontSize: HFontSizes.smallest,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    strutStyle: highLine != null ? StrutStyle(forceStrutHeight: true, fontSize: highLine) : null,
    maxLines: maxLines,
    overflow: TextOverflow.ellipsis,
  );
}

Widget smallestTitleActive(text, {color}) {
  return Text(
    text,
    style: TextStyle(
      color: color ?? HColors.mainColorText,
      fontSize: HFontSizes.smallest,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget smallestTitleUnActive(text) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.unactiveText,
      fontSize: HFontSizes.smallest,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget smallestTitleWhite(text) {
  return Text(
    text,
    style: const TextStyle(
      color: Colors.white,
      fontSize: HFontSizes.smallest,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget subTitleTxtActive(text) {
  return Text(
    text ?? '',
    style: TextStyle(
      color: HColors.mainColorText.withOpacity(.7),
      fontSize: HFontSizes.small,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget subTitleTxtRes(text) {
  return Text(
    text,
    style: const TextStyle(
      color: Colors.white,
      fontSize: HFontSizes.small,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget subTitleTxtWidthIcon(text, {fontSize, color, maxLine, lineHeight}) {
  return Text(
    text,
    style: TextStyle(
      color: color ?? HColors.subColorText,
      fontSize: fontSize ?? HFontSizes.small,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
    maxLines: maxLine,
    overflow: TextOverflow.ellipsis,
    strutStyle: StrutStyle(forceStrutHeight: true, fontSize: lineHeight ?? 18.0),
  );
}

Widget secondaryTxt(text) {
  return Text(text);
}

Widget placeholderTxt(text) {
  return Text(text);
}

Widget timeTxt(text) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.mainText,
      fontSize: HFontSizes.min,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget timeActiveTxt(text) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.mainColor,
      fontSize: HFontSizes.small,
      fontFamily: 'Comfortaa',
      fontWeight: FontWeight.w600,
    ),
  );
}

Widget subMainTxt(text) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.subColorText,
      fontSize: HFontSizes.larger,
      fontFamily: 'Comfortaa',
    ),
  );
}

Widget highLightTxt(text) {
  return Text(
    text,
    style: const TextStyle(
      color: HColors.hightLight,
      fontSize: HFontSizes.hugest,
      fontFamily: 'Comfortaa',
    ),
    strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: HFontSizes.hugest),
  );
}

Widget statusTxt(text, {String? state}) {
  return Container(
    height: 16.0,
    padding: const EdgeInsets.symmetric(horizontal: 8.0),
    alignment: Alignment.center,
    decoration: BoxDecoration(
      color: HColors.statusTagBg,
      borderRadius: BorderRadius.circular(8.0),
    ),
    child: Row(
      children: [
        state == 'done'
            ? Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: assetImage12(AssetsInfo.checkSuccessIcon),
              )
            : state == 'close'
                ? Padding(padding: const EdgeInsets.only(right: 8.0), child: assetImage12(AssetsInfo.closeErrorIcon))
                : const SizedBox(),
        Text(
          text,
          style: TextStyle(
            color: state == 'done'
                ? HColors.mainColor
                : state == 'close'
                    ? HColors.warning
                    : HColors.statusText,
            fontSize: HFontSizes.min,
            fontFamily: 'Comfortaa',
          ),
          strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: HFontSizes.min),
        ),
      ],
    ),
  );
}

Widget priAndState() {
  return RichText(
    text: TextSpan(
        text: "login_agree_tip".tr,
        children: [
          TextSpan(
            text: "login_agree_tip_1".tr,
            style: const TextStyle(color: HColors.mainColorText),
            recognizer: TapGestureRecognizer()..onTap = () => Get.toNamed(Routes.statement, arguments: 'statement'),
          ),
          TextSpan(text: "login_agree_tip_2".tr),
          TextSpan(
            text: "login_agree_tip_3".tr,
            style: const TextStyle(color: HColors.mainColorText),
            recognizer: TapGestureRecognizer()..onTap = () => Get.toNamed(Routes.statement, arguments: 'privacy'),
          )
        ],
        style: const TextStyle(
          color: HColors.mainText,
          fontSize: HFontSizes.smallest,
          fontFamily: 'Comfortaa',
        )),
    // overflow: TextOverflow.ellipsis,
    // maxLines: 1,
  );
}

Widget payState() {
  return RichText(
    text: TextSpan(
        text: "pay_agree_tip".tr,
        children: [
          TextSpan(
            text: "pay_agree_tip_1".tr,
            style: const TextStyle(color: HColors.mainColorText),
            recognizer: TapGestureRecognizer()..onTap = () => Get.toNamed(Routes.statement, arguments: 'pay'),
          ),
        ],
        style: const TextStyle(
          color: HColors.mainText,
          fontSize: HFontSizes.smallest,
          fontFamily: 'Comfortaa',
        )),
    // overflow: TextOverflow.ellipsis,
    // maxLines: 1,
  );
}

Widget resendMsg({onTap}) {
  return RichText(
    text: TextSpan(
        text: "forget_resend_1".tr,
        children: [
          TextSpan(
            text: "forget_resend_2".tr,
            style: const TextStyle(color: HColors.mainColorText),
            recognizer: TapGestureRecognizer()..onTap = () => onTap,
          ),
        ],
        style: const TextStyle(
          color: HColors.mainText,
          fontSize: HFontSizes.smallest,
          fontFamily: 'Comfortaa',
        )),
    // overflow: TextOverflow.ellipsis,
    // maxLines: 1,
  );
}

Widget paymentWidget() {
  return RichText(
    text: TextSpan(
        text: "payment_tip".tr,
        children: [
          TextSpan(
            text: "payment_tip_content".tr,
            style: const TextStyle(color: HColors.mainColorText),
            recognizer: TapGestureRecognizer()..onTap = () => Get.toNamed(Routes.statement, arguments: 'statement'),
          ),
        ],
        style: const TextStyle(
          color: HColors.mainText,
          fontSize: HFontSizes.smaller,
          fontFamily: 'Comfortaa',
        )),
    // overflow: TextOverflow.ellipsis,
    // maxLines: 1,
  );
}

Widget richTitle80Txt(textStart, textMiddle, textEnd) {
  return RichText(
    text: TextSpan(
      text: textStart,
      children: [
        TextSpan(
            text: textMiddle,
            style: const TextStyle(
              color: HColors.mainText,
              fontWeight: FontWeight.w600,
            )),
        TextSpan(text: textEnd),
      ],
      style: TextStyle(
        color: HColors.mainText.withOpacity(.8),
        fontSize: HFontSizes.small,
        fontFamily: 'Comfortaa',
      ),
    ),
    strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: HFontSizes.large),
  );
}

Widget richTitleQesTxt(textStart, {bool isRequired = false}) {
  return RichText(
    text: TextSpan(
      text: textStart,
      children: [
        TextSpan(
          text: isRequired ? "*" : "",
          style: const TextStyle(color: Colors.red),
        ),
      ],
      style: const TextStyle(
        color: HColors.mainText,
        fontSize: HFontSizes.normal,
        fontFamily: 'Comfortaa',
        fontWeight: FontWeight.w600,
      ),
    ),
    strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: HFontSizes.large),
  );
}
