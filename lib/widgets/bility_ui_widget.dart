import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/card_model.dart';
import 'package:Heals/model/clinic_model.dart';
import 'package:Heals/model/family_model.dart';
import 'package:Heals/model/form_model.dart';
import 'package:Heals/model/note_key_model.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/avatar_widget.dart';
import 'package:Heals/widgets/normal_input_widget.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

Widget bookList(
    {List? clinicList,
    doctor,
    phone,
    address,
    time,
    onPressBtn,
    onViewMore,
    curPosi,
    showNavigator = true,
    isQueue = false,
    isShowOpen = false}) {
  return Column(
    children: List.generate(clinicList!.length, (index) {
      ClinicModel clinicModel = clinicList[index];
      List<String> rowList = clinicModel.openingHours == null ? [] : clinicModel.openingHours!.split("<br />");

      return shadowCard(
          child: Column(
        children: [
          !isQueue
              ? const SizedBox()
              : Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Row(
                    children: [
                      queueGroup("queue_title".tr, doctor.position.toString()),
                      queueGroup("est_title".tr, doctor.waitTime ?? '00:00'),
                    ],
                  ),
                ),
          iconWidthCont(icon: AssetsInfo.clinicIcon, content: clinicModel.name ?? '', iconSize: 14.0, smallTxt: true),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => launch("tel:${clinicModel.phoneOne}"),
            child: iconWidthCont(
                icon: AssetsInfo.phoneIcon, content: clinicModel.phoneOne ?? '', iconSize: 14.0, smallTxt: true),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => selectMap(clinicModel),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: iconWidthCont(
                      icon: AssetsInfo.addressIcon,
                      content: clinicModel.addressOne ?? '',
                      iconSize: 14.0,
                      maxLine: 10,
                      smallTxt: true),
                ),
                !showNavigator
                    ? const SizedBox()
                    : GestureDetector(
                        onTap: () => selectMap(clinicModel),
                        behavior: HitTestBehavior.translucent,
                        child: Padding(
                          padding: const EdgeInsets.only(
                            right: 16.0,
                            left: 10.0,
                            top: 10.0,
                          ),
                          child: Row(
                            children: [
                              Image.asset(
                                AssetsInfo.navigatorIcon,
                                width: 22.0,
                                fit: BoxFit.fitWidth,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: curPosi == null
                                    ? const SizedBox()
                                    : smallestTitle(
                                        getDistance(
                                          clinicModel.latitude!,
                                          clinicModel.longitude!,
                                          curPosi!.latitude,
                                          curPosi!.longitude,
                                        ),
                                      ),
                              )
                            ],
                          ),
                        ),
                      )
              ],
            ),
          ),
          clinicModel.openingHours == null || clinicModel.openingHours!.isEmpty
              ? const SizedBox()
              : !isShowOpen
                  ? GestureDetector(
                      onTap: onViewMore,
                      behavior: HitTestBehavior.translucent,
                      child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 12.0),
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(color: HColors.border, width: 1.0),
                            ),
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              subTitleTxt("more".tr),
                              Image.asset(
                                AssetsInfo.arrowDownIcon,
                                width: 18.0,
                              )
                            ],
                          )),
                    )
                  : iconWidthCont(
                      icon: AssetsInfo.timeIcon,
                      contentWidget: Column(
                        children: List.generate(rowList.length - 1, (index) {
                          String weekPre = rowList[index].split(": ")[0];
                          String weekStart = Constants.week[weekPre] ?? weekPre;
                          String dayEnd = rowList[index].split(": ")[1];
                          return Row(
                            children: [Expanded(child: subTitleTxtWidthIcon(weekStart)), subTitleTxtWidthIcon(dayEnd)],
                          );
                        }),
                      ),
                      content: '',
                      iconSize: 14.0,
                      smallTxt: true),
          // iconWidthCont(icon: AssetsInfo.clockIcon, content: 'Mon - Fri 09:00 - 17:00', iconSize: 14.0, smallTxt: true),
          onPressBtn == null
              ? const SizedBox()
              : longBtn(title: "doctor_appointment_btn".tr, active: true, onTap: () => onPressBtn(clinicModel))
        ],
      ));
    }),
  );
}

Widget appointmentCard(BookingModel appointmentModel, {checkIn = false, showSmallCheckIn = true, onCheckIn}) {
  return appointmentModel.doctor == null
      ? const SizedBox()
      : shadowCard(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              appointmentModel.appointmentDate == null || checkIn
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(
                        left: 16.0,
                        top: 23.0,
                      ),
                      child: titleBigTxt(textJudge(appointmentModel.appointmentDate)),
                    ),
              appointmentModel.appointmentDate == null
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          titleBigTxt(textJudge(appointmentModel.appointmentTime)),
                          const SizedBox(
                            width: 12.0,
                          ),
                          // subMainTxt("AM"),
                          checkIn || !showSmallCheckIn
                              ? const SizedBox()
                              : !isUpcoming(appointmentModel)
                                  ? const SizedBox()
                                  : Expanded(
                                      child: Container(
                                        height: 30.0,
                                        alignment: Alignment.centerRight,
                                        child:
                                            shortBtn(title: "check_in_btn".tr, height: 30.0, width: 94.0, active: true),
                                      ),
                                    ),
                        ],
                      ),
                    ),
              appointmentModel.appointmentDate != null || checkIn
                  ? const SizedBox()
                  : Padding(
                      padding: const EdgeInsets.only(top: 30.0, left: 16.0, right: 16.0, bottom: 20.0),
                      child: title33Txt('TBC')),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: iconFirWidthCont(icon: AssetsInfo.userIcon, content: textJudge(appointmentModel.user!.name)),
              ),
              iconWidthCont(icon: AssetsInfo.doctorIcon, content: textJudge(appointmentModel.doctor!.name)),
              iconWidthCont(icon: AssetsInfo.houseIcon, content: textJudge(appointmentModel.clinic!.name), maxLine: 2),
              GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => launch("tel:${appointmentModel.clinic!.phoneOne}"),
                  child: iconWidthCont(
                      icon: AssetsInfo.callingIcon, content: textJudge(appointmentModel.clinic!.phoneOne))),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: iconWidthCont(
                      icon: AssetsInfo.addressIcon,
                      maxLine: 4,
                      paddingR: 0,
                      content: textJudge(appointmentModel.clinic!.addressOne),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () => selectMap(appointmentModel.clinic!),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        right: 16.0,
                        left: 10.0,
                        top: 10.0,
                      ),
                      child: Image.asset(
                        AssetsInfo.navigatorIcon,
                        width: 22.0,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  )
                ],
              ),
              // !checkIn ? const SizedBox() :
              iconWidthCont(content: "appt_At".tr + textJudge(appointmentModel.date)),
              // checkIn
              //     ? const SizedBox()
              //     :
              iconWidthCont(content: "clinic_reply_At".tr + textJudge(appointmentModel.responseTime)),
              checkIn
                  ? longBtn(title: "check_in_btn".tr, isCircleBtn: true, active: true, onTap: onCheckIn)
                  : const SizedBox()
            ],
          ),
        );
}

Widget depositGroup(String deposit, String total) {
  return shadowCard(
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1))),
            child: Row(
              children: [
                Expanded(
                  child: titleNormalTxt("Deposit"),
                ),
                titleNormalTxt("\$ $deposit.00"),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(bottom: 16.0, top: 16.0),
            child: Row(
              children: [
                Expanded(
                  child: titleNormalTxt("Total"),
                ),
                titleTxt("\$ $total.00"),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Widget cardGroup(String cardInfo, {onTap}) {
  return shadowCard(
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1))),
            child: Row(
              children: [
                Expanded(
                    child: Row(
                  children: [
                    Padding(padding: const EdgeInsets.only(right: 10.0), child: assetImage33(AssetsInfo.unionIcon)),
                    titleNormalTxt("Credit card"),
                  ],
                )),
                // assetImage18(AssetsInfo.selectIconHlIcon),
              ],
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.only(bottom: 16.0, top: 16.0),
              child: Row(
                children: [
                  Expanded(
                    child: titleNormalMainTxt("Payment card"),
                  ),
                  subTitleTxt(cardInfo),
                  assetImage12(AssetsInfo.arrorSmallIcon)
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

Widget cardNumGroup(teCtrl) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 16.0, bottom: 14.0),
        child: titleNormalTxt("Card number"),
      ),
      inputLayout(
        child: Row(
          children: [
            Expanded(
                child: NormalInputWidget(
              teCtrl: teCtrl,
              layout: false,
            ))
          ],
        ),
      )
    ],
  );
}

Widget cardHolderGroup(teCtrl) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.only(top: 24.0, left: 16.0, bottom: 14.0),
        child: titleNormalTxt("Cardholder name "),
      ),
      inputLayout(
        child: Row(
          children: [
            Expanded(
                child: NormalInputWidget(
              teCtrl: teCtrl,
              layout: false,
            ))
          ],
        ),
      )
    ],
  );
}

Widget cardecGroup(teCtrl, {String? year, String? month, onSelect}) {
  return Container(
    margin: const EdgeInsets.only(top: 28.0, left: 16.0, right: 16.0),
    child: Row(
      children: [
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: titleNormalTxt("Expiry date"),
            ),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: onSelect,
              child: inputLayout(
                child: titleNormalTxt("$month  /  $year"),
                margin: const EdgeInsets.only(right: 8.0),
              ),
            )
          ],
        )),
        Expanded(
            child: Column(
          children: [
            Padding(padding: const EdgeInsets.only(bottom: 10.0), child: titleNormalTxt("cvv / cvc ")),
            inputLayout(
              margin: const EdgeInsets.only(left: 8.0),
              child: NormalInputWidget(
                teCtrl: teCtrl,
                layout: false,
              ),
            )
          ],
        )),
      ],
    ),
  );
}

Widget inputLayout({child, margin}) {
  return Container(
    margin: margin ?? const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 20.0),
    height: 32.0,
    constraints: const BoxConstraints(minHeight: 40.0),
    alignment: Alignment.center,
    decoration: BoxDecoration(
        color: Colors.white.withOpacity(.5),
        border: Border.all(
          color: HColors.mainColor,
          width: 1.0,
        ),
        boxShadow: const [BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))],
        borderRadius: BorderRadius.circular(20.0)),
    child: child,
  );
}

Widget cardSelectWidget(List list, {selectId, onSelect, onAdd}) {
  List<Widget> listWidget = List.generate(list.length, (index) {
    CardModel model = list[index] as CardModel;
    return rowWidget(model.name ?? '',
        subWidet: selectId == model.id
            ? assetImage18(AssetsInfo.selectIconHlIcon)
            : assetImage18(AssetsInfo.selectIconNlIcon),
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        isShowArrow: false,
        onTap: () => onSelect(index));
  });
  return Container(
    decoration: const BoxDecoration(color: Colors.white),
    padding: const EdgeInsets.only(bottom: 40.0, left: 16.0, right: 16.0),
    child: SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            alignment: Alignment.center,
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(color: HColors.border, width: 1.0),
              ),
            ),
            child: titleTxt("Choose card"),
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                Column(
                  children: listWidget,
                ),
                GestureDetector(
                  onTap: onAdd,
                  behavior: HitTestBehavior.translucent,
                  child: rowWidget("Add new card ",
                      icon: AssetsInfo.plusIcon, padding: const EdgeInsets.symmetric(vertical: 12.0)),
                )
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Widget infoAreaOne(NoteKeyModel noteKeyModel) {
  List<Widget> list = List.generate(
      noteKeyModel.otherNotes!.length,
      (index) => Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: titleTxt(noteKeyModel.otherNotes![index].content, maxLines: 10),
          ));
  return Padding(
    padding: const EdgeInsets.only(top: 4.0),
    child: borderLayout(
        child: Container(
      color: HColors.dateCardBg,
      alignment: Alignment.topLeft,
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: list,
      ),
    )),
  );
}

Widget infoAreaTwo(NoteKeyModel noteKeyModel) {
  return Padding(
    padding: const EdgeInsets.only(top: 14.0),
    child: borderLayout(
        child: Container(
      color: HColors.dateCardBg,
      alignment: Alignment.topLeft,
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              assetImage18(AssetsInfo.questionIcon),
              const SizedBox(width: 8.0),
              titleTxt('How to "Check in" ?'),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10.0),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      richTitle80Txt("Click on ", "Check In", " button  in Today's appointment  box on Home page. or,"),
                      const SizedBox(height: 16.0),
                      richTitle80Txt("Click on ", "Check-in", " icon at the bottom menu to access Check in page."),
                    ],
                  ),
                ),
                assetImage(AssetsInfo.detailPic, width: 152.0)
              ],
            ),
          )
        ],
      ),
    )),
  );
}

Widget inputGroup(title,
    {key, leading, readOnly, teCtrl, hintText, actionIcon, bool isRequired = false, onChanged, onTap}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.only(top: 0, left: 16.0, bottom: 14.0),
        child: Row(
          children: [
            Flexible(child: titleNormalTxt(title, maxLines: 10)),
            isRequired
                ? titleLargeTxt(
                    "*",
                    color: Colors.red,
                  )
                : const SizedBox()
          ],
        ),
      ),
      inputLayout(
        child: Row(
          children: [
            Expanded(
                child: NormalInputWidget(
              teCtrl: teCtrl,
              key: key,
              hintText: hintText,
              layout: false,
              onChanged: onChanged,
              readOnly: readOnly,
              leading: leading,
              onTap: onTap,
              action: actionIcon != null
                  ? Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: assetImage16(actionIcon),
                    )
                  : null,
            ))
          ],
        ),
      )
    ],
  );
}

Widget questionGroup(index, {required FormModel questionModel, isRequired = false, List<String>? answer, onTap}) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
    alignment: Alignment.topLeft,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // Container(
        //   width: 18.0,
        //   height: 18.0,
        //   margin: const EdgeInsets.only(right: 16.0),
        //   decoration: BoxDecoration(borderRadius: BorderRadius.circular(9.0), color: HColors.mainColor),
        //   alignment: Alignment.center,
        //   child: subTitleTxtRes(index.toString()),
        // ),
        Row(
          children: [
            Flexible(child: richTitleQesTxt(questionModel.itemTitle, isRequired: isRequired)),
          ],
        ),
        Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Wrap(
              alignment: WrapAlignment.start,
              textDirection: TextDirection.ltr,
              children: List.generate(questionModel.itemOptions!.length, (i) {
                String answerOption = questionModel.itemOptions![i].value!;
                return Padding(
                    padding: const EdgeInsets.only(right: 16.0, bottom: 12.0),
                    child: shortBtn(
                      title: answerOption,
                      width: 120.0,
                      active: answer != null && answer.contains(answerOption),
                      onTap: () => onTap(answerOption),
                    ));
              }),
            ))
      ],
    ),
  );
}

Widget areaNumWidget(title, {onTap}) {
  return GestureDetector(
    // onTap: onTap,
    behavior: HitTestBehavior.translucent,
    child: Padding(
      padding: const EdgeInsets.only(left: 12.0),
      child: Row(
        children: [
          titleNormalTxt('+$title'),
          Padding(
            padding: const EdgeInsets.only(left: 4.0),
            child: assetImage8(AssetsInfo.arrorBottomBtn),
          )
        ],
      ),
    ),
  );
}

Widget tagItem({preIcon, title, subTitle, type}) {
  Color? borderColor;
  late Color bgColor;
  switch (type) {
    case 'ticket':
      borderColor = const Color(0xFF227CC8);
      bgColor = const Color(0xFFD0EAFF);
      break;
    case 'appo':
      borderColor = const Color(0xFFFF8213);
      bgColor = const Color(0xFFFFF1E6);
      break;
    case 'tele':
      borderColor = const Color(0xFF009234);
      bgColor = const Color(0xFFE2FBEB);
      break;
    default:
  }
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.0),
    margin: const EdgeInsets.only(right: 6.0),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0), color: bgColor, border: Border.all(color: borderColor!, width: 1.0)),
    child: Row(
      children: [
        preIcon != null
            ? Padding(
                padding: const EdgeInsets.only(right: 4.0),
                child: Image.asset(
                  AssetsInfo.clockIcon,
                  width: 14.0,
                  fit: BoxFit.fitWidth,
                ),
              )
            : const SizedBox(),
        smallestTitleActive(title, color: borderColor),
        subTitle != null ? titleActiveTxt(subTitle) : const SizedBox()
      ],
    ),
  );
}

Widget notificationTip({onTap}) {
  return GestureDetector(
    behavior: HitTestBehavior.translucent,
    onTap: onTap,
    child: Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      // margin: const EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(color: HColors.hightLight, borderRadius: BorderRadius.circular(4.0)),
      alignment: Alignment.center,
      child: titleNormalTxt("home_tip".tr),
    ),
  );
}

Widget familyWidget(FamilyModel model, {onTap, onTapEdit, onDelete}) {
  return GestureDetector(
    onTap: onTap,
    behavior: HitTestBehavior.translucent,
    child: shadowCard(
        margin: const EdgeInsets.only(bottom: 16.0),
        child: Padding(
          padding: const EdgeInsets.only(top: 12.0, left: 12.0, bottom: 12.0),
          child: Row(
            children: [
              Expanded(
                  child: Row(
                children: [
                  AvatarWidget(
                    avatar: model.icon ?? '',
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          titleNormalTxt(model.profile!.nickname),
                          titleNormalTxt(phoneFormat(model.profile!.phone)),
                          titleNormalTxt(model.profile!.address, maxLines: 3),
                        ],
                      ),
                    ),
                  )
                ],
              )),
              Column(
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: onTapEdit,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                      child: assetImage26(
                        AssetsInfo.editBlueIcon,
                      ),
                    ),
                  ),
                  onDelete == null
                      ? const SizedBox()
                      : GestureDetector(
                          onTap: onDelete,
                          behavior: HitTestBehavior.translucent,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
                            child: assetImage26(AssetsInfo.deleteIcon),
                          ),
                        ),
                ],
              )
            ],
          ),
        )),
  );
}
