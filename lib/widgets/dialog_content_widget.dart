import 'package:flutter/material.dart';

class DialogContentWidget extends StatelessWidget {
  final Widget child;
  const DialogContentWidget({required this.child});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 12.0, top: 12.0),
      child: child,
    );
  }
}
