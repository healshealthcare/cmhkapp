import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FakeSearchWidget extends StatelessWidget {
  final Function? onTap;
  final String? placeholderText;

  const FakeSearchWidget({Key? key, this.onTap, this.placeholderText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (onTap != null) onTap!();
      },
      child: Container(
        margin: const EdgeInsets.only(top: 5.0, bottom: 10.0, left: 16.0, right: 16.0),
        height: 40.0,
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20.0),
        ),
        alignment: Alignment.center,
        child: Row(
          children: [
            Image.asset(AssetsInfo.search, width: 14.0, height: 14.0),
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 2.0, 4.0, 4.0),
              child: Text(placeholderText ?? "search".tr,
                  style: const TextStyle(
                      color: HColors.placeholderColor,
                      fontSize: 12.0,
                      fontFamily: 'Comfortaa',
                      fontWeight: FontWeight.w600)),
            ),
          ],
        ),
      ),
    );
  }
}
