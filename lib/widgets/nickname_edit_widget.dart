import 'package:Heals/constants/style.dart';
import 'package:Heals/widgets/normal_input_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NicknameEditWidget extends StatefulWidget {
  final Rx<TextEditingController> textEditingController;
  final String? defaultValue;
  final RxString inputText;
  final String? title;
  final String? hintText;
  final String? value;
  final int? maxLength;
  final Function? onCancel;
  final Function onSubmit;
  final bool? obscureText;
  final Function toggleEditNickname;
  final Widget? leading;
  const NicknameEditWidget(
      {Key? key,
      required this.textEditingController,
      required this.inputText,
      required this.onSubmit,
      required this.toggleEditNickname,
      this.onCancel,
      this.obscureText,
      this.defaultValue = '',
      this.title,
      this.value,
      this.hintText,
      this.leading,
      this.maxLength})
      : super(key: key);

  @override
  _NicknameEditWidgetState createState() => _NicknameEditWidgetState();
}

class _NicknameEditWidgetState extends State<NicknameEditWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 24.0),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(
            16.0,
          ),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widget.title ?? 'nickname'.tr,
            style: const TextStyle(
              color: HColors.mainColor,
              fontSize: HFontSizes.large,
            ),
            strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: HFontSizes.large),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, bottom: 16.0),
            height: 40.0,
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: const [
                  BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
                ],
                border: Border.all(
                  color: HColors.mainColor,
                  width: 1.0,
                ),
                borderRadius: BorderRadius.circular(20.0)),
            child: inputLayoutUI(
              noPadding: true,
              child: Row(
                children: [
                  Expanded(
                      child: NormalInputWidget(
                    teCtrl: widget.textEditingController(),
                    key: widget.key,
                    hintText: widget.hintText,
                    layout: false,
                    obscureText: widget.obscureText,
                    // onChanged: onChanged,
                    // readOnly: readOnly,
                    leading: widget.leading,
                    // onTap: onTap,
                  ))
                ],
              ),
            ),
          ),
          Row(children: [
            Expanded(
              child: longBtn(
                title: 'cancel'.tr,
                isCircleBtn: true,
                onTap: () {
                  Get.back();
                  if (widget.onCancel != null) {
                    widget.onCancel!();
                    return;
                  }
                  widget.textEditingController.update((data) {
                    data!.text = widget.defaultValue!;
                  });
                },
              ),
            ),
            const SizedBox(
              width: 16.0,
            ),
            Expanded(
              child: Ink(
                child: longBtn(
                  title: 'confirm'.tr,
                  active: true,
                  isCircleBtn: true,
                  onTap: () {
                    Get.back();
                    widget.toggleEditNickname();
                    widget.onSubmit();
                  },
                ),
              ),
            ),
          ])
        ],
      ),
    );
  }
}
