import 'package:Heals/config/assets.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Header extends StatelessWidget {
  final Function()? onTap;
  final String? title;
  final bool? isCenterTitle;
  final Widget? titleWidget;
  final Widget? leading;
  final Widget? action;

  const Header(
      {Key? key, this.leading, this.onTap, this.title, this.titleWidget, this.action, this.isCenterTitle = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return headerBg(
        child: Container(
      padding: const EdgeInsets.only(top: 44.0),
      child: Row(
        children: [
          isCenterTitle!
              ? Expanded(
                  child: Row(
                    children: [leading ?? backIcon(onTap: onTap)],
                  ),
                )
              : Row(
                  children: [leading ?? backIcon()],
                ),
          isCenterTitle! ? titleWidget ?? titleTxtRes(title) : Expanded(child: titleWidget ?? titleTxtRes(title)),
          isCenterTitle! ? Expanded(child: action ?? const SizedBox()) : action ?? const SizedBox()
        ],
      ),
    ));
  }

  Widget headerBg({required Widget child}) {
    return Stack(
      children: [
        Image.asset(
          AssetsInfo.headerBg,
          width: Get.width,
          height: 88.0,
          fit: BoxFit.cover,
        ),
        child
      ],
    );
  }
}

Widget backIcon({icon, onTap}) {
  return GestureDetector(
    behavior: HitTestBehavior.translucent,
    onTap: onTap ?? () => Get.back(),
    child: Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 12.0),
      child: Image.asset(
        icon ?? AssetsInfo.backIcon,
        width: 18.0,
        height: 18.0,
        fit: BoxFit.contain,
      ),
    ),
  );
}
