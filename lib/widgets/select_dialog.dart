import 'package:Heals/constants/style.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide Action, Page;
import 'package:get/get.dart';

class SelectDialog extends AlertDialog {
  final List<SelectItemModel> contentList;
  final Widget? custom;
  final void Function(dynamic)? callback;
  final bool? isBlackBg;

  const SelectDialog({Key? key, required this.contentList, required this.callback, this.isBlackBg, this.custom})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenW = MediaQuery.of(context).size.width;
    double boxH = 60.0 * contentList.length;
    Color textColor = HColors.mainText;

    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => Navigator.of(context).pop(),
        child: Container(
          color: Colors.transparent,
          padding: const EdgeInsets.only(top: 16.0),
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                left: 0,
                child: SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        width: screenW - 32.0,
                        height: boxH,
                        constraints: BoxConstraints(maxHeight: Get.height - 88.0),
                        decoration: BoxDecoration(
                          color: HColors.dialogBgBlack,
                          borderRadius: BorderRadius.circular(24.0),
                        ),
                        margin: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: ScrollConfiguration(
                          behavior: const CupertinoScrollBehavior(),
                          child: ListView.builder(
                            padding: const EdgeInsets.only(top: 0),
                            itemCount: contentList.length,
                            shrinkWrap: true,
                            // physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) {
                              final item = contentList[index];
                              if (item.custom != null && item.custom!) {
                                return custom!;
                              }
                              if (item.cancel != null && item.cancel == true) {
                                textColor = HColors.mainText.withOpacity(.4);
                              } else if (item.isSelected != null && item.isSelected == true) {
                                if (isBlackBg != null && isBlackBg == true) {
                                  textColor = HColors.mainColorText;
                                } else {
                                  textColor = Theme.of(context).selectedRowColor;
                                }
                              } else {
                                if (isBlackBg != null && isBlackBg == true) {
                                  textColor = HColors.mainText;
                                } else {
                                  textColor = Theme.of(context).colorScheme.primary;
                                }
                              }
                              return Material(
                                color: Colors.transparent,
                                // borderRadius: BorderRadius.only(
                                //     topLeft: Radius.circular(
                                //       (index == 0 ? 24.0 : 0),
                                //     ),
                                //     topRight: Radius.circular(
                                //       (index == 0 ? 24.0 : 0),
                                //     ),
                                //     bottomLeft: Radius.circular(
                                //       (index == contentList.length - 1 ? 24.0 : 0),
                                //     ),
                                //     bottomRight: Radius.circular(
                                //       (index == contentList.length - 1 ? 24.0 : 0),
                                //     )),
                                child: Ink(
                                  child: InkWell(
                                    enableFeedback: true,
                                    onTap: () => callback!(item),
                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 60.0,
                                      child: Text(
                                        item.title ?? '',
                                        style: TextStyle(color: textColor, fontSize: HFontSizes.small),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
