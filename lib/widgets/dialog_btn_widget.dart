import 'package:Heals/constants/style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DialogBtnWidget extends StatelessWidget {
  final Function? onTap;
  final int? typ;
  final String? textConfirm;
  final String? textCancel;
  final bool onlySubmitBtn;
  const DialogBtnWidget({Key? key, this.typ, this.onTap, this.textConfirm, this.textCancel, this.onlySubmitBtn = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (onTap != null) onTap!();
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 20.0),
        // padding: const EdgeInsets.symmetric(vertical: 11.0, horizontal: 54.0),
        height: 44.0,
        width: onlySubmitBtn ? Get.width - 112 : 120.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: typ == 1 ? const Color(0x33FFFFFF) : HColors.mainColor,
          borderRadius: BorderRadius.circular(30.0),
          border: typ == 1 ? Border.all(color: HColors.btnBorder) : null,
          boxShadow: const [
            BoxShadow(color: Color(0x2B467D99), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
          ],
        ),
        child: typ == 1
            ? Text(textCancel ?? "no".tr,
                strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: 16.0),
                style: const TextStyle(color: HColors.mainText, fontSize: 16.0))
            : Text(textConfirm ?? "yes".tr,
                strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: 16.0),
                style: const TextStyle(color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.w600)),
      ),
    );
  }
}
