import 'package:Heals/constants/style.dart';
import 'package:flutter/material.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:get/get.dart';

class SelectAddressWidget extends StatefulWidget {
  final List? list;
  const SelectAddressWidget(RxList addrList, {Key? key, this.list}) : super(key: key);

  @override
  _SelectAddressWidgetState createState() => _SelectAddressWidgetState();
}

class _SelectAddressWidgetState extends State<SelectAddressWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(24.0), topRight: Radius.circular(24)),
        ),
        child: Column(
          children: [
            bottomHeader('Select Area'),
          ],
        ));
  }
}
