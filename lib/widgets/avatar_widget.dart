import 'dart:convert';
import 'dart:io';

import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AvatarWidget extends StatelessWidget {
  final String? avatar;
  final double width;
  final double borderWidth;
  final Color? borderColor;
  const AvatarWidget({Key? key, this.avatar, this.width = 68.0, this.borderWidth = 1.0, this.borderColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget? widget;
    if (avatar != null && avatar!.isNotEmpty) {
      File file = File(avatar!);
      if (file.existsSync()) {
        widget = Image.file(
          file,
          width: width,
          height: width,
          fit: BoxFit.cover,
        );
      }
      if (avatar!.startsWith("data:image")) {
        widget = Image.memory(
          base64.decode(avatar!.split(',')[1]),
          width: width, //设置宽度
          height: width,
          fit: BoxFit.cover, //填充
          gaplessPlayback: true, //防止重绘
        );
      } else if (!avatar!.startsWith("http")) {
        widget = Image.memory(
          base64.decode(avatar!),
          width: width, //设置宽度
          height: width,
          fit: BoxFit.cover, //填充
          gaplessPlayback: true, //防止重绘
        );
      }
    }

    widget = widget ??
        CachedNetworkImage(
            imageUrl: avatar!,
            imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
            placeholder: (context, url) => Container(color: Colors.black),
            errorWidget: (context, url, error) => const DefaultAvatar());

    return Container(
      width: width + borderWidth,
      height: width + borderWidth,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: HColors.primary,
        border: Border.all(color: borderColor ?? Colors.white.withOpacity(.1), width: borderWidth),
        borderRadius: BorderRadius.circular(200 / 2),
      ),
      child: ClipRRect(borderRadius: BorderRadius.circular(width / 2), child: widget),
    );
  }
}

class DefaultAvatar extends StatelessWidget {
  final double width;
  final double borderWidth;
  final Color? borderColor;
  final String? defaultIcon;
  const DefaultAvatar({this.width = 68.0, this.borderWidth = 1.0, this.borderColor, this.defaultIcon});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width + borderWidth,
      height: width + borderWidth,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: HColors.primary,
        border: Border.all(color: borderColor ?? Colors.white.withOpacity(.4), width: borderWidth),
        borderRadius: BorderRadius.circular((width + borderWidth) / 2),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(width / 2),
        child: Image.asset(
          defaultIcon ?? AssetsInfo.defaultAvatar,
          width: width,
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}

class AvatarGroup extends StatelessWidget {
  final String? avatar;
  final double width;
  final double borderWidth;
  final Color? borderColor;
  final String? defaultIcon;

  const AvatarGroup(
      {Key? key, this.avatar, this.width = 68.0, this.borderWidth = 0, this.borderColor, this.defaultIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return avatar == null || avatar!.isEmpty
        ? DefaultAvatar(
            width: width,
            borderColor: borderColor,
            borderWidth: borderWidth,
            defaultIcon: defaultIcon,
          )
        : AvatarWidget(avatar: avatar, width: width, borderColor: borderColor, borderWidth: borderWidth);
  }
}
