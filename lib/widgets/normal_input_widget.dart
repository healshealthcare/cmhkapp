import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NormalInputWidget extends StatelessWidget {
  final TextEditingController? teCtrl;
  final Widget? leading;
  final Widget? action;
  final String? leadIcon;
  final bool readOnly;
  final bool layout;
  final bool autoFocus;
  final bool clearBtn;
  final bool refreshBtn;
  final bool showHint;
  final bool? obscureText;
  final String? hintText;
  final String? inputRule;
  final TextAlign textAlign;
  final FocusNode? focusNode;
  final int maxLength;
  final Function? clearFunc;
  final Function? refreshFunc;
  final Function? onSubmitted;
  final Function? onTap;
  final Function? onChanged;
  const NormalInputWidget(
      {Key? key,
      this.teCtrl,
      this.leading,
      this.action,
      this.leadIcon,
      this.readOnly = false,
      this.layout = true,
      this.autoFocus = false,
      this.showHint = true,
      this.clearBtn = false,
      this.refreshBtn = false,
      this.obscureText,
      this.hintText,
      this.inputRule,
      this.textAlign = TextAlign.left,
      this.focusNode,
      this.maxLength = 15,
      this.onSubmitted,
      this.onTap,
      this.onChanged,
      this.clearFunc,
      this.refreshFunc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: layout ? 20.0 : 0),
          decoration: !layout
              ? null
              : BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
                  ],
                  borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            alignment: Alignment.center,
            height: 40.0,
            decoration: !layout
                ? null
                : BoxDecoration(
                    color: HColors.inputBg,
                    border: Border.all(
                      color: HColors.mainColor,
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.circular(20.0)),
            child: Row(
              children: [
                SizedBox(
                  child: leading ??
                      (leadIcon != null
                          ? Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Image.asset(
                                leadIcon!,
                                width: 18.0,
                              ),
                            )
                          : const SizedBox()),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: TextField(
                      focusNode: focusNode,
                      key: key,
                      controller: teCtrl,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 10.0),
                        border: InputBorder.none,
                        hintText: hintText ?? (showHint ? teCtrl?.text : 'biu_tips'.tr),
                        hintStyle: const TextStyle(
                          color: HColors.timeText,
                          fontSize: HFontSizes.medium,
                          fontFamily: 'Comfortaa',
                        ),
                      ),
                      // inputFormatters: <TextInputFormatter>[
                      //   // FilteringTextInputFormatter.allow(RegExp(inputRule ?? Config.contentRule)),
                      // ],
                      obscureText: obscureText ?? false,
                      textAlign: textAlign,
                      style: const TextStyle(
                        color: HColors.mainText,
                        fontSize: HFontSizes.medium,
                        fontFamily: 'Comfortaa',
                      ),
                      cursorColor: HColors.mainText,
                      readOnly: readOnly,
                      autofocus: autoFocus,
                      onSubmitted: (str) {
                        if (onSubmitted != null) onSubmitted!(str.trim());
                      },
                      onChanged: (str) {
                        if (onChanged != null) onChanged!(str);
                      },
                      onTap: () {
                        if (onTap != null) onTap!();
                      },
                    ),
                  ),
                ),
                action ?? const SizedBox()
              ],
            ),
          ),
        ),
        clearBtn == true
            ? Positioned(
                right: 0,
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    teCtrl?.clear();
                    if (clearFunc != null) clearFunc!();
                  },
                  child: Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
                      margin: const EdgeInsets.only(top: 2.0),
                      child: Image.asset(
                        AssetsInfo.inputClearBtn,
                        width: 20,
                        fit: BoxFit.fitWidth,
                      )),
                ),
              )
            : const SizedBox(),
        refreshBtn == true
            ? Positioned(
                right: 0,
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    if (refreshFunc != null) refreshFunc!();
                  },
                  child: Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
                      margin: const EdgeInsets.only(top: 2.0),
                      child: Icon(
                        Icons.refresh_outlined,
                        size: 20.0,
                        color: Colors.white.withOpacity(.6),
                      )),
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}
