import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget arrawRight() {
  return Padding(
    padding: const EdgeInsets.only(right: 16.0, left: 8.0),
    child: Image.asset(
      AssetsInfo.arrowRight,
      height: 10.0,
      fit: BoxFit.fitHeight,
    ),
  );
}

Widget shadowCard({child, margin, bg, borderRadius, shadowColor}) {
  return Container(
    margin: margin ?? const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
    alignment: Alignment.topLeft,
    decoration: BoxDecoration(
      color: bg ?? Colors.white,
      borderRadius: borderRadius ?? BorderRadius.circular(6.0),
      boxShadow: [
        BoxShadow(color: shadowColor ?? HColors.shadowColor, spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
      ],
    ),
    child: child,
  );
}

// 长按钮
Widget longBtn(
    {onTap,
    title,
    icon,
    titleColor,
    isCenter = true,
    active = false,
    dead = false,
    height,
    cleanHorPadd = false,
    cleanVerPadd,
    isCircleBtn = false}) {
  return SafeArea(
    top: false,
    child: GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: Container(
        height: height ?? 44.0,
        width: Get.width - 32.0,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        margin: EdgeInsets.symmetric(horizontal: cleanHorPadd ? 0.0 : 16.0, vertical: cleanVerPadd ?? 16.0),
        decoration: BoxDecoration(
          color: dead ? HColors.btnClose : (active ? HColors.mainColor : HColors.btnBg),
          borderRadius: BorderRadius.circular(isCircleBtn ? 22.0 : 6.0),
          border:
              dead ? Border.all(color: HColors.borderClose) : (active ? null : Border.all(color: HColors.btnBorder)),
          boxShadow: const [
            BoxShadow(color: Color(0x2B467D99), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
          ],
        ),
        child: Row(
          mainAxisAlignment: isCenter ? MainAxisAlignment.center : MainAxisAlignment.start,
          children: [
            icon == null
                ? const SizedBox()
                : Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Image.asset(icon, width: 28.0, fit: BoxFit.fitWidth),
                  ),
            Text(
              title,
              strutStyle: const StrutStyle(forceStrutHeight: true, fontSize: HFontSizes.normal),
              style: TextStyle(
                  color: titleColor ?? (dead ? HColors.txtClose : ((active ? Colors.white : HColors.mainText))),
                  fontSize: HFontSizes.medium,
                  fontFamily: 'Comfortaa',
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    ),
  );
}

Widget listBar(
    {String? icon,
    required String title,
    String? suffixTxt,
    Widget? suffixWidget,
    EdgeInsetsGeometry? padding,
    bool smallSize = false,
    bool isShowArror = true,
    bool isShowBorderBottom = true,
    onPress}) {
  return GestureDetector(
    onTap: onPress,
    behavior: HitTestBehavior.translucent,
    child: Container(
      padding: padding ?? EdgeInsets.symmetric(vertical: smallSize ? 20.0 : 26.0),
      decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
      child: Row(
        children: [
          icon == null
              ? const SizedBox()
              : Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Image.asset(icon, width: 22.0, fit: BoxFit.fitWidth)),
          Expanded(child: smallSize ? titleNormalMainTxt(title) : titleTxt(title)),
          suffixWidget ??
              (suffixTxt == null
                  ? const SizedBox()
                  : Container(
                      constraints: BoxConstraints(maxWidth: Get.width / 2),
                      padding: const EdgeInsets.only(left: 8.0),
                      child: titleTxt(suffixTxt, maxLines: 1, textAlign: TextAlign.right))),
          isShowBorderBottom
              ? Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Image.asset(
                    AssetsInfo.listArrowRightIcon,
                    height: 8.0,
                    fit: BoxFit.fitWidth,
                  ),
                )
              : const SizedBox()
        ],
      ),
    ),
  );
}

Widget iconFirWidthCont({required String icon, required String content}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(
          icon,
          width: 22.0,
          fit: BoxFit.fitWidth,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: subTitleTxtWidthIcon(content, maxLine: 1, fontSize: HFontSizes.large, color: HColors.subColorText),
          ),
        ),
      ],
    ),
  );
}

Widget iconWidthCont(
    {String? icon,
    required String content,
    Widget? contentWidget,
    double? iconSize,
    bool smallTxt = false,
    Color? color,
    int? maxLine,
    double? paddingR}) {
  return Padding(
    padding: EdgeInsets.only(left: 16.0, top: 9.0, bottom: 9.0, right: paddingR ?? 16.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        icon == null
            ? const SizedBox()
            : Padding(
                padding: const EdgeInsets.only(top: 4.0),
                child: Image.asset(
                  icon,
                  width: iconSize ?? 22.0,
                  fit: BoxFit.fitWidth,
                ),
              ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: contentWidget ??
                subTitleTxtWidthIcon(content, color: color, maxLine: maxLine, lineHeight: smallTxt ? 18.0 : null),
          ),
        )
      ],
    ),
  );
}

Widget assetImage8(icon) {
  return Image.asset(
    icon,
    width: 8.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage12(icon) {
  return Image.asset(
    icon,
    width: 12.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage14(icon) {
  return Image.asset(
    icon,
    width: 14.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage16(icon) {
  return Image.asset(
    icon,
    width: 16.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage18(icon) {
  return Image.asset(
    icon,
    width: 18.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage26(icon) {
  return Image.asset(
    icon,
    width: 33.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage33(icon) {
  return Image.asset(
    icon,
    width: 33.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage44(icon) {
  return Image.asset(
    icon,
    width: 44.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage56(icon) {
  return Image.asset(
    icon,
    width: 56.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage64(icon) {
  return Image.asset(
    icon,
    width: 64.0,
    fit: BoxFit.fitWidth,
  );
}

Widget assetImage(icon, {required width, height, fit}) {
  return Image.asset(
    icon,
    width: width,
    height: height,
    fit: fit ?? BoxFit.fitWidth,
  );
}

Widget shortBtn(
    {onTap,
    required String title,
    icon,
    active = false,
    type = 'small',
    width = 56.0,
    height = 28.0,
    bgColor,
    borderRadius,
    fontSize,
    fontWeight = FontWeight.w600}) {
  return GestureDetector(
    behavior: HitTestBehavior.translucent,
    onTap: onTap,
    child: Container(
      height: height,
      width: width,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: bgColor ?? (active ? HColors.mainColor : HColors.border),
          borderRadius: borderRadius ?? BorderRadius.circular(100.0),
          border: active ? null : Border.all(color: HColors.btnBorder, width: 1.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon == null
              ? const SizedBox()
              : Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Image.asset(icon, width: 16.0, fit: BoxFit.fitWidth),
                ),
          active ? titleNormalTxtRes(title, fontSize: fontSize) : titleNormalMainTxt(title, fontSize: fontSize),
        ],
      ),
    ),
  );
}

Widget shortAutoBtn(
    {onTap,
    required String title,
    icon,
    active = false,
    type = 'small',
    width,
    height,
    EdgeInsetsGeometry? padding,
    bgColor,
    fontSize,
    fontWeight = FontWeight.w600}) {
  return GestureDetector(
    behavior: HitTestBehavior.translucent,
    onTap: onTap,
    child: Container(
      height: height,
      width: width,
      padding: padding,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: bgColor ?? (active ? HColors.mainColor : HColors.border),
          borderRadius: BorderRadius.circular(100.0),
          border: active ? null : Border.all(color: HColors.btnBorder, width: 1.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon == null
              ? const SizedBox()
              : Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Image.asset(icon, width: 16.0, fit: BoxFit.fitWidth),
                ),
          active ? titleNormalTxtRes(title, fontSize: fontSize) : titleNormalTxt(title, fontSize: fontSize),
        ],
      ),
    ),
  );
}

Widget inputLayoutUI({required child, color, noPadding = false}) {
  return Container(
    height: 40.0,
    padding: noPadding ? null : const EdgeInsets.symmetric(horizontal: 16.0),
    decoration: BoxDecoration(
        color: color ?? Colors.white,
        boxShadow: const [BoxShadow(color: Color(0x2B467D99), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))],
        borderRadius: BorderRadius.circular(4.0)),
    child: child,
  );
}

Widget emptyWidget({title}) {
  return Container(
    alignment: Alignment.center,
    child: thirdTitle(title ?? "no_data".tr),
  );
}

Widget emptyPage({title, emptyIcon, iconWidth, noPadding = false}) {
  return Container(
    alignment: Alignment.center,
    padding: noPadding ? null : const EdgeInsets.only(top: 127.0, left: 40.0, right: 40.0),
    child: Column(
      children: [
        emptyIcon == null
            ? const SizedBox()
            : Image.asset(
                emptyIcon,
                width: iconWidth ?? 120.0,
                fit: BoxFit.fitWidth,
              ),
        Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: titleTxt(title ?? "no_data".tr, textAlign: TextAlign.center, maxLines: 10),
        ),
      ],
    ),
  );
}

Widget rowWidget(String title,
    {bool smallType = false,
    padding,
    String? subtitle,
    Widget? subWidet,
    String? icon,
    bool isShowArrow = true,
    onTap}) {
  return GestureDetector(
    behavior: HitTestBehavior.translucent,
    onTap: onTap,
    child: Container(
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(color: HColors.border, width: 1.0),
        ),
      ),
      padding: padding ?? const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      child: Row(
        children: [
          icon == null
              ? const SizedBox()
              : Padding(padding: const EdgeInsets.only(right: 8.0), child: assetImage18(icon)),
          Expanded(child: smallType ? subTitleTxt(title) : titleNormalTxt(title)),
          subWidet ?? (smallType ? subTitleTxt(subtitle) : titleNormalTxt(subtitle)),
          isShowArrow
              ? Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: assetImage8(AssetsInfo.listArrowRightIcon),
                )
              : const SizedBox()
        ],
      ),
    ),
  );
}

Widget borderLayout({child}) {
  return Container(
    margin: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
    decoration: BoxDecoration(
        border: Border.all(color: HColors.mainColor, width: .5), borderRadius: BorderRadius.circular(2.0)),
    alignment: Alignment.center,
    child: child,
  );
}

Widget loadingMore(bool isLoading, bool isLoadAll) {
  return !isLoading || isLoadAll
      ? const SizedBox()
      : Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          child: const Text('', style: TextStyle(color: HColors.timeText, fontSize: HFontSizes.smallest)),
        );
}

Widget bottomHeader(String title) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 16.0),
    decoration: const BoxDecoration(
      border: Border(bottom: BorderSide(color: HColors.border, width: 1.0)),
    ),
    child: Stack(
      children: [
        titleTxt(title),
      ],
    ),
  );
}

Widget queueGroup(title, content) {
  return Expanded(
    child: Column(
      children: [
        subTitleTxt(title),
        Container(
          padding: const EdgeInsets.only(top: 10.0, left: 16.0, right: 16.0),
          margin: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Container(
            alignment: Alignment.center,
            height: 44.0,
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            constraints: const BoxConstraints(minWidth: 120.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0), border: Border.all(color: HColors.hightLight, width: 1.0)),
            child: titleHugestTxtActive(content, color: HColors.hightLight, textAlign: TextAlign.center),
          ),
        ),
      ],
    ),
  );
}

Widget unreadWidget(isShow) {
  return !isShow
      ? const SizedBox()
      : Positioned(
          right: 0,
          child: Container(
            width: 8.0,
            height: 8.0,
            decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(10.0)),
          ));
}

Widget appointmentTip() {
  return subTitleTxt("today_schedule_tip".tr, maxLines: 2, fontSize: 14.0);
}

Widget addressWidget(title, phone, address, {onTapEdit, onDelete}) {
  return shadowCard(
    margin: const EdgeInsets.only(bottom: 20),
    child: Container(
      padding: const EdgeInsets.only(left: 16.0, top: 12.0, bottom: 12.0),
      child: Row(
        children: [
          Expanded(
              child: Column(
            children: [
              Row(
                children: [
                  titleLargeMainTxt(title),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: timeTxt(phone),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12.0),
                child: titleNormalTxt(address, maxLines: 3),
              )
            ],
          )),
          Column(
            children: [
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: onTapEdit,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                  child: assetImage26(
                    AssetsInfo.editBlueIcon,
                  ),
                ),
              ),
              onDelete == null
                  ? const SizedBox()
                  : GestureDetector(
                      onTap: onDelete,
                      behavior: HitTestBehavior.translucent,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: assetImage26(AssetsInfo.deleteIcon),
                      ),
                    )
            ],
          ),
        ],
      ),
    ),
  );
}

Widget line() {
  return Container(
    decoration: const BoxDecoration(
      color: HColors.border,
    ),
    height: 1.0,
  );
}

Widget cacheImage(url, {width, height, BoxFit? fit}) {
  return CachedNetworkImage(
    imageUrl: url,
    width: width,
    height: height,
    fit: fit ?? BoxFit.fitWidth,
  );
}
