// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'booking_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookingDataModel<T> _$BookingDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    BookingDataModel<T>(
      data: (json['appointments'] as List<dynamic>?)
          ?.map((e) => BookingModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      bookingModel: json['appointment'] == null
          ? null
          : BookingModel<dynamic>.fromJson(
              json['appointment'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$BookingDataModelToJson<T>(
  BookingDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'appointments': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'appointment': instance.bookingModel?.toJson(
        (value) => value,
      ),
    };
