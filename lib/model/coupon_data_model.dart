import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/coupon_type_model.dart';
// ignore: depend_on_referenced_packages
import 'package:json_annotation/json_annotation.dart';

part 'coupon_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CouponDataModel<T> extends BaseModel {
  @JsonKey(name: "coupons")
  CouponTypeModel? data;

  CouponDataModel({this.data});

  factory CouponDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CouponDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CouponDataModelToJson(this, toJsonT);
}
