import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/invoice_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'invoice_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class InvoiceDataModel<T> extends BaseModel {
  @JsonKey(name: "invoices")
  List<InvoiceModel>? data;

  InvoiceDataModel({this.data});

  factory InvoiceDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$InvoiceDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$InvoiceDataModelToJson(this, toJsonT);
}
