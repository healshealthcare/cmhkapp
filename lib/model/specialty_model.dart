import 'package:json_annotation/json_annotation.dart';

part 'specialty_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class SpecialtyModel<T> {
  @JsonKey(name: "id", defaultValue: 0)
  int? id;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  SpecialtyModel({this.id, this.name});

  factory SpecialtyModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$SpecialtyModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$SpecialtyModelToJson(this, toJsonT);
}
