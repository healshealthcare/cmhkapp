import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'booking_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class BookingDataModel<T> extends BaseModel {
  @JsonKey(name: "appointments")
  List<BookingModel>? data;
  @JsonKey(name: "appointment")
  BookingModel? bookingModel;

  BookingDataModel({this.data, this.bookingModel});

  factory BookingDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$BookingDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$BookingDataModelToJson(this, toJsonT);
}
