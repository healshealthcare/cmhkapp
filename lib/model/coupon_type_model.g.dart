// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponTypeModel<T> _$CouponTypeModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CouponTypeModel<T>(
      allData: (json['ALL'] as List<dynamic>?)
          ?.map((e) => CouponModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      validData: (json['VALID'] as List<dynamic>?)
          ?.map((e) => CouponModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      usedData: (json['USED'] as List<dynamic>?)
          ?.map((e) => CouponModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      invalidData: (json['INVALID'] as List<dynamic>?)
          ?.map((e) => CouponModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    );

Map<String, dynamic> _$CouponTypeModelToJson<T>(
  CouponTypeModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'ALL': instance.allData
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'VALID': instance.validData
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'USED': instance.usedData
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'INVALID': instance.invalidData
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
