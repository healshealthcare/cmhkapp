import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/coupon_model.dart';
// ignore: depend_on_referenced_packages
import 'package:json_annotation/json_annotation.dart';

part 'coupon_type_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CouponTypeModel<T> {
  @JsonKey(name: "ALL")
  List<CouponModel>? allData;
  @JsonKey(name: "VALID")
  List<CouponModel>? validData;
  @JsonKey(name: "USED")
  List<CouponModel>? usedData;
  @JsonKey(name: "INVALID")
  List<CouponModel>? invalidData;

  CouponTypeModel({this.allData, this.validData, this.usedData, this.invalidData});

  factory CouponTypeModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CouponTypeModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CouponTypeModelToJson(this, toJsonT);
}
