// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'select_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SelectItemModel _$SelectItemModelFromJson(Map<String, dynamic> json) =>
    SelectItemModel(
      id: json['id'] as int? ?? 0,
      title: json['title'] as String? ?? '',
      cancel: json['cancel'] as bool?,
      isSelected: json['isSelected'] as bool?,
      custom: json['custom'] as bool?,
    );

Map<String, dynamic> _$SelectItemModelToJson(SelectItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'cancel': instance.cancel,
      'isSelected': instance.isSelected,
      'custom': instance.custom,
    };
