// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ticket_deposit_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TicketDepositModel<T> _$TicketDepositModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    TicketDepositModel<T>(
      id: json['id'] as String? ?? '',
      type: json['type'] as String? ?? '',
      state: json['state'] as String? ?? '',
      name: json['name'] as String? ?? '',
      amount: (json['amount'] as num?)?.toDouble() ?? 0,
    );

Map<String, dynamic> _$TicketDepositModelToJson<T>(
  TicketDepositModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'state': instance.state,
      'name': instance.name,
      'amount': instance.amount,
    };
