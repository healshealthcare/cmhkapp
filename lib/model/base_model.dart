import 'package:json_annotation/json_annotation.dart';

part 'base_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class BaseModel<T> {
  @JsonKey(name: "message_code")
  String? errorCode;
  @JsonKey(name: "success")
  bool? success;
  @JsonKey(name: "error_messages")
  List<String>? errorMessages;

  BaseModel({this.errorCode, this.success, this.errorMessages});

  factory BaseModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$BaseModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$BaseModelToJson(this, toJsonT);
}
