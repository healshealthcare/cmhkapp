// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'questions_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QuestionsDataModel<T> _$QuestionsDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    QuestionsDataModel<T>(
      data: (json['questions'] as List<dynamic>?)
          ?.map((e) => QuestionModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$QuestionsDataModelToJson<T>(
  QuestionsDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'questions': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
