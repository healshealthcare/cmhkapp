// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'relationship_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RelationshipDataModel<T> _$RelationshipDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    RelationshipDataModel<T>(
      data: (json['users'] as List<dynamic>?)
          ?.map((e) => FamilyModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      relationship: (json['relationship'] as List<dynamic>?)
          ?.map((e) => FamilyModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$RelationshipDataModelToJson<T>(
  RelationshipDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'users': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'relationship': instance.relationship
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
