import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/specialty_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'specialty_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class SpecialtyDataModel<T> extends BaseModel {
  @JsonKey(name: "specialties")
  List<SpecialtyModel>? data;

  SpecialtyDataModel({this.data});

  factory SpecialtyDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$SpecialtyDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$SpecialtyDataModelToJson(this, toJsonT);
}
