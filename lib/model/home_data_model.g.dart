// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeDataModel<T> _$HomeDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    HomeDataModel<T>(
      data: (json['feeds'] as List<dynamic>?)
          ?.map((e) => HomeModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$HomeDataModelToJson<T>(
  HomeDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'feeds': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
