import 'package:Heals/model/date_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'clinic_model.dart';
import 'doctor_model.dart';
import 'family_model.dart';

part 'booking_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class BookingModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "create_time")
  String? date;
  @JsonKey(name: "doctor_id")
  String? doctorId;
  @JsonKey(name: "date")
  String? appointmentDate;
  @JsonKey(name: "time")
  String? appointmentTime;
  @JsonKey(name: "state")
  String? state;
  @JsonKey(name: "transaction_id")
  String? transactionId;
  @JsonKey(name: "user_reply")
  String? userReply;
  @JsonKey(name: "user_cancel")
  String? userCancel;
  @JsonKey(name: "clinic_cancel")
  String? clinicCancel;
  @JsonKey(name: "response_time")
  String? responseTime;
  @JsonKey(name: "clinic")
  ClinicModel? clinic;
  @JsonKey(name: "desired_time_slots")
  List<DateModel>? desiredTimeSlots;
  @JsonKey(name: "doctor")
  DoctorModel? doctor;
  @JsonKey(name: "user")
  FamilyModel? user;
  BookingModel({
    this.id,
    this.date,
    this.doctorId,
    this.appointmentDate,
    this.appointmentTime,
    this.state,
    this.userCancel,
    this.userReply,
    this.clinicCancel,
    this.responseTime,
    this.desiredTimeSlots,
    this.clinic,
    this.doctor,
    this.user,
  });

  factory BookingModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$BookingModelFromJson(json, fromJsonT);

  get itemId => null;

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$BookingModelToJson(this, toJsonT);
}
