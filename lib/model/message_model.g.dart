// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageModel<T> _$MessageModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    MessageModel<T>(
      id: json['id'] as int?,
      userId: json['user_id'] as int?,
      creatorId: json['creator_id'] as int?,
      params: json['params'] as Map<String, dynamic>?,
      businessType: json['business_type'] as String? ?? '',
      messageType: json['message_type'] as String? ?? '',
      content: json['content'] as String? ?? '',
      createAt: json['create_at'] as String? ?? '',
      readAt: json['read_at'] as String? ?? '',
      updateAt: json['update_at'] as String? ?? '',
    );

Map<String, dynamic> _$MessageModelToJson<T>(
  MessageModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'creator_id': instance.creatorId,
      'params': instance.params,
      'business_type': instance.businessType,
      'message_type': instance.messageType,
      'content': instance.content,
      'read_at': instance.readAt,
      'create_at': instance.createAt,
      'update_at': instance.updateAt,
    };
