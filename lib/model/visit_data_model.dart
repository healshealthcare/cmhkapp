import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/visit_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'visit_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class VisitDataModel<T> extends BaseModel {
  @JsonKey(name: "allow_check_in", defaultValue: false)
  bool? allowCheckIn;
  @JsonKey(name: "visit")
  VisitModel? data;

  VisitDataModel({this.data, this.allowCheckIn});

  factory VisitDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$VisitDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$VisitDataModelToJson(this, toJsonT);
}
