// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'video_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VideoInfoModel<T> _$VideoInfoModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    VideoInfoModel<T>(
      uid: json['uid'] as String? ?? '',
      name: json['name'] as String? ?? '',
      appId: json['appId'] as String? ?? '',
      meetId: json['meetId'] as String? ?? '',
      uidType: json['uidType'] as String? ?? '',
      rtcToken: json['rtcToken'] as String? ?? '',
      rtmToken: json['rtmToken'] as String? ?? '',
    );

Map<String, dynamic> _$VideoInfoModelToJson<T>(
  VideoInfoModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'appId': instance.appId,
      'meetId': instance.meetId,
      'uidType': instance.uidType,
      'rtcToken': instance.rtcToken,
      'rtmToken': instance.rtmToken,
    };
