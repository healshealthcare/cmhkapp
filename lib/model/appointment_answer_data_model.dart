import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'appointment_answer_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class AppointmentAnswerDataModel<T> extends BaseModel {
  @JsonKey(name: "transaction_id")
  String? transactionId;
  @JsonKey(name: "questionnaire_id")
  int? questionnaireId;

  AppointmentAnswerDataModel({this.transactionId, this.questionnaireId});

  factory AppointmentAnswerDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$AppointmentAnswerDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$AppointmentAnswerDataModelToJson(this, toJsonT);
}
