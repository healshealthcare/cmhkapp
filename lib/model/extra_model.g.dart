// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'extra_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExtraModel<T> _$ExtraModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    ExtraModel<T>(
      user: json['user'] == null
          ? null
          : FamilyModel<dynamic>.fromJson(
              json['user'] as Map<String, dynamic>, (value) => value),
      doctor: json['doctor'] == null
          ? null
          : DoctorModel<dynamic>.fromJson(
              json['doctor'] as Map<String, dynamic>, (value) => value),
      clinic: json['clinic'] == null
          ? null
          : ClinicModel<dynamic>.fromJson(
              json['clinic'] as Map<String, dynamic>, (value) => value),
    );

Map<String, dynamic> _$ExtraModelToJson<T>(
  ExtraModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'user': instance.user?.toJson(
        (value) => value,
      ),
      'doctor': instance.doctor?.toJson(
        (value) => value,
      ),
      'clinic': instance.clinic?.toJson(
        (value) => value,
      ),
    };
