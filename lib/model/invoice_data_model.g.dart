// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invoice_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InvoiceDataModel<T> _$InvoiceDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    InvoiceDataModel<T>(
      data: (json['invoices'] as List<dynamic>?)
          ?.map((e) => InvoiceModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$InvoiceDataModelToJson<T>(
  InvoiceDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'invoices': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
