// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'booking_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookingModel<T> _$BookingModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    BookingModel<T>(
      id: json['id'] as int?,
      date: json['create_time'] as String?,
      doctorId: json['doctor_id'] as String?,
      appointmentDate: json['date'] as String?,
      appointmentTime: json['time'] as String?,
      state: json['state'] as String?,
      userCancel: json['user_cancel'] as String?,
      userReply: json['user_reply'] as String?,
      clinicCancel: json['clinic_cancel'] as String?,
      responseTime: json['response_time'] as String?,
      desiredTimeSlots: (json['desired_time_slots'] as List<dynamic>?)
          ?.map((e) => DateModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      clinic: json['clinic'] == null
          ? null
          : ClinicModel<dynamic>.fromJson(
              json['clinic'] as Map<String, dynamic>, (value) => value),
      doctor: json['doctor'] == null
          ? null
          : DoctorModel<dynamic>.fromJson(
              json['doctor'] as Map<String, dynamic>, (value) => value),
      user: json['user'] == null
          ? null
          : FamilyModel<dynamic>.fromJson(
              json['user'] as Map<String, dynamic>, (value) => value),
    )..transactionId = json['transaction_id'] as String?;

Map<String, dynamic> _$BookingModelToJson<T>(
  BookingModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'create_time': instance.date,
      'doctor_id': instance.doctorId,
      'date': instance.appointmentDate,
      'time': instance.appointmentTime,
      'state': instance.state,
      'transaction_id': instance.transactionId,
      'user_reply': instance.userReply,
      'user_cancel': instance.userCancel,
      'clinic_cancel': instance.clinicCancel,
      'response_time': instance.responseTime,
      'clinic': instance.clinic?.toJson(
        (value) => value,
      ),
      'desired_time_slots': instance.desiredTimeSlots
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'doctor': instance.doctor?.toJson(
        (value) => value,
      ),
      'user': instance.user?.toJson(
        (value) => value,
      ),
    };
