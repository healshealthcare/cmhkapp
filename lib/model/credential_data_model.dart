import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/credential_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'credential_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CredentialDataModel<T> extends BaseModel {
  @JsonKey(name: "credential")
  CredentialModel? data;

  CredentialDataModel({this.data});

  factory CredentialDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CredentialDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CredentialDataModelToJson(this, toJsonT);
}
