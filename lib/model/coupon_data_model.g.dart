// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponDataModel<T> _$CouponDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CouponDataModel<T>(
      data: json['coupons'] == null
          ? null
          : CouponTypeModel<dynamic>.fromJson(
              json['coupons'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$CouponDataModelToJson<T>(
  CouponDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'coupons': instance.data?.toJson(
        (value) => value,
      ),
    };
