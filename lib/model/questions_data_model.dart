import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/question_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'questions_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class QuestionsDataModel<T> extends BaseModel {
  @JsonKey(name: "questions")
  List<QuestionModel>? data;

  QuestionsDataModel({this.data});

  factory QuestionsDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$QuestionsDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$QuestionsDataModelToJson(this, toJsonT);
}
