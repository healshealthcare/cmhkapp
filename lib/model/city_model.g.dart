// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CityModel<T> _$CityModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CityModel<T>(
      id: json['id'] as int?,
      parentId: json['parent_id'] as int?,
      layerLevel: json['layer_level'] as int?,
      levelTag: json['level_tag'] as String?,
      name: json['name'] as String?,
      childs: (json['childs'] as List<dynamic>?)
          ?.map((e) => CityModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    );

Map<String, dynamic> _$CityModelToJson<T>(
  CityModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'parent_id': instance.parentId,
      'layer_level': instance.layerLevel,
      'level_tag': instance.levelTag,
      'name': instance.name,
      'childs': instance.childs
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
