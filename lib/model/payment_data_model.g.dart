// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentDataModel<T> _$PaymentDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    PaymentDataModel<T>(
      data: json['data'] == null
          ? null
          : PaymentModel<dynamic>.fromJson(
              json['data'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$PaymentDataModelToJson<T>(
  PaymentDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'data': instance.data?.toJson(
        (value) => value,
      ),
    };
