// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_tree_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressTreeModel<T> _$AddressTreeModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    AddressTreeModel<T>(
      id: json['id'] as int?,
      name: json['name'] as String? ?? '',
      parentId: json['parent_id'] as int?,
      layerLevel: json['layer_level'] as int?,
      layerTag: json['layer_tag'] as String? ?? '',
      city: json['city'] as String? ?? '',
      childs: (json['childs'] as List<dynamic>?)
              ?.map((e) => AddressTreeModel<dynamic>.fromJson(
                  e as Map<String, dynamic>, (value) => value))
              .toList() ??
          [],
    );

Map<String, dynamic> _$AddressTreeModelToJson<T>(
  AddressTreeModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'parent_id': instance.parentId,
      'layer_level': instance.layerLevel,
      'layer_tag': instance.layerTag,
      'city': instance.city,
      'name': instance.name,
      'childs': instance.childs
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
