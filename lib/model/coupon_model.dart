import 'package:json_annotation/json_annotation.dart';

part 'coupon_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CouponModel<T> {
  @JsonKey(name: "coupon_code")
  String? id;
  @JsonKey(name: "description", defaultValue: '')
  String? description;
  @JsonKey(name: "discount_amount", defaultValue: '')
  String? discountAmount;
  @JsonKey(name: "end_date", defaultValue: '')
  String? endDate;
  @JsonKey(name: "status", defaultValue: 'VALID')
  String? status;
  @JsonKey(name: "coupon_desc", defaultValue: '')
  String? couponDesc;
  @JsonKey(name: "coupon_clause", defaultValue: '')
  String? couponClause;
  CouponModel({this.id, this.description, this.discountAmount, this.endDate, this.couponDesc, this.couponClause});

  factory CouponModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CouponModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CouponModelToJson(this, toJsonT);
}
