import 'package:json_annotation/json_annotation.dart';

part 'credential_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CredentialModel<T> {
  @JsonKey(name: "provider", defaultValue: '')
  String? provider;
  @JsonKey(name: "region", defaultValue: '')
  String? region;
  @JsonKey(name: "bucket", defaultValue: '')
  String? bucket;
  @JsonKey(name: "access_key_d", defaultValue: '')
  String? assessKeyId;
  @JsonKey(name: "secret_access_key", defaultValue: '')
  String? secretAccessKey;
  @JsonKey(name: "session_token", defaultValue: '')
  String? sessionToken;
  CredentialModel({this.provider, this.region, this.bucket, this.assessKeyId, this.secretAccessKey, this.sessionToken});

  factory CredentialModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CredentialModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CredentialModelToJson(this, toJsonT);
}
