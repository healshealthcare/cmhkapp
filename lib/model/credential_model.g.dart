// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credential_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CredentialModel<T> _$CredentialModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CredentialModel<T>(
      provider: json['provider'] as String? ?? '',
      region: json['region'] as String? ?? '',
      bucket: json['bucket'] as String? ?? '',
      assessKeyId: json['access_key_d'] as String? ?? '',
      secretAccessKey: json['secret_access_key'] as String? ?? '',
      sessionToken: json['session_token'] as String? ?? '',
    );

Map<String, dynamic> _$CredentialModelToJson<T>(
  CredentialModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'provider': instance.provider,
      'region': instance.region,
      'bucket': instance.bucket,
      'access_key_d': instance.assessKeyId,
      'secret_access_key': instance.secretAccessKey,
      'session_token': instance.sessionToken,
    };
