import 'package:json_annotation/json_annotation.dart';
import 'clinic_model.dart';
import 'doctor_model.dart';
import 'family_model.dart';

part 'extra_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ExtraModel<T> {
  @JsonKey(name: "user")
  FamilyModel? user;
  @JsonKey(name: "doctor")
  DoctorModel? doctor;
  @JsonKey(name: "clinic")
  ClinicModel? clinic;
  ExtraModel({this.user, this.doctor, this.clinic});

  factory ExtraModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$ExtraModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$ExtraModelToJson(this, toJsonT);
}
