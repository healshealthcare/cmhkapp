// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cancel_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CancelModel<T> _$CancelModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CancelModel<T>(
      transactionId: json['transaction_id'] as String?,
      state: json['state'] as String? ?? '',
    );

Map<String, dynamic> _$CancelModelToJson<T>(
  CancelModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'transaction_id': instance.transactionId,
      'state': instance.state,
    };
