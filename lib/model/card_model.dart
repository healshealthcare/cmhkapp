import 'package:json_annotation/json_annotation.dart';

part 'card_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CardModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "card_no", defaultValue: '')
  String? cardNo;
  CardModel({this.id, this.name, this.cardNo});

  factory CardModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CardModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CardModelToJson(this, toJsonT);
}
