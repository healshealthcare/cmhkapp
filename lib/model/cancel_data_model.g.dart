// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cancel_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CancelDataModel<T> _$CancelDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CancelDataModel<T>(
      cancelFine: json['cancel_fine'] == null
          ? null
          : CancelModel<dynamic>.fromJson(
              json['cancel_fine'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$CancelDataModelToJson<T>(
  CancelDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'cancel_fine': instance.cancelFine?.toJson(
        (value) => value,
      ),
    };
