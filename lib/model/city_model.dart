import 'package:json_annotation/json_annotation.dart';

part 'city_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CityModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "parent_id")
  int? parentId;
  @JsonKey(name: "layer_level")
  int? layerLevel;
  @JsonKey(name: "level_tag")
  String? levelTag;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "childs")
  List<CityModel>? childs;
  CityModel({this.id, this.parentId, this.layerLevel, this.levelTag, this.name, this.childs});

  factory CityModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CityModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CityModelToJson(this, toJsonT);
}
