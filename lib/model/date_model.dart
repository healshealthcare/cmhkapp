import 'package:json_annotation/json_annotation.dart';

part 'date_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DateModel<T> {
  @JsonKey(name: "date", defaultValue: '')
  String? date;
  @JsonKey(name: "period", defaultValue: '')
  String? period;
  DateModel({this.date, this.period});

  factory DateModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DateModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DateModelToJson(this, toJsonT);
}
