// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageDataModel<T> _$MessageDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    MessageDataModel<T>(
      data: json['messages'] == null
          ? null
          : MessageMidModel<dynamic>.fromJson(
              json['messages'] as Map<String, dynamic>, (value) => value),
      unread: json['unread_count'] as int? ?? 0,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$MessageDataModelToJson<T>(
  MessageDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'messages': instance.data?.toJson(
        (value) => value,
      ),
      'unread_count': instance.unread,
    };
