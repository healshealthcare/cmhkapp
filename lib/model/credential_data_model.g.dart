// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credential_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CredentialDataModel<T> _$CredentialDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CredentialDataModel<T>(
      data: json['credential'] == null
          ? null
          : CredentialModel<dynamic>.fromJson(
              json['credential'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$CredentialDataModelToJson<T>(
  CredentialDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'credential': instance.data?.toJson(
        (value) => value,
      ),
    };
