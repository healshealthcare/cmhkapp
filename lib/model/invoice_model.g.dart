// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invoice_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InvoiceModel<T> _$InvoiceModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    InvoiceModel<T>(
      id: json['id'] as int? ?? 0,
      type: json['type'] as String? ?? '',
      state: json['state'] as String? ?? '',
      totalAmount: (json['total_amount'] as num?)?.toDouble() ?? 0,
      copaymentAmount: (json['copayment_amount'] as num?)?.toDouble() ?? 0,
      invoiceItems: (json['invoice_items'] as List<dynamic>?)
          ?.map((e) => InvoiceItemModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      ticketDeposit: json['ticket_deposit'] == null
          ? null
          : TicketDepositModel<dynamic>.fromJson(
              json['ticket_deposit'] as Map<String, dynamic>, (value) => value),
      transactions: (json['transactions'] as List<dynamic>?)
          ?.map((e) => TransactionModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      invoiceCode: json['invoice_code'] as String? ?? '',
    )
      ..createdDate = json['created_date'] as String? ?? ''
      ..extra = json['extra'] == null
          ? null
          : ExtraModel<dynamic>.fromJson(
              json['extra'] as Map<String, dynamic>, (value) => value);

Map<String, dynamic> _$InvoiceModelToJson<T>(
  InvoiceModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'state': instance.state,
      'created_date': instance.createdDate,
      'invoice_code': instance.invoiceCode,
      'total_amount': instance.totalAmount,
      'copayment_amount': instance.copaymentAmount,
      'extra': instance.extra?.toJson(
        (value) => value,
      ),
      'invoice_items': instance.invoiceItems
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'ticket_deposit': instance.ticketDeposit?.toJson(
        (value) => value,
      ),
      'transactions': instance.transactions
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
