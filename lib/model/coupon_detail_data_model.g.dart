// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_detail_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponDetailDataModel<T> _$CouponDetailDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CouponDetailDataModel<T>(
      data: json['coupon_info'] == null
          ? null
          : CouponModel<dynamic>.fromJson(
              json['coupon_info'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$CouponDetailDataModelToJson<T>(
  CouponDetailDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'coupon_info': instance.data?.toJson(
        (value) => value,
      ),
    };
