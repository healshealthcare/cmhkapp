// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'doctor_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DoctorModel<T> _$DoctorModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    DoctorModel<T>(
      name: json['name'] as String?,
      doctorId: json['doctor_id'] as String?,
      clinicId: json['clinic_id'] as String?,
      waitTime: json['wait_time'] as int?,
      favoriteId: json['favorite_id'] as int?,
      position: json['position'] as int? ?? 0,
      remoteEnabled: json['remote_enabled'] as int?,
      appointmentEnabled: json['appointment_enabled'] as int?,
      videoVisitEnabled: json['video_visit_enabled'] as int?,
      clinic: json['clinic'] == null
          ? null
          : ClinicModel<dynamic>.fromJson(
              json['clinic'] as Map<String, dynamic>, (value) => value),
    )
      ..remoteIsFull = json['remote_is_full'] as int? ?? 0
      ..doctor = json['doctor'] == null
          ? null
          : DoctorInfoModel<dynamic>.fromJson(
              json['doctor'] as Map<String, dynamic>, (value) => value);

Map<String, dynamic> _$DoctorModelToJson<T>(
  DoctorModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'name': instance.name,
      'doctor_id': instance.doctorId,
      'favorite_id': instance.favoriteId,
      'clinic_id': instance.clinicId,
      'wait_time': instance.waitTime,
      'position': instance.position,
      'remote_is_full': instance.remoteIsFull,
      'remote_enabled': instance.remoteEnabled,
      'appointment_enabled': instance.appointmentEnabled,
      'video_visit_enabled': instance.videoVisitEnabled,
      'clinic': instance.clinic?.toJson(
        (value) => value,
      ),
      'doctor': instance.doctor?.toJson(
        (value) => value,
      ),
    };
