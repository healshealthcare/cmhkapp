// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressDataModel<T> _$AddressDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    AddressDataModel<T>(
      data: (json['address_books'] as List<dynamic>?)
          ?.map((e) => AddressModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList()
      ..dataTree = (json['address_trees'] as List<dynamic>?)
          ?.map((e) => AddressTreeModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList();

Map<String, dynamic> _$AddressDataModelToJson<T>(
  AddressDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'address_books': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'address_trees': instance.dataTree
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
