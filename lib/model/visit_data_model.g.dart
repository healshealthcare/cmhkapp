// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'visit_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VisitDataModel<T> _$VisitDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    VisitDataModel<T>(
      data: json['visit'] == null
          ? null
          : VisitModel<dynamic>.fromJson(
              json['visit'] as Map<String, dynamic>, (value) => value),
      allowCheckIn: json['allow_check_in'] as bool? ?? false,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$VisitDataModelToJson<T>(
  VisitDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'allow_check_in': instance.allowCheckIn,
      'visit': instance.data?.toJson(
        (value) => value,
      ),
    };
