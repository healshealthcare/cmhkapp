// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionModel<T> _$TransactionModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    TransactionModel<T>(
      id: json['id'] as String? ?? '',
      type: json['type'] as String? ?? '',
      state: json['state'] as String? ?? '',
      amount: (json['amount'] as num?)?.toDouble() ?? 0,
      copaymentAmount: (json['copayment_amount'] as num?)?.toDouble() ?? 0,
    )..createdDate = json['created_date'] as String? ?? '';

Map<String, dynamic> _$TransactionModelToJson<T>(
  TransactionModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'state': instance.state,
      'created_date': instance.createdDate,
      'amount': instance.amount,
      'copayment_amount': instance.copaymentAmount,
    };
