import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/cancel_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cancel_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CancelDataModel<T> extends BaseModel {
  @JsonKey(name: "cancel_fine")
  CancelModel? cancelFine;

  CancelDataModel({this.cancelFine});

  factory CancelDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CancelDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CancelDataModelToJson(this, toJsonT);
}
