// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'clinic_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClinicModel<T> _$ClinicModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    ClinicModel<T>(
      id: json['id'] as String?,
      type: json['type'] as String?,
      name: json['name'] as String?,
      status: json['status'] as String?,
      connected: json['connected'] as int? ?? 0,
      phoneOne: json['phone_one'] as String? ?? '',
      addressOne: json['address_one'] as String? ?? '',
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      district: json['district'] == null
          ? null
          : DistrictModel<dynamic>.fromJson(
              json['district'] as Map<String, dynamic>, (value) => value),
    )..openingHours = json['opening_hours'] as String? ?? '';

Map<String, dynamic> _$ClinicModelToJson<T>(
  ClinicModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': instance.type,
      'status': instance.status,
      'connected': instance.connected,
      'phone_one': instance.phoneOne,
      'address_one': instance.addressOne,
      'opening_hours': instance.openingHours,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'district': instance.district?.toJson(
        (value) => value,
      ),
    };
