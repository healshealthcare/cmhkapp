import 'package:json_annotation/json_annotation.dart';

part 'district_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DistrictModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "latitude")
  double? latitude;
  @JsonKey(name: "longitude")
  double? longitude;
  DistrictModel({this.id, this.name, this.latitude, this.longitude});

  factory DistrictModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DistrictModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DistrictModelToJson(this, toJsonT);
}
