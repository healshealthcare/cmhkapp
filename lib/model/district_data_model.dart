import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/district_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'district_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DistrictDataModel<T> extends BaseModel {
  @JsonKey(name: "districts")
  List<DistrictModel>? data;

  DistrictDataModel({this.data});

  factory DistrictDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DistrictDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DistrictDataModelToJson(this, toJsonT);
}
