import 'package:json_annotation/json_annotation.dart';

part 'service_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ServiceModel<T> {
  @JsonKey(name: "id", defaultValue: 0)
  int? id;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  ServiceModel({this.id, this.name});

  factory ServiceModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$ServiceModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$ServiceModelToJson(this, toJsonT);
}
