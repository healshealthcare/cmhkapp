import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/family_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'relationship_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class RelationshipDataModel<T> extends BaseModel {
  @JsonKey(name: "users")
  List<FamilyModel>? data;
  @JsonKey(name: "relationship")
  List<FamilyModel>? relationship;

  RelationshipDataModel({this.data, this.relationship});

  factory RelationshipDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$RelationshipDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$RelationshipDataModelToJson(this, toJsonT);
}
