import 'package:json_annotation/json_annotation.dart';

part 'select_item_model.g.dart';

@JsonSerializable()
class SelectItemModel {
  @JsonKey(name: "id", defaultValue: 0)
  int? id;
  @JsonKey(name: "title", defaultValue: '')
  String? title;
  @JsonKey(name: "cancel")
  bool? cancel;
  @JsonKey(name: "isSelected")
  bool? isSelected;
  @JsonKey(name: "custom")
  bool? custom;

  SelectItemModel({this.id, this.title, this.cancel, this.isSelected, this.custom});

  factory SelectItemModel.fromJson(Map<String, dynamic> json) => _$SelectItemModelFromJson(json);
  Map<String, dynamic> toJson() => _$SelectItemModelToJson(this);
}
