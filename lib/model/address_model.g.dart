// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddressModel<T> _$AddressModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    AddressModel<T>(
      id: json['id'] as int?,
      name: json['user_id'] as int?,
      city: json['city'] as String? ?? '',
      area: json['area'] as String? ?? '',
      region: json['region'] as String? ?? '',
      district: json['district'] as String? ?? '',
      streetAddr1: json['street_addr_1'] as String? ?? '',
      streetAddr2: json['street_addr_2'] as String? ?? '',
      recipientName: json['recipient_name'] as String? ?? '',
      recipientPhone: json['recipient_phone'] as String? ?? '',
      deliveryNo: json['delivery_no'] as String? ?? '',
      createdAt: json['created_at'] as String? ?? '',
      isDefault: json['is_default'] as int?,
      updatedAt: json['updated_at'] as String? ?? '',
    );

Map<String, dynamic> _$AddressModelToJson<T>(
  AddressModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.name,
      'city': instance.city,
      'area': instance.area,
      'region': instance.region,
      'district': instance.district,
      'street_addr_1': instance.streetAddr1,
      'street_addr_2': instance.streetAddr2,
      'recipient_name': instance.recipientName,
      'recipient_phone': instance.recipientPhone,
      'is_default': instance.isDefault,
      'delivery_no': instance.deliveryNo,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
    };
