// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInfoModel<T> _$UserInfoModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    UserInfoModel<T>(
      id: json['id_num'] as String?,
      dependentId: json['user_id'] as int?,
      avatar: json['icon'] as String?,
      idType: json['id_type'] as String?,
      chiName: json['chi_name'] as String?,
      engName: json['eng_name'] as String?,
      gender: json['gender'] as String?,
      dob: json['dob'] as String? ?? '',
      phone: json['phone'] as String?,
      nickname: json['nickname'] as String?,
      telephone: json['telephone'] as String?,
      address: json['address'] as String?,
      email: json['email'] as String?,
      emergName: json['emerg_name'] as String?,
      emergRelation: json['emerg_relation'] as String?,
      emergPhone: json['emerg_phone'] as String?,
      emergRelationId: json['emerg_relation_id'] as int?,
      receiveLang: json['receive_lang'] as String?,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$UserInfoModelToJson<T>(
  UserInfoModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'user_id': instance.dependentId,
      'icon': instance.avatar,
      'id_type': instance.idType,
      'id_num': instance.id,
      'chi_name': instance.chiName,
      'eng_name': instance.engName,
      'gender': instance.gender,
      'dob': instance.dob,
      'phone': instance.phone,
      'nickname': instance.nickname,
      'telephone': instance.telephone,
      'address': instance.address,
      'email': instance.email,
      'emerg_name': instance.emergName,
      'emerg_relation': instance.emergRelation,
      'emerg_phone': instance.emergPhone,
      'emerg_relation_id': instance.emergRelationId,
      'receive_lang': instance.receiveLang,
    };
