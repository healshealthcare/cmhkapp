// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'note_key_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NoteKeyModel<T> _$NoteKeyModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    NoteKeyModel<T>(
      referralNotes: (json['REFERRAL_NOTES'] as List<dynamic>?)
          ?.map((e) => NoteModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      otherNotes: (json['OTHER_NOTES'] as List<dynamic>?)
          ?.map((e) => NoteModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    );

Map<String, dynamic> _$NoteKeyModelToJson<T>(
  NoteKeyModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'REFERRAL_NOTES': instance.referralNotes
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'OTHER_NOTES': instance.otherNotes
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
