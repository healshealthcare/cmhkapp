import 'package:json_annotation/json_annotation.dart';

part 'ticket_deposit_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class TicketDepositModel<T> {
  @JsonKey(name: "id", defaultValue: '')
  String? id;
  @JsonKey(name: "type", defaultValue: '')
  String? type;
  @JsonKey(name: "state", defaultValue: '')
  String? state;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "amount", defaultValue: 0)
  double? amount;
  TicketDepositModel({this.id, this.type, this.state, this.name, this.amount});

  factory TicketDepositModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$TicketDepositModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$TicketDepositModelToJson(this, toJsonT);
}
