import 'package:json_annotation/json_annotation.dart';

part 'question_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class QuestionModel<T> {
  @JsonKey(name: "question_no")
  int? id;
  @JsonKey(name: "question_txt")
  String? name;
  @JsonKey(name: "answer_options")
  List<String>? answerOptions;
  @JsonKey(name: "answer", defaultValue: '')
  String? answer;
  @JsonKey(name: "has_user_txt")
  int? hasUserTxt;
  @JsonKey(name: "user_txt")
  String? userTxt;
  QuestionModel({this.id, this.name, this.answerOptions, this.hasUserTxt, this.userTxt});

  factory QuestionModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$QuestionModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$QuestionModelToJson(this, toJsonT);
}
