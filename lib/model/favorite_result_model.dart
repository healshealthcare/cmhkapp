import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'favorite_result_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class FavoriteResultModel<T> extends BaseModel {
  @JsonKey(name: "favorite_id")
  int? favoriteId;
  FavoriteResultModel({this.favoriteId});

  factory FavoriteResultModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$FavoriteResultModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$FavoriteResultModelToJson(this, toJsonT);
}
