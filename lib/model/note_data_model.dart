import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/note_key_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'note_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class NoteDataModel<T> extends BaseModel {
  @JsonKey(name: "appoitment_notes")
  NoteKeyModel? data;

  NoteDataModel({this.data});

  factory NoteDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$NoteDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$NoteDataModelToJson(this, toJsonT);
}
