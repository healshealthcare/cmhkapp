// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'specialty_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpecialtyDataModel<T> _$SpecialtyDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    SpecialtyDataModel<T>(
      data: (json['specialties'] as List<dynamic>?)
          ?.map((e) => SpecialtyModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$SpecialtyDataModelToJson<T>(
  SpecialtyDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'specialties': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
