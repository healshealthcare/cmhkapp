import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/coupon_model.dart';
import 'package:Heals/model/coupon_type_model.dart';
// ignore: depend_on_referenced_packages
import 'package:json_annotation/json_annotation.dart';

part 'coupon_detail_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CouponDetailDataModel<T> extends BaseModel {
  @JsonKey(name: "coupon_info")
  CouponModel? data;

  CouponDetailDataModel({this.data});

  factory CouponDetailDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CouponDetailDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CouponDetailDataModelToJson(this, toJsonT);
}
