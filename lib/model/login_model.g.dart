// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginModel<T> _$LoginModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    LoginModel<T>(
      userProfile: json['user'] == null
          ? null
          : UserInfoModel<dynamic>.fromJson(
              json['user'] as Map<String, dynamic>, (value) => value),
      token: json['token'] as String?,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$LoginModelToJson<T>(
  LoginModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'user': instance.userProfile?.toJson(
        (value) => value,
      ),
      'token': instance.token,
    };
