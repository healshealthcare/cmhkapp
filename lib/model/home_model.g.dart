// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeModel<T> _$HomeModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    HomeModel<T>(
      appointmentId: json['appointment_id'] as int?,
      type: json['type'] as String?,
      vistToken: json['vist_token'] as String?,
      position: json['position'] as int? ?? 0,
      arrivalTime: json['arrival_time'] as Map<String, dynamic>?,
      waitTime: json['wait_time'] as String?,
      time: json['time'] as String?,
      address: json['address'] as String?,
      clinicName: json['clinic_name'] as String?,
      doctorName: json['doctor_name'] as String?,
      latitude: (json['latitude'] as num?)?.toDouble(),
      longitude: (json['longitude'] as num?)?.toDouble(),
      phone: json['phone'] as String?,
      ticketType: json['ticket_type'] as String?,
      cancelTime: json['cancel_time'] as int?,
      userId: json['user_id'] as int?,
      doctorId: json['doctor_id'] as String?,
      clinicId: json['clinic_id'] as String?,
      isVideoVisit: json['is_video_visit'] as int? ?? 0,
      user: json['user'] as String?,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$HomeModelToJson<T>(
  HomeModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'appointment_id': instance.appointmentId,
      'type': instance.type,
      'vist_token': instance.vistToken,
      'position': instance.position,
      'time': instance.time,
      'arrival_time': instance.arrivalTime,
      'wait_time': instance.waitTime,
      'address': instance.address,
      'doctor_name': instance.doctorName,
      'clinic_name': instance.clinicName,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'phone': instance.phone,
      'ticket_type': instance.ticketType,
      'cancel_time': instance.cancelTime,
      'user': instance.user,
      'doctor_id': instance.doctorId,
      'clinic_id': instance.clinicId,
      'user_id': instance.userId,
      'is_video_visit': instance.isVideoVisit,
    };
