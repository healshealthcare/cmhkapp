import 'package:Heals/model/address_model.dart';
import 'package:Heals/model/address_tree_model.dart';
import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'address_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class AddressDataModel<T> extends BaseModel {
  @JsonKey(name: "address_books")
  List<AddressModel>? data;
  @JsonKey(name: "address_trees")
  List<AddressTreeModel>? dataTree;

  AddressDataModel({this.data});

  factory AddressDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$AddressDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$AddressDataModelToJson(this, toJsonT);
}
