import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/cancel_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'appointment_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class AppointmentDataModel<T> extends BaseModel {
  @JsonKey(name: "appointment_id")
  int? data;
  @JsonKey(name: "cancel_fine")
  CancelModel? cancelFine;

  AppointmentDataModel({this.data, this.cancelFine});

  factory AppointmentDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$AppointmentDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$AppointmentDataModelToJson(this, toJsonT);
}
