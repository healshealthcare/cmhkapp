import 'package:json_annotation/json_annotation.dart';

part 'payment_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class PaymentModel<T> {
  @JsonKey(name: "payment_url", defaultValue: '')
  String? paymentUrl;
  PaymentModel({this.paymentUrl});

  factory PaymentModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$PaymentModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$PaymentModelToJson(this, toJsonT);
}
