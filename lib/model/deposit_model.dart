import 'package:json_annotation/json_annotation.dart';

part 'deposit_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DepositModel<T> {
  @JsonKey(name: "amount", defaultValue: '')
  String? amount;
  @JsonKey(name: "note", defaultValue: '')
  String? note;
  @JsonKey(name: "payment_methods", defaultValue: [])
  List<String>? paymentMethods;
  DepositModel({
    this.amount,
    this.note,
    this.paymentMethods,
  });

  factory DepositModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DepositModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DepositModelToJson(this, toJsonT);
}
