import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/home_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'home_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class HomeDataModel<T> extends BaseModel {
  @JsonKey(name: "feeds")
  List<HomeModel>? data;

  HomeDataModel({this.data});

  factory HomeDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$HomeDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$HomeDataModelToJson(this, toJsonT);
}
