import 'package:json_annotation/json_annotation.dart';
import 'package:Heals/model/video_info_model.dart';

part 'visit_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class VisitModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "create_time")
  String? date;
  @JsonKey(name: "doctor_id")
  String? doctorId;
  @JsonKey(name: "arrival_time")
  String? arrivalTime;
  @JsonKey(name: "queue_pos", defaultValue: 0)
  int? queuePos;
  @JsonKey(name: "date")
  String? appointmentDate;
  @JsonKey(name: "time")
  String? appointmentTime;
  @JsonKey(name: "doctor_arrival_time")
  String? doctorArrivalTime;
  @JsonKey(name: "state")
  String? state;
  @JsonKey(name: "transaction_id")
  String? transactionId;
  @JsonKey(name: "user_reply")
  String? userReply;
  @JsonKey(name: "user_cancel")
  String? userCancel;
  @JsonKey(name: "clinic_cancel")
  String? clinicCancel;
  @JsonKey(name: "is_video_visit", defaultValue: 0)
  int? isVideoVisit;
  @JsonKey(name: "video_params")
  VideoInfoModel? videoInfo;
  VisitModel(
      {this.id,
      this.date,
      this.doctorId,
      this.appointmentDate,
      this.appointmentTime,
      this.doctorArrivalTime,
      this.state,
      this.userCancel,
      this.userReply,
      this.clinicCancel,
      this.isVideoVisit,
      this.videoInfo,
      this.arrivalTime});

  factory VisitModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$VisitModelFromJson(json, fromJsonT);

  get itemId => null;

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$VisitModelToJson(this, toJsonT);
}
