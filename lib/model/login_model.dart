import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'user_info_model.dart';

part 'login_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class LoginModel<T> extends BaseModel {
  @JsonKey(name: "user")
  UserInfoModel? userProfile;
  @JsonKey(name: "token")
  String? token;

  LoginModel({this.userProfile, this.token});

  factory LoginModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$LoginModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$LoginModelToJson(this, toJsonT);
}
