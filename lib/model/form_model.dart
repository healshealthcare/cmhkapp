import 'package:Heals/model/option_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'form_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class FormModel<T> {
  @JsonKey(name: "item_id")
  String? itemId;
  @JsonKey(name: "item_title", defaultValue: '')
  String? itemTitle;
  @JsonKey(name: "item_type", defaultValue: '')
  String? itemType;
  @JsonKey(name: "default_value", defaultValue: '')
  String? defaultValue;
  @JsonKey(name: "has_user_txt", defaultValue: 0)
  int? hasUserTxt;
  @JsonKey(name: "prompt_txt", defaultValue: '')
  String? promptTxt;
  @JsonKey(name: "is_required", defaultValue: 0)
  int? isRequired;
  @JsonKey(name: "item_options")
  List<OptionModel>? itemOptions;
  FormModel(
      {this.itemId,
      this.itemTitle,
      this.itemType,
      this.defaultValue,
      this.hasUserTxt,
      this.promptTxt,
      this.isRequired,
      this.itemOptions});

  factory FormModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$FormModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$FormModelToJson(this, toJsonT);
}
