import 'package:json_annotation/json_annotation.dart';
import 'clinic_model.dart';
import 'doctor_info_model.dart';

part 'doctor_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DoctorModel<T> {
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "doctor_id")
  String? doctorId;
  @JsonKey(name: "favorite_id")
  int? favoriteId;
  @JsonKey(name: "clinic_id")
  String? clinicId;
  @JsonKey(name: "wait_time")
  int? waitTime;
  @JsonKey(name: "position", defaultValue: 0)
  int? position;
  @JsonKey(name: "remote_is_full", defaultValue: 0)
  int? remoteIsFull;
  @JsonKey(name: "remote_enabled")
  int? remoteEnabled;
  @JsonKey(name: "appointment_enabled")
  int? appointmentEnabled;
  @JsonKey(name: "video_visit_enabled")
  int? videoVisitEnabled;
  @JsonKey(name: "clinic")
  ClinicModel? clinic;
  @JsonKey(name: "doctor")
  DoctorInfoModel? doctor;
  DoctorModel(
      {this.name,
      this.doctorId,
      this.clinicId,
      this.waitTime,
      this.favoriteId,
      this.position,
      this.remoteEnabled,
      this.appointmentEnabled,
      this.videoVisitEnabled,
      this.clinic});

  factory DoctorModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DoctorModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DoctorModelToJson(this, toJsonT);
}
