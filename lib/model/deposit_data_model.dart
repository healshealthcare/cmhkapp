import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/deposit_model.dart';
import 'package:Heals/model/card_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'deposit_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DepositDataModel<T> extends BaseModel {
  @JsonKey(name: "appointment_deposit_settings")
  DepositModel? data;
  @JsonKey(name: "cards")
  List<CardModel>? cardData;

  DepositDataModel({this.data, this.cardData});

  factory DepositDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DepositDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DepositDataModelToJson(this, toJsonT);
}
