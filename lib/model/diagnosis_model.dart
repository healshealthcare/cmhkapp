import 'package:json_annotation/json_annotation.dart';

part 'diagnosis_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DiagnosisModel<T> {
  @JsonKey(name: "id", defaultValue: 0)
  int? id;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  DiagnosisModel({this.id, this.name});

  factory DiagnosisModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DiagnosisModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DiagnosisModelToJson(this, toJsonT);
}
