// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'district_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DistrictDataModel<T> _$DistrictDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    DistrictDataModel<T>(
      data: (json['districts'] as List<dynamic>?)
          ?.map((e) => DistrictModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$DistrictDataModelToJson<T>(
  DistrictDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'districts': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
