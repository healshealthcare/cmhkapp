import 'package:json_annotation/json_annotation.dart';

part 'invoice_item_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class InvoiceItemModel<T> {
  @JsonKey(name: "id", defaultValue: 0)
  int? id;
  @JsonKey(name: "type", defaultValue: '')
  String? type;
  @JsonKey(name: "state", defaultValue: '')
  String? state;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "amount", defaultValue: '')
  String? amount;
  InvoiceItemModel({this.id, this.type, this.state, this.name, this.amount});

  factory InvoiceItemModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$InvoiceItemModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$InvoiceItemModelToJson(this, toJsonT);
}
