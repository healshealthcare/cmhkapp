import 'package:json_annotation/json_annotation.dart';

part 'option_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class OptionModel<T> {
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "value", defaultValue: '')
  String? value;
  OptionModel({this.name, this.value});

  factory OptionModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$OptionModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$OptionModelToJson(this, toJsonT);
}
