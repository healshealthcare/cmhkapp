import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/city_model.dart';
import 'package:Heals/model/form_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'check_in_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CheckInDataModel<T> extends BaseModel {
  @JsonKey(name: "forms")
  List<List<FormModel>>? data;
  @JsonKey(name: "address_trees")
  List<CityModel>? addTree;

  CheckInDataModel({this.data, this.addTree});

  factory CheckInDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CheckInDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CheckInDataModelToJson(this, toJsonT);
}
