// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'note_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NoteDataModel<T> _$NoteDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    NoteDataModel<T>(
      data: json['appoitment_notes'] == null
          ? null
          : NoteKeyModel<dynamic>.fromJson(
              json['appoitment_notes'] as Map<String, dynamic>,
              (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$NoteDataModelToJson<T>(
  NoteDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'appoitment_notes': instance.data?.toJson(
        (value) => value,
      ),
    };
