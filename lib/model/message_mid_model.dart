import 'package:json_annotation/json_annotation.dart';
import 'package:Heals/model/message_model.dart';

part 'message_mid_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class MessageMidModel<T> {
  @JsonKey(name: "data")
  List<MessageModel>? data;
  @JsonKey(name: "current_page")
  int? curPage;
  MessageMidModel({this.data, this.curPage});

  factory MessageMidModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$MessageMidModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$MessageMidModelToJson(this, toJsonT);
}
