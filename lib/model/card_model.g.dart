// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CardModel<T> _$CardModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CardModel<T>(
      id: json['id'] as int?,
      name: json['name'] as String? ?? '',
      cardNo: json['card_no'] as String? ?? '',
    );

Map<String, dynamic> _$CardModelToJson<T>(
  CardModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'card_no': instance.cardNo,
    };
