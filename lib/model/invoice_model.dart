import 'package:Heals/model/invoice_item_model.dart';
import 'package:Heals/model/ticket_deposit_model.dart';
import 'package:Heals/model/transaction_model.dart';
import 'package:Heals/model/extra_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'invoice_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class InvoiceModel<T> {
  @JsonKey(name: "id", defaultValue: 0)
  int? id;
  @JsonKey(name: "type", defaultValue: '')
  String? type;
  @JsonKey(name: "state", defaultValue: '')
  String? state;
  @JsonKey(name: "created_date", defaultValue: '')
  String? createdDate;
  @JsonKey(name: "invoice_code", defaultValue: '')
  String? invoiceCode;
  @JsonKey(name: "total_amount", defaultValue: 0)
  double? totalAmount;
  @JsonKey(name: "copayment_amount", defaultValue: 0)
  double? copaymentAmount;
  @JsonKey(name: "extra")
  ExtraModel? extra;
  @JsonKey(name: "invoice_items")
  List<InvoiceItemModel>? invoiceItems;
  @JsonKey(name: "ticket_deposit")
  TicketDepositModel? ticketDeposit;
  @JsonKey(name: "transactions")
  List<TransactionModel>? transactions;
  InvoiceModel(
      {this.id,
      this.type,
      this.state,
      this.totalAmount,
      this.copaymentAmount,
      this.invoiceItems,
      this.ticketDeposit,
      this.transactions,
      this.invoiceCode});

  factory InvoiceModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$InvoiceModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$InvoiceModelToJson(this, toJsonT);
}
