// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'queue_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QueueDataModel<T> _$QueueDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    QueueDataModel<T>(
      visitToken: json['visit_token'] as String?,
      transactionId: json['transaction_id'] as String?,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$QueueDataModelToJson<T>(
  QueueDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'visit_token': instance.visitToken,
      'transaction_id': instance.transactionId,
    };
