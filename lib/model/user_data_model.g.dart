// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserDataModel<T> _$UserDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    UserDataModel<T>(
      userProfile: json['user_profile'] == null
          ? null
          : UserInfoModel<dynamic>.fromJson(
              json['user_profile'] as Map<String, dynamic>, (value) => value),
      medicineModel: (json['medical_histories'] as List<dynamic>?)
          ?.map((e) => MedicineModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$UserDataModelToJson<T>(
  UserDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'user_profile': instance.userProfile?.toJson(
        (value) => value,
      ),
      'medical_histories': instance.medicineModel
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
