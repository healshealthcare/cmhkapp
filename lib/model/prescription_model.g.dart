// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prescription_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PrescriptionModel<T> _$PrescriptionModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    PrescriptionModel<T>(
      name: json['name'] as String? ?? '',
      usage: json['usage'] as String? ?? '',
      startDate: json['start_date'] as String? ?? '',
      frequency: (json['frequency'] as num?)?.toDouble() ?? 0.0,
      duration: (json['duration'] as num?)?.toDouble() ?? 0.0,
      dosage: (json['dosage'] as num?)?.toDouble() ?? 0.0,
    );

Map<String, dynamic> _$PrescriptionModelToJson<T>(
  PrescriptionModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'name': instance.name,
      'usage': instance.usage,
      'start_date': instance.startDate,
      'frequency': instance.frequency,
      'duration': instance.duration,
      'dosage': instance.dosage,
    };
