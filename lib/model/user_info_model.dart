import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_info_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class UserInfoModel<T> extends BaseModel {
  @JsonKey(name: "user_id")
  int? dependentId;
  @JsonKey(name: "icon")
  String? avatar;
  @JsonKey(name: "id_type")
  String? idType;
  @JsonKey(name: "id_num")
  String? id;
  @JsonKey(name: "chi_name")
  String? chiName;
  @JsonKey(name: "eng_name")
  String? engName;
  @JsonKey(name: "gender")
  String? gender;
  @JsonKey(name: "dob", defaultValue: '')
  String? dob;
  @JsonKey(name: "phone")
  String? phone;
  @JsonKey(name: "nickname")
  String? nickname;
  @JsonKey(name: "telephone")
  String? telephone;
  @JsonKey(name: "address")
  String? address;
  @JsonKey(name: "email")
  String? email;
  @JsonKey(name: "emerg_name")
  String? emergName;
  @JsonKey(name: "emerg_relation")
  String? emergRelation;
  @JsonKey(name: "emerg_phone")
  String? emergPhone;
  @JsonKey(name: "emerg_relation_id")
  int? emergRelationId;
  @JsonKey(name: "receive_lang")
  String? receiveLang;
  // @JsonKey(name: "communication_sms")
  // String? communicationSms;
  // @JsonKey(name: "communication_email")
  // int? communicationEmail;
  // @JsonKey(name: "marketing_sms")
  // String? marketingSms;
  // @JsonKey(name: "marketing_email")
  // String? marketingEmail;

  UserInfoModel({
    this.id,
    this.dependentId,
    this.avatar,
    this.idType,
    this.chiName,
    this.engName,
    this.gender,
    this.dob,
    this.phone,
    this.nickname,
    this.telephone,
    this.address,
    this.email,
    this.emergName,
    this.emergRelation,
    this.emergPhone,
    this.emergRelationId,
    this.receiveLang,
    // this.communicationSms,
    // this.communicationEmail,
    // this.marketingSms,
    // this.marketingEmail
  });

  factory UserInfoModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$UserInfoModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$UserInfoModelToJson(this, toJsonT);
}
