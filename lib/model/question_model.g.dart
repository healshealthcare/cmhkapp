// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QuestionModel<T> _$QuestionModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    QuestionModel<T>(
      id: json['question_no'] as int?,
      name: json['question_txt'] as String?,
      answerOptions: (json['answer_options'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      hasUserTxt: json['has_user_txt'] as int?,
      userTxt: json['user_txt'] as String?,
    )..answer = json['answer'] as String? ?? '';

Map<String, dynamic> _$QuestionModelToJson<T>(
  QuestionModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'question_no': instance.id,
      'question_txt': instance.name,
      'answer_options': instance.answerOptions,
      'answer': instance.answer,
      'has_user_txt': instance.hasUserTxt,
      'user_txt': instance.userTxt,
    };
