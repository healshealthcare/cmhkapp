import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'user_info_model.dart';
import 'medicine_model.dart';

part 'user_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class UserDataModel<T> extends BaseModel {
  @JsonKey(name: "user_profile")
  UserInfoModel? userProfile;
  @JsonKey(name: "medical_histories")
  List<MedicineModel>? medicineModel;

  UserDataModel({this.userProfile, this.medicineModel});

  factory UserDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$UserDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$UserDataModelToJson(this, toJsonT);
}
