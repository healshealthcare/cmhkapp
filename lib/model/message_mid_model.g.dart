// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_mid_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageMidModel<T> _$MessageMidModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    MessageMidModel<T>(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => MessageModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      curPage: json['current_page'] as int?,
    );

Map<String, dynamic> _$MessageMidModelToJson<T>(
  MessageMidModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'data': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'current_page': instance.curPage,
    };
