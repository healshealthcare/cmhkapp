import 'package:json_annotation/json_annotation.dart';

part 'address_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class AddressModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "user_id")
  int? name;
  @JsonKey(name: "city", defaultValue: '')
  String? city;
  @JsonKey(name: "area", defaultValue: '')
  String? area;
  @JsonKey(name: "region", defaultValue: '')
  String? region;
  @JsonKey(name: "district", defaultValue: '')
  String? district;
  @JsonKey(name: "street_addr_1", defaultValue: '')
  String? streetAddr1;
  @JsonKey(name: "street_addr_2", defaultValue: '')
  String? streetAddr2;
  @JsonKey(name: "recipient_name", defaultValue: '')
  String? recipientName;
  @JsonKey(name: "recipient_phone", defaultValue: '')
  String? recipientPhone;
  @JsonKey(name: "is_default")
  int? isDefault;
  @JsonKey(name: "delivery_no", defaultValue: '')
  String? deliveryNo;
  @JsonKey(name: "created_at", defaultValue: '')
  String? createdAt;
  @JsonKey(name: "updated_at", defaultValue: '')
  String? updatedAt;
  AddressModel(
      {this.id,
      this.name,
      this.city,
      this.area,
      this.region,
      this.district,
      this.streetAddr1,
      this.streetAddr2,
      this.recipientName,
      this.recipientPhone,
      this.deliveryNo,
      this.createdAt,
      this.isDefault,
      this.updatedAt});

  factory AddressModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$AddressModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$AddressModelToJson(this, toJsonT);
}
