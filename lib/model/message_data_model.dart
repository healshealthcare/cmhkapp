import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/message_mid_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'message_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class MessageDataModel<T> extends BaseModel {
  @JsonKey(name: "messages")
  MessageMidModel? data;
  @JsonKey(name: "unread_count", defaultValue: 0)
  int? unread;

  MessageDataModel({this.data, this.unread});

  factory MessageDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$MessageDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$MessageDataModelToJson(this, toJsonT);
}
