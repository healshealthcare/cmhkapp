// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'deposit_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DepositDataModel<T> _$DepositDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    DepositDataModel<T>(
      data: json['appointment_deposit_settings'] == null
          ? null
          : DepositModel<dynamic>.fromJson(
              json['appointment_deposit_settings'] as Map<String, dynamic>,
              (value) => value),
      cardData: (json['cards'] as List<dynamic>?)
          ?.map((e) => CardModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$DepositDataModelToJson<T>(
  DepositDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'appointment_deposit_settings': instance.data?.toJson(
        (value) => value,
      ),
      'cards': instance.cardData
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
