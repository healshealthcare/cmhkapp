import 'package:json_annotation/json_annotation.dart';
import 'district_model.dart';

part 'clinic_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ClinicModel<T> {
  @JsonKey(name: "id")
  String? id;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "type")
  String? type;
  @JsonKey(name: "status")
  String? status;
  @JsonKey(name: "connected", defaultValue: 0)
  int? connected;
  @JsonKey(name: "phone_one", defaultValue: '')
  String? phoneOne;
  @JsonKey(name: "address_one", defaultValue: '')
  String? addressOne;
  @JsonKey(name: "opening_hours", defaultValue: '')
  String? openingHours;
  @JsonKey(name: "latitude")
  double? latitude;
  @JsonKey(name: "longitude")
  double? longitude;
  @JsonKey(name: "district")
  DistrictModel? district;
  ClinicModel(
      {this.id,
      this.type,
      this.name,
      this.status,
      this.connected,
      this.phoneOne,
      this.addressOne,
      this.latitude,
      this.longitude,
      this.district});

  factory ClinicModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$ClinicModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$ClinicModelToJson(this, toJsonT);
}
