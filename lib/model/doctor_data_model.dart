import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'doctor_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DoctorDataModel<T> extends BaseModel {
  @JsonKey(name: "doctor_clinics")
  List<DoctorModel>? data;
  @JsonKey(name: "favorite_doctors")
  List<DoctorModel>? favoriteDoctors;
  @JsonKey(name: "doctor_status")
  DoctorModel? doctorStatus;

  DoctorDataModel({this.data, this.favoriteDoctors, this.doctorStatus});

  factory DoctorDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DoctorDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DoctorDataModelToJson(this, toJsonT);
}
