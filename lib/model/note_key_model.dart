import 'package:Heals/model/note_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'note_key_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class NoteKeyModel<T> {
  @JsonKey(name: "REFERRAL_NOTES")
  List<NoteModel>? referralNotes;
  @JsonKey(name: "OTHER_NOTES")
  List<NoteModel>? otherNotes;
  NoteKeyModel({this.referralNotes, this.otherNotes});

  factory NoteKeyModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$NoteKeyModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$NoteKeyModelToJson(this, toJsonT);
}
