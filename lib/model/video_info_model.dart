import 'package:json_annotation/json_annotation.dart';

part 'video_info_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class VideoInfoModel<T> {
  @JsonKey(name: "uid", defaultValue: '')
  String? uid;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "appId", defaultValue: '')
  String? appId;
  @JsonKey(name: "meetId", defaultValue: '')
  String? meetId;
  @JsonKey(name: "uidType", defaultValue: '')
  String? uidType;
  @JsonKey(name: "rtcToken", defaultValue: '')
  String? rtcToken;
  @JsonKey(name: "rtmToken", defaultValue: '')
  String? rtmToken;
  VideoInfoModel({this.uid, this.name, this.appId, this.meetId, this.uidType, this.rtcToken, this.rtmToken});

  factory VideoInfoModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$VideoInfoModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$VideoInfoModelToJson(this, toJsonT);
}
