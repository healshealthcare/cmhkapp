// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'doctor_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DoctorDataModel<T> _$DoctorDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    DoctorDataModel<T>(
      data: (json['doctor_clinics'] as List<dynamic>?)
          ?.map((e) => DoctorModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      favoriteDoctors: (json['favorite_doctors'] as List<dynamic>?)
          ?.map((e) => DoctorModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      doctorStatus: json['doctor_status'] == null
          ? null
          : DoctorModel<dynamic>.fromJson(
              json['doctor_status'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$DoctorDataModelToJson<T>(
  DoctorDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'doctor_clinics': instance.data
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'favorite_doctors': instance.favoriteDoctors
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'doctor_status': instance.doctorStatus?.toJson(
        (value) => value,
      ),
    };
