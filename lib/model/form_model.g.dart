// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FormModel<T> _$FormModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    FormModel<T>(
      itemId: json['item_id'] as String?,
      itemTitle: json['item_title'] as String? ?? '',
      itemType: json['item_type'] as String? ?? '',
      defaultValue: json['default_value'] as String? ?? '',
      hasUserTxt: json['has_user_txt'] as int? ?? 0,
      promptTxt: json['prompt_txt'] as String? ?? '',
      isRequired: json['is_required'] as int? ?? 0,
      itemOptions: (json['item_options'] as List<dynamic>?)
          ?.map((e) => OptionModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    );

Map<String, dynamic> _$FormModelToJson<T>(
  FormModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'item_id': instance.itemId,
      'item_title': instance.itemTitle,
      'item_type': instance.itemType,
      'default_value': instance.defaultValue,
      'has_user_txt': instance.hasUserTxt,
      'prompt_txt': instance.promptTxt,
      'is_required': instance.isRequired,
      'item_options': instance.itemOptions
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
