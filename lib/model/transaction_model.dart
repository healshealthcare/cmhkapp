import 'package:json_annotation/json_annotation.dart';

part 'transaction_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class TransactionModel<T> {
  @JsonKey(name: "id", defaultValue: '')
  String? id;
  @JsonKey(name: "type", defaultValue: '')
  String? type;
  @JsonKey(name: "state", defaultValue: '')
  String? state;
  @JsonKey(name: "created_date", defaultValue: '')
  String? createdDate;
  @JsonKey(name: "amount", defaultValue: 0)
  double? amount;
  @JsonKey(name: "copayment_amount", defaultValue: 0)
  double? copaymentAmount;
  TransactionModel({this.id, this.type, this.state, this.amount, this.copaymentAmount});

  factory TransactionModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$TransactionModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$TransactionModelToJson(this, toJsonT);
}
