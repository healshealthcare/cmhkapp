import 'package:json_annotation/json_annotation.dart';

part 'message_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class MessageModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "user_id")
  int? userId;
  @JsonKey(name: "creator_id")
  int? creatorId;
  @JsonKey(name: "params")
  Map<String, dynamic>? params;
  @JsonKey(name: "business_type", defaultValue: '')
  String? businessType;
  @JsonKey(name: "message_type", defaultValue: '')
  String? messageType;
  @JsonKey(name: "content", defaultValue: '')
  String? content;
  @JsonKey(name: "read_at", defaultValue: '')
  String? readAt;
  @JsonKey(name: "create_at", defaultValue: '')
  String? createAt;
  @JsonKey(name: "update_at", defaultValue: '')
  String? updateAt;
  MessageModel(
      {this.id,
      this.userId,
      this.creatorId,
      this.params,
      this.businessType,
      this.messageType,
      this.content,
      this.createAt,
      this.readAt,
      this.updateAt});

  factory MessageModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$MessageModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$MessageModelToJson(this, toJsonT);
}
