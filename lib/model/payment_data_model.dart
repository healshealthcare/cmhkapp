import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:Heals/model/payment_model.dart';

part 'payment_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class PaymentDataModel<T> extends BaseModel {
  @JsonKey(name: "data")
  PaymentModel? data;

  PaymentDataModel({this.data});

  factory PaymentDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$PaymentDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$PaymentDataModelToJson(this, toJsonT);
}
