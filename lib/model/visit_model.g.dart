// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'visit_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VisitModel<T> _$VisitModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    VisitModel<T>(
      id: json['id'] as int?,
      date: json['create_time'] as String?,
      doctorId: json['doctor_id'] as String?,
      appointmentDate: json['date'] as String?,
      appointmentTime: json['time'] as String?,
      doctorArrivalTime: json['doctor_arrival_time'] as String?,
      state: json['state'] as String?,
      userCancel: json['user_cancel'] as String?,
      userReply: json['user_reply'] as String?,
      clinicCancel: json['clinic_cancel'] as String?,
      isVideoVisit: json['is_video_visit'] as int? ?? 0,
      videoInfo: json['video_params'] == null
          ? null
          : VideoInfoModel<dynamic>.fromJson(
              json['video_params'] as Map<String, dynamic>, (value) => value),
      arrivalTime: json['arrival_time'] as String?,
    )
      ..queuePos = json['queue_pos'] as int? ?? 0
      ..transactionId = json['transaction_id'] as String?;

Map<String, dynamic> _$VisitModelToJson<T>(
  VisitModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'create_time': instance.date,
      'doctor_id': instance.doctorId,
      'arrival_time': instance.arrivalTime,
      'queue_pos': instance.queuePos,
      'date': instance.appointmentDate,
      'time': instance.appointmentTime,
      'doctor_arrival_time': instance.doctorArrivalTime,
      'state': instance.state,
      'transaction_id': instance.transactionId,
      'user_reply': instance.userReply,
      'user_cancel': instance.userCancel,
      'clinic_cancel': instance.clinicCancel,
      'is_video_visit': instance.isVideoVisit,
      'video_params': instance.videoInfo?.toJson(
        (value) => value,
      ),
    };
