import 'package:json_annotation/json_annotation.dart';

part 'note_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class NoteModel<T> {
  @JsonKey(name: "item_type", defaultValue: '')
  String? itemType;
  @JsonKey(name: "content", defaultValue: '')
  String? content;
  // @JsonKey(name: "content_tc", defaultValue: '')
  // String? contentTc;
  // @JsonKey(name: "content_sc", defaultValue: '')
  // String? contentSc;
  NoteModel({this.itemType, this.content});

  factory NoteModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$NoteModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$NoteModelToJson(this, toJsonT);
}
