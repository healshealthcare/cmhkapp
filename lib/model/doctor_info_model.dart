import 'package:json_annotation/json_annotation.dart';
import 'specialty_model.dart';

part 'doctor_info_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class DoctorInfoModel<T> {
  @JsonKey(name: "id")
  String? id;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "phone", defaultValue: '')
  String? phone;
  @JsonKey(name: "is_favorite_doctor", defaultValue: 0)
  int? isFavoriteDoctor;
  @JsonKey(name: "specialty")
  SpecialtyModel? specialty;
  DoctorInfoModel({this.name, this.phone, this.specialty});

  factory DoctorInfoModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$DoctorInfoModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$DoctorInfoModelToJson(this, toJsonT);
}
