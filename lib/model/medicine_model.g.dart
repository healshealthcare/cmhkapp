// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medicine_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MedicineModel<T> _$MedicineModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    MedicineModel<T>(
      clinicModel: json['clinic'] == null
          ? null
          : ClinicModel<dynamic>.fromJson(
              json['clinic'] as Map<String, dynamic>, (value) => value),
      user: json['user'] == null
          ? null
          : UserInfoModel<dynamic>.fromJson(
              json['user'] as Map<String, dynamic>, (value) => value),
      doctor: json['doctors'] == null
          ? null
          : DoctorInfoModel<dynamic>.fromJson(
              json['doctors'] as Map<String, dynamic>, (value) => value),
      deliveryParams: json['delivery_params'] == null
          ? null
          : AddressModel<dynamic>.fromJson(
              json['delivery_params'] as Map<String, dynamic>,
              (value) => value),
      date: json['date'] as String? ?? '',
      prescriptions: (json['prescriptions'] as List<dynamic>?)
          ?.map((e) => PrescriptionModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
      diagnosis: (json['diagnosis'] as List<dynamic>?)
          ?.map((e) => DiagnosisModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    );

Map<String, dynamic> _$MedicineModelToJson<T>(
  MedicineModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'clinic': instance.clinicModel?.toJson(
        (value) => value,
      ),
      'user': instance.user?.toJson(
        (value) => value,
      ),
      'doctors': instance.doctor?.toJson(
        (value) => value,
      ),
      'delivery_params': instance.deliveryParams?.toJson(
        (value) => value,
      ),
      'prescriptions': instance.prescriptions
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'diagnosis': instance.diagnosis
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
      'date': instance.date,
    };
