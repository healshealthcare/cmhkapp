// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'doctor_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DoctorInfoModel<T> _$DoctorInfoModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    DoctorInfoModel<T>(
      name: json['name'] as String?,
      phone: json['phone'] as String? ?? '',
      specialty: json['specialty'] == null
          ? null
          : SpecialtyModel<dynamic>.fromJson(
              json['specialty'] as Map<String, dynamic>, (value) => value),
    )
      ..id = json['id'] as String?
      ..isFavoriteDoctor = json['is_favorite_doctor'] as int? ?? 0;

Map<String, dynamic> _$DoctorInfoModelToJson<T>(
  DoctorInfoModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'phone': instance.phone,
      'is_favorite_doctor': instance.isFavoriteDoctor,
      'specialty': instance.specialty?.toJson(
        (value) => value,
      ),
    };
