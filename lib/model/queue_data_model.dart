import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'queue_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class QueueDataModel<T> extends BaseModel {
  @JsonKey(name: "visit_token")
  String? visitToken;
  @JsonKey(name: "transaction_id")
  String? transactionId;

  QueueDataModel({this.visitToken, this.transactionId});

  factory QueueDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$QueueDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$QueueDataModelToJson(this, toJsonT);
}
