// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'note_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NoteModel<T> _$NoteModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    NoteModel<T>(
      itemType: json['item_type'] as String? ?? '',
      content: json['content'] as String? ?? '',
    );

Map<String, dynamic> _$NoteModelToJson<T>(
  NoteModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'item_type': instance.itemType,
      'content': instance.content,
    };
