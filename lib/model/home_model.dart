import 'package:Heals/model/base_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'home_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class HomeModel<T> extends BaseModel {
  @JsonKey(name: "appointment_id")
  int? appointmentId;
  @JsonKey(name: "type")
  String? type;
  @JsonKey(name: "vist_token")
  String? vistToken;
  @JsonKey(name: "position", defaultValue: 0)
  int? position;
  @JsonKey(name: "time")
  String? time;
  @JsonKey(name: "arrival_time")
  Map<String, dynamic>? arrivalTime;
  @JsonKey(name: "wait_time")
  String? waitTime;
  @JsonKey(name: "address")
  String? address;
  @JsonKey(name: "doctor_name")
  String? doctorName;
  @JsonKey(name: "clinic_name")
  String? clinicName;
  @JsonKey(name: "latitude")
  double? latitude;
  @JsonKey(name: "longitude")
  double? longitude;
  @JsonKey(name: "phone")
  String? phone;
  @JsonKey(name: "ticket_type")
  String? ticketType;
  @JsonKey(name: "cancel_time")
  int? cancelTime;
  @JsonKey(name: "user")
  String? user;
  @JsonKey(name: "doctor_id")
  String? doctorId;
  @JsonKey(name: "clinic_id")
  String? clinicId;
  @JsonKey(name: "user_id")
  int? userId;
  @JsonKey(name: "is_video_visit", defaultValue: 0)
  int? isVideoVisit;

  HomeModel(
      {this.appointmentId,
      this.type,
      this.vistToken,
      this.position,
      this.arrivalTime,
      this.waitTime,
      this.time,
      this.address,
      this.clinicName,
      this.doctorName,
      this.latitude,
      this.longitude,
      this.phone,
      this.ticketType,
      this.cancelTime,
      this.userId,
      this.doctorId,
      this.clinicId,
      this.isVideoVisit,
      this.user});

  factory HomeModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$HomeModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$HomeModelToJson(this, toJsonT);
}
