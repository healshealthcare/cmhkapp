// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_result_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FavoriteResultModel<T> _$FavoriteResultModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    FavoriteResultModel<T>(
      favoriteId: json['favorite_id'] as int?,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$FavoriteResultModelToJson<T>(
  FavoriteResultModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'favorite_id': instance.favoriteId,
    };
