import 'package:json_annotation/json_annotation.dart';

part 'prescription_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class PrescriptionModel<T> {
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "usage", defaultValue: '')
  String? usage;
  @JsonKey(name: "start_date", defaultValue: '')
  String? startDate;
  @JsonKey(name: "frequency", defaultValue: 0.0)
  double? frequency;
  @JsonKey(name: "duration", defaultValue: 0.0)
  double? duration;
  @JsonKey(name: "dosage", defaultValue: 0.0)
  double? dosage;
  PrescriptionModel({this.name, this.usage, this.startDate, this.frequency, this.duration, this.dosage});

  factory PrescriptionModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$PrescriptionModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$PrescriptionModelToJson(this, toJsonT);
}
