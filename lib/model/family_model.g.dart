// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'family_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FamilyModel<T> _$FamilyModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    FamilyModel<T>(
      id: json['id'] as int?,
      name: json['name'] as String? ?? '',
      nickname: json['nickname'] as String? ?? '',
      icon: json['icon'] as String? ?? '',
      profile: json['profile'] == null
          ? null
          : UserInfoModel<dynamic>.fromJson(
              json['profile'] as Map<String, dynamic>, (value) => value),
    );

Map<String, dynamic> _$FamilyModelToJson<T>(
  FamilyModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'nickname': instance.nickname,
      'icon': instance.icon,
      'profile': instance.profile?.toJson(
        (value) => value,
      ),
    };
