// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_answer_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentAnswerDataModel<T> _$AppointmentAnswerDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    AppointmentAnswerDataModel<T>(
      transactionId: json['transaction_id'] as String?,
      questionnaireId: json['questionnaire_id'] as int?,
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$AppointmentAnswerDataModelToJson<T>(
  AppointmentAnswerDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'transaction_id': instance.transactionId,
      'questionnaire_id': instance.questionnaireId,
    };
