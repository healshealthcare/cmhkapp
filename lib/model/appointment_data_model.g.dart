// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentDataModel<T> _$AppointmentDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    AppointmentDataModel<T>(
      data: json['appointment_id'] as int?,
      cancelFine: json['cancel_fine'] == null
          ? null
          : CancelModel<dynamic>.fromJson(
              json['cancel_fine'] as Map<String, dynamic>, (value) => value),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$AppointmentDataModelToJson<T>(
  AppointmentDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'appointment_id': instance.data,
      'cancel_fine': instance.cancelFine?.toJson(
        (value) => value,
      ),
    };
