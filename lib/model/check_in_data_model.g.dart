// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_in_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckInDataModel<T> _$CheckInDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CheckInDataModel<T>(
      data: (json['forms'] as List<dynamic>?)
          ?.map((e) => (e as List<dynamic>)
              .map((e) => FormModel<dynamic>.fromJson(
                  e as Map<String, dynamic>, (value) => value))
              .toList())
          .toList(),
      addTree: (json['address_trees'] as List<dynamic>?)
          ?.map((e) => CityModel<dynamic>.fromJson(
              e as Map<String, dynamic>, (value) => value))
          .toList(),
    )
      ..errorCode = json['message_code'] as String?
      ..success = json['success'] as bool?
      ..errorMessages = (json['error_messages'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList();

Map<String, dynamic> _$CheckInDataModelToJson<T>(
  CheckInDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'message_code': instance.errorCode,
      'success': instance.success,
      'error_messages': instance.errorMessages,
      'forms': instance.data
          ?.map((e) => e
              .map((e) => e.toJson(
                    (value) => value,
                  ))
              .toList())
          .toList(),
      'address_trees': instance.addTree
          ?.map((e) => e.toJson(
                (value) => value,
              ))
          .toList(),
    };
