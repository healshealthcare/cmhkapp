// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentModel<T> _$PaymentModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    PaymentModel<T>(
      paymentUrl: json['payment_url'] as String? ?? '',
    );

Map<String, dynamic> _$PaymentModelToJson<T>(
  PaymentModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'payment_url': instance.paymentUrl,
    };
