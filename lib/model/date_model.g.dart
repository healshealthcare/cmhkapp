// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'date_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DateModel<T> _$DateModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    DateModel<T>(
      date: json['date'] as String? ?? '',
      period: json['period'] as String? ?? '',
    );

Map<String, dynamic> _$DateModelToJson<T>(
  DateModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'date': instance.date,
      'period': instance.period,
    };
