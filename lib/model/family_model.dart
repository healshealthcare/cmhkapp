import 'package:json_annotation/json_annotation.dart';
import 'user_info_model.dart';

part 'family_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class FamilyModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "nickname", defaultValue: '')
  String? nickname;
  @JsonKey(name: "icon", defaultValue: '')
  String? icon;
  @JsonKey(name: "profile")
  UserInfoModel? profile;
  FamilyModel({this.id, this.name, this.nickname, this.icon, this.profile});

  factory FamilyModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$FamilyModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$FamilyModelToJson(this, toJsonT);
}
