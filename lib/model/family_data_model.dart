import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/family_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'family_data_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class FamilyDataModel<T> extends BaseModel {
  @JsonKey(name: "relationship")
  List<FamilyModel>? data;

  FamilyDataModel({this.data});

  factory FamilyDataModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$FamilyDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$FamilyDataModelToJson(this, toJsonT);
}
