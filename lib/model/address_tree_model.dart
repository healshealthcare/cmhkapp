import 'package:json_annotation/json_annotation.dart';

part 'address_tree_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class AddressTreeModel<T> {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "parent_id")
  int? parentId;
  @JsonKey(name: "layer_level")
  int? layerLevel;
  @JsonKey(name: "layer_tag", defaultValue: '')
  String? layerTag;
  @JsonKey(name: "city", defaultValue: '')
  String? city;
  @JsonKey(name: "name", defaultValue: '')
  String? name;
  @JsonKey(name: "childs", defaultValue: [])
  List<AddressTreeModel>? childs;
  AddressTreeModel({this.id, this.name, this.parentId, this.layerLevel, this.layerTag, this.city, this.childs});

  factory AddressTreeModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$AddressTreeModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$AddressTreeModelToJson(this, toJsonT);
}
