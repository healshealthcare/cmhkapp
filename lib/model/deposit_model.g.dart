// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'deposit_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DepositModel<T> _$DepositModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    DepositModel<T>(
      amount: json['amount'] as String? ?? '',
      note: json['note'] as String? ?? '',
      paymentMethods: (json['payment_methods'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          [],
    );

Map<String, dynamic> _$DepositModelToJson<T>(
  DepositModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'amount': instance.amount,
      'note': instance.note,
      'payment_methods': instance.paymentMethods,
    };
