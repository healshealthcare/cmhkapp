import 'package:json_annotation/json_annotation.dart';
import 'package:Heals/model/clinic_model.dart';
import 'package:Heals/model/doctor_info_model.dart';
import 'package:Heals/model/prescription_model.dart';
import 'package:Heals/model/diagnosis_model.dart';
import 'package:Heals/model/user_info_model.dart';
import 'package:Heals/model/address_model.dart';

part 'medicine_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class MedicineModel<T> {
  @JsonKey(name: "clinic")
  ClinicModel? clinicModel;
  @JsonKey(name: "user")
  UserInfoModel? user;
  @JsonKey(name: "doctors")
  DoctorInfoModel? doctor;
  @JsonKey(name: "delivery_params")
  AddressModel? deliveryParams;
  @JsonKey(name: "prescriptions")
  List<PrescriptionModel>? prescriptions;
  @JsonKey(name: "diagnosis")
  List<DiagnosisModel>? diagnosis;
  @JsonKey(name: "date", defaultValue: '')
  String? date;
  MedicineModel(
      {this.clinicModel, this.user, this.doctor, this.deliveryParams, this.date, this.prescriptions, this.diagnosis});

  factory MedicineModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$MedicineModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$MedicineModelToJson(this, toJsonT);
}
