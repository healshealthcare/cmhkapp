import 'package:json_annotation/json_annotation.dart';

part 'cancel_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class CancelModel<T> {
  @JsonKey(name: "transaction_id")
  String? transactionId;
  @JsonKey(name: "state", defaultValue: '')
  String? state;
  CancelModel({this.transactionId, this.state});

  factory CancelModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$CancelModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) => _$CancelModelToJson(this, toJsonT);
}
