// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coupon_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CouponModel<T> _$CouponModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    CouponModel<T>(
      id: json['coupon_code'] as String?,
      description: json['description'] as String? ?? '',
      discountAmount: json['discount_amount'] as String? ?? '',
      endDate: json['end_date'] as String? ?? '',
      couponDesc: json['coupon_desc'] as String? ?? '',
      couponClause: json['coupon_clause'] as String? ?? '',
    )..status = json['status'] as String? ?? 'VALID';

Map<String, dynamic> _$CouponModelToJson<T>(
  CouponModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'coupon_code': instance.id,
      'description': instance.description,
      'discount_amount': instance.discountAmount,
      'end_date': instance.endDate,
      'status': instance.status,
      'coupon_desc': instance.couponDesc,
      'coupon_clause': instance.couponClause,
    };
