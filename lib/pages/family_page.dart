import 'package:Heals/constants/style.dart';
import 'package:Heals/model/family_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/family_controller.dart';

class FamilyPage extends GetView<FamilyController> {
  const FamilyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Obx(
                      () => Container(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Column(
                          children: List.generate(controller.familyList.length, (index) {
                            FamilyModel model = controller.familyList[index];
                            return GestureDetector(
                              onTap: () => controller.handleFamily(index),
                              child: Container(
                                padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                                child: familyWidget(model,
                                    onTap: () => controller.handleFamily(index),
                                    onTapEdit: () => Get.toNamed(Routes.profile,
                                        arguments: {"profile": model.profile, "dependentId": model.id}),
                                    onDelete: model.id == controller.appSrv.userInfoModel().dependentId
                                        ? null
                                        : () => controller.deleteFamily(index)),
                              ),
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                  child: longBtn(
                    title: "family_add_btn".tr,
                    onTap: () => Get.toNamed(Routes.profile, arguments: "add"),
                    isCircleBtn: true,
                    active: true,
                  ),
                )
              ],
            ),
          ),
          Header(
            title: "page_family_title".tr,
          ),
        ],
      ),
    );
  }
}
