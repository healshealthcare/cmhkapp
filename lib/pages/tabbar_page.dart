import 'package:Heals/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/tabbar_controller.dart';

class TabbarPage extends GetView<TabbarController> {
  const TabbarPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.transparent,
      bottomNavigationBar: BottomAppBar(
        color: HColors.tabbarBg,
        child: Obx(() => Row(
              children: [
                _tabItem(
                    icon: AssetsInfo.tabHome,
                    activeIcon: AssetsInfo.tabHomeActive,
                    tabName: "tab_home".tr,
                    onPressed: () => controller.onPressedTab(0),
                    isActive: controller.tabIndex.value == 0),
                // _tabItem(
                //     icon: AssetsInfo.tabCheckIn,
                //     activeIcon: AssetsInfo.tabCheckInActive,
                //     tabName: "tab_check".tr,
                //     onPressed: () => controller.onPressedTab(1),
                //     isActive: controller.tabIndex.value == 1),
                Obx(() => _tabItem(
                    icon: AssetsInfo.tabHealthSup,
                    activeIcon: AssetsInfo.tabHealthSupActive,
                    tabName: "tab_msg".tr,
                    showUnread: controller.notifyPointService.messageCount > 0,
                    onPressed: () => controller.onPressedTab(1),
                    isActive: controller.tabIndex.value == 1)),
                _tabItem(
                    icon: AssetsInfo.tabAccount,
                    activeIcon: AssetsInfo.tabAccountActive,
                    tabName: "tab_account".tr,
                    onPressed: () => controller.onPressedTab(2),
                    isActive: controller.tabIndex.value == 2),
              ],
            )),
      ),
      body: Obx(() => controller.list[controller.tabIndex.value]),
    );
  }

  Widget _tabItem(
      {required String icon,
      required String activeIcon,
      required String tabName,
      bool showUnread = false,
      void Function()? onPressed,
      bool isActive = false}) {
    return Expanded(
      child: GestureDetector(
        onTap: onPressed,
        behavior: HitTestBehavior.translucent,
        child: Container(
          height: 50,
          padding: const EdgeInsets.only(top: 9.0),
          child: Column(
            children: [
              Stack(
                children: [
                  Image.asset(
                    isActive ? activeIcon : icon,
                    width: 20.0,
                    fit: BoxFit.fitWidth,
                    gaplessPlayback: true,
                  ),
                  !showUnread
                      ? const SizedBox()
                      : Positioned(
                          right: 0,
                          child: Container(
                            width: 12.0,
                            height: 12.0,
                            decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(10.0)),
                          ))
                ],
              ),
              const SizedBox(height: 4.0),
              tabTxt(
                tabName,
                color: (isActive) ? HColors.mainColor : HColors.secondaryText,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
