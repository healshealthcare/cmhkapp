import 'package:Heals/controllers/statement_controller.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:get/get.dart';

class StatementPage extends StatelessWidget {
  final StatementController controller = Get.put(StatementController());

  StatementPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Obx(() => Header(title: controller.xment.value)),
          BodyLayout(
              child: Obx(
            () => controller.isErr.isFalse && controller.linkUrl.value.isNotEmpty
                ? WebView(
                    initialUrl: controller.linkUrl.value,
                    javascriptMode: JavascriptMode.unrestricted,
                    onWebViewCreated: (WebViewController webViewController) {},
                    javascriptChannels: const <JavascriptChannel>{},
                    onWebResourceError: (err) {
                      controller.isErr.toggle();
                    },
                    navigationDelegate: (NavigationRequest request) {
                      return NavigationDecision.navigate;
                    },
                    onPageStarted: (String url) {
                      if (EasyLoading.isShow) EasyLoading.dismiss();
                    },
                    onPageFinished: (String url) {},
                    gestureNavigationEnabled: true,
                  )
                : Container(),
          ))
        ],
      ),
    );
  }
}
