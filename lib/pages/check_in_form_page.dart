import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/check_in_form_controller.dart';
import 'package:Heals/model/form_model.dart';
import 'package:Heals/model/option_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class CheckInFormPage extends GetView<CheckInFormController> {
  @override
  final CheckInFormController controller = Get.put(CheckInFormController());
  CheckInFormPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Get.width,
        height: Get.height,
        decoration: const BoxDecoration(color: HColors.primary),
        child: Stack(
          children: [
            BodyLayout(
              child: Column(
                children: [
                  Obx(() => formTop()),
                  Expanded(
                      child: SingleChildScrollView(
                          child: Column(
                    children: [
                      Obx(() => controller.formList.isEmpty
                          ? const SizedBox()
                          : Container(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Obx(
                                () => Column(
                                    children: List.generate(
                                        controller.formList[controller.step.value].length, (index) => formItem(index))),
                              ),
                            )),
                      Row(
                        children: [
                          Expanded(
                            child: Obx(
                              () => longBtn(
                                  title: controller.step.value > 0 ? "check_in_back".tr : "check_in_cancel".tr,
                                  isCircleBtn: true,
                                  onTap: controller.step.value > 0 ? () => controller.handleBack() : () => Get.back()),
                            ),
                          ),
                          Expanded(
                            child: Obx(() => longBtn(
                                  title: controller.step.value == controller.formList.length - 1
                                      ? "check_in_submit".tr
                                      : "check_in_continue".tr,
                                  isCircleBtn: true,
                                  active: true,
                                  onTap: controller.step.value == controller.formList.length - 1
                                      ? () => controller.handleSubmit()
                                      : () => controller.handleContinue(),
                                )),
                          ),
                        ],
                      )
                    ],
                  )))
                ],
              ),
            ),
            Header(
              title: "page_check_form".tr,
            ),
          ],
        ),
      ),
    );
  }

  Widget formItem(index) {
    FormModel model = controller.formList[controller.step.value][index];
    var theAnswer = controller.result[model.itemId];
    List<SelectItemModel> listSelect = [];
    if (model.itemType == Constants.formSelect) {
      for (var i = 0; i < model.itemOptions!.length; i++) {
        OptionModel item = model.itemOptions![i];
        SelectItemModel selectItemModel = SelectItemModel(id: i, title: item.name);
        listSelect.add(selectItemModel);
      }
    }
    // if (model.itemType == Constants.formAddr) {
    //   return Container();
    // }

    if (model.itemType == Constants.formRadio || model.itemType == Constants.formCheckbox) {
      return questionGroup(index,
          questionModel: model,
          isRequired: model.isRequired == 1,
          answer: (theAnswer is String) ? [theAnswer] : theAnswer,
          onTap: (result) => controller.handleAnswer(index, result));
    }

    return inputGroup(
      model.itemTitle,
      key: Key(model.itemId!),
      isRequired: model.isRequired == 1,
      readOnly: model.itemType != Constants.formText &&
          model.itemType != Constants.formPhone &&
          model.itemType != Constants.formAddr,
      actionIcon: model.itemType == Constants.formDate
          ? AssetsInfo.calenderIcon
          : model.itemType == Constants.formSelect
              ? AssetsInfo.arrowDownIcon
              : null,
      leading: model.itemType == Constants.formPhone
          ? areaNumWidget(
              controller.phoneResult[model.itemId] ?? '852',
              onTap: () => controller.showSelect(index),
            )
          : null,
      onTap: () {
        if (model.itemType == Constants.formPhone ||
            model.itemType == Constants.formText ||
            model.itemType == Constants.formAddr) return;
        if (model.itemType == Constants.formDate) {
          controller.showCancelder(index);
          return;
        }
        controller.handleTapInput(index, listSelect);
      },
      hintText: theAnswer ?? model.defaultValue,
      onChanged: (text) => controller.handleInput(index, text),
    );
  }

  Widget line(isShow) {
    return Expanded(
        child: !isShow
            ? const SizedBox()
            : Container(
                height: 1.0, decoration: const BoxDecoration(color: Colors.white), alignment: Alignment.center));
  }

  Widget formTop() {
    List<Widget> stepWidget = List.generate(
        controller.formList.length,
        (index) => Expanded(
              child: Row(
                children: [
                  line(index != 0),
                  assetImage16(controller.step.value == index ? AssetsInfo.stepHl : AssetsInfo.stepNl),
                  line(index != controller.formList.length - 1),
                ],
              ),
            ));
    return Stack(
      children: [
        Image.asset(
          AssetsInfo.formTop,
          height: 114.0,
          width: Get.width,
          fit: BoxFit.cover,
        ),
        Container(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
          child: Column(
            children: [
              titleNormalTxtRes("Please fill out Patient registration form, it will take 3-5 minutes."),
              Container(
                padding: const EdgeInsets.only(top: 20.0),
                alignment: Alignment.center,
                child: Row(
                  children: stepWidget,
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
