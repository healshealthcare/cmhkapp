import 'package:Heals/config/assets.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/reset_password_controller.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/normal_input_widget.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResetPasswordPage extends GetView<ResetPasswordController> {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: Stack(
        children: [
          Column(
            children: [
              Stack(
                children: [
                  Image.asset(
                    AssetsInfo.topResetBg,
                    height: 300.0,
                    width: Get.width,
                    fit: BoxFit.cover,
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 167.0, left: 32.0, right: 32.0),
                    alignment: Alignment.center,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        titleLargerTxtActive("reset_pass_title".tr),
                        Padding(
                          padding: const EdgeInsets.only(top: 24.0),
                          child: titleNormalTxt("reset_pass_content".tr, maxLines: 10),
                        )
                      ],
                    ),
                  )
                ],
              ),
              Flexible(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 20.0),
                        padding: const EdgeInsets.only(top: 16.0),
                        constraints: const BoxConstraints(minHeight: 236.0),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 4, offset: Offset(0, 0))
                          ],
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(8.0),
                            bottomRight: Radius.circular(8.0),
                          ),
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: NormalInputWidget(
                                teCtrl: controller.resetPasswordOldteCtrl,
                                focusNode: controller.focusNodePassOld,
                                hintText: "pass_old_hint_text".tr,
                                obscureText: true,
                                inputRule: Config.passRule,
                                leadIcon: AssetsInfo.lockIcon,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: NormalInputWidget(
                                teCtrl: controller.resetPasswordteCtrl,
                                focusNode: controller.focusNodePass,
                                hintText: "pass_new_hint_text".tr,
                                obscureText: true,
                                inputRule: Config.passRule,
                                leadIcon: AssetsInfo.lockIcon,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: NormalInputWidget(
                                teCtrl: controller.resetRepPasswordteCtrl,
                                focusNode: controller.focusNodePassRep,
                                hintText: "pass_repeat_hint_text".tr,
                                obscureText: true,
                                inputRule: Config.passRule,
                                leadIcon: AssetsInfo.lockIcon,
                              ),
                            ),
                            longBtn(
                                title: "reset_pass_btn".tr,
                                isCircleBtn: true,
                                active: true,
                                onTap: () => controller.resetPassword())
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Header(
            title: "page_reset_password".tr,
          ),
        ],
      ),
    );
  }
}
