import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/appointment_detail_controller.dart';

class AppointmentDetailPage extends GetView<AppointmentDetailController> {
  const AppointmentDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: SingleChildScrollView(
              child: Obx(
                () => controller.noteKeyModel().referralNotes == null || controller.appointmentModel().id == null
                    ? const SizedBox()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          isPenddding(controller.appointmentModel()) || isFailed(controller.appointmentModel())
                              ? Container(
                                  alignment: Alignment.center,
                                  padding: const EdgeInsets.only(top: 12.0, left: 16.0, right: 16.0),
                                  child: titleNormal80Txt(
                                    getotherStatus(controller.appointmentModel()) ??
                                        (isHold(controller.appointmentModel().state!)
                                            ? "appointment_hold_descripe".tr
                                            : "appointment_unhold_descripe".tr),
                                  ),
                                )
                              : const SizedBox(),
                          isUpcoming(controller.appointmentModel()) || isFinish(controller.appointmentModel().state!)
                              ? Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: assetImage(AssetsInfo.successAppointment, width: 204.0),
                                    ),
                                    Container(
                                      alignment: Alignment.center,
                                      padding: const EdgeInsets.only(top: 12.0),
                                      child: titleTxt("appointment_confirm_title".tr),
                                    ),
                                    GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      onTap: () => Get.toNamed(Routes.statement, arguments: 'help'),
                                      child: Container(
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.only(top: 8.0),
                                        child:
                                            subTitleTxt("appointment_confirm_title_tip".tr, color: HColors.mainColor),
                                      ),
                                    ),
                                  ],
                                )
                              : const SizedBox(),
                          Obx(() => appointmentCard(controller.appointmentModel(), showSmallCheckIn: false)),
                          // isUpcoming(controller.appointmentModel()) || isFinish(controller.appointmentModel().state!)
                          //     ? Column(
                          //         children: [
                          //           // Padding(
                          //           //   padding: const EdgeInsets.only(top: 20.0, left: 16.0),
                          //           //   child: subTitleTxtActive(controller.noteKeyModel().referralNotes![0].content),
                          //           // ),
                          //           // Padding(
                          //           //   padding: const EdgeInsets.only(top: 12.0, left: 16.0, right: 16.0),
                          //           //   child: subTitleTxt(controller.noteKeyModel().referralNotes![1].content),
                          //           // ),
                          //           // uploadArea(),
                          //           // Padding(
                          //           //   padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 30.0),
                          //           //   child: titleNormal80Txt(controller.noteKeyModel().otherNotes![0].content),
                          //           // ),
                          //           // infoAreaOne(controller.noteKeyModel()), // hard code
                          //           infoAreaTwo(controller.noteKeyModel()), // hard code
                          //           const SizedBox(height: 32.0),
                          //         ],
                          //       )
                          //     : const SizedBox(),
                          isFailed(controller.appointmentModel())
                              ? const SizedBox()
                              : Column(
                                  children: [
                                    !isHold(controller.appointmentModel().state!)
                                        ? const SizedBox()
                                        : longBtn(
                                            title: "appointment_accept_title".tr,
                                            active: true,
                                            isCircleBtn: true,
                                            cleanVerPadd: 8.0,
                                            onTap: () => controller.replyAppoDialog(1),
                                          ),
                                    isUpcoming(controller.appointmentModel()) ||
                                            isHold(controller.appointmentModel().state!)
                                        ? longBtn(
                                            title: "appointment_reschedule_title".tr,
                                            isCircleBtn: true,
                                            cleanVerPadd: 8.0,
                                            onTap: () {
                                              controller.cancelAppointment();
                                              Get.toNamed(
                                                Routes.appointment,
                                                arguments: {
                                                  "doctorId": controller.appointmentModel().doctorId,
                                                  "clinicId": controller.appointmentModel().clinic!.id
                                                },
                                              );
                                            })
                                        : const SizedBox(),
                                    isPenddding(controller.appointmentModel()) ||
                                            isUpcoming(controller.appointmentModel())
                                        ? longBtn(
                                            title: "appointment_cancel_title".tr,
                                            isCircleBtn: true,
                                            cleanVerPadd: 8.0,
                                            onTap: () => controller.cancelAppointment(),
                                          )
                                        : const SizedBox()
                                  ],
                                ),
                        ],
                      ),
              ),
            ),
          ),
          Header(
            title: "page_appointment_detail".tr,
          ),
        ],
      ),
    );
  }

  // Widget uploadArea() {
  //   return GestureDetector(
  //     behavior: HitTestBehavior.translucent,
  //     onTap: () => controller.chooseAlbum(),
  //     child: borderLayout(
  //       child: Padding(
  //         padding: const EdgeInsets.symmetric(vertical: 48.0),
  //         child: assetImage44(AssetsInfo.uploadIcon),
  //       ),
  //     ),
  //   );
  // }
}
