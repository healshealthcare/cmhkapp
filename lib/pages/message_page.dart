import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/model/message_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/message_controller.dart';

class MeassagePage extends GetView<MeassageController> {
  @override
  final MeassageController controller = Get.put(MeassageController());
  MeassagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: Colors.white),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              children: [
                Container(
                  decoration:
                      const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
                  margin: const EdgeInsets.only(left: 16.0, right: 16.0, top: 12.0),
                  child: TabBar(
                    indicatorColor: HColors.mainColor,
                    controller: controller.tabController,
                    indicatorWeight: 2,
                    indicatorPadding: const EdgeInsets.symmetric(horizontal: 32.0),
                    indicatorSize: TabBarIndicatorSize.tab,
                    labelStyle: const TextStyle(
                      fontSize: HFontSizes.medium,
                      color: HColors.mainColorText,
                      fontFamily: 'Comfortaa',
                    ),
                    labelColor: HColors.mainColorText,
                    unselectedLabelColor: HColors.mainText,
                    tabs: [
                      Tab(
                        text: "tab_todo".tr,
                        icon: assetImage(
                          AssetsInfo.tabCheckIcon,
                          width: 42.0,
                        ),
                      ),
                      Tab(
                        text: "tab_notice".tr,
                        icon: assetImage(
                          AssetsInfo.tabMsgIcon,
                          width: 42.0,
                        ),
                      ),
                    ],
                  ),
                ),
                Flexible(
                  child: Obx(
                    () => TabBarView(
                      controller: controller.tabController,
                      children: [
                        RefreshIndicator(
                          onRefresh: () => controller.getData('TODO'),
                          color: HColors.mainColor,
                          child: messageList(controller.todoList, controller.scrollControllerLeft, type: 'todo'),
                        ),
                        RefreshIndicator(
                          onRefresh: () => controller.getData('NOTICE'),
                          color: HColors.mainColor,
                          child: messageList(controller.noticeList, controller.scrollControllerRight, type: 'notice'),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Header(
            leading: const SizedBox(),
            title: "page_message_title".tr,
          ),
        ],
      ),
    );
  }

  Widget messageList(list, controller, {type}) {
    return list.isEmpty
        ? emptyPage(
            emptyIcon: type == 'todo' ? AssetsInfo.emptyTodo : AssetsInfo.emptyMsg,
            iconWidth: 190.0,
            title: type == 'todo' ? "message_todo_empty".tr : "message_empty".tr,
          )
        : ListView.builder(
            padding: const EdgeInsets.all(0),
            controller: controller,
            itemBuilder: (BuildContext context, index) {
              MessageModel model = list[index];
              return messageItem(model);
            },
            itemCount: list.length,
          );
  }

  Widget messageItem(MessageModel model) {
    return GestureDetector(
      onTap: () {
        controller.markReadd(model.id);
        if (model.params != null) {
          if (model.businessType == 'Appointment') {
            Get.toNamed(Routes.appointmentDetail, arguments: model.params!["appointment_id"]);
          } else if (model.businessType == 'VisitFinish') {
            Get.toNamed(Routes.paymentDetail, arguments: {
              "invoice_id": model.params!["invoice_id"],
              "transaction_id": model.params!["transaction_id"]
            });
          } else if (model.businessType == 'TicketUpdate') {
            Get.toNamed(Routes.queueDetail, arguments: {
              "doctorId": model.params!["doctor_id"],
              "user_id": model.userId,
              "clinicId": model.params!["clinic_id"]
            });
          }
        }
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        margin: const EdgeInsets.only(left: 16.0),
        padding: const EdgeInsets.only(top: 18.0, bottom: 20.0, right: 16.0),
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(color: HColors.border, width: 1.0),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Stack(
                  children: [
                    assetImage18(AssetsInfo.checkIcon),
                    unreadWidget(model.readAt == null || model.readAt!.isEmpty),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: titleNormalTxt(
                      model.businessType == 'Appointment'
                          ? "message_title_appointment".tr
                          : model.businessType == 'VisitFinish'
                              ? "message_title_payment".tr
                              : "message_title_checkin".tr,
                    ),
                  ),
                ),
                titleNormalTxt(model.updateAt),
                assetImage8(AssetsInfo.arrowRight),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 32.0),
              child: smallestTitle(model.content!, maxLines: 10, color: HColors.mainText.withOpacity(.7)),
            )
          ],
        ),
      ),
    );
  }
}
