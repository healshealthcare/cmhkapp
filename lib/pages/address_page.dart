import 'package:Heals/constants/style.dart';
import 'package:Heals/model/address_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/address_controller.dart';

class AddressPage extends GetView<AddressController> {
  @override
  final AddressController controller = Get.put(AddressController());
  AddressPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Obx(
                      () => Container(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Column(
                          children: List.generate(controller.addressList.length, (index) {
                            AddressModel model = controller.addressList[index];
                            return GestureDetector(
                              onTap: () => controller.handleAddress(index),
                              child: Container(
                                padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                                child: addressWidget(model.recipientName, phoneFormat(model.recipientPhone),
                                    "${model.streetAddr1} ${model.district} ${model.region} ${model.area} ${model.city}",
                                    onTapEdit: () => Get.toNamed(Routes.editAddress, arguments: model),
                                    onDelete: () => controller.deleteAddress(index)),
                              ),
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      shortBtn(
                        title: "address_add_btn".tr,
                        onTap: () => Get.toNamed(Routes.editAddress),
                        active: true,
                        width: 160.0,
                      ),
                      shortBtn(
                        title: "back_page".tr,
                        onTap: () => Get.back(),
                        active: true,
                        width: 160.0,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Header(
            title: "page_address_title".tr,
          ),
        ],
      ),
    );
  }
}
