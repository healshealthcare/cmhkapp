import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/search_controller.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchPage extends GetView<SearchController> {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: HColors.primary,
      child: Stack(
        children: [
          Obx(() => BodyLayout(
                child: Container(
                  padding: const EdgeInsets.only(top: 24.0),
                  child: RefreshIndicator(
                    onRefresh: () => controller.refreshData(),
                    color: HColors.mainColor,
                    child: Column(
                      children: [
                        Expanded(
                          child: controller.doctorList.isEmpty
                              ? emptyWidget()
                              : ListView.builder(
                                  padding: const EdgeInsets.all(0),
                                  controller: controller.scrollController,
                                  physics: const AlwaysScrollableScrollPhysics(),
                                  itemBuilder: (BuildContext context, int index) {
                                    DoctorModel item = controller.doctorList[index];
                                    return shadowCard(
                                        margin: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                                        child: doctorCard(
                                            doctorItem: item,
                                            islike: item.doctor!.isFavoriteDoctor == 1,
                                            onPressed: () => item.remoteEnabled == 1
                                                ? Get.toNamed(Routes.appointment,
                                                    arguments: {"doctorId": item.doctorId, "clinicId": item.clinicId})
                                                : Get.toNamed(Routes.doctorDetail, arguments: {"doctor": item})));
                                  },
                                  itemCount: controller.doctorList.length,
                                ),
                        ),
                        loadingMore(controller.isLoadingMore.isTrue, controller.isLoadAll.isTrue)
                      ],
                    ),
                  ),
                ),
              )),
          Obx(() => controller.displaySelect > 0
              ? selectList(controller.displaySelect.value == 1
                  ? controller.specityList
                  : controller.displaySelect.value == 2
                      ? controller.serviceList
                      : controller.displaySelect.value == 3
                          ? controller.districtList
                          : controller.filterList)
              : const SizedBox()),
          header()
        ],
      ),
    );
  }

  Widget header() {
    return Column(
      children: [
        Header(
          titleWidget: searchInput(),
          isCenterTitle: false,
          action: GestureDetector(
            onTap: () => controller.cleanSearch(),
            child: Container(
              padding: const EdgeInsets.only(right: 16.0, left: 16.0),
              alignment: Alignment.centerRight,
              child: subTitleTxtRes("search_cancel".tr),
            ),
          ),
        ),
        Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))],
          ),
          child: Obx(() => Row(
                children: [
                  selectItem(
                      controller.specialtyIndex.value > 0
                          ? controller.specityList[controller.specialtyIndex.value].name
                          : "search_spec".tr,
                      controller.specialtyValue.value > 0,
                      controller.displaySelect.value == 1, onPress: () {
                    if (controller.displaySelect.value == 1) {
                      controller.displaySelect(0);
                      return;
                    }
                    controller.displaySelect(1);
                  }),
                  selectItem(
                      controller.serviceIndex.value > 0
                          ? controller.serviceList[controller.serviceIndex.value].name
                          : "search_ser".tr,
                      controller.sevicesValue.value > 0,
                      controller.displaySelect.value == 2, onPress: () {
                    if (controller.displaySelect.value == 2) {
                      controller.displaySelect(0);
                      return;
                    }
                    controller.displaySelect(2);
                  }),
                  selectItem(
                      controller.districtIndex.value > 0
                          ? controller.districtList[controller.districtIndex.value].name
                          : "search_dist".tr,
                      controller.districtValue.value > 0,
                      controller.displaySelect.value == 3, onPress: () {
                    if (controller.displaySelect.value == 3) {
                      controller.displaySelect(0);
                      return;
                    }
                    controller.displaySelect(3);
                  }),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () {
                      if (controller.displaySelect.value == 4) {
                        controller.displaySelect(0);
                        return;
                      }
                      controller.displaySelect(4);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Image.asset(
                        AssetsInfo.shiftIcon,
                        width: 22.0,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  )
                ],
              )),
        ),
      ],
    );
  }

  Widget selectList(list, {type}) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => controller.displaySelect(0),
      child: Container(
        width: Get.width,
        height: Get.height,
        color: Colors.black.withOpacity(.3),
        padding: const EdgeInsets.only(top: 88.0),
        child: Stack(
          children: [
            Container(
              constraints: BoxConstraints(maxHeight: Get.height - 150.0, minHeight: 50.0),
              padding: const EdgeInsets.only(bottom: 16.0),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(6.0),
                  bottomRight: Radius.circular(6.0),
                ),
              ),
              child: list.length == 0
                  ? emptyWidget()
                  : Obx(
                      () => ListView.builder(
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, index) {
                            var item = list[index];
                            bool isActive = false;
                            switch (controller.displaySelect.value) {
                              case 1:
                                if (controller.specialtyIndex.value == index) isActive = true;
                                break;
                              case 2:
                                if (controller.serviceIndex.value == index) isActive = true;
                                break;
                              case 3:
                                if (controller.districtIndex.value == index) isActive = true;
                                break;
                              case 4:
                                if (controller.shiftIndex.value == index + 1) isActive = true;
                                break;
                              default:
                            }
                            return item.name.isEmpty
                                ? const SizedBox()
                                : GestureDetector(
                                    onTap: () => controller.changeSelectValue(index),
                                    behavior: HitTestBehavior.translucent,
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                                      alignment: Alignment.center,
                                      child: isActive ? titleNormalMainTxt(item.name) : titleNormalTxt(item.name),
                                    ),
                                  );
                          },
                          itemCount: list.length),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  Widget doctorCard({required DoctorModel doctorItem, islike = false, onPressed}) {
    return GestureDetector(
      onTap: onPressed,
      behavior: HitTestBehavior.translucent,
      child: Container(
        padding: const EdgeInsets.fromLTRB(14.0, 14.0, 0, 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  AssetsInfo.medicineBoxIcon,
                  width: 22.0,
                  fit: BoxFit.fitWidth,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: titleActiveTxt(doctorItem.doctor!.name),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 14.0),
                  child: Image.asset(
                    !islike ? AssetsInfo.likeIcon : AssetsInfo.likeActiveIcon,
                    width: 22.0,
                    fit: BoxFit.fitWidth,
                    gaplessPlayback: true,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 12.0),
              child: thirdTitle(doctorItem.doctor!.specialty!.name),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 16.0),
              child: Row(
                children: [
                  Expanded(child: titleNormalTxt(doctorItem.clinic!.name, maxLines: 10)),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () => selectMap(doctorItem.clinic),
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: Row(
                        children: [
                          Image.asset(
                            AssetsInfo.navigatorIcon,
                            width: 20.0,
                            fit: BoxFit.fitWidth,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: controller.appSrv.curPosi == null
                                ? const SizedBox()
                                : smallestTitle(
                                    getDistance(
                                      doctorItem.clinic!.latitude!,
                                      doctorItem.clinic!.longitude!,
                                      controller.appSrv.curPosi!.latitude,
                                      controller.appSrv.curPosi!.longitude,
                                    ),
                                  ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 1.0,
              alignment: Alignment.center,
              color: HColors.border,
              margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Row(
                children: [
                  Expanded(
                      child: Row(
                    children: [
                      doctorItem.remoteEnabled == 0
                          ? const SizedBox()
                          : tagItem(title: "tag_ticket".tr, type: 'ticket'),
                      doctorItem.appointmentEnabled == 0
                          ? const SizedBox()
                          : tagItem(title: "tag_appointment".tr, type: 'appo'),
                      doctorItem.videoVisitEnabled == 0
                          ? const SizedBox()
                          : tagItem(title: "tag_tele".tr, type: 'tele'),
                    ],
                  )),
                  (doctorItem.remoteEnabled == 1 && doctorItem.clinic!.connected == 0) || doctorItem.remoteIsFull == 1
                      ? Container(
                          height: 20.0,
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          decoration: BoxDecoration(
                            color: HColors.btnClose,
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(
                              color: HColors.borderClose,
                            ),
                          ),
                          child: subTitleGreyTxt("search_clin_close".tr, color: HColors.txtClose),
                        )
                      : const SizedBox(),
                  (doctorItem.remoteEnabled == 0 || doctorItem.clinic!.connected == 0) || doctorItem.remoteIsFull == 1
                      ? const SizedBox()
                      : Row(
                          children: [
                            smallestTitle("search_cur_queue".tr),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: titleHugestTxtActive(
                                doctorItem.position.toString(),
                                color: HColors.heiLightText,
                              ),
                            )
                          ],
                        )
                  // tagItem(title: 'Est.', preIcon: AssetsInfo.clockIcon, subTitle: '17:01'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget searchInput() {
    return SizedBox(
      height: 28.0,
      child: TextField(
        controller: controller.teCtrl(),
        focusNode: controller.focusNode,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIconConstraints: const BoxConstraints(maxWidth: 38.0),
          prefixIcon: Container(
              padding: const EdgeInsets.only(right: 8.0),
              alignment: Alignment.centerRight,
              child: Image.asset(
                AssetsInfo.search,
                width: 14.0,
                fit: BoxFit.fitHeight,
              )),
          suffixIcon: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => controller.teCtrl.value.clear(),
            child: Container(
              padding: const EdgeInsets.only(right: 12.0, left: 12.0),
              child: Obx(
                () => controller.teCtrl().text.isNotEmpty
                    ? Image.asset(
                        AssetsInfo.inputClearBtn,
                        width: 18.0,
                        fit: BoxFit.fitWidth,
                      )
                    : const SizedBox(),
              ),
            ),
          ),
          suffixIconConstraints: const BoxConstraints(minHeight: 64.0),
          hintText: "search_doctor_tip".tr,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(18.0), borderSide: BorderSide.none),
          contentPadding: const EdgeInsets.only(left: 12.0, right: 12.0),
          focusColor: HColors.mainColorText,
          hoverColor: HColors.mainColorText,
          hintStyle: const TextStyle(fontSize: HFontSizes.smallest, color: HColors.placeholderColor),
        ),
        cursorColor: HColors.mainColorText,
        textInputAction: TextInputAction.search,
        onSubmitted: (text) => controller.handleSearch(),
        onEditingComplete: () {},
        style: const TextStyle(fontSize: HFontSizes.small, color: HColors.mainColorText),
      ),
    );
  }

  Widget selectItem(title, active, tabActive, {onPress}) {
    return Expanded(
      child: GestureDetector(
        onTap: onPress,
        behavior: HitTestBehavior.translucent,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(child: active ? thirdTitleActive(title, maxLines: 1) : thirdTitle(title, maxLines: 1)),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Image.asset(
                  tabActive ? AssetsInfo.triTopIcon : AssetsInfo.triBottomIcon,
                  width: 8.0,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
