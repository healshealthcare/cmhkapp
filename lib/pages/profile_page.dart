import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/profile_controller.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/avatar_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfilePage extends GetView<ProfileController> {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        controller.backJudge();
        return false;
      },
      child: Container(
        width: Get.width,
        height: Get.height,
        decoration: const BoxDecoration(color: HColors.lightPrimary),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: BodyLayout(
                child: Container(
                  color: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Obx(
                    () => Column(
                      children: [
                        listBar(
                          title: "avatar_title".tr,
                          smallSize: true,
                          padding: const EdgeInsets.symmetric(vertical: 12.0),
                          suffixWidget: AvatarGroup(
                            avatar: controller.userInfoModel().avatar ?? '',
                            width: 42.0,
                          ),
                          onPress: () => controller.chooseAlbum(),
                        ),
                        listBar(
                          title: "eng_name_title".tr,
                          smallSize: true,
                          suffixTxt: controller.userInfoModel().engName,
                          onPress: () => controller.showNicknameEdit("eng_name"),
                        ),
                        listBar(
                          title: "chi_name_title".tr,
                          smallSize: true,
                          suffixTxt: controller.userInfoModel().chiName,
                          onPress: () => controller.showNicknameEdit("chi_name"),
                        ),
                        listBar(
                          title: "nick_title".tr,
                          smallSize: true,
                          suffixTxt: controller.userInfoModel().nickname,
                          onPress: () => controller.showNicknameEdit("nickname"),
                        ),
                        Obx(
                          () => listBar(
                            title: "birth_title".tr,
                            smallSize: true,
                            suffixWidget: SizedBox(
                              width: 100.0,
                              child: controller.userInfoModel().chiName == null && controller.profileEditType.value != 2
                                  ? const SizedBox()
                                  : DateTimePicker(
                                      type: DateTimePickerType.date,
                                      dateMask: 'yyyy-MM-dd',
                                      initialValue: controller.userInfoModel().dob == null ||
                                              controller.userInfoModel().dob!.isEmpty
                                          ? DateTime.now().toString()
                                          : controller.userInfoModel().dob,
                                      firstDate: DateTime(1900),
                                      lastDate: DateTime.now(),
                                      locale: Get.locale,
                                      onChanged: (val) => controller.userEdit("dob", value: val),
                                    ),
                            ),
                          ),
                        ),
                        listBar(
                          title: "gender_title".tr,
                          smallSize: true,
                          suffixTxt: controller.userInfoModel().gender == "M"
                              ? "gender_male".tr
                              : controller.userInfoModel().gender == "F"
                                  ? "gender_female".tr
                                  : "gender_other".tr,
                          onPress: () => controller.changeGender(),
                        ),
                        listBar(
                          title: "id_title".tr,
                          smallSize: true,
                          suffixTxt: controller.userInfoModel().id,
                          onPress: () => controller.showNicknameEdit("id_num"),
                        ),
                        listBar(
                          title: "mobile_title".tr,
                          smallSize: true,
                          suffixTxt: phoneFormat(controller.userInfoModel().telephone),
                          onPress: () => controller.showNicknameEdit("telephone"),
                        ),
                        listBar(
                          title: "email_title".tr,
                          smallSize: true,
                          suffixTxt: controller.userInfoModel().email,
                          onPress: () => controller.showNicknameEdit("email"),
                        ),
                        listBar(
                          title: "emer_con_title".tr,
                          smallSize: true,
                          suffixTxt: controller.userInfoModel().emergName,
                          onPress: () => controller.showNicknameEdit("emerg_name"),
                        ),
                        // listBar(
                        //   title: "rel_title".tr,
                        //   smallSize: true,
                        //   suffixTxt: controller.relativeList.isEmpty ? '' : controller.relativeModel().title ?? '',
                        //   onPress: () => controller.changeRelation(),
                        // ),
                        listBar(
                          title: "con_phone_title".tr,
                          smallSize: true,
                          suffixTxt: phoneFormat(controller.userInfoModel().emergPhone),
                          isShowBorderBottom: false,
                          onPress: () => controller.showNicknameEdit("emerg_phone"),
                        ),
                        Obx(
                          () => controller.profileEditType.value == 2
                              ? longBtn(
                                  title: "save_btn".tr,
                                  onTap: () =>
                                      controller.saveProfile(controller.userInfoModel().toJson((value) => null)),
                                  active: true,
                                  isCircleBtn: true)
                              : longBtn(
                                  title: "back_page".tr,
                                  onTap: () {
                                    controller.backJudge();
                                  },
                                  active: true,
                                  isCircleBtn: true),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Obx(
              () => Header(
                title: controller.profileEditType.value == 1
                    ? "page_family_add_title".trArgs([controller.userInfoModel().nickname ?? ""])
                    : controller.profileEditType.value == 2
                        ? "page_family_update_title".tr
                        : "page_profile_title".tr,
                onTap: () => controller.backJudge(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
