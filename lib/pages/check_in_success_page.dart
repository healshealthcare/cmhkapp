import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/check_in_success_controller.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class CheckInSuccessPage extends GetView<CheckInSuccessController> {
  const CheckInSuccessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Container(
              padding: const EdgeInsets.only(top: 60.0, left: 40.0, right: 40.0),
              child: Column(
                children: [
                  Image.asset(
                    AssetsInfo.appointmentSuccess,
                    width: 120.0,
                    fit: BoxFit.fitWidth,
                  ),
                  const SizedBox(height: 16.0),
                  titleLargeTxt("check_in_success".tr),
                  const SizedBox(height: 60.0),
                  titleNormalTxt("check_in_descripe".tr, highLine: 22.0),
                ],
              ),
            ),
          ),
          Header(
            title: "page_sent_form".tr,
          ),
        ],
      ),
    );
  }
}
