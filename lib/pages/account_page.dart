import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/account_controller.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/avatar_widget.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class AccountPage extends GetView<AccountController> {
  @override
  final AccountController controller = Get.put(AccountController());
  AccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset(
                  AssetsInfo.homeTopBg,
                  height: 255.0,
                  width: Get.width,
                  fit: BoxFit.cover,
                ),
                Container(
                  height: 255.0,
                  padding: const EdgeInsets.only(top: 44.0),
                  alignment: Alignment.center,
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => Get.toNamed(Routes.setting),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                            child: Image.asset(
                              AssetsInfo.settingIcon,
                              width: 18.0,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                        )
                      ],
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => Get.toNamed(Routes.profile),
                      child: Obx(
                        () => Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: AvatarGroup(
                                avatar: controller.appSrv.userInfoModel().avatar ?? '',
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: titleTxtRes(controller.appSrv.userInfoModel().nickname ?? ''),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 12.0),
                              child: controller.appSrv.userInfoModel().phone == null
                                  ? const SizedBox()
                                  : subTitleTxtRes(phoneFormat(controller.appSrv.userInfoModel().phone)),
                            ),
                          ],
                        ),
                      ),
                    )
                  ]),
                ),
              ],
            ),
            shadowCard(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  listBar(
                    icon: AssetsInfo.myFamilyIcon,
                    title: 'page_family_title'.tr,
                    onPress: () => Get.toNamed(Routes.family),
                  ),
                  listBar(
                    icon: AssetsInfo.healthPassportIcon,
                    title: "health_history_title".tr,
                    onPress: () => Get.toNamed(Routes.medicineBox, arguments: false),
                  ),
                  listBar(
                    icon: AssetsInfo.medicineBoxIcon,
                    title: "medicine_box_title".tr,
                    onPress: () => Get.toNamed(Routes.medicineBox, arguments: true),
                  ),
                  listBar(
                    icon: AssetsInfo.favoriteDoctorIcon,
                    title: "favorite_doctor_title".tr,
                    onPress: () => Get.toNamed(
                      Routes.favoriteDoctor,
                    ),
                  ),
                  listBar(
                    icon: AssetsInfo.myWalletIcon,
                    title: "my_wallet_title".tr,
                    onPress: () => Get.toNamed(
                      Routes.paymentList,
                    ),
                  ),
                  listBar(
                    icon: AssetsInfo.manageAddressIcon,
                    title: "manage_address_title".tr,
                    onPress: () => Get.toNamed(
                      Routes.address,
                    ),
                  ),
                  listBar(
                    icon: AssetsInfo.couponIcon,
                    title: "manage_coupon_title".tr,
                    onPress: () => Get.toNamed(Routes.couponList),
                  ),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

  Widget iconFirWidthCont({required String icon, required String content}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            icon,
            width: 22.0,
            fit: BoxFit.fitWidth,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: subTitleTxtWidthIcon(content, maxLine: 1, fontSize: HFontSizes.large, color: HColors.subColorText),
            ),
          ),
          Stack(
            children: [
              Container(
                height: 20.0,
                margin: const EdgeInsets.only(top: 1.0),
                alignment: Alignment.center,
                padding: const EdgeInsets.only(left: 24.0, right: 12.0),
                decoration: BoxDecoration(
                  color: HColors.fifthColor,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: timeTxt("check_in_btn".tr),
              ),
              Image.asset(
                AssetsInfo.editIcon,
                fit: BoxFit.fitWidth,
                width: 22.0,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget iconWidthCont({required String icon, required String content, int? maxLine = 1, double? paddingR}) {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, top: 9.0, bottom: 9.0, right: paddingR ?? 16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            icon,
            width: 22.0,
            fit: BoxFit.fitWidth,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: subTitleTxtWidthIcon(content, maxLine: maxLine),
            ),
          )
        ],
      ),
    );
  }
}
