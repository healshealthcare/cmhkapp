import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/edit_address_controller.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class EditAddressPae extends GetView<EditAddressController> {
  @override
  final EditAddressController controller = Get.put(EditAddressController());
  EditAddressPae({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: Stack(
        children: [
          BodyLayout(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Column(
                  children: [
                    inputGroup(
                      "address_receipient_title".tr,
                      teCtrl: controller.receipientCtrl,
                      isRequired: true,
                      readOnly: false,
                      onTap: () {
                        // controller.handleTapInput(index, listSelect);
                      },
                    ),
                    inputGroup(
                      "address_receipient_phone_title".tr,
                      teCtrl: controller.phoneCtrl,
                      isRequired: true,
                      readOnly: false,
                      leading: Obx(
                        () => areaNumWidget(
                          controller.phoneItem().title,
                          onTap: () => controller.showSelect(),
                        ),
                      ),
                      onTap: () {
                        // controller.handleTapInput(index, listSelect);
                      },
                    ),
                    inputGroup(
                      "address_street_1_title".tr,
                      teCtrl: controller.street1Ctrl,
                      isRequired: true,
                      readOnly: false,
                      onTap: () {
                        // controller.handleTapInput(index, listSelect);
                      },
                    ),
                    inputGroup(
                      "address_street_2_title".tr,
                      teCtrl: controller.street2Ctrl,
                      isRequired: false,
                      readOnly: false,
                      onTap: () {
                        // controller.handleTapInput(index, listSelect);
                      },
                    ),
                    inputGroup(
                      "address_area_title".tr,
                      teCtrl: controller.areaCtrl,
                      isRequired: true,
                      readOnly: true,
                      actionIcon: AssetsInfo.arrowDownIcon,
                      onTap: () {
                        controller.tapArea();
                      },
                    ),
                    inputGroup(
                      "address_region_title".tr,
                      teCtrl: controller.regionCtrl,
                      isRequired: true,
                      readOnly: true,
                      actionIcon: AssetsInfo.arrowDownIcon,
                      onTap: () {
                        controller.tapRegion();
                      },
                    ),
                    inputGroup(
                      "address_district_title".tr,
                      teCtrl: controller.districtCtrl,
                      isRequired: true,
                      readOnly: true,
                      actionIcon: AssetsInfo.arrowDownIcon,
                      onTap: () {
                        controller.tapDistrict();
                      },
                    ),
                    longBtn(
                        title: "address_save_btn".tr,
                        isCircleBtn: true,
                        active: true,
                        onTap: () => controller.addAddress())
                  ],
                ),
              ),
            ),
          ),
          Header(
            title: "page_address_title".tr,
          ),
        ],
      ),
    );
  }
}
