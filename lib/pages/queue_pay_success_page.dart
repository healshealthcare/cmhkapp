import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/queue_pay_success_controller.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rive/rive.dart';
import 'package:get/get.dart';

class QueuePaySuccessPage extends GetView<QueuePaySuccessController> {
  const QueuePaySuccessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Container(
              padding: const EdgeInsets.only(top: 60.0, left: 40.0, right: 40.0),
              child: Column(
                children: [
                  Obx(() => controller.animaIsReady.isFalse
                      ? const SizedBox()
                      : controller.isFail.isFalse
                          ? Rive(
                              artboard: controller.artboard!,
                              fit: BoxFit.cover,
                              useArtboardSize: true,
                            )
                          : Image.asset(
                              AssetsInfo.waitFail,
                              width: 160.0,
                              fit: BoxFit.fitWidth,
                            )),
                  const SizedBox(height: 16.0),
                  Obx(() => controller.isFail.isFalse
                      ? const SizedBox()
                      : Padding(
                          padding: const EdgeInsets.only(bottom: 32.0),
                          child: titleLargeTxt("queue_fail_title".tr),
                        )),
                  Obx(() => titleNormalMainTxt(
                      controller.isFail.isTrue ? "queue_fail_content".tr : "queue_success_content".tr,
                      maxLines: 10)),
                  const SizedBox(height: 60.0),
                  // longBtn(
                  //     title: "queue_checkin_btn".tr,
                  //     isCircleBtn: true,
                  //     isCenter: true,
                  //     active: true,
                  //     onTap: () => Get.offAndToNamed(Routes.queueDetail, arguments: {"doctorId": controller.doctorId}))
                ],
              ),
            ),
          ),
          Header(
            title: "page_queue_success_title".tr,
          ),
        ],
      ),
    );
  }
}
