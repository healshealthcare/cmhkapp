import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/setting_controller.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class SettingPage extends GetView<SettingController> {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: Stack(
        children: [
          SingleChildScrollView(
            child: BodyLayout(
              child: Column(
                children: [
                  shadowCard(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      children: [
                        listBar(
                          icon: AssetsInfo.languageIcon,
                          title: "language_setting".tr,
                          onPress: () => controller.changeLanguage(),
                        ),
                        listBar(
                          icon: AssetsInfo.passwordIcon,
                          title: "pass_setting".tr,
                          onPress: () => Get.toNamed(Routes.resetPass),
                        ),
                        Obx(
                          () => listBar(
                              icon: AssetsInfo.clearCacheIcon,
                              title: "clean_setting".tr,
                              suffixTxt: controller.cache.value.isEmpty ? '' : '${controller.cache.value}MB',
                              onPress: () => controller.cleanCacheDia()),
                        ),
                        listBar(
                          icon: AssetsInfo.termServiceIcon,
                          title: "term_setting".tr,
                          onPress: () => Get.toNamed(Routes.statement, arguments: 'statement'),
                        ),
                        listBar(
                          icon: AssetsInfo.privacyPolicyIcon,
                          title: "privacy_setting".tr,
                          onPress: () => Get.toNamed(Routes.statement, arguments: 'privacy'),
                        ),
                        listBar(
                          icon: AssetsInfo.checkUpdateIcon,
                          title: "update_setting".tr,
                          onPress: () => controller.onTapVersion(),
                        ),
                        listBar(
                          icon: AssetsInfo.clearCacheIcon,
                          title: "delete_account_setting".tr,
                          onPress: () => controller.deleteAccount(),
                        ),
                      ],
                    ),
                  )),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child:
                        longBtn(title: "out_setting".tr, titleColor: HColors.warning, onTap: () => controller.logout()),
                  )
                ],
              ),
            ),
          ),
          Header(
            title: "page_setting".tr,
          ),
        ],
      ),
    );
  }

  Widget iconFirWidthCont({required String icon, required String content}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            icon,
            width: 22.0,
            fit: BoxFit.fitWidth,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: subTitleTxtWidthIcon(content, maxLine: 1, fontSize: HFontSizes.large, color: HColors.subColorText),
            ),
          ),
          Stack(
            children: [
              Container(
                height: 20.0,
                margin: const EdgeInsets.only(top: 1.0),
                alignment: Alignment.center,
                padding: const EdgeInsets.only(left: 24.0, right: 12.0),
                decoration: BoxDecoration(
                  color: HColors.fifthColor,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: timeTxt('check-in'),
              ),
              Image.asset(
                AssetsInfo.editIcon,
                fit: BoxFit.fitWidth,
                width: 22.0,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget iconWidthCont({required String icon, required String content, int? maxLine = 1, double? paddingR}) {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, top: 9.0, bottom: 9.0, right: paddingR ?? 16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            icon,
            width: 22.0,
            fit: BoxFit.fitWidth,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: subTitleTxtWidthIcon(content, maxLine: maxLine),
            ),
          )
        ],
      ),
    );
  }
}
