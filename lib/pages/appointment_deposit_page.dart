import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/appointment_deposit_controller.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class AppointmentDepositPage extends GetView<AppointmentDepositController> {
  const AppointmentDepositPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Obx(() =>
                          depositGroup(controller.depositModel().amount ?? '', controller.depositModel().amount ?? '')),
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                        child: Obx(() => titleNormalTxt(controller.depositModel().note, highLine: 22.0)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30.0, left: 16.0),
                        child: titleNormalMainTxt("Payment method"),
                      ),
                      Obx(
                        () => cardGroup(
                          controller.cardModel().cardNo == null
                              ? ""
                              : "xxxx信用卡[${controller.cardModel().cardNo!.replaceAll('x', '')}]",
                          onTap: () => controller.showCardDialog(),
                        ),
                      ),
                    ],
                  ),
                ),
                // longBtn(
                //   icon: AssetsInfo.payCardIcon,
                //   title: "Credit card",
                //   active: true,
                //   isCircleBtn: true,
                //   isCenter: false,
                // ),
                // Obx(() => cardNumGroup(controller.teCtrl.value)),
                // Obx(() => cardHolderGroup(controller.teHolderCtrl.value)),
                // Obx(() => cardecGroup(controller.teCvvCtrl.value)),
                Padding(
                  padding: const EdgeInsets.only(top: 48.0),
                  child: longBtn(
                    title: "Submit",
                    active: true,
                    isCircleBtn: true,
                    onTap: () => controller.handleDeposit(),
                  ),
                )
              ],
            ),
          ),
          const Header(
            title: "Appointment Deposit",
          ),
        ],
      ),
    );
  }
}
