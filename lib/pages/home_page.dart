import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/model/home_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/fake_search_widget.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/controllers/home_controller.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

const topHeight = 306.0;

class HomePage extends GetView<HomeController> {
  @override
  final HomeController controller = Get.put(HomeController());
  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: RefreshIndicator(
        onRefresh: () => controller.getHome(),
        color: HColors.mainColor,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Image.asset(
                    AssetsInfo.homeTopBg,
                    height: topHeight,
                    width: Get.width,
                    fit: BoxFit.cover,
                  ),
                  SizedBox(
                    height: topHeight,
                    child: Column(children: [
                      Obx(
                        () => Container(
                          padding: const EdgeInsets.only(left: 16.0, right: 12.0),
                          margin: const EdgeInsets.only(bottom: 6.0, top: 44.0),
                          child: Row(children: [
                            Expanded(
                              child: titleNormalTxtRes(
                                  "hello".tr + ' ${textJudge(controller.appSrv.userInfoModel().nickname)}!'),
                            ),
                            GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () => Get.toNamed(Routes.statement, arguments: 'help'),
                              child: titleNormalTxtRes("help_center".tr),
                            ),
                            // Image.asset(
                            //   AssetsInfo.notification,
                            //   width: 40.0,
                            //   fit: BoxFit.fitWidth,
                            // )
                          ]),
                        ),
                      ),
                      Stack(
                        children: [
                          FakeSearchWidget(
                            placeholderText: "search_doctor_tip".tr,
                            onTap: () => Get.toNamed(Routes.search),
                          ),
                          // Obx(() => controller.notifyPointService.messageCount.value == 0
                          //     ? const SizedBox()
                          //     : notificationTip(onTap: () => Get.find<TabbarController>().onPressedTab(1)))
                        ],
                      ),
                      _cardLayout(),
                    ]),
                  ),
                ],
              ),
              _appointWidget()
            ],
          ),
        ),
      ),
    );
  }

  Widget _cardLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Row(
        children: [
          _funCard("video_consultation".tr, AssetsInfo.videoConsultation,
              vertical: true, onTap: () => controller.handleVisit()),
          Expanded(
            child: Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
              _funCard("find_doctor".tr, AssetsInfo.findDoctor, onTap: () => Get.toNamed(Routes.search)),
              const SizedBox(
                height: 14.0,
              ),
              _funCard("my_booking".tr, AssetsInfo.myBooking,
                  height: 60.0, onTap: () => Get.toNamed(Routes.appointmentList)),
              // _funCard('Drug\nDelivery', AssetsInfo.drugDelivery),
              // const SizedBox(width: 16.0),
            ]),
          ),
        ],
      ),
    );
  }

  Widget _funCard(title, icon, {vertical = false, height = 80.0, bottom = 0.0, onTap}) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: Container(
        height: vertical ? 155.0 : height,
        margin: EdgeInsets.only(
          left: vertical ? 16.0 : 10.0,
          right: 10.0,
          bottom: bottom,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(6.0)),
        child: vertical
            ? Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: titleNormalMainTxt(title, fontSize: 19.0),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Image.asset(
                      icon,
                      width: 94.0,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ],
              )
            : Row(
                children: [
                  Expanded(
                    child: titleNormalMainTxt(title, fontSize: 16.0),
                  ),
                  Image.asset(
                    icon,
                    width: height == 80.0 ? 54.0 : 44.0,
                    fit: BoxFit.fitWidth,
                  ),
                ],
              ),
      ),
    );
  }

  Widget _poiWidget() {
    return Container(
      child: shadowCard(
        shadowColor: HColors.notColor,
        margin: const EdgeInsets.only(bottom: 20.0, left: 16.0, right: 16.0),
        bg: HColors.notBg,
        borderRadius: BorderRadius.circular(40.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: HColors.heiLightText,
            ),
            borderRadius: BorderRadius.circular(40.0),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: assetImage(AssetsInfo.notice, width: 22.0),
              ),
              Expanded(child: titleNormalTxt("home_queue_status".tr, fontSize: 15.0)),
              // Obx(() => titleHugeTxt(
              //     controller.visitStatusModel().data == null
              //         ? '0'
              //         : controller.visitStatusModel().data!.queuePos ?? '0',
              //     color: HColors.heiLightText))
            ],
          ),
        ),
      ),
    );
  }

  Widget _appointWidget() {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.only(top: 16.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        // _poiWidget(),
        Row(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  titleTxt("today_schedule".tr),
                  // appointmentTip(),
                ],
              ),
            )),
            // GestureDetector(
            //   behavior: HitTestBehavior.translucent,
            //   onTap: () => controller.changeTab(),
            //   child: Row(
            //     children: [timeActiveTxt("view_all".tr), arrawRight()],
            //   ),
            // )
          ],
        ),
        _swipeCard()
      ]),
    );
  }

  Widget _swipeCard() {
    return Container(
      height: 430.0,
      padding: const EdgeInsets.only(top: 10.0),
      child: Obx(
        () => controller.isLoading.isFalse && controller.feeds.isEmpty
            ? Container(
                margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 5.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  boxShadow: const [
                    BoxShadow(color: HColors.tagBg, spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
                  ],
                ),
                height: 100,
                alignment: Alignment.center,
                padding: const EdgeInsets.only(top: 78.0),
                child: emptyPage(emptyIcon: AssetsInfo.noAppointment, title: "no_appointment_tip".tr, noPadding: true),
              )
            : Swiper(
                itemCount: controller.feeds.length,
                itemBuilder: (BuildContext context, int index) {
                  HomeModel homeModel = controller.feeds[index];
                  return GestureDetector(
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(16.0, 5.0, 16.0, 35.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6.0),
                        boxShadow: const [
                          BoxShadow(color: HColors.tagBg, spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
                        ],
                      ),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: homeModel.type == 'queue' ? 10.0 : 23.0, bottom: 12.0, left: 16.0, right: 16.0),
                            child: homeModel.time == null
                                ? const SizedBox()
                                : Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      titleBigTxt(textJudge(homeModel.time!.split(" ")[0])),
                                      const SizedBox(
                                        width: 12.0,
                                      ),
                                      subMainTxt(homeModel.time!.split(" ")[1].toUpperCase()),
                                      Expanded(
                                        child: Container(
                                          height: 30.0,
                                          alignment: Alignment.centerRight,
                                          child: shortBtn(
                                            title: "check_in_btn".tr,
                                            height: 30.0,
                                            width: 94.0,
                                            active: true,
                                            onTap: () => controller.checkIn(homeModel),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                          ),
                          homeModel.type != 'queue'
                              ? const SizedBox()
                              : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                  child: Row(
                                    children: [
                                      // queueGroup(
                                      //     "queue_poi_tip".tr,
                                      //     homeModel.position == null
                                      //         ? "00"
                                      //         : homeModel.position! < 10
                                      //             ? "0${homeModel.position}"
                                      //             : homeModel.position.toString()),
                                      homeModel.isVideoVisit != 1
                                          ? const SizedBox()
                                          : Row(
                                              children: [
                                                tagItem(title: "tag_ticket".tr, type: 'ticket'),
                                                tagItem(title: "tag_tele".tr, type: 'tele'),
                                              ],
                                            ),
                                      const Expanded(
                                        child: SizedBox(),
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.only(top: 0, left: 8.0),
                                        child: shortBtn(
                                          title: "view_btn".tr,
                                          height: 30.0,
                                          width: 124.0,
                                          active: true,
                                          onTap: () => controller.visitCheckIn(homeModel),
                                        ),
                                      ),
                                      // const Expanded(
                                      //   child: SizedBox(),
                                      // ),
                                    ],
                                  ),
                                ),
                          iconFirWidthCont(icon: AssetsInfo.userIcon, content: textJudge(homeModel.user)),
                          iconWidthCont(icon: AssetsInfo.doctorIcon, content: textJudge(homeModel.doctorName)),
                          iconWidthCont(
                              icon: AssetsInfo.houseIcon, content: textJudge(homeModel.clinicName), maxLine: 2),
                          iconWidthCont(icon: AssetsInfo.callingIcon, content: textJudge(homeModel.phone), maxLine: 4),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: iconWidthCont(
                                  icon: AssetsInfo.addressIcon,
                                  maxLine: 3,
                                  paddingR: 0,
                                  content: textJudge(homeModel.address),
                                ),
                              ),
                              GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () => selectMap(homeModel),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    right: 16.0,
                                    left: 10.0,
                                    top: 10.0,
                                  ),
                                  child: Image.asset(
                                    AssetsInfo.navigatorIcon,
                                    width: 22.0,
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
                pagination: const SwiperPagination(
                  builder: DotSwiperPaginationBuilder(
                    activeColor: HColors.mainColor,
                    color: HColors.dotBg,
                  ),
                ),
                loop: false,
              ),
      ),
    );
  }
}
