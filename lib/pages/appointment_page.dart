import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/appointment_controller.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/avatar_widget.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:date_format/date_format.dart';
import 'package:url_launcher/url_launcher.dart';

class AppointmentPage extends GetView<AppointmentController> {
  const AppointmentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Get.width,
        height: Get.height,
        decoration: const BoxDecoration(color: HColors.primary),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                color: HColors.darkPrimary,
                child: Stack(
                  children: [
                    Image.asset(
                      AssetsInfo.doctorBg,
                      height: 255.0,
                      width: Get.width,
                      fit: BoxFit.cover,
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 44.0),
                      alignment: Alignment.center,
                      child: Column(children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            backIcon(icon: AssetsInfo.backWhiteIcon),
                          ],
                        ),
                        Obx(
                          () => Column(
                            children: [
                              const Padding(
                                padding: EdgeInsets.only(top: 4.0),
                                child: AvatarGroup(
                                  defaultIcon: AssetsInfo.doctorDefaultAvatar,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: titleTxtRes(controller.doctorInfo().name ?? ''),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 12.0),
                                child: subTitleTxtRes(controller.doctorInfo().specialty == null
                                    ? ''
                                    : textJudge(controller.doctorInfo().specialty!.name)),
                              ),
                              _clinicInfo()
                            ],
                          ),
                        )
                      ]),
                    ),
                    Positioned(
                        top: 96.0,
                        right: 0,
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => controller.handleFavorite(),
                          child: Container(
                            height: 30.0,
                            padding: const EdgeInsets.symmetric(horizontal: 8.0),
                            decoration: const BoxDecoration(
                              color: HColors.tipBg,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15.0),
                                bottomLeft: Radius.circular(15.0),
                              ),
                            ),
                            child: Obx(() => Row(
                                  children: [
                                    Image.asset(
                                      controller.doctorInfo().isFavoriteDoctor == 0
                                          ? AssetsInfo.likeWhiteIcon
                                          : AssetsInfo.likeActiveIcon,
                                      width: 18.0,
                                      fit: BoxFit.fitWidth,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 4.0),
                                      child: smallestTitleWhite(controller.doctorInfo().isFavoriteDoctor == 1
                                          ? "appointment_added_favorite".tr
                                          : "appointment_add_favorite".tr),
                                    )
                                  ],
                                )),
                          ),
                        )),
                  ],
                ),
              ),
              _bookInputArea(),
              // Obx(
              //   () => controller.doctorData().remoteEnabled == 1
              //       ? Padding(
              //           padding: const EdgeInsets.only(top: 32.0, left: 16.0, right: 16.0),
              //           child: paymentWidget(),
              //         )
              //       : const SizedBox(),
              // ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                child: paymentWidget(),
              ),
              Obx(
                () => controller.doctorData().remoteEnabled == 1
                    ? longBtn(
                        title: 'appointment_queue_btn'.tr,
                        active: controller.doctorData().clinic!.connected == 1,
                        dead: controller.doctorData().clinic!.connected == 0,
                        isCircleBtn: true,
                        onTap: () => controller.queueNow(),
                      )
                    : longBtn(
                        title: "appointment_continue_btn".tr,
                        active: true,
                        isCircleBtn: true,
                        onTap: () => controller.bookAppoingment(),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _clinicInfo() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      decoration: BoxDecoration(color: Colors.white.withOpacity(.1), borderRadius: BorderRadius.circular(6.0)),
      child: Column(
        children: [
          iconWidthCont(
              icon: AssetsInfo.clinicWhiteIcon,
              content: controller.clinic().name ?? '',
              iconSize: 14.0,
              color: Colors.white,
              smallTxt: true),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => launch("tel:${controller.clinic().phoneOne}"),
            child: iconWidthCont(
                icon: AssetsInfo.phoneWhiteIcon,
                content: phoneFormat(controller.clinic().phoneOne),
                iconSize: 14.0,
                color: Colors.white,
                smallTxt: true),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => selectMap(controller.clinic()),
            child: iconWidthCont(
                icon: AssetsInfo.addressWhiteIcon,
                content: controller.clinic().addressOne ?? '',
                iconSize: 14.0,
                maxLine: 10,
                color: Colors.white,
                smallTxt: true),
          ),
        ],
      ),
    );
  }

  Widget _bookInputArea() {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 40.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Obx(() => controller.doctorData().remoteEnabled == 1
              ? shadowCard(
                  margin: const EdgeInsets.only(bottom: 16.0),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0, right: 22.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: titleNormalTxt("appointment_queue_tip".tr),
                        ),
                        highLightTxt(controller.doctorData().position.toString())
                      ],
                    ),
                  ))
              : const SizedBox()),
          Obx(
            () => controller.doctorData().videoVisitEnabled == 2 || controller.doctorData().videoVisitEnabled == 0
                ? const SizedBox()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      shortBtn(
                        icon: controller.diaType.value == 1 ? AssetsInfo.addressWhiteIcon : AssetsInfo.addressIcon,
                        title: "appointment_in_clinic_tip".tr,
                        active: controller.diaType.value == 1,
                        width: (Get.width - 48.0) / 2,
                        height: 30.0,
                        onTap: () => controller.diaType(1),
                      ),
                      shortBtn(
                        icon: controller.diaType.value == 2 ? AssetsInfo.videoIcon : AssetsInfo.videoActiveIcon,
                        title: "appointment_video_visit_tip".tr,
                        active: controller.diaType.value == 2,
                        width: (Get.width - 48.0) / 2,
                        height: 30.0,
                        onTap: () => controller.diaType(2),
                      ),
                    ],
                  ),
          ),
          Obx(
            () => controller.diaType.value == 1
                ? const SizedBox()
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 16.0),
                        child: Row(
                          children: [
                            titleNormalMainTxt("appointment_drug_delivery".tr),
                            GestureDetector(
                              onTap: () => Get.toNamed(Routes.statement, arguments: "delivery"),
                              behavior: HitTestBehavior.translucent,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                child: assetImage14(AssetsInfo.infoIcon),
                              ),
                            )
                          ],
                        ),
                      ),
                      Obx(
                        () => shadowCard(
                          margin: const EdgeInsets.all(0),
                          child: Column(
                            children: [
                              rowWidget(
                                "appointment_home_delivery".tr,
                                // onTap: () => controller.homeDelivery('HOME'),
                                subWidget: controller.homeDelivery.value == 'HOME'
                                    ? assetImage18(AssetsInfo.selectIconHlIcon)
                                    : assetImage18(AssetsInfo.selectIconNlIcon),
                              ),
                              rowWidget(
                                "appointment_pick_up".tr,
                                // onTap: () => controller.homeDelivery('PICKUP'),
                                disable: true,
                                subWidget: controller.homeDelivery.value != 'HOME'
                                    ? assetImage18(AssetsInfo.selectIconHlIcon)
                                    : assetImage18(AssetsInfo.selectIconNlIcon),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30.0, bottom: 12.0),
                        child: Row(
                          children: [
                            titleNormalMainTxt("appointment_select_service".tr),
                            GestureDetector(
                              onTap: () => Get.toNamed(Routes.statement, arguments: "delivery"),
                              behavior: HitTestBehavior.translucent,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                child: assetImage14(AssetsInfo.infoIcon),
                              ),
                            )
                          ],
                        ),
                      ),
                      Obx(
                        () => shadowCard(
                          margin: const EdgeInsets.all(0),
                          child: Column(
                            children: [
                              rowWidget(
                                "appointment_standard_delivery".tr,
                                // onTap: () => controller.typeDelivery('STANDARD'),
                                subWidget: controller.typeDelivery.value == 'STANDARD'
                                    ? assetImage18(AssetsInfo.selectIconHlIcon)
                                    : assetImage18(AssetsInfo.selectIconNlIcon),
                              ),
                              rowWidget(
                                "appointment_express_delivery".tr,
                                // onTap: () => controller.typeDelivery('EXPRESS'),
                                disable: true,
                                subWidget: controller.typeDelivery.value != 'STANDARD'
                                    ? assetImage18(AssetsInfo.selectIconHlIcon)
                                    : assetImage18(AssetsInfo.selectIconNlIcon),
                              ),
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => controller.showAddress(),
                        behavior: HitTestBehavior.translucent,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 30.0, bottom: 12.0),
                          child: Row(
                            children: [
                              Expanded(child: titleNormalMainTxt("appointment_select_address".tr)),
                              titleNormalTxt("appointment_choose_tip".tr),
                              arrawRight()
                            ],
                          ),
                        ),
                      ),
                      Obx(
                        () => controller.addressModel().name == null
                            ? const SizedBox()
                            : GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () => Get.toNamed(Routes.editAddress, arguments: controller.addressModel()),
                                child: addressWidget(
                                  controller.addressModel().recipientName,
                                  phoneFormat(controller.addressModel().recipientPhone),
                                  "${controller.addressModel().streetAddr1} ${controller.addressModel().district} ${controller.addressModel().region} ${controller.addressModel().area}",
                                ),
                              ),
                      ),
                      GestureDetector(
                        onTap: () => controller.showSelectRel(),
                        behavior: HitTestBehavior.translucent,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 30.0, bottom: 12.0),
                          child: Row(
                            children: [
                              Expanded(child: titleNormalMainTxt("appointment_select_family".tr)),
                              titleNormalTxt("appointment_choose_tip".tr),
                              arrawRight()
                            ],
                          ),
                        ),
                      ),
                      Obx(
                        () => controller.familyModel().name == null
                            ? const SizedBox()
                            : familyWidget(
                                controller.familyModel(),
                                onTap: () => Get.toNamed(Routes.profile, arguments: {
                                  "profile": controller.familyModel().profile,
                                  "dependentId": controller.familyModel().id
                                }),
                                onTapEdit: () => Get.toNamed(Routes.profile, arguments: {
                                  "profile": controller.familyModel().profile,
                                  "dependentId": controller.familyModel().id
                                }),
                              ),
                      ),
                      GestureDetector(
                        onTap: () => controller.showSelectCoupon(),
                        behavior: HitTestBehavior.translucent,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 30.0, bottom: 12.0),
                          child: Row(
                            children: [
                              Expanded(child: titleNormalMainTxt("appointment_select_coupon".tr)),
                              titleNormalTxt(
                                  controller.couponModel().id != null
                                      ? "${controller.couponModel().description}"
                                      : "appointment_choose_tip".tr,
                                  maxLines: 1),
                              arrawRight()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: inputLayoutUI(
              color: HColors.primary,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () => controller.showNicknameEdit(),
                child: Row(
                  children: [
                    titleNormalTxt("appointment_contact_phone_tip".tr),
                    Expanded(
                        //     child: NormalInputWidget(
                        //   teCtrl: controller.teCtrl,
                        //   hintText: "appointment_pleose_input_tip".tr,
                        //   focusNode: controller.focusNode,
                        //   layout: false,
                        //   textAlign: TextAlign.right,
                        // )),
                        child: Obx(
                      () => controller.inputText.isEmpty
                          ? subTitleGreyTxt("appointment_pleose_input_tip".tr, textAlign: TextAlign.right)
                          : titleNormalTxt(controller.selectPhoneModel().title! + ' ' + controller.inputText(),
                              textAlign: TextAlign.right),
                    )),
                  ],
                ),
              ),
            ),
          ),
          Obx(
            () => controller.doctorData().remoteEnabled == 1
                ? const SizedBox()
                : Padding(
                    padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                    child: smallestTitle("appointment_choose_date_title".tr),
                  ),
          ),
          Obx(
            () => controller.doctorData().remoteEnabled == 1
                ? Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: titleTxt(
                      "appointment_note_tip".tr,
                      fontSize: HFontSizes.smaller,
                      maxLines: 10,
                    ),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      shortBtn(
                        title: "appointment_earlist_date_tip".tr,
                        active: controller.bookWay.value == 1,
                        width: (Get.width - 48.0) / 2,
                        height: 30.0,
                        fontSize: HFontSizes.smallest,
                        onTap: () => controller.bookWay(1),
                      ),
                      shortBtn(
                        title: "appointment_choose_date_tip".tr,
                        active: controller.bookWay.value == 2,
                        width: (Get.width - 48.0) / 2,
                        height: 30.0,
                        fontSize: HFontSizes.smaller,
                        onTap: () => controller.bookWay(2),
                      ),
                    ],
                  ),
          ),
          Obx(() => controller.bookWay.value == 1 ? const SizedBox() : chooseDateWidget()),
        ],
      ),
    );
  }

  Widget chooseDateWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 8.0),
      child: Column(
        children: [
          Obx(
            () => chooseItem(1,
                date: formatDate(controller.date1.value, [yyyy, '/', mm, '/', dd]),
                time: controller.selectItem1().title,
                onTap: () => controller.pickDate(1),
                onTapTime: () => controller.selectDay(1)),
          ),
          Obx(() => chooseItem(2,
              date: controller.choice2Select.isFalse
                  ? "appointment_select_date_tip".tr
                  : formatDate(controller.date2.value, [yyyy, '/', mm, '/', dd]),
              time: controller.selectItem2().title,
              onTap: () => controller.pickDate(2),
              onTapTime: () => controller.selectDay(2))),
          Obx(() => chooseItem(3,
              date: controller.choice3Select.isFalse
                  ? "appointment_select_date_tip".tr
                  : formatDate(controller.date3.value, [yyyy, '/', mm, '/', dd]),
              time: controller.selectItem3().title,
              onTap: () => controller.pickDate(3),
              onTapTime: () => controller.selectDay(3))),
        ],
      ),
    );
  }

  Widget chooseItem(index, {date, time, onTap, onTapTime}) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
      child: Row(
        children: [
          Expanded(
              child: titleNormalMainTxt(
                  "appointment_choice_pri".tr + '${index > 1 ? "$index" + "appointment_choice_suf".tr : index}')),
          Row(
            children: [
              GestureDetector(behavior: HitTestBehavior.translucent, onTap: onTap, child: titleNormalTxt(date ?? "")),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: onTapTime,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Row(
                    children: [
                      titleNormalTxt(" ${time ?? 'AM'}"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6.0),
                        child: assetImage8(AssetsInfo.arrorBottomBtn),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

Widget rowWidget(title, {subtitle, disable = false, subWidget, onTap}) {
  return GestureDetector(
    onTap: onTap,
    behavior: HitTestBehavior.translucent,
    child: Container(
      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
      alignment: Alignment.center,
      child: Row(
        children: [
          Expanded(child: titleNormalTxt(title, color: disable ? HColors.greyText : HColors.mainText)),
          Row(
            children: [
              subWidget ?? titleNormalMainTxt(subtitle ?? ''),
            ],
          ),
        ],
      ),
    ),
  );
}
