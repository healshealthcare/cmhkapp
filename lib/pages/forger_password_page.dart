import 'package:Heals/config/assets.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/forger_password_controller.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/normal_input_widget.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_box/verification_box.dart';
import 'package:get/get.dart';

class ForgerPasswordPage extends GetView<ForgerPasswordController> {
  const ForgerPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: Column(
        children: [
          Stack(
            children: [
              Image.asset(
                AssetsInfo.topResetBg,
                height: 300.0,
                width: Get.width,
                fit: BoxFit.cover,
              ),
              Container(
                padding: const EdgeInsets.only(top: 167.0, left: 32.0, right: 32.0),
                alignment: Alignment.center,
                child: Obx(() => controller.step.value == 1 || controller.step.value == 2
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          titleLargerTxtActive(
                              controller.step.value == 2 ? "forget_send_code_title".tr : "forget_pass_title".tr),
                          Padding(
                            padding: const EdgeInsets.only(top: 24.0),
                            child: titleNormalTxt(
                                controller.step.value == 2
                                    ? ("${"forget_send_code_content".tr}+${controller.selectItemModel().title!} ${controller.forgetPasswordPhoneteCtrl.text}") +
                                        (controller.countDown.value > 0 ? "(${controller.countDown.value}s)" : '')
                                    : "forget_pass_content".tr,
                                maxLines: 10),
                          )
                        ],
                      )
                    : assetImage(AssetsInfo.passwordBg, width: 180.0)),
              ),
              const Header(
                title: "",
              ),
            ],
          ),
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 20.0),
                    padding: const EdgeInsets.only(top: 30.0),
                    constraints: const BoxConstraints(minHeight: 236.0),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 4, offset: Offset(0, 0))
                      ],
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(8.0),
                        bottomRight: Radius.circular(8.0),
                      ),
                    ),
                    child: Column(
                      children: [
                        Obx(() => controller.step.value == 1
                            ? NormalInputWidget(
                                teCtrl: controller.forgetPasswordPhoneteCtrl,
                                focusNode: controller.focusNodePhone,
                                hintText: "phone_hint_text".tr,
                                inputRule: Config.phoneRule,
                                leading: Obx(() => areaNumWidget(controller.selectItemModel().title,
                                    onTap: () => controller.showSelect())),
                              )
                            : const SizedBox()),
                        Obx(() => controller.step.value == 3
                            ? Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 16.0),
                                    child: NormalInputWidget(
                                      teCtrl: controller.forgetPasswordPassteCtrl,
                                      // focusNode: controller.focusNodePass,
                                      hintText: "pass_hint_text".tr,
                                      obscureText: true,
                                      inputRule: Config.passRule,
                                      leadIcon: AssetsInfo.lockIcon,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 16.0),
                                    child: NormalInputWidget(
                                      teCtrl: controller.forgetPasswordConfirmPassteCtrl,
                                      // focusNode: controller.focusNodeConfirmPass,
                                      hintText: "pass_repeat_hint_text".tr,
                                      obscureText: true,
                                      inputRule: Config.passRule,
                                      leadIcon: AssetsInfo.lockIcon,
                                    ),
                                  ),
                                ],
                              )
                            : const SizedBox()),
                        Obx(() => controller.step.value == 2
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  SizedBox(
                                    height: 45,
                                    child: VerificationBox(
                                      count: 8,
                                      itemWidget: 30.0,
                                      cursorColor: HColors.mainColor,
                                      decoration: BoxDecoration(
                                        color: HColors.mainColor.withOpacity(.08),
                                        border: const Border(
                                          bottom: BorderSide(color: HColors.mainColor, width: 3.0),
                                        ),
                                      ),
                                      onSubmitted: (value) {
                                        controller.msgCode = value;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 16.0, right: 16.0),
                                    child: resendMsg(onTap: () => controller.getSms()),
                                  )
                                ],
                              )
                            : const SizedBox()),
                        Padding(
                          padding: const EdgeInsets.only(top: 32.0),
                          child: longBtn(
                              title: controller.step.value == 3 ? "forget_pass_submit_btn".tr : "forget_pass_btn".tr,
                              isCircleBtn: true,
                              active: true,
                              onTap: () => controller.handleSubmit()),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget sendCode(countDown) {
    return GestureDetector(
      onTap: () => controller.getSms(),
      child: Container(
        height: 22.0,
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        margin: const EdgeInsets.only(right: 8.0),
        constraints: const BoxConstraints(minWidth: 64.0),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: countDown > 0 ? HColors.mainColor.withOpacity(.1) : HColors.mainColor,
            borderRadius: BorderRadius.circular(11.0)),
        child: countDown > 0
            ? smallestTitleUnActive("cound_down_tip".trArgs([countDown]))
            : smallestTitleWhite("veri_code_tip".tr),
      ),
    );
  }

  Widget agreeWidget(top) {
    return Padding(
      padding: EdgeInsets.only(top: top),
      child: Row(
        children: [
          Obx(() => GestureDetector(
                onTap: () => controller.isAgree.toggle(),
                behavior: HitTestBehavior.translucent,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 8.0, top: 10.0, bottom: 10.0),
                  child: assetImage18(controller.isAgree.isTrue ? AssetsInfo.selectIconHl : AssetsInfo.selectIconNl),
                ),
              )),
          Expanded(child: priAndState()),
        ],
      ),
    );
  }

  Widget resetPasswordWechat() {
    return Container(
      margin: const EdgeInsets.only(top: 60.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [_line(), subTitleGreyTxt("log_way_chagne_tip".tr, textAlign: TextAlign.center), _line()],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 18.0),
          child: assetImage44(AssetsInfo.wechatIcon),
        )
      ]),
    );
  }

  Widget _line() {
    return Container(
      height: 1.0,
      width: 60.0,
      alignment: Alignment.center,
      color: HColors.borderHr,
    );
  }
}
