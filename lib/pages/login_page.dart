import 'package:Heals/config/assets.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/normal_input_widget.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/login_controller.dart';

class LoginPage extends GetView<LoginController> {
  @override
  final LoginController controller = Get.put(LoginController());
  LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: Column(
        children: [
          Stack(
            children: [
              Image.asset(
                AssetsInfo.loginTopBg,
                height: 300.0,
                width: Get.width,
                fit: BoxFit.cover,
              ),
              Container(
                padding: const EdgeInsets.only(top: 252.0),
                alignment: Alignment.center,
                child: TabBar(
                  indicatorColor: HColors.mainColor,
                  controller: controller.tabController,
                  indicatorWeight: 2,
                  indicatorSize: TabBarIndicatorSize.tab,
                  isScrollable: true,
                  indicatorPadding: const EdgeInsets.symmetric(horizontal: 12.0),
                  labelStyle: const TextStyle(
                    fontSize: HFontSizes.medium,
                    color: HColors.mainColorText,
                    fontFamily: 'Comfortaa',
                  ),
                  labelColor: HColors.mainColorText,
                  unselectedLabelColor: HColors.mainText,
                  tabs: [
                    Tab(text: "login_tab".tr),
                    Tab(text: "register_tab".tr),
                  ],
                ),
              ),
              const Header(title: ""),
            ],
          ),
          Flexible(
              child: TabBarView(
            controller: controller.tabController,
            children: [
              // login
              SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 20.0),
                      padding: const EdgeInsets.only(top: 30.0),
                      constraints: const BoxConstraints(minHeight: 236.0),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 4, offset: Offset(0, 0))
                        ],
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8.0),
                          bottomRight: Radius.circular(8.0),
                        ),
                      ),
                      child: Column(
                        children: [
                          NormalInputWidget(
                            teCtrl: controller.loginPhoneteCtrl,
                            focusNode: controller.focusNodePhone,
                            hintText: "phone_hint_text".tr,
                            inputRule: Config.phoneRule,
                            leading: Obx(() => areaNumWidget(controller.selectItemModel().title,
                                onTap: () => controller.showSelect())),
                          ),
                          Obx(
                            () => controller.isPassLoginWay.isTrue
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 16.0),
                                    child: NormalInputWidget(
                                      teCtrl: controller.loginPassteCtrl,
                                      focusNode: controller.focusNodePass,
                                      hintText: "pass_hint_text".tr,
                                      obscureText: true,
                                      inputRule: Config.passRule,
                                      leadIcon: AssetsInfo.lockIcon,
                                    ),
                                  )
                                : Padding(
                                    padding: const EdgeInsets.only(top: 16.0),
                                    child: NormalInputWidget(
                                        teCtrl: controller.loginCodeteCtrl,
                                        focusNode: controller.focusNodeSms,
                                        hintText: "veri_hint_text".tr,
                                        inputRule: Config.passRule,
                                        leadIcon: AssetsInfo.verifyIcon,
                                        action: Obx(() => sendCode(controller.countDown.value, false))),
                                  ),
                          ),
                          longBtn(
                              title: "log_in_btn".tr,
                              isCircleBtn: true,
                              active: true,
                              onTap: () =>
                                  controller.isPassLoginWay.isFalse ? controller.loginInByCode() : controller.loginIn())
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () => controller.changeLoginWay(),
                            behavior: HitTestBehavior.translucent,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 16.0),
                              // child: Obx(() => subTitleTxtWidthIcon(
                              //     controller.isPassLoginWay.isFalse ? "pass_change_tip".tr : "otp_change_tip".tr,
                              //     color: HColors.mainColorText)),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => Get.toNamed(Routes.forgetPass),
                          behavior: HitTestBehavior.translucent,
                          child: Padding(
                            padding: const EdgeInsets.only(right: 16.0),
                            child: subTitleTxtActive("forget_pass_tip".tr),
                          ),
                        ),
                      ],
                    ),
                    agreeWidget(18.0),
                    // loginWechat()
                  ],
                ),
              ),
              // register
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                    padding: const EdgeInsets.only(top: 30.0),
                    // height: 260.0,
                    constraints: const BoxConstraints(minHeight: 260.0),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Color(0x2B46998C), spreadRadius: 2.0, blurRadius: 2, offset: Offset(0, 0))
                      ],
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(8.0),
                        bottomRight: Radius.circular(8.0),
                      ),
                    ),
                    child: Column(
                      children: [
                        NormalInputWidget(
                          teCtrl: controller.registerPhoneteCtrl,
                          focusNode: controller.focusNodeRegPhone,
                          hintText: "phone_hint_text".tr,
                          inputRule: Config.phoneRule,
                          leading: Obx(() =>
                              areaNumWidget(controller.selectItemModel().title, onTap: () => controller.showSelect())),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: NormalInputWidget(
                              teCtrl: controller.registerCodeteCtrl,
                              focusNode: controller.focusNodeRegSms,
                              hintText: "veri_hint_text".tr,
                              inputRule: Config.passRule,
                              leadIcon: AssetsInfo.verifyIcon,
                              action: Obx(() => sendCode(controller.countDown.value, true))),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: NormalInputWidget(
                            teCtrl: controller.registerPassteCtrl,
                            focusNode: controller.focusNodeRegPass,
                            hintText: "pass_hint_text".tr,
                            obscureText: true,
                            inputRule: Config.passRule,
                            leadIcon: AssetsInfo.lockIcon,
                          ),
                        ),
                        longBtn(
                            title: "register_btn".tr,
                            isCircleBtn: true,
                            active: true,
                            onTap: () => controller.postRegister())
                      ],
                    ),
                  ),
                  agreeWidget(8.0),
                  // loginWechat()
                ],
              ),
            ],
          )),
        ],
      ),
    );
  }

  Widget sendCode(int countDown, isRegister) {
    return GestureDetector(
      onTap: () => isRegister ? controller.getRegSms() : controller.getSms(),
      child: Container(
        height: 22.0,
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        margin: const EdgeInsets.only(right: 8.0),
        constraints: const BoxConstraints(minWidth: 64.0),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: countDown > 0 ? HColors.mainColor.withOpacity(.1) : HColors.mainColor,
            borderRadius: BorderRadius.circular(11.0)),
        child: countDown > 0
            ? smallestTitleUnActive("cound_down_tip".trArgs([countDown.toString()]))
            : smallestTitleWhite("veri_code_tip".tr),
      ),
    );
  }

  Widget agreeWidget(top) {
    return Padding(
      padding: EdgeInsets.only(top: top),
      child: Row(
        children: [
          Obx(() => GestureDetector(
                onTap: () => controller.isAgree.toggle(),
                behavior: HitTestBehavior.translucent,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 8.0, top: 10.0, bottom: 10.0),
                  child: assetImage18(controller.isAgree.isTrue ? AssetsInfo.selectIconHl : AssetsInfo.selectIconNl),
                ),
              )),
          Expanded(child: priAndState()),
        ],
      ),
    );
  }

  Widget loginWechat() {
    return Container(
      margin: const EdgeInsets.only(top: 60.0),
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [_line(), subTitleGreyTxt("log_way_chagne_tip".tr, textAlign: TextAlign.center), _line()],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 18.0),
          child: assetImage44(AssetsInfo.wechatIcon),
        )
      ]),
    );
  }

  Widget _line() {
    return Container(
      height: 1.0,
      width: 60.0,
      alignment: Alignment.center,
      color: HColors.borderHr,
    );
  }
}
