import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/appointment_list_controller.dart';

class AppointmentListPage extends GetView<AppointmentListController> {
  const AppointmentListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                  child: searchInput(),
                ),
                Container(
                  decoration:
                      const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
                  margin: const EdgeInsets.only(left: 16.0, right: 16.0, top: 12.0),
                  child: TabBar(
                    indicatorColor: HColors.mainColor,
                    controller: controller.tabController,
                    indicatorWeight: 2,
                    indicatorSize: TabBarIndicatorSize.tab,
                    isScrollable: true,
                    labelStyle: const TextStyle(
                      fontSize: HFontSizes.medium,
                      color: HColors.mainColorText,
                      fontFamily: 'Comfortaa',
                    ),
                    labelColor: HColors.mainColorText,
                    unselectedLabelColor: HColors.mainText,
                    tabs: [
                      Tab(text: "tab_all".tr),
                      Tab(text: "tab_upcoming".tr),
                      Tab(text: "tab_pending".tr),
                      Tab(text: "tab_past".tr),
                    ],
                  ),
                ),
                Flexible(
                  child: Obx(
                    () => TabBarView(
                      controller: controller.tabController,
                      children: [
                        RefreshIndicator(
                            onRefresh: () => controller.refreshData(),
                            color: HColors.mainColor,
                            child: appointlist(controller.appointmentList, status: 1)),
                        RefreshIndicator(
                            onRefresh: () => controller.refreshData(),
                            color: HColors.mainColor,
                            child: appointlist(controller.appointmentUpcommingList, status: 2)),
                        RefreshIndicator(
                            onRefresh: () => controller.refreshData(),
                            color: HColors.mainColor,
                            child: appointlist(controller.appointmentPenddingList, status: 3)),
                        RefreshIndicator(
                            onRefresh: () => controller.refreshData(),
                            color: HColors.mainColor,
                            child: appointlist(controller.appointmentPastList, status: 4)),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Header(
            title: "page_appointment_history".tr,
          ),
        ],
      ),
    );
  }

  Widget appointlist(list, {status = 1}) {
    return list.length == 0
        ? emptyWidget()
        : ListView.builder(
            padding: const EdgeInsets.all(0),
            itemBuilder: (BuildContext context, index) {
              BookingModel bookingModel = list[index];
              return appointItem(bookingModel);
            },
            itemCount: list.length,
          );
  }

  Widget appointItem(BookingModel bookingModel) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => controller.handleChoose(bookingModel),
      child: shadowCard(
          margin: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(child: titleLargeMainTxt(bookingModel.user!.name, maxLines: 1)),
                    statusTxt(getStatusTxt(bookingModel),
                        state: isFinish(bookingModel.state!)
                            ? "status_done".tr
                            : isFailState(bookingModel.state!)
                                ? "status_close".tr
                                : ''),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 14.0),
                  child: titleNormalTxt(bookingModel.doctor!.name),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 14.0),
                  child: subTitleTxt(bookingModel.clinic!.name),
                ),
                bookingModel.appointmentDate == null && bookingModel.desiredTimeSlots == null
                    ? const SizedBox()
                    : dateWidget(bookingModel),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Row(
                    children: [
                      isHold(bookingModel.state!)
                          ? Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: shortAutoBtn(
                                title: "appointment_accept_btn".tr,
                                height: 30.0,
                                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                active: true,
                                onTap: () => controller.replyAppointment(bookingModel.id, 1),
                              ),
                            )
                          : const SizedBox(),
                      isUpcoming(bookingModel) || isHold(bookingModel.state!)
                          ? Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: shortAutoBtn(
                                  title: "appointment_reschedule_btn".tr,
                                  height: 30.0,
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  active: true,
                                  onTap: () {
                                    controller.cancelAppointment(bookingModel.id);
                                    Get.toNamed(
                                      Routes.appointment,
                                      arguments: {
                                        "doctorId": bookingModel.doctor!.doctorId,
                                        "clinicId": bookingModel.doctor!.clinicId
                                      },
                                    );
                                  }),
                            )
                          : const SizedBox(),
                      isPenddding(bookingModel) || isUpcoming(bookingModel)
                          ? shortAutoBtn(
                              title: "appointment_cancel_btn".tr,
                              height: 30.0,
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              onTap: () => controller.cancelAppointment(bookingModel.id),
                            )
                          : const SizedBox(),
                      isPastCanAgain(bookingModel)
                          ? shortAutoBtn(
                              title: "appointment_book_again_btn".tr,
                              height: 30.0,
                              active: true,
                              onTap: () => Get.toNamed(
                                Routes.appointment,
                                arguments: {"doctorId": bookingModel.doctorId, "clinicId": bookingModel.clinic!.id},
                              ),
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                            )
                          : const SizedBox(),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }

  Widget dateWidget(BookingModel bookingModel) {
    return Container(
      margin: const EdgeInsets.only(top: 16.0),
      padding: const EdgeInsets.all(16.0),
      decoration: const BoxDecoration(color: HColors.dateCardBg),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              assetImage14(AssetsInfo.clockIcon),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child:
                    subTitleTxt(isPenddding(bookingModel) ? "appointment_date_title".tr : "appointment_date_title".tr),
              ),
            ],
          ),
          Column(
            children: List.generate(
                bookingModel.appointmentDate == null ? bookingModel.desiredTimeSlots!.length : 1,
                (index) => Padding(
                      padding: const EdgeInsets.only(top: 14.0, left: 22.0),
                      child: subTitleTxt(
                        bookingModel.appointmentDate == null
                            ? ("${bookingModel.desiredTimeSlots![index].date}  ${bookingModel.desiredTimeSlots![index].period!.toUpperCase()}")
                            : ("${bookingModel.appointmentDate}  ${bookingModel.appointmentTime}"),
                      ),
                    )),
          )
        ],
      ),
    );
  }

  Widget searchInput() {
    return SizedBox(
      height: 40.0,
      child: TextField(
        controller: controller.teCtrl(),
        focusNode: controller.focusNode,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIconConstraints: const BoxConstraints(maxWidth: 38.0),
          prefixIcon: Container(
              padding: const EdgeInsets.only(right: 8.0),
              alignment: Alignment.centerRight,
              child: Image.asset(
                AssetsInfo.search,
                width: 14.0,
                fit: BoxFit.fitHeight,
              )),
          suffixIcon: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => controller.teCtrl.value.clear(),
            child: Container(
              padding: const EdgeInsets.only(right: 12.0, left: 12.0),
              child: Obx(
                () => controller.teCtrl().text.isNotEmpty
                    ? Image.asset(
                        AssetsInfo.inputClearBtn,
                        width: 18.0,
                        fit: BoxFit.fitWidth,
                      )
                    : const SizedBox(),
              ),
            ),
          ),
          suffixIconConstraints: const BoxConstraints(minHeight: 64.0),
          hintText: "search_tip".tr,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(18.0), borderSide: BorderSide.none),
          contentPadding: const EdgeInsets.only(left: 12.0, right: 12.0),
          focusColor: HColors.mainColorText,
          hoverColor: HColors.mainColorText,
          hintStyle: const TextStyle(fontSize: HFontSizes.smallest, color: HColors.placeholderColor),
        ),
        cursorColor: HColors.mainColorText,
        textInputAction: TextInputAction.search,
        onSubmitted: (text) => controller.handleSearch(),
        style: const TextStyle(fontSize: HFontSizes.small, color: HColors.mainColorText),
      ),
    );
  }
}
