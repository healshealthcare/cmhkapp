import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/appointment_question_controller.dart';
import 'package:Heals/model/question_model.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppointmentQuestionPage extends GetView<AppointmentQuestionController> {
  const AppointmentQuestionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Get.width,
        height: Get.height,
        decoration: const BoxDecoration(color: HColors.lightPrimary),
        child: Stack(
          children: [
            BodyLayout(child: questionList()),
            Header(
              title: "page_appointment_questions".tr,
            ),
          ],
        ),
      ),
    );
  }

  Widget questionList() {
    return Obx(
      () => controller.questions.isEmpty
          ? const SizedBox()
          : Container(
              padding: const EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    titleNormalTxt("appointment_question_tip".tr, highLine: 22.0),
                    // titleNormalTxt(
                    //     "The information collected is for clinic's purpose only and never shared with any third party.",
                    //     highLine: 22.0),
                    Padding(
                      padding: const EdgeInsets.only(top: 22.0),
                      child: Column(
                        children: List.generate(controller.questions.length, (index) {
                          QuestionModel questionModel = controller.questions[index];
                          return Padding(
                            padding: const EdgeInsets.only(top: 18.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      width: 18.0,
                                      height: 18.0,
                                      margin: const EdgeInsets.only(right: 16.0),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(9.0), color: HColors.mainColor),
                                      alignment: Alignment.center,
                                      child: subTitleTxtRes((index + 1).toString()),
                                    ),
                                    Expanded(child: titleNormalTxt(questionModel.name)),
                                  ],
                                ),
                                Padding(
                                    padding: const EdgeInsets.only(top: 16.0),
                                    child: Wrap(
                                      alignment: WrapAlignment.start,
                                      textDirection: TextDirection.ltr,
                                      children: List.generate(questionModel.answerOptions!.length, (i) {
                                        String answerOption = questionModel.answerOptions![i];
                                        return Padding(
                                          padding: const EdgeInsets.only(right: 16.0, bottom: 12.0),
                                          child: shortBtn(
                                              title: answerOption,
                                              width: 94.0,
                                              active: answerOption == controller.answer[index].answer,
                                              onTap: () => controller.answerQues(index, answerOption)),
                                        );
                                      }),
                                    )),
                                questionModel.hasUserTxt == 0
                                    ? const SizedBox()
                                    : titleNormalTxt("appointment_or".tr, highLine: 22.0),
                                if (questionModel.hasUserTxt == 0)
                                  const SizedBox()
                                else
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Container(
                                      margin: const EdgeInsets.symmetric(horizontal: 20.0),
                                      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          boxShadow: const [
                                            BoxShadow(
                                                color: Color(0x2B46998C),
                                                spreadRadius: 2.0,
                                                blurRadius: 2,
                                                offset: Offset(0, 0))
                                          ],
                                          border: Border.all(
                                            color: HColors.mainColor,
                                            width: 1.0,
                                          ),
                                          borderRadius: BorderRadius.circular(20.0)),
                                      child: TextField(
                                        showCursor: true,
                                        controller: controller.teCtrl,
                                        decoration: InputDecoration(
                                          isDense: true,
                                          border: InputBorder.none,
                                          counterText: "",
                                          hintText: "appointment_question_limit".tr,
                                          hintStyle: const TextStyle(
                                              color: HColors.placeholderColor, fontSize: HFontSizes.small),
                                        ),
                                        // inputFormatters: <TextInputFormatter>[
                                        //   // FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]|[\u4e00-\u9fa5]|[0-9]")),
                                        //   // CustomLengthLimitingTextInputFormatter(Config.momentPubMaxLen),
                                        // ],
                                        maxLines: 10,
                                        style:
                                            const TextStyle(color: HColors.mainColorText, fontSize: HFontSizes.small),
                                        cursorColor: HColors.mainColorText,
                                        cursorHeight: 18.0,
                                        maxLength: 200,
                                        autofocus: false,
                                        textInputAction: TextInputAction.send,
                                        onSubmitted: (msg) => controller.answerQues(index, msg),
                                        // focusNode: controller.focusNode,
                                        keyboardType: TextInputType.multiline,
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          );
                        }),
                      ),
                    ),

                    longBtn(
                        title: "appointment_continue_btn".tr,
                        active: true,
                        cleanHorPadd: true,
                        isCircleBtn: true,
                        onTap: () => controller.submitQues())
                  ],
                ),
              ),
            ),
    );
  }
}
