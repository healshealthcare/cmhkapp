import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/queue_success_controller.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class QueueSuccessPage extends GetView<QueueSuccessController> {
  const QueueSuccessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: SafeArea(
              child: Container(
                padding: const EdgeInsets.only(top: 60.0, left: 40.0, right: 40.0),
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      AssetsInfo.appointmentSuccess,
                      width: 120.0,
                      fit: BoxFit.fitWidth,
                    ),
                    const SizedBox(height: 16.0),
                    titleLargeTxt("queue_success_tip".tr, textAlign: TextAlign.center),
                    const SizedBox(height: 60.0),
                    Obx(
                      () => bookList(
                        clinicList: controller.clinicList,
                        isQueue: true,
                      ),
                    ),
                    const SizedBox(height: 60.0),
                    longBtn(
                        title: "back_home".tr,
                        isCircleBtn: true,
                        isCenter: true,
                        active: true,
                        onTap: () => Get.offAllNamed(Routes.root))
                  ],
                ),
              ),
            ),
          ),
          Header(
            title: "page_queue_quccess".tr,
          ),
        ],
      ),
    );
  }
}
