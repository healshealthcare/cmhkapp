import 'dart:collection';

import 'package:Heals/controllers/appointment_list_controller.dart';
import 'package:Heals/controllers/home_controller.dart';
import 'package:Heals/controllers/pay_controller.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';

class PayPage extends StatelessWidget {
  final PayController controller = Get.put(PayController());

  PayPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey webViewKey = GlobalKey();
    InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
        crossPlatform: InAppWebViewOptions(
          useShouldOverrideUrlLoading: true,
          mediaPlaybackRequiresUserGesture: false,
        ),
        android: AndroidInAppWebViewOptions(
          useHybridComposition: true,
        ),
        ios: IOSInAppWebViewOptions(
          allowsInlineMediaPlayback: true,
        ));
    return Scaffold(
      body: Stack(
        children: [
          Header(title: "page_payment_title".tr),
          BodyLayout(
              child: Obx(
            () => controller.isErr.isFalse && controller.linkUrl.value.isNotEmpty
                ? InAppWebView(
                    key: webViewKey,
                    // initialUrlRequest: URLRequest(
                    //     url: Uri.parse('https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-pay/success.html')),
                    // initialUrlRequest: URLRequest(
                    //     url: Uri.parse('https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-pay/failure.html')),
                    initialUrlRequest: URLRequest(url: Uri.parse(controller.linkUrl.value)),
                    initialUserScripts: UnmodifiableListView<UserScript>([]),
                    initialOptions: options,
                    onWebViewCreated: (controller) {},
                    androidOnPermissionRequest: (controller, origin, resources) async {
                      return PermissionRequestResponse(
                          resources: resources, action: PermissionRequestResponseAction.GRANT);
                    },

                    shouldOverrideUrlLoading: (inappController, navigationAction) async {
                      Uri? uri = navigationAction.request.url!;
                      if (uri.query.startsWith('result=success')) {
                        Get.find<HomeController>().delayGetHome();
                        if (controller.type == 'queue') {
                          Get.offAndToNamed(Routes.queuePaySuccess, arguments: {
                            "doctorId": controller.doctorId,
                            "userId": controller.userId,
                            "clinicId": controller.clinicId
                          });
                        } else if (controller.type == 'appointment') {
                          Get.offAndToNamed(Routes.appointmentQuestion, arguments: controller.appointmentId);
                        } else {
                          if (Get.isRegistered<AppointmentListController>()) {
                            Get.find<AppointmentListController>().getData();
                          }
                          Get.back(result: true);
                        }
                      } else if (uri.query.startsWith('result=failure')) {
                        Get.back(result: true);
                      }

                      return NavigationActionPolicy.ALLOW;
                    },
                  )
                : Container(),
          ))
        ],
      ),
    );
  }
}
