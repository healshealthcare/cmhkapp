import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/doctor_detail_controller.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/avatar_widget.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class DoctorDetailPage extends GetView<DoctorDetailController> {
  const DoctorDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.primary),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Image.asset(
                  AssetsInfo.homeTopBg,
                  height: 260.0,
                  width: Get.width,
                  fit: BoxFit.cover,
                ),
                Container(
                  height: 260.0,
                  padding: const EdgeInsets.only(top: 44.0),
                  alignment: Alignment.center,
                  child: Column(children: [
                    SizedBox(
                      height: 44.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          backIcon(icon: AssetsInfo.backWhiteIcon),
                        ],
                      ),
                    ),
                    Obx(
                      () => Column(
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(top: 4.0),
                            child: AvatarGroup(
                              defaultIcon: AssetsInfo.doctorDefaultAvatar,
                            ),
                          ),
                          controller.doctorData().doctor == null
                              ? const SizedBox()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: titleTxtRes(controller.doctorData().doctor!.name ?? ''),
                                ),
                          controller.doctorData().doctor == null
                              ? const SizedBox()
                              : Padding(
                                  padding: const EdgeInsets.only(top: 12.0),
                                  child: subTitleTxtRes(textJudge(controller.doctorData().doctor!.specialty!.name)),
                                ),
                        ],
                      ),
                    )
                  ]),
                ),
                Positioned(
                    top: 96.0,
                    right: 0,
                    child: Container(
                      height: 30.0,
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      decoration: const BoxDecoration(
                        color: HColors.tipBg,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15.0),
                          bottomLeft: Radius.circular(15.0),
                        ),
                      ),
                      child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () => controller.postFavoriteDoctor(),
                        child: Obx(
                          () => controller.doctorData().doctor == null
                              ? const SizedBox()
                              : Row(
                                  children: [
                                    Image.asset(
                                      controller.doctorData().doctor!.isFavoriteDoctor == 1
                                          ? AssetsInfo.likeActiveIcon
                                          : AssetsInfo.likeWhiteIcon,
                                      width: 18.0,
                                      fit: BoxFit.fitWidth,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 4.0),
                                      child: smallestTitleWhite(controller.doctorData().doctor!.isFavoriteDoctor == 1
                                          ? "appointment_added_favorite".tr
                                          : "appointment_add_favorite".tr),
                                    )
                                  ],
                                ),
                        ),
                      ),
                    )),
              ],
            ),
            Obx(
              () => controller.isMedecinePage.isTrue
                  ? medicineWidget()
                  : Column(
                      children: [
                        Obx(
                          () => Padding(
                            padding: const EdgeInsets.only(top: 16.0),
                            child: bookList(
                                clinicList: controller.clinicList,
                                isShowOpen: controller.isShowOpen.value,
                                onViewMore: () => controller.isShowOpen(true)),
                          ),
                        ),
                        longBtn(
                          title: "doctor_appointment_btn".tr,
                          active: true,
                          isCircleBtn: true,
                          onTap: () => Get.offAndToNamed(
                            Routes.appointment,
                            arguments: {
                              "doctorId": controller.doctorData().doctor!.id,
                              "clinicId": controller.doctorData().clinicId
                            },
                          ),
                        )
                      ],
                    ),
            )
          ],
        ),
      ),
    );
  }

  Widget medicineWidget() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 24.0, bottom: 16.0),
          child: titleLargerTxtActive("consultation_completed".tr),
        ),
        shadowCard(
            child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 20.0),
                child: titleLargerTxtActive("consultation_next".tr),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 40.0),
                child: titleNormalTxt("consultation_payment".tr),
              ),
              assetImage(AssetsInfo.payment, width: 120.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: titleNormalTxt("consultation_payment_content".tr, maxLines: 10),
              ),
              const SizedBox(height: 40.0)
            ],
          ),
        ))
      ],
    );
  }
}
