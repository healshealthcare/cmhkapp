import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/medicine_controller.dart';
import 'package:Heals/model/medicine_model.dart';
import 'package:Heals/model/prescription_model.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class MedicinePage extends GetView<MedicineController> {
  @override
  final MedicineController controller = Get.put(MedicineController());
  MedicinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
              child: Obx(
            () => controller.medicineList.isEmpty && controller.isLoading.isFalse
                ? emptyPage(emptyIcon: AssetsInfo.medicineEmpty)
                : ListView.builder(
                    padding: controller.isMedicine.isTrue ? const EdgeInsets.all(0) : const EdgeInsets.only(top: 16.0),
                    itemBuilder: (BuildContext context, index) {
                      MedicineModel model = controller.medicineList[index];
                      return controller.isMedicine.isTrue ? medicineItem(model) : historyItem(model);
                    },
                    itemCount: controller.medicineList.length,
                  ),
          )),
          Obx(
            () => Header(
              title: controller.isMedicine.isTrue ? "page_medicine_box".tr : "page_health_history".tr,
            ),
          ),
        ],
      ),
    );
  }

  Widget historyItem(MedicineModel model) {
    List<String> dianosis = [];
    for (var item in model.diagnosis!) {
      dianosis.add(item.name!);
    }
    // List<String> prescriptions = [];
    // for (var item in model.prescriptions!) {
    //   prescriptions.add(item.name!);
    // }
    return Stack(
      children: [
        Container(
          width: 2.0,
          // height: 100.0,

          margin: const EdgeInsets.only(left: 19.0, top: 12.0),
          color: HColors.timeLineColor,
          alignment: Alignment.center,
        ),
        Container(
          padding: const EdgeInsets.only(left: 16.0),
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: assetImage(AssetsInfo.timeLineIcon, width: 8.0)),
                  subTitleTxt(model.date),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 0),
                child: shadowCard(
                    child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      titleLargeMainTxt(model.doctor!.name),
                      model.user == null
                          ? const SizedBox()
                          : subTitleTxt(
                              "user_name_tip".tr + (model.user!.nickname ?? ''),
                            ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: subTitleTxt(model.clinicModel!.name),
                      ),
                      // subTitleTxt(
                      //   "prescription_tip".tr + prescriptions.join(" "),
                      // ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: subTitleTxt("diagnosis_tip".tr + dianosis.join(" "), maxLines: 20),
                      ),

                      model.prescriptions == null || model.prescriptions!.isEmpty
                          ? const SizedBox()
                          : Padding(
                              padding: const EdgeInsets.only(top: 12.0),
                              child: descriptionItem(model.prescriptions!),
                            ),
                    ],
                  ),
                )),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget descriptionItem(List<PrescriptionModel> prescriptionModels) {
    List<Widget> priWidget = List.generate(prescriptionModels.length, (index) {
      PrescriptionModel model = prescriptionModels[index];
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(
          children: [
            SizedBox(width: 120.0, child: subTitleTxt("dia_name".tr)),
            Expanded(child: subTitleTxt(model.name ?? '', maxLines: 10))
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(width: 120.0, child: subTitleTxt("usg_name".tr)),
            Expanded(child: subTitleTxt(model.usage ?? '', maxLines: 3))
          ],
        ),
        Row(
          children: [
            SizedBox(width: 120.0, child: subTitleTxt("start_date_name".tr)),
            Expanded(child: subTitleTxt(model.startDate ?? ''))
          ],
        ),
        Row(
          children: [
            SizedBox(width: 120.0, child: subTitleTxt("fre_name".tr)),
            Expanded(child: subTitleTxt(model.frequency.toString()))
          ],
        ),
        Row(
          children: [
            SizedBox(width: 120.0, child: subTitleTxt("dur_name".tr)),
            Expanded(child: subTitleTxt(model.duration.toString()))
          ],
        ),
        Row(
          children: [
            SizedBox(width: 120.0, child: subTitleTxt("dos_name".tr)),
            Expanded(child: subTitleTxt(model.dosage.toString()))
          ],
        ),
      ]);
    });
    return Column(children: priWidget);
  }

  Widget medicineItem(MedicineModel model) {
    List<String> dianosis = [];
    for (var item in model.diagnosis!) {
      dianosis.add(item.name!);
    }
    List<String> prescriptions = [];
    String prescriptionsClock = '';
    for (var item in model.prescriptions!) {
      prescriptions.add(item.name!);
      Map<String, dynamic> preMap = item.toJson((value) => {});
      for (var key in preMap.keys) {
        prescriptionsClock = prescriptionsClock + '\n$key：  ${preMap[key]}';
      }
    }
    return shadowCard(
      child: Container(
        padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0, bottom: 18.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: titleLargeMainTxt(
                    model.date,
                  ),
                ),
                GestureDetector(
                    onTap: () => addEventtoCalender(
                          title: model.diagnosis,
                          description: prescriptionsClock,
                          startDate: model.date!,
                          endDate: model.date!,
                        ),
                    behavior: HitTestBehavior.translucent,
                    child: assetImage26(AssetsInfo.notificationIcon))
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        model.user == null
                            ? const SizedBox()
                            : subTitleTxt(
                                "user_name_tip".tr + (model.user!.nickname ?? ''),
                              ),
                        subTitleTxt(
                          "doctor_tip".tr + (model.doctor!.name ?? ''),
                        ),
                        subTitleTxt(
                          "diagnosis_tip".tr + dianosis.join(" "),
                        ),
                        subTitleTxt(
                          "prescription_tip".tr,
                        ),
                        model.prescriptions == null || model.prescriptions!.isEmpty
                            ? const SizedBox()
                            : Padding(
                                padding: const EdgeInsets.only(top: 12.0),
                                child: descriptionItem(model.prescriptions!),
                              ),
                        model.deliveryParams == null
                            ? const SizedBox()
                            : subTitleTxt(
                                "waybill_tip".tr + model.deliveryParams!.deliveryNo!,
                              ),
                      ],
                    ),
                  ),
                  // assetImage26(AssetsInfo.notificationIcon)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
