import 'package:Heals/constants/style.dart';
import 'package:Heals/config/assets.dart';
import 'package:Heals/controllers/splash_controller.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:get/get.dart';

class SplashPage extends StatelessWidget {
  final controller = Get.put(SplashController());
  SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: Obx(
          () => Swiper(
            itemCount: 3,
            itemBuilder: (BuildContext context, int index) {
              return swipeItem(index);
            },
            onIndexChanged: (index) => controller.splashIndex(index),
            pagination: SwiperPagination(
              builder: DotSwiperPaginationBuilder(
                activeColor: controller.splashIndex.value == 2 ? Colors.transparent : HColors.mainColor,
                color: controller.splashIndex.value == 2 ? Colors.transparent : HColors.dotBg,
              ),
            ),
            loop: false,
          ),
        ));
  }

  Widget swipeItem(index) {
    String icon = '';
    switch (index) {
      case 0:
        icon = AssetsInfo.guide1;
        break;
      case 1:
        icon = AssetsInfo.guide2;
        break;
      case 2:
        icon = AssetsInfo.guide3;
        break;
      default:
    }
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(top: 54.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(alignment: Alignment.center, child: assetImage(icon, width: 300.0)),
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.symmetric(horizontal: 32.0),
              child: titleHugeTxt("guide_title_${index + 1}".tr),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.only(top: 64.0, left: 32.0, right: 32.0),
                child: titleNormalTxt("guide_content_${index + 1}".tr, color: HColors.subColorText),
              ),
            ),
            index == 2
                ? longBtn(
                    title: "start".tr,
                    isCenter: true,
                    isCircleBtn: true,
                    active: true,
                    onTap: () => Get.offAndToNamed(Routes.root),
                  )
                : const SizedBox()
          ],
        ),
      ),
    );
  }
}
