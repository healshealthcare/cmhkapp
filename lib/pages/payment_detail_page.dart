import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/payment_detail_controller.dart';
import 'package:Heals/model/invoice_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class PaymentDetailPage extends GetView<PaymentDetailController> {
  const PaymentDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: const EdgeInsets.only(top: 24.0, left: 16.0, right: 16.0),
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            child: Image.asset(
                              AssetsInfo.payment,
                              width: 120.0,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.only(top: 10.0),
                          //   child: titleNormalTxt("payment_tip".tr, highLine: 27.0),
                          // ),
                          Obx(
                            () => controller.invoiceData().id == null
                                ? const SizedBox()
                                : invoiceInfo(controller.invoiceData()),
                          ),
                          // Obx(
                          //   () => controller.invoiceData().id == null
                          //       ? const SizedBox()
                          //       : invoiceItem(controller.invoiceData()),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ),
                Obx(
                  () => controller.invoiceData().id == null ||
                          controller.invoiceData().state != 'PENDING' ||
                          controller.invoiceData().transactions == null ||
                          controller.invoiceData().transactions![0].amount! <= 0
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: longBtn(
                            title: "back_home".tr,
                            isCircleBtn: true,
                            cleanVerPadd: 8.0,
                            active: true,
                            onTap: () => Get.offAllNamed(Routes.root),
                          ),
                        )
                      : Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: longBtn(
                            title: "payment_btn".tr,
                            isCircleBtn: true,
                            cleanVerPadd: 8.0,
                            active: true,
                            onTap: () => controller.toPay(),
                          ),
                        ),
                ),
              ],
            ),
          ),
          Header(
            title: "page_payment_detail_title".tr,
          ),
        ],
      ),
    );
  }

  Widget invoiceInfo(InvoiceModel model) {
    return shadowCard(
        child: Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 30.0),
          alignment: Alignment.center,
          child: titleLargeTxt('payment_invoce'.tr, textAlign: TextAlign.center),
        ),
        Column(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16.0),
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Row(
                children: [
                  Expanded(child: subTitleTxt("payment_patient".tr)),
                  subTitleTxt(model.extra!.user!.name ?? '')
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16.0),
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Row(
                children: [Expanded(child: subTitleTxt("payment_doctor".tr)), subTitleTxt(model.extra!.doctor!.name)],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16.0),
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: subTitleTxt("payment_clinic".tr),
                  ),
                  Expanded(child: subTitleTxt(model.extra!.clinic!.name, maxLines: 10, textAlign: TextAlign.right))
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16.0),
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Row(
                children: [
                  Expanded(child: subTitleTxt("payment_visit_date".tr)),
                  subTitleTxt(model.transactions![0].createdDate)
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16.0),
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Row(
                children: [
                  Expanded(child: subTitleTxt("payment_incoice_date".tr)),
                  subTitleTxt(model.transactions![0].createdDate)
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16.0),
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Row(
                children: [Expanded(child: subTitleTxt("payment_incoice_code".tr)), subTitleTxt(model.invoiceCode)],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16.0),
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Row(
                children: [
                  Expanded(child: subTitleTxt("payment_incoice_status".tr)),
                  subTitleTxt(model.state == 'DONE'
                      ? "payment_status_done".tr
                      : model.state == 'VOID'
                          ? "payment_status_void".tr
                          : "payment_status_pending".tr)
                ],
              ),
            ),
            model.ticketDeposit == null || model.transactions![0].type == 'TICKET_DEPOSIT'
                ? const SizedBox()
                : Container(
                    margin: const EdgeInsets.symmetric(horizontal: 16.0),
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    decoration:
                        const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
                    child: Row(
                      children: [
                        Expanded(child: subTitleTxt("payment_deposit".tr)),
                        subTitleTxt("-\$${model.ticketDeposit!.amount}")
                      ],
                    ),
                  ),
            invoiceItem(model),
          ],
        )
      ],
    ));
  }

  Widget invoiceItem(InvoiceModel model) {
    List<Widget> rowList = List.generate(
        model.invoiceItems!.length,
        (index) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
              child: Row(
                children: [
                  Expanded(child: subTitleTxt(model.invoiceItems![index].name, maxLines: 3)),
                  subTitleTxt("\$${model.invoiceItems![index].amount}")
                ],
              ),
            ));
    return Column(
      children: [
        Column(
          children: [
            Container(
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
              child: Column(
                children: rowList,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
              child: Row(
                children: [
                  Expanded(child: titleActiveTxt("payment_total".tr)),
                  titleActiveTxt("\$${model.transactions![0].amount}")
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
