import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/payment_list_controller.dart';
import 'package:Heals/model/invoice_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class PaymentListPage extends GetView<PaymentListController> {
  const PaymentListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: SafeArea(
              top: false,
              child: Obx(
                () => controller.invoiceData.isEmpty
                    ? emptyPage()
                    : ListView.builder(
                        padding: const EdgeInsets.only(top: 8.0),
                        itemBuilder: (BuildContext context, index) {
                          InvoiceModel model = controller.invoiceData[index];
                          return invoiceItem(model);
                        },
                        itemCount: controller.invoiceData.length,
                      ),
              ),
            ),
          ),
          Header(
            title: "page_payment_history_title".tr,
          ),
        ],
      ),
    );
  }

  Widget invoiceItem(InvoiceModel model) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => Get.toNamed(Routes.paymentDetail, arguments: {"invoice_id": model.id, "transaction_id": ''}),
      child: shadowCard(
          margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
            decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: HColors.border, width: 1.0))),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    titleNormalTxt(model.transactions![0].createdDate),
                    // const Expanded(child: SizedBox()),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: titleNormalTxt(model.extra!.clinic!.name, maxLines: 4, textAlign: TextAlign.right),
                    ))
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Row(
                    children: [
                      statusTxt(
                          model.state == 'PENDING'
                              ? "status_pending".tr
                              : model.state == 'DONE'
                                  ? "status_done".tr
                                  : model.state == 'VOID'
                                      ? "status_void".tr
                                      : '',
                          state: model.state == 'DONE' ? 'done' : ''),
                      const Expanded(child: SizedBox()),
                      titleNormalTxt("\$${model.transactions![0].amount}"),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: assetImage8(AssetsInfo.arrowRight),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
