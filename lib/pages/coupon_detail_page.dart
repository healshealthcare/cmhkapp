import 'package:Heals/constants/style.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/coupon_detail_controller.dart';
import 'package:flutter_html/flutter_html.dart';

class CouponDetailPage extends GetView<CouponDetailController> {
  @override
  final CouponDetailController controller = Get.put(CouponDetailController());
  CouponDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.mainColor),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              children: [
                Obx(
                  () => Container(
                    padding: const EdgeInsets.only(top: 27.0),
                    margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    child: Column(
                      children: [
                        titleMediumMainTxt(controller.couponModel().description ?? ""),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 14.0),
                          child: smallestTitle("coupon_expired_at".tr + (controller.couponModel().endDate ?? "")),
                        ),
                        controller.couponModel().status != 'VALID' || !controller.isSelectMode
                            ? const SizedBox()
                            : Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: shortBtn(
                                  title: "use".tr,
                                  width: 164.0,
                                  active: true,
                                  height: 40.0,
                                  onTap: () => controller.handleSelect(),
                                ),
                              ),
                      ],
                    ),
                  ),
                ),
                Obx(
                  () => Flexible(
                    child: Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.fromLTRB(16.0, 0, 16.0, 20.0),
                      constraints: const BoxConstraints(minHeight: 300.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      padding: const EdgeInsets.all(20.0),
                      child: SingleChildScrollView(
                        child: Html(data: controller.couponModel().couponClause ?? ""),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Header(
            title: "page_coupon_detail_title".tr,
          ),
        ],
      ),
    );
  }
}
