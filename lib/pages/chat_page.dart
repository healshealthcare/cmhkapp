import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/chat_controller.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;

class ChatPage extends GetView<ChatController> {
  @override
  final ChatController controller = Get.put(ChatController());
  ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Container(
        width: Get.width,
        height: Get.height,
        alignment: Alignment.center,
        decoration: const BoxDecoration(color: HColors.primary),
        child: _renderVideo(),
      ),
    );
  }

  _renderVideo() {
    return Stack(
      children: [
        Obx(
          () => controller.isCompleteDio.isTrue
              ? const SizedBox()
              : Container(
                  color: Colors.white,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      titleLargeMainTxt("doctor_late_title".tr, maxLines: 10),
                      // titleLargeMainTxt("patient_late_title".tr, maxLines: 10),
                      const SizedBox(height: 36.0),
                      titleNormalTxt("doctor_late_content".tr),
                      // titleNormalTxt("patient_late_content".tr),
                    ],
                  ),
                ),
        ),
        Obx(
          () => controller.rtcService.cameraOpen.isFalse
              ? const SizedBox()
              : controller.isCompleteDio.isFalse || controller.currentUid.value != 0
                  ? controller.currentUid.value != 0
                      ? RtcRemoteView.SurfaceView(
                          uid: controller.currentUid.value,
                          channelId: controller.channelId.value,
                        )
                      : const SizedBox()
                  : RtcLocalView.SurfaceView(
                      zOrderMediaOverlay: true,
                      zOrderOnTop: true,
                    ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Obx(
            () => Column(
              children: [
                Column(
                    children: List.of(controller.rtcService.remoteUid.map(
                  (e) => e == controller.currentUid.value
                      ? const SizedBox()
                      : GestureDetector(
                          onTap: () {
                            controller.onTapWin(e);
                          },
                          child: miniWinLayout(
                            child: RtcRemoteView.SurfaceView(
                              uid: e,
                              channelId: controller.channelId.value,
                            ),
                          ),
                        ),
                ))),
                Obx(
                  () => controller.isCompleteDio.isFalse || controller.currentUid.value != 0
                      ? miniWinLayout(
                          child: Obx(
                            () => controller.rtcService.cameraOpen.isTrue
                                ? GestureDetector(
                                    onTap: () {
                                      controller.onTapWin(0);
                                    },
                                    child: RtcLocalView.SurfaceView(
                                      zOrderMediaOverlay: true,
                                      zOrderOnTop: true,
                                    ),
                                  )
                                : const SizedBox(),
                          ),
                        )
                      : const SizedBox(),
                ),
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 48.0),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () => controller.rtcService.rtcSwitchCamera(),
                  child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                      child: assetImage64(AssetsInfo.switchCamera)),
                ),
                GestureDetector(
                  onTap: () => controller.setEnableSpeakerphone(),
                  child: Obx(
                    () => Container(
                      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(.3),
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                      child: assetImage64(
                          controller.isEnableSpeaker.isTrue ? AssetsInfo.speakerOnIcon : AssetsInfo.speakerOffIcon),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        footer()
      ],
    );
  }

  Widget footer() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: SizedBox(
        height: 128.0,
        child: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 30.0),
              margin: const EdgeInsets.only(left: 16.0),
              child: assetImage(
                AssetsInfo.videoBottom,
                width: Get.width - 32.0,
                height: 98.0,
                fit: BoxFit.contain,
              ),
            ),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                      onTap: () => controller.rtcService.rtcToggleVoice(),
                      child: Obx(
                        () => Container(
                          margin: const EdgeInsets.only(left: 45.0),
                          child: assetImage56(
                            controller.rtcService.voiceOpen.isTrue ? AssetsInfo.vioceOpen : AssetsInfo.voiceClose,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                        onTap: () {
                          controller.leaveRoom();
                        },
                        child: assetImage64(AssetsInfo.videoOff)),
                    GestureDetector(
                      onTap: () => controller.rtcService.rtcToggleCamera(),
                      child: Obx(
                        () => Container(
                          margin: const EdgeInsets.only(right: 45.0),
                          child: assetImage56(
                            controller.rtcService.cameraOpen.isTrue ? AssetsInfo.videoOpen : AssetsInfo.videoClose,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Obx(() => Container(
                      margin: const EdgeInsets.only(top: 12.0),
                      child: titleNormalTxt(controller.doctorModel().name ?? '', color: Colors.white),
                    )),
                Obx(
                  () => Container(
                    margin: const EdgeInsets.only(top: 2.0),
                    child:
                        subTitleTxt(timeFormatterSeconds(controller.rtcService.duringTime.value), color: Colors.white),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget miniWinLayout({child}) {
  return Container(
    margin: const EdgeInsets.only(top: 32.0, right: 12.0),
    height: 160,
    width: 100,
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(12.0)),
    alignment: Alignment.center,
    child: ClipRRect(
      borderRadius: BorderRadius.circular(12.0),
      child: child,
    ),
  );
}
