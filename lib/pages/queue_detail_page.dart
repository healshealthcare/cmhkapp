import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/queue_detail_controller.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:rive/rive.dart';

class QueueDetailPage extends StatelessWidget {
  final QueueDetailController controller = Get.find<QueueDetailController>();
  QueueDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 10.0),
                    alignment: Alignment.center,
                    child: titleNormalTxt(
                      "queue_tip".tr,
                      highLine: 27.0,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Obx(
                    () => controller.visitStatusModel().data == null ||
                            controller.visitStatusModel().data!.queuePos == null ||
                            controller.visitStatusModel().data!.state != 'CONFIRMED'
                        ? controller.animaIsReady.isFalse || controller.visitStatusModel().data == null
                            ? const SizedBox()
                            : controller.visitStatusModel().data!.state != 'REMOTED_FAILED'
                                ? Rive(
                                    artboard: controller.artboard!,
                                    fit: BoxFit.cover,
                                    useArtboardSize: true,
                                  )
                                : Image.asset(
                                    AssetsInfo.waitFail,
                                    width: 160.0,
                                    fit: BoxFit.fitWidth,
                                  )
                        : Container(
                            margin: const EdgeInsets.only(top: 27.0, bottom: 25.0),
                            width: 120.0,
                            height: 120.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(60.0),
                              border: Border.all(color: HColors.hightLight, width: 0.5),
                              color: HColors.hightLightOp,
                            ),
                            alignment: Alignment.center,
                            child: titleLargeMainTxt(controller.visitStatusModel().data!.queuePos ?? '0',
                                fontSize: 60.0, color: HColors.heiLightText),
                          ),
                  ),
                  // subTitleTxt("queue_est_time".tr, color: HColors.smallImportantColor),
                  // subTitleTxt(controller.visitStatusModel().data.e, color: HColors.smallImportantColor),
                  Obx(
                    () => controller.doctorData.value.doctor == null
                        ? const SizedBox()
                        : appointmentCard(controller.appointmentModel()),
                  ),
                  Obx(
                    () => controller.visitStatusModel().allowCheckIn != null &&
                            !controller.visitStatusModel().allowCheckIn! &&
                            controller.visitStatusModel().data!.arrivalTime != null
                        ? Container(
                            padding: const EdgeInsets.only(top: 10.0, left: 16.0, right: 16.0),
                            alignment: Alignment.center,
                            child: titleNormalTxt(
                              "queue_detail_success_tip".tr,
                              highLine: 27.0,
                              maxLines: 10,
                              textAlign: TextAlign.left,
                            ),
                          )
                        : const SizedBox(),
                  ),
                  Obx(
                    () => (controller.visitStatusModel().allowCheckIn != null &&
                                !controller.visitStatusModel().allowCheckIn! &&
                                controller.visitStatusModel().data!.arrivalTime != null) ||
                            controller.visitStatusModel().data == null ||
                            controller.visitStatusModel().data!.isVideoVisit == 1
                        ? const SizedBox()
                        : Padding(
                            padding: const EdgeInsets.only(top: 30.0),
                            child: longBtn(
                              title: "check_in_space_btn".tr,
                              isCircleBtn: true,
                              cleanVerPadd: 8.0,
                              active: true,
                              onTap: () => controller.checkQueue(),
                            ),
                          ),
                  ),
                  Obx(() => controller.visitStatusModel().data == null ||
                          controller.visitStatusModel().data!.queuePos! > 2 ||
                          controller.visitStatusModel().data == null ||
                          controller.visitStatusModel().data!.videoInfo == null
                      ? const SizedBox()
                      : Padding(
                          padding: const EdgeInsets.only(top: 30.0),
                          child: longBtn(
                            title: "join_video_room".tr,
                            isCircleBtn: true,
                            cleanVerPadd: 8.0,
                            active: true,
                            onTap: () => controller.handleJoinRoom(),
                          ),
                        )),
                  Obx(
                    () => controller.visitStatusModel().data == null ||
                            (controller.visitStatusModel().allowCheckIn != null &&
                                !controller.visitStatusModel().allowCheckIn!)
                        ? const SizedBox()
                        : longBtn(
                            title: "leave_queue_btn".tr,
                            isCircleBtn: true,
                            cleanVerPadd: 8.0,
                            onTap: () => controller.cancelQueue(),
                          ),
                  ),
                  // queue_detail_note
                  Obx(() => controller.visitStatusModel().data == null ||
                          controller.visitStatusModel().data!.isVideoVisit == null ||
                          controller.visitStatusModel().data!.isVideoVisit != 1
                      ? const SizedBox()
                      : Container(
                          padding: const EdgeInsets.only(top: 10.0, left: 16.0, right: 16.0),
                          alignment: Alignment.center,
                          child: titleNormalTxt(
                            "queue_detail_note".tr,
                            highLine: 27.0,
                            textAlign: TextAlign.center,
                          ),
                        )),
                ],
              ),
            ),
          ),
          // Obx(() => controller.notifyPointService.messageCount.value == 0 ? const SizedBox() : notificationTip()),
          Header(
            title: "page_queue_detail".tr,
          ),
        ],
      ),
    );
  }
}
