import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/favorite_doctor_controller.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FavoriteDoctorPage extends GetView<FavoriteDoctorController> {
  const FavoriteDoctorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        color: HColors.primary,
        child: Stack(
          children: [
            Obx(() => BodyLayout(
                  child: Container(
                    child: controller.doctorList.isEmpty
                        ? emptyWidget()
                        : ListView.builder(
                            padding: const EdgeInsets.all(0),
                            itemBuilder: (BuildContext context, int index) {
                              DoctorModel item = controller.doctorList[index];
                              return shadowCard(
                                  margin: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                                  child: doctorCard(
                                      doctorItem: item,
                                      islike: item.doctor!.isFavoriteDoctor == 1,
                                      onPressed: () => Get.toNamed(Routes.doctorDetail, arguments: {"doctor": item})));
                            },
                            itemCount: controller.doctorList.length,
                          ),
                  ),
                )),
            Header(
              title: "page_favorite_doctor".tr,
            )
          ],
        ),
      ),
    );
  }

  Widget doctorCard({required DoctorModel doctorItem, islike = false, onPressed}) {
    return GestureDetector(
      onTap: onPressed,
      behavior: HitTestBehavior.translucent,
      child: Container(
        padding: const EdgeInsets.fromLTRB(14.0, 14.0, 0, 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  AssetsInfo.medicineBoxIcon,
                  width: 22.0,
                  fit: BoxFit.fitWidth,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: titleActiveTxt(doctorItem.doctor!.name),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 14.0),
                  child: Image.asset(
                    !islike ? AssetsInfo.likeIcon : AssetsInfo.likeActiveIcon,
                    width: 22.0,
                    fit: BoxFit.fitWidth,
                    gaplessPlayback: true,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 12.0),
              child: thirdTitle(doctorItem.doctor!.specialty!.name),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 16.0),
              child: Row(
                children: [
                  Expanded(
                      child: doctorItem.clinic == null ? const SizedBox() : titleNormalTxt(doctorItem.clinic!.name)),
                  Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Row(
                      children: [
                        Image.asset(
                          AssetsInfo.navigatorIcon,
                          width: 20.0,
                          fit: BoxFit.fitWidth,
                        ),
                        Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: smallestTitle(
                              controller.appSrv.curPosi == null
                                  ? ''
                                  : getDistance(doctorItem.clinic!.latitude!, doctorItem.clinic!.longitude!,
                                      controller.appSrv.curPosi!.latitude, controller.appSrv.curPosi!.longitude),
                            ))
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 1.0,
              alignment: Alignment.center,
              color: HColors.border,
              margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: Row(
                children: [
                  // tagItem(title: 'e-Ticket'),
                  doctorItem.appointmentEnabled == 0 ? const SizedBox() : tagItem(title: "doctor_tag".tr),
                  doctorItem.videoVisitEnabled == 0 ? const SizedBox() : tagItem(title: "telehealth_tag".tr),
                  // tagItem(title: 'Est.', preIcon: AssetsInfo.clockIcon, subTitle: '17:01'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget tagItem({preIcon, title, subTitle}) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.0),
      margin: const EdgeInsets.only(right: 6.0),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(12.0), color: HColors.tagBg),
      child: Row(
        children: [
          preIcon != null
              ? Padding(
                  padding: const EdgeInsets.only(right: 4.0),
                  child: Image.asset(
                    AssetsInfo.clockIcon,
                    width: 14.0,
                    fit: BoxFit.fitWidth,
                  ),
                )
              : const SizedBox(),
          smallestTitleActive(title),
          subTitle != null ? titleActiveTxt(subTitle) : const SizedBox()
        ],
      ),
    );
  }

  Widget searchInput() {
    return SizedBox(
      height: 28.0,
      child: TextField(
        controller: controller.teCtrl(),
        focusNode: controller.focusNode,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          prefixIconConstraints: const BoxConstraints(maxWidth: 38.0),
          prefixIcon: Container(
              padding: const EdgeInsets.only(right: 8.0),
              alignment: Alignment.centerRight,
              child: Image.asset(
                AssetsInfo.search,
                width: 14.0,
                fit: BoxFit.fitHeight,
              )),
          suffixIcon: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () => controller.teCtrl.value.clear(),
            child: Container(
              padding: const EdgeInsets.only(right: 12.0, left: 12.0),
              child: Obx(
                () => controller.teCtrl().text.isNotEmpty
                    ? Image.asset(
                        AssetsInfo.inputClearBtn,
                        width: 18.0,
                        fit: BoxFit.fitWidth,
                      )
                    : const SizedBox(),
              ),
            ),
          ),
          suffixIconConstraints: const BoxConstraints(minHeight: 64.0),
          hintText: "FavoriteDoctor_doctor_tip".tr,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(18.0), borderSide: BorderSide.none),
          contentPadding: const EdgeInsets.only(left: 12.0, right: 12.0),
          focusColor: HColors.mainColorText,
          hoverColor: HColors.mainColorText,
          hintStyle: const TextStyle(fontSize: HFontSizes.smallest, color: HColors.placeholderColor),
        ),
        cursorColor: HColors.mainColorText,
        textInputAction: TextInputAction.search,
        // onSubmitted: (text) => controller.handleFavoriteDoctor(),
        style: const TextStyle(fontSize: HFontSizes.small, color: HColors.mainColorText),
      ),
    );
  }

  Widget selectItem(title, active, tabActive, {onPress}) {
    return Expanded(
      child: GestureDetector(
        onTap: onPress,
        behavior: HitTestBehavior.translucent,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          alignment: Alignment.center,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              active ? thirdTitleActive(title) : thirdTitle(title),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Image.asset(
                  tabActive ? AssetsInfo.triTopIcon : AssetsInfo.triBottomIcon,
                  width: 8.0,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
