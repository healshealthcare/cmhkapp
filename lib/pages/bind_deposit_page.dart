import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/bind_deposit_controller.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class BindDepositPage extends GetView<BindDepositController> {
  const BindDepositPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Obx(() => cardNumGroup(controller.teCtrl.value)),
                      Obx(() => cardHolderGroup(controller.teHolderCtrl.value)),
                      Obx(
                        () => cardecGroup(
                          controller.teCvvCtrl.value,
                          year: controller.year.value,
                          month: controller.month.value,
                          onSelect: () => controller.showCanlender(),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 48.0),
                  child: longBtn(
                    title: "Add Card ",
                    icon: AssetsInfo.payCardIcon,
                    active: true,
                    isCircleBtn: true,
                    onTap: () => controller.bindUnion(),
                  ),
                )
              ],
            ),
          ),
          const Header(
            title: "Add New Card ",
          ),
        ],
      ),
    );
  }
}
