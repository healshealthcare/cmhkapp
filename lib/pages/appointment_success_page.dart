import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/appointment_success_controller.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class AppointmentSuccessPage extends GetView<AppointmentSuccessController> {
  const AppointmentSuccessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: SafeArea(
              child: Container(
                padding: const EdgeInsets.only(top: 60.0, left: 40.0, right: 40.0),
                child: Column(
                  children: [
                    Image.asset(
                      AssetsInfo.appointmentSuccess,
                      width: 120.0,
                      fit: BoxFit.fitWidth,
                    ),
                    const SizedBox(height: 16.0),
                    titleLargeTxt("appointment_success_tip".tr),
                    const SizedBox(height: 60.0),
                    titleNormalTxt("appointment_success_discripe".tr, highLine: 22.0, maxLines: 10),
                    const SizedBox(height: 60.0),
                    longBtn(
                        title: "appointment_success_btn".tr,
                        active: true,
                        isCircleBtn: true,
                        onTap: () => controller.toDetail())
                  ],
                ),
              ),
            ),
          ),
          Header(
            title: "page_appointment_success".tr,
          ),
        ],
      ),
    );
  }
}
