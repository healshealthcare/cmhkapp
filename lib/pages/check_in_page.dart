import 'package:Heals/constants/style.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/check_in_controller.dart';

class CheckInPage extends GetView<CheckInController> {
  @override
  final CheckInController controller = Get.put(CheckInController());
  CheckInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: RefreshIndicator(
        onRefresh: () => controller.getData(),
        color: HColors.mainColor,
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Stack(
            children: [
              BodyLayout(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      // Column(
                      //   children: [
                      //     Padding(
                      //       padding: const EdgeInsets.only(top: 12.0, left: 16.0, right: 16.0),
                      //       child: titleNormalTxt('Find all apoointment(s) for today on this page'),
                      //     ),
                      //     Obx(() => controller.noteKeyModel().otherNotes == null
                      //         ? const SizedBox()
                      //         : Padding(
                      //             padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 12.0),
                      //             child: titleNormal80Txt(controller.noteKeyModel().otherNotes![0].content),
                      //           )),
                      //     Obx(() => controller.noteKeyModel().otherNotes == null
                      //         ? const SizedBox()
                      //         : infoAreaOne(controller.noteKeyModel())), // hard code
                      //   ],
                      // ),
                      Obx(
                        () => Column(
                            children: List.generate(
                          controller.appointmentList.length,
                          (index) => appointmentCard(controller.appointmentList()[index],
                              checkIn: true,
                              onCheckIn: () => controller.toCheckInForm(controller.appointmentList()[index])),
                        )),
                      ),
                    ],
                  ),
                ),
              ),
              Header(
                leading: const SizedBox(),
                title: "page_check_in".tr,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
