import 'package:Heals/config/assets.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/model/coupon_model.dart';
import 'package:Heals/widgets/body_layout.dart';
import 'package:Heals/widgets/header.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Heals/controllers/coupon_controller.dart';

class CouponPage extends StatelessWidget {
  @override
  final CouponController controller = Get.find<CouponController>();
  CouponPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: Get.height,
      decoration: const BoxDecoration(color: HColors.lightPrimary),
      child: Stack(
        children: [
          BodyLayout(
            child: Obx(
              () => Column(
                children: [
                  TabBar(
                    indicatorColor: Colors.black,
                    labelColor: Colors.black,
                    isScrollable: false,
                    controller: controller.tabController,
                    unselectedLabelColor: HColors.secondaryText,
                    tabs: [
                      Tab(text: 'coupon_valid'.tr),
                      Tab(text: 'coupon_used'.tr),
                      Tab(text: 'coupon_expired'.tr),
                      Tab(text: 'coupon_all'.tr),
                    ],
                  ),
                  Expanded(
                      child: TabBarView(
                    controller: controller.tabController,
                    children: [
                      controller.couponValidList.isEmpty && controller.isLoading.isFalse
                          ? emptyPage(emptyIcon: AssetsInfo.couponIcon, title: "coupon_empty".tr)
                          : ListView.builder(
                              padding: const EdgeInsets.only(top: 12.0),
                              itemBuilder: (context, index) => couponItem(controller.couponValidList[index]),
                              itemCount: controller.couponValidList.length,
                            ),
                      controller.couponUsedList.isEmpty && controller.isLoading.isFalse
                          ? emptyPage(emptyIcon: AssetsInfo.couponIcon, title: "coupon_empty".tr)
                          : ListView.builder(
                              padding: const EdgeInsets.only(top: 12.0),
                              itemBuilder: (context, index) => couponItem(controller.couponUsedList[index]),
                              itemCount: controller.couponUsedList.length,
                            ),
                      controller.couponInvalidList.isEmpty && controller.isLoading.isFalse
                          ? emptyPage(emptyIcon: AssetsInfo.couponIcon, title: "coupon_empty".tr)
                          : ListView.builder(
                              padding: const EdgeInsets.only(top: 12.0),
                              itemBuilder: (context, index) => couponItem(controller.couponInvalidList[index]),
                              itemCount: controller.couponInvalidList.length,
                            ),
                      controller.couponAllList.isEmpty && controller.isLoading.isFalse
                          ? emptyPage(emptyIcon: AssetsInfo.couponIcon, title: "coupon_empty".tr)
                          : ListView.builder(
                              padding: const EdgeInsets.only(top: 12.0),
                              itemBuilder: (context, index) => couponItem(controller.couponAllList[index]),
                              itemCount: controller.couponAllList.length,
                            ),
                    ],
                  )),
                ],
              ),
            ),
          ),
          Header(
            title: "page_coupon_title".tr,
          ),
        ],
      ),
    );
  }

  Widget couponItem(CouponModel model) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 8.0),
      child: Stack(
        children: [
          SizedBox(
              height: 160.0, child: assetImage(AssetsInfo.couponBgIcon, width: Get.width - 16.0, fit: BoxFit.fill)),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 27.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 16.0),
                          child: assetImage(AssetsInfo.couponIcon, width: 72.0),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10.0, right: 16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: titleMediumTxt(
                                        model.description ?? '',
                                        color: HColors.mainColorText,
                                        maxLines: 2,
                                      ),
                                    ),
                                    model.status != 'VALID' || !controller.isSelectMode
                                        ? const SizedBox()
                                        : shortBtn(
                                            title: "use".tr,
                                            active: true,
                                            onTap: () => controller.handleSelect(model),
                                          )
                                  ],
                                ),
                                // smallestTitle((model.description ?? "")),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () => controller.toDetail(model),
                child: Padding(
                  padding: const EdgeInsets.only(top: 32.0, left: 20.0, right: 20.0),
                  child: Row(
                    children: [
                      Expanded(child: smallestTitle("coupon_expired_at".tr + (model.endDate ?? ""))),
                      smallestTitle("coupon_detail".tr),
                      Padding(
                        padding: const EdgeInsets.only(left: 2.0),
                        child: assetImage(AssetsInfo.iconExpandCoupon, width: 5.0),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
