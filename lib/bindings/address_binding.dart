import 'package:Heals/controllers/address_controller.dart';
import 'package:get/instance_manager.dart';

class AddressBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AddressController());
  }
}
