import 'package:Heals/controllers/edit_address_controller.dart';
import 'package:get/instance_manager.dart';

class EditAddressBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EditAddressController());
  }
}
