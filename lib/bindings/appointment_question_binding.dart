import 'package:Heals/controllers/appointment_question_controller.dart';
import 'package:get/instance_manager.dart';

class AppointmentQuestionBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AppointmentQuestionController());
  }
}
