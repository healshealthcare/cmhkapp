import 'package:Heals/controllers/coupon_controller.dart';
import 'package:get/instance_manager.dart';

class CouponBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CouponController());
  }
}
