import 'package:Heals/controllers/queue_success_controller.dart';
import 'package:get/instance_manager.dart';

class QueueSuccessBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(QueueSuccessController());
  }
}
