import 'package:Heals/controllers/appointment_success_controller.dart';
import 'package:get/instance_manager.dart';

class AppointmentSuccessBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AppointmentSuccessController());
  }
}
