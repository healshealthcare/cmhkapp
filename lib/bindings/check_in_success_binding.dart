import 'package:Heals/controllers/check_in_success_controller.dart';
import 'package:get/instance_manager.dart';

class CheckInSuccessBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CheckInSuccessController());
  }
}
