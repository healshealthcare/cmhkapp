import 'package:Heals/controllers/appointment_deposit_controller.dart';
import 'package:get/instance_manager.dart';

class AppointmentDepositBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AppointmentDepositController());
  }
}
