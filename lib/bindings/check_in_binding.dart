import 'package:Heals/controllers/check_in_controller.dart';
import 'package:get/instance_manager.dart';

class CheckInBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CheckInController());
  }
}
