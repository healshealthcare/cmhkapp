import 'package:Heals/controllers/appointment_controller.dart';
import 'package:get/instance_manager.dart';

class AppointmentBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AppointmentController());
  }
}
