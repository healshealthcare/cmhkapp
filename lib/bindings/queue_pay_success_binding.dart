import 'package:Heals/controllers/queue_pay_success_controller.dart';
import 'package:get/instance_manager.dart';

class QueuePaySuccessBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(QueuePaySuccessController());
  }
}
