import 'package:Heals/controllers/family_controller.dart';
import 'package:get/instance_manager.dart';

class FamilyBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(FamilyController());
  }
}
