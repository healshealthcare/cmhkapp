import 'package:Heals/controllers/payment_list_controller.dart';
import 'package:get/instance_manager.dart';

class PaymentListBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PaymentListController());
  }
}
