import 'package:Heals/controllers/appointment_detail_controller.dart';
import 'package:get/instance_manager.dart';

class AppointmentDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AppointmentDetailController());
  }
}
