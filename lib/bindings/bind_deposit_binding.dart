import 'package:Heals/controllers/bind_deposit_controller.dart';
import 'package:get/instance_manager.dart';

class BindDepositBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(BindDepositController());
  }
}
