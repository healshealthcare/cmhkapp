import 'package:Heals/controllers/queue_detail_controller.dart';
import 'package:get/instance_manager.dart';

class QueueDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(QueueDetailController());
  }
}
