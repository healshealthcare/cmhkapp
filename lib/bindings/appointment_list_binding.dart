import 'package:Heals/controllers/appointment_list_controller.dart';
import 'package:get/instance_manager.dart';

class AppointmentListBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AppointmentListController());
  }
}
