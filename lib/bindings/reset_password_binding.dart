import 'package:Heals/controllers/reset_password_controller.dart';
import 'package:get/instance_manager.dart';

class ResetPasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ResetPasswordController());
  }
}
