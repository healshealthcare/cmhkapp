import 'package:Heals/controllers/medicine_controller.dart';
import 'package:get/instance_manager.dart';

class MedicineBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(MedicineController());
  }
}
