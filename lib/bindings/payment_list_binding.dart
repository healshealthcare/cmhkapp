import 'package:Heals/controllers/payment_detail_controller.dart';
import 'package:get/instance_manager.dart';

class PaymentDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PaymentDetailController());
  }
}
