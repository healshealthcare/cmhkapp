import 'package:Heals/controllers/chat_controller.dart';
import 'package:get/instance_manager.dart';

class ChatBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ChatController());
  }
}
