import 'package:Heals/controllers/forger_password_controller.dart';
import 'package:get/instance_manager.dart';

class ForgetPasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ForgerPasswordController());
  }
}
