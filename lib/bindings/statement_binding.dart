import 'package:Heals/controllers/statement_controller.dart';
import 'package:get/instance_manager.dart';

class StatementBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(StatementController());
  }
}
