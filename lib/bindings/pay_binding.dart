import 'package:Heals/controllers/pay_controller.dart';
import 'package:get/instance_manager.dart';

class PayBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PayController());
  }
}
