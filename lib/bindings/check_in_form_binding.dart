import 'package:Heals/controllers/check_in_form_controller.dart';
import 'package:get/instance_manager.dart';

class CheckInFormBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CheckInFormController());
  }
}
