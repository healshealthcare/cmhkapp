import 'package:Heals/controllers/coupon_detail_controller.dart';
import 'package:get/instance_manager.dart';

class CouponDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CouponDetailController());
  }
}
