import 'package:Heals/controllers/setting_controller.dart';
import 'package:get/instance_manager.dart';

class SettingBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SettingController());
  }
}
