import 'package:Heals/controllers/tabbar_controller.dart';
import 'package:get/instance_manager.dart';

class TabbarBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(TabbarController());
  }
}
