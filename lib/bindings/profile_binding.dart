import 'package:Heals/controllers/profile_controller.dart';
import 'package:get/instance_manager.dart';

class ProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ProfileController());
  }
}
