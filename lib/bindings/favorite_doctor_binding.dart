import 'package:Heals/controllers/favorite_doctor_controller.dart';
import 'package:get/instance_manager.dart';

class FavoriteDoctorBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(FavoriteDoctorController());
  }
}
