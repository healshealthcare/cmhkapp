import 'package:Heals/controllers/doctor_detail_controller.dart';
import 'package:get/instance_manager.dart';

class DoctorDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(DoctorDetailController());
  }
}
