import 'dart:async';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/tabbar_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/home_data_model.dart';
import 'package:Heals/model/home_model.dart';
import 'package:Heals/model/visit_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/services/notify_point_service.dart';
import 'package:Heals/utils/custom_exception.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class HomeController extends BaseController {
  final String tag = "HOME_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  final NotifyPointService notifyPointService = Get.find<NotifyPointService>();
  var isLoading = true.obs;
  var feeds = [].obs;
  var tabIndex = 0.obs;
  // var isAgreeTerms = false.obs;
  Timer? timer;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getHome();
    // timer = Timer.periodic(const Duration(seconds: 2), (_timer) {
    //   // 正式还是注释吧，浪费流量。
    //   if (Get.find<AppController>().isLogin) {
    //     getHome();
    //   }
    // });
  }

  void updateData() {
    getHome();
  }

  void delayGetHome() {
    Future.delayed(const Duration(seconds: 5), () {
      getHome();
    });
  }

  Future<void> getHome() async {
    try {
      final HomeDataModel? data = await apiService.toFetch<HomeDataModel>(ApiCfg.homeUrl);
      if (data == null) return;
      if (data.errorCode != null) return;
      feeds(data.data);
    } catch (e) {}
    isLoading(false);
  }

  void changeTab() {
    Get.find<TabbarController>().tabIndex(1);
  }

  void visitCheckIn(HomeModel homeModel) async {
    Get.toNamed(Routes.queueDetail,
        arguments: {"doctorId": homeModel.doctorId, "visitToken": homeModel.vistToken, "clinicId": homeModel.clinicId});
  }

  void checkIn(HomeModel homeModel) async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.visitCheckinUrl,
          arguments: {"clinic_id": homeModel.clinicId, "doctor_id": homeModel.doctorId});
      if (data == null) return;
      if (data.errorCode != null) return;
      getHome();
      Get.toNamed(Routes.queueDetail, arguments: {
        "doctorId": homeModel.doctorId,
        "visitToken": homeModel.vistToken,
        "clinicId": homeModel.clinicId
      });
    } on ApiException catch (e) {
      if (e.code == 2200) {
        EasyLoading.showToast("eticket_again_tip".tr);
      }
    }

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void handleVisit() {
    showCuzDialog(
      '',
      autoBack: false,
      barrierDismissible: false,
      titleWidget: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0),
              child: titleLargeMainTxt('video_title'.tr),
            ),
            titleTxt('video_tip'.tr, maxLines: 100),
            const SizedBox(height: 20.0),
            // line(),
            // const SizedBox(height: 20.0),
            // GestureDetector(
            //   onTap: () => isAgreeTerms.toggle(),
            //   child: Row(
            //     children: [
            //       Obx(
            //         () => Padding(
            //           padding: const EdgeInsets.only(right: 8.0),
            //           child:
            //               assetImage18(isAgreeTerms.isTrue ? AssetsInfo.selectIconHlIcon : AssetsInfo.selectIconNlIcon),
            //         ),
            //       ),
            //       Expanded(child: titleTxt("video_check".tr, maxLines: 100)),
            //     ],
            //   ),
            // )
          ],
        ),
      ),
      // onConfirm: () => Get.toNamed(Routes.chat),
      onConfirm: () {
        // if (isAgreeTerms.isTrue) {
        Get.back();
        //   isAgreeTerms(false);
        Get.toNamed(Routes.search, arguments: 'visit');
        // } else {
        //   EasyLoading.showToast("video_must_check_tip".tr);
        // }
      },
    );
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
