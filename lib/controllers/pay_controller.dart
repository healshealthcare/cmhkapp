import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/model/payment_data_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class PayController extends BaseController {
  final String tag = "PAY_CONTROLLER";
  var linkUrl = ''.obs;
  var isErr = false.obs;
  String transactionId = '';
  String? doctorId = '';
  String? clinicId = '';
  String? visitToken = '';
  int? appointmentId;
  String? type = '';
  int? userId;

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      transactionId = Get.arguments["transactionId"];
      appointmentId = Get.arguments["appointmentId"];
      visitToken = Get.arguments["visitToken"];
      doctorId = Get.arguments["doctorId"];
      type = Get.arguments["type"];
      userId = Get.arguments["userId"];
      if (Get.arguments.containsKey("clinicId")) {
        clinicId = Get.arguments["clinicId"];
      }
    }
    getData();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final PaymentDataModel? data = await apiService.toFetch<PaymentDataModel>(ApiCfg.payUrl, arguments: {
        "transaction_id": transactionId,
      });
      if (EasyLoading.isShow) EasyLoading.dismiss();

      if (data == null) return;
      if (EasyLoading.isShow) EasyLoading.dismiss();
      if (data.errorCode != null || data.data == null) {
        return;
      }
      linkUrl(data.data!.paymentUrl);
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
