import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/address_controller.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/address_data_model.dart';
import 'package:Heals/model/address_model.dart';
import 'package:Heals/model/address_tree_model.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class EditAddressController extends BaseController {
  final String tag = "EditAddress_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var phoneItem = SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false).obs;

  final List<SelectItemModel> areaNum = [
    SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 3, title: '853'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 5, title: '86'.tr, cancel: false, isSelected: false),
  ];
  TextEditingController receipientCtrl = TextEditingController();
  TextEditingController phoneCtrl = TextEditingController();
  TextEditingController street1Ctrl = TextEditingController();
  TextEditingController street2Ctrl = TextEditingController();
  TextEditingController areaCtrl = TextEditingController();
  TextEditingController regionCtrl = TextEditingController();
  TextEditingController districtCtrl = TextEditingController();
  List<AddressTreeModel> areaList = [];
  List<AddressTreeModel> regionList = [];
  List<AddressTreeModel> districtList = [];
  List<SelectItemModel> areaSelect = [];
  List<SelectItemModel> regionSelect = [];
  List<SelectItemModel> districtSelect = [];
  var areaModel = AddressTreeModel().obs;
  var regionModel = AddressTreeModel().obs;
  var districtModel = AddressTreeModel().obs;
  int id = 0;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null && Get.arguments.runtimeType == AddressModel) {
      AddressModel model = Get.arguments;
      if (model.id != null && model.id != 0) {
        id = model.id!;
      }
      receipientCtrl.text = model.recipientName ?? '';
      phoneCtrl.text = model.recipientPhone == null ? '' : model.recipientPhone!.split("_")[1];
      street1Ctrl.text = model.streetAddr1 ?? '';
      street2Ctrl.text = model.streetAddr2 ?? '';
      areaCtrl.text = model.area ?? '';
      regionCtrl.text = model.region ?? '';
      districtCtrl.text = model.district ?? '';
    }
    getAddress();
  }

  void getAddress() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    final AddressDataModel? data = await apiService.toFetch<AddressDataModel>(ApiCfg.addressUrl);
    if (EasyLoading.isShow) EasyLoading.dismiss();
    if (data == null) return;
    if (data.errorCode != null) return;
    areaList = data.dataTree!.firstWhere((item) => item.name == 'Hong Kong' || item.name == '香港').childs ?? [];
    for (var item in areaList) {
      areaSelect.add(SelectItemModel(id: item.id, title: item.name));
      if (item.name == areaCtrl.text) {
        areaModel(item);
        regionList = areaModel().childs ?? [];
        for (var itemReg in regionList) {
          regionSelect.add(SelectItemModel(id: itemReg.id, title: itemReg.name));
          if (itemReg.name == regionCtrl.text) {
            regionModel(itemReg);
            districtList = itemReg.childs ?? [];
            for (var itemDis in districtList) {
              districtSelect.add(SelectItemModel(id: itemDis.id, title: itemDis.name));
            }
          }
        }
      }
    }
  }

  void addAddress() async {
    if (areaCtrl.text.isEmpty ||
        regionCtrl.text.isEmpty ||
        districtCtrl.text.isEmpty ||
        street1Ctrl.text.isEmpty ||
        phoneCtrl.text.isEmpty) {
      EasyLoading.showToast("address_fill_tip".tr);
      return;
    }
    if (id != 0) {
      try {
        final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.updateAddressUrl, arguments: {
          "id": id,
          "city": 'Hong Kong',
          "area": areaCtrl.text,
          "region": regionCtrl.text,
          "district": districtCtrl.text,
          "street_addr_1": street1Ctrl.text,
          "street_addr_2": street2Ctrl.text,
          "recipient_name": receipientCtrl.text,
          "recipient_phone": phoneItem().title! + '_' + phoneCtrl.text,
          "is_default": 0,
        });
        if (data == null) return;
        if (data.errorCode != null) return;
        if (Get.isRegistered<AddressController>()) Get.find<AddressController>().getMyAddress();
        Get.back();
      } catch (e) {}
    } else {
      try {
        final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.addAddressUrl, arguments: {
          "city": 'Hong Kong',
          "area": areaCtrl.text,
          "region": regionCtrl.text,
          "district": districtCtrl.text,
          "street_addr_1": street1Ctrl.text,
          "street_addr_2": street2Ctrl.text,
          "recipient_name": receipientCtrl.text,
          "recipient_phone": phoneItem().title! + '_' + phoneCtrl.text,
          "is_default": 0,
        });
        if (data == null) return;
        if (data.errorCode != null) return;
        if (Get.isRegistered<AddressController>()) Get.find<AddressController>().getMyAddress();
        Get.back();
      } catch (e) {}
    }
  }

  void showSelect() {
    Get.bottomSheet(SelectDialog(
        contentList: areaNum,
        isBlackBg: true,
        callback: (item) {
          Get.back();
          if (item.id == -1) return;
          phoneItem(item);
        }));
  }

  void tapArea() {
    Get.bottomSheet(
      SelectDialog(
        contentList: areaSelect,
        isBlackBg: true,
        callback: (item) {
          Get.back();
          if (item.id == -1) return;
          regionSelect = [];
          areaModel(areaList.firstWhere((model) => model.id == item.id));
          regionList = areaModel().childs ?? [];
          regionModel(AddressTreeModel());
          districtModel(AddressTreeModel());
          regionCtrl.text = '';
          districtCtrl.text = '';
          districtSelect = [];
          areaCtrl.text = areaModel().name!;
          for (var item in regionList) {
            regionSelect.add(SelectItemModel(id: item.id, title: item.name));
          }
        },
      ),
    );
  }

  void tapRegion() {
    if (regionSelect.isEmpty) return;
    Get.bottomSheet(
        SelectDialog(
          contentList: regionSelect,
          isBlackBg: true,
          callback: (item) {
            Get.back();
            if (item.id == -1) return;
            districtSelect = [];
            regionModel(regionList.firstWhere((model) => model.id == item.id));
            districtList = regionList.firstWhere((model) => model.id == item.id).childs ?? [];
            districtModel(AddressTreeModel());
            districtCtrl.text = '';
            regionCtrl.text = regionModel().name!;
            for (var item in districtList) {
              districtSelect.add(SelectItemModel(id: item.id, title: item.name));
            }
          },
        ),
        isScrollControlled: true);
  }

  void tapDistrict() {
    if (districtSelect.isEmpty) return;
    Get.bottomSheet(
        SelectDialog(
          contentList: districtSelect,
          isBlackBg: true,
          callback: (item) {
            Get.back();
            if (item.id == -1) return;
            districtModel(districtList.firstWhere((model) => model.id == item.id));
            districtCtrl.text = districtModel().name!;
          },
        ),
        isScrollControlled: true);
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
