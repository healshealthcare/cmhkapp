import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/appointment_controller.dart';
import 'package:Heals/model/coupon_detail_data_model.dart';
import 'package:Heals/model/coupon_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class CouponDetailController extends BaseController {
  final String tag = "COUPON_DETAIL_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var couponModel = CouponModel().obs;
  bool isSelectMode = false;

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      couponModel(Get.arguments["coupon"]);
      if (Get.arguments.containsKey("mode")) {
        isSelectMode = Get.arguments["mode"] == 'isSelect';
      }
    }
    getCoupon();
  }

  void getCoupon() async {
    try {
      final CouponDetailDataModel? data = await apiService
          .toFetch<CouponDetailDataModel>(ApiCfg.couponDetailUrl, arguments: {"coupon_code": couponModel().id});
      if (EasyLoading.isShow) EasyLoading.dismiss();
      if (data == null || data.data == null) return;
      if (data.errorCode != null) return;
      couponModel.update((val) {
        val!.couponClause = data.data!.couponClause;
      });
    } catch (e) {}

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void handleSelect() {
    if (Get.isRegistered<AppointmentController>()) {
      Get.find<AppointmentController>().couponModel(couponModel());
      Get.back();
      Get.back();
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
