import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/appointment_controller.dart';
import 'package:Heals/model/coupon_data_model.dart';
import 'package:Heals/model/coupon_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class CouponController extends BaseController with GetSingleTickerProviderStateMixin {
  final String tag = "Coupon_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  late TabController tabController;
  var isLoading = true.obs;
  var couponAllList = [].obs;
  var couponValidList = [].obs;
  var couponUsedList = [].obs;
  var couponInvalidList = [].obs;
  bool isSelectMode = false;

  @override
  void onInit() {
    tabController = TabController(vsync: this, length: 4);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      if (Get.arguments.containsKey("mode")) {
        isSelectMode = Get.arguments["mode"] == 'isSelect';
      }
    }
    getCoupon();
  }

  void getCoupon() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final CouponDataModel? data = await apiService.toFetch<CouponDataModel>(ApiCfg.couponGroupListUrl);
      if (EasyLoading.isShow) EasyLoading.dismiss();
      if (data == null || data.data == null) return;
      if (data.errorCode != null) return;
      couponAllList(data.data!.allData ?? []);
      couponValidList(data.data!.validData ?? []);
      couponUsedList(data.data!.usedData ?? []);
      couponInvalidList(data.data!.invalidData ?? []);
      isLoading(false);
    } catch (e) {}

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void handleSelect(couponModel) {
    if (Get.isRegistered<AppointmentController>()) {
      Get.find<AppointmentController>().couponModel(couponModel);
      Get.back();
    }
  }

  void toDetail(CouponModel model) {
    Get.toNamed(Routes.couponDetail, arguments: {"coupon": model, 'mode': isSelectMode ? 'isSelect' : ''});
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
