import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/appointment_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/relationship_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class FamilyController extends BaseController {
  final String tag = "family_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var familyList = [].obs;
  bool isSelectMode = false;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      isSelectMode = Get.arguments;
    }
    getMyfamily();
  }

  void getMyfamily() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final RelationshipDataModel? data = await apiService.toFetch<RelationshipDataModel>(ApiCfg.relationshipUrl);
      if (data == null) return;
      if (data.errorCode != null) return;
      familyList(data.data);
    } catch (e) {}

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void handleFamily(index) async {
    if (isSelectMode) {
      Get.back();
      Get.find<AppointmentController>().familyModel(familyList[index]);
    } else {
      Get.toNamed(Routes.family, arguments: familyList[index]);
    }
  }

  void deleteFamily(index) async {
    showCuzDialog("delete_family_tip".tr, onConfirm: () async {
      EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
      try {
        final BaseModel? data = await apiService
            .toFetch<BaseModel>(ApiCfg.deleteFamilyUrl, arguments: {"dependent_id": familyList[index].id});
        if (data == null) return;
        if (data.errorCode != null) return;
        familyList.removeAt(index);
      } catch (e) {}
      if (EasyLoading.isShow) EasyLoading.dismiss();
    });
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
