import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/appointment_deposit_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import 'base_controller.dart';

class BindDepositController extends BaseController {
  final String tag = "BindDeposit_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var teCtrl = TextEditingController().obs;
  var teHolderCtrl = TextEditingController().obs;
  var teCvvCtrl = TextEditingController().obs;
  var date = DateTime.now().obs;
  var year = ''.obs;
  var month = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  void bindUnion() async {
    if (teCtrl().text.isEmpty || teHolderCtrl().text.isEmpty || year.isEmpty || teCvvCtrl().text.isEmpty) {
      EasyLoading.showToast("信息填写不完整");
      return;
    }
    if (!(teCtrl().text.length >= 13 && teCtrl().text.length <= 19)) {
      EasyLoading.showToast("卡号不正确");
      return;
    }
    if (teCvvCtrl().text.length != 3) {
      EasyLoading.showToast("cvv不正确");
      return;
    }
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.unionpayAddUrl, arguments: {
        "account_no": teCtrl().text,
        "name": teHolderCtrl().text,
        "cvv": teCvvCtrl().text,
        "expire_date": "${year.value}${month.value}"
      });
      if (data == null) return;
      if (data.errorCode != null) return;
      Get.back();
      if (Get.isRegistered<AppointmentDepositController>()) {
        Get.find<AppointmentDepositController>().getCardInfo();
      }
    } catch (e) {
      print(e);
    }
    EasyLoading.showToast("success");
  }

  void showCanlender() {
    Get.bottomSheet(SfDateRangePicker(
      backgroundColor: Colors.white,
      confirmText: "submit",
      enablePastDates: false,
      selectionColor: HColors.mainColor,
      selectionMode: DateRangePickerSelectionMode.single,
      showActionButtons: true,
      onSubmit: (args) {
        DateRangePickerSelectionChangedArgs a = DateRangePickerSelectionChangedArgs(args);
        Get.back();
        DateTime datetime = a.value;
        date(a.value);
        year(datetime.year.toString().substring(2, 4));
        month(datetime.month.toString());
      },
      onCancel: () {
        Get.back();
      },
    ));
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
