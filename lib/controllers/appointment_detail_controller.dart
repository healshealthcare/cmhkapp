import 'dart:async';
import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/appointment_list_controller.dart';
import 'package:Heals/controllers/home_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/booking_data_model.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/cancel_data_model.dart';
import 'package:Heals/model/note_data_model.dart';
import 'package:Heals/model/note_key_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/dialog_content_widget.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class AppointmentDetailController extends BaseController {
  final String tag = "CHECK_IN_CONTROLLER";
  var appointmentId = 0.obs;
  AppController appSrv = Get.find<AppController>();
  var appointmentModel = BookingModel().obs;
  Timer? timer;
  var noteKeyModel = NoteKeyModel().obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      appointmentId(Get.arguments);
    }
    timer = Timer.periodic(const Duration(seconds: Config.appointmentimer), (_timer) {
      getData();
    });
    getData();
    getNotes();
  }

  void getData() async {
    final BookingDataModel? data =
        await apiService.toFetch<BookingDataModel>(ApiCfg.myBookingUrl, arguments: {"id": appointmentId.value});
    if (data == null) return;
    if (data.errorCode != null) return;

    appointmentModel(data.bookingModel);
  }

  void getNotes() async {
    final NoteDataModel? data = await apiService.toFetch<NoteDataModel>(ApiCfg.appointmentNotesUrl);
    if (data == null) return;
    if (data.errorCode != null) return;
    noteKeyModel(data.data);
  }

  void cancelAppointment() async {
    final CancelDataModel? data = await apiService
        .toFetch<CancelDataModel>(ApiCfg.cancelAppointUrl, arguments: {"appointment_id": appointmentId.value});
    if (data == null) return;
    if (data.errorCode != null) return;
    getData();
    Get.find<HomeController>().updateData();
    if (data.cancelFine != null && data.cancelFine!.transactionId != null) {
      Get.offAndToNamed(Routes.pay, arguments: {"transactionId": data.cancelFine!.transactionId});
    }
    if (Get.isRegistered<AppointmentListController>()) {
      Get.find<AppointmentListController>().getData();
    }
  }

  void replyAppoDialog(int accept) {
    showCuzDialog('',
        titleWidget: Column(
          children: [
            titleLargeMainTxt("appointment_accept_note_title".tr, textAlign: TextAlign.center),
            DialogContentWidget(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Text(
                  "appointment_accept_note".tr,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    color: HColors.mainColorText,
                    fontSize: 16.0,
                    fontFamily: 'Comfortaa',
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            )
          ],
        ),
        textAlign: TextAlign.left,
        onlySubmitBtn: true, onConfirm: () {
      replyAppointment(accept);
    });
  }

  void replyAppointment(int accept) async {
    final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.replyAppointUrl,
        arguments: {"appointment_id": appointmentId.value, "accept": accept});
    if (data == null) return;
    if (data.errorCode != null) return;
    getData();
    Get.find<HomeController>().updateData();
    if (Get.isRegistered<AppointmentListController>()) {
      Get.find<AppointmentListController>().getData();
    }
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
