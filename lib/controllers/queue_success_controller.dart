import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/doctor_data_model.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class QueueSuccessController extends BaseController {
  final String tag = "ACCOUNT_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var doctorData = DoctorModel().obs;
  var clinicList = [].obs;
  var doctorId = '';

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      doctorId = Get.arguments;
    }

    getData();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final DoctorDataModel? data = await apiService.toFetch<DoctorDataModel>(ApiCfg.doctorListUrl, arguments: {
        "page": 1,
        "doctor_id": doctorId,
      });
      if (EasyLoading.isShow) EasyLoading.dismiss();

      if (data == null) return;
      if (data.errorCode != null || data.data!.isEmpty) {
        return;
      }
      DoctorModel doctorModel = data.data![0];
      doctorData(doctorModel);
      // doctorInfo(doctorModel.doctor);
      // clinic(doctorModel.clinic);
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
