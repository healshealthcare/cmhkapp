import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/message_data_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class MeassageController extends BaseController with SingleGetTickerProviderMixin {
  final String tag = "HEALTH_SUPPORT_CONTROLLER";
  ScrollController scrollControllerLeft = ScrollController();
  ScrollController scrollControllerRight = ScrollController();
  late TabController tabController;
  int todoPage = 1;
  int noticePage = 1;
  var todoList = [].obs;
  var noticeList = [].obs;
  var isLoadingMore = false.obs;
  var isLoadAll = false.obs;
  var isLoadingRMore = false.obs;
  var isLoadAllR = false.obs;
  var tabIndex = 0.obs;

  @override
  void onInit() {
    tabController = TabController(length: 2, vsync: this);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    scrollControllerLeft.addListener(() {
      if (isLoadingMore.isTrue || isLoadAll.isTrue) return;
      if (scrollControllerLeft.position.pixels == scrollControllerLeft.position.maxScrollExtent) {
        isLoadingMore(true);
        getData('TODO');
      }
    });
    scrollControllerRight.addListener(() {
      if (isLoadingRMore.isTrue || isLoadAllR.isTrue) return;
      if (scrollControllerRight.position.pixels == scrollControllerRight.position.maxScrollExtent) {
        isLoadingMore(true);
        getData('NOTICE');
      }
    });
    getData('TODO');
    getData('NOTICE');
  }

  initData() {
    todoPage = 1;
    noticePage = 1;
    getData('TODO');
    getData('NOTICE');
  }

  Future<void> getData(category) async {
    try {
      final MessageDataModel? data = await apiService.toFetch<MessageDataModel>(ApiCfg.messageUrl,
          arguments: {'category': category, 'page': category == 'TODO' ? todoPage : noticePage});
      if (data == null) return;
      if (data.errorCode != null) return;
      handleData(data, category);
      isLoadingMore(false);
      isLoadingRMore(false);
    } catch (e) {}
  }

  Future<void> markReadd(id) async {
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.markReadUrl, arguments: {'message_id': id});
      if (data == null) return;
      if (data.errorCode != null) return;
      initData();
    } catch (e) {}
  }

  void handleData(MessageDataModel data, type) {
    if (type == 'TODO') {
      todoPage = data.data!.curPage! + 1;
      if (data.data!.curPage == 1) {
        todoList(data.data!.data);
      } else {
        todoList.addAll(data.data!.data ?? []);
      }
      if (data.data!.data!.length < 20) {
        isLoadAll(true);
      }
    } else {
      noticePage = data.data!.curPage! + 1;
      if (data.data!.curPage == 1) {
        noticeList(data.data!.data);
      } else {
        noticeList.addAll(data.data!.data ?? []);
      }
      if (data.data!.data!.length < 20) {
        isLoadAllR(true);
      }
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
