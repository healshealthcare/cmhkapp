import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/home_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/booking_data_model.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/cancel_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class AppointmentListController extends BaseController with SingleGetTickerProviderMixin {
  final String tag = "HEALTH_SUPPORT_CONTROLLER";
  var teCtrl = TextEditingController().obs;
  final FocusNode focusNode = FocusNode();
  late TabController tabController;
  var appointmentList = [].obs;
  var appointmentUpcommingList = [].obs;
  var appointmentPenddingList = [].obs;
  var appointmentPastList = [].obs;

  @override
  void onInit() {
    tabController = TabController(length: 4, vsync: this);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getData();
  }

  Future<void> refreshData() async {
    getData();
  }

  void getData({String? doctorName}) async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    var allList = [];
    var upcomingList = [];
    var pendingList = [];
    var pastList = [];
    try {
      final BookingDataModel? data =
          await apiService.toFetch<BookingDataModel>(ApiCfg.myBookingUrl, arguments: {"query": doctorName});
      if (data == null) return;
      if (data.errorCode != null) return;
      appointmentList(data.data);
      for (var i = 0; i < appointmentList.length; i++) {
        BookingModel bookingModel = appointmentList[i];
        if (isUpcoming(bookingModel)) {
          upcomingList.add(bookingModel);
          continue;
        }
        if (isPenddding(bookingModel)) {
          pendingList.add(bookingModel);
          continue;
        }
        if (isPast(bookingModel)) {
          pastList.add(bookingModel);
          continue;
        }
      }
      appointmentUpcommingList(upcomingList);
      appointmentPenddingList(pendingList);
      appointmentPastList(pastList);
    } catch (e) {
      print(e);
    }

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void cancelAppointment(appointmentId) async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    final CancelDataModel? data = await apiService
        .toFetch<CancelDataModel>(ApiCfg.cancelAppointUrl, arguments: {"appointment_id": appointmentId});
    if (EasyLoading.isShow) EasyLoading.dismiss();
    if (data == null) return;
    if (data.errorCode != null) return;
    if (data.cancelFine != null && data.cancelFine!.transactionId != null) {
      Get.toNamed(Routes.pay, arguments: {"transactionId": data.cancelFine!.transactionId});
    }
    getData();
  }

  void replyAppointment(appointmentId, accept) async {
    final BaseModel? data = await apiService
        .toFetch<BaseModel>(ApiCfg.replyAppointUrl, arguments: {"appointment_id": appointmentId, "accept": accept});
    if (data == null) return;
    if (data.errorCode != null) return;
    getData();
    Get.find<HomeController>().updateData();
  }

  void handleSearch() {
    getData(doctorName: teCtrl().text);
  }

  void handleChoose(BookingModel bookingModel) {
    if (isQuestionnaire(bookingModel.state!)) {
      Get.toNamed(Routes.appointmentQuestion, arguments: bookingModel.id);
      return;
    }
    if (isDeposit(bookingModel.state!)) {
      Get.toNamed(Routes.appointmentDeposit,
          arguments: {"appointmentId": bookingModel.id, "transactionId": bookingModel.transactionId});
      return;
    }
    Get.toNamed(Routes.appointmentDetail, arguments: bookingModel.id);
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
