import 'dart:async';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class ForgerPasswordController extends BaseController with SingleGetTickerProviderMixin {
  @override
  final String tag = "resetPassword_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  TextEditingController forgetPasswordPhoneteCtrl = TextEditingController();
  TextEditingController forgetPasswordCodeteCtrl = TextEditingController();
  TextEditingController forgetPasswordPassteCtrl = TextEditingController();
  TextEditingController forgetPasswordConfirmPassteCtrl = TextEditingController();
  FocusNode focusNodePhone = FocusNode();
  // FocusNode focusNodeSms = FocusNode();
  // FocusNode focusNodePass = FocusNode();
  // FocusNode focusNodeConfirmPass = FocusNode();

  var step = 1.obs;

  var selectItemModel = SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false).obs;

  final List<SelectItemModel> areaNum = [
    SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 3, title: '853'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 5, title: '86'.tr, cancel: false, isSelected: false),
  ];

  var countDown = 0.obs;
  Timer? cdTimer;
  String msgCode = '';
  var isAgree = false.obs;

  void showSelect() {
    areaNum.firstWhere((item) => item.id == selectItemModel().id).isSelected = true;
    Get.bottomSheet(SelectDialog(
        contentList: areaNum,
        isBlackBg: true,
        callback: (item) {
          Get.back();
          if (item.id == -1) return;
          selectItemModel(item);
        }));
  }

  void getSms() async {
    RegExp regExpPhone = RegExp(Constants.phoneRegex);
    if (!regExpPhone.hasMatch('${selectItemModel().title}' + forgetPasswordPhoneteCtrl.text)) {
      EasyLoading.showToast("login_phone_correct_tip".tr);
      return;
    }

    if (countDown > 0) return;
    countDown(59);
    cdTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (countDown.value <= 0) {
        countDown(0);
        timer.cancel();
      } else {
        countDown(countDown.value - 1);
      }
    });
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.forgetPswUrl, arguments: {
        "phone": '${selectItemModel().title}_' + forgetPasswordPhoneteCtrl.text,
      });
      if (data == null) return;
      // if (data.errorCode != null) return;
      EasyLoading.showToast("login_msg_sent_tip".tr);
    } catch (e) {}
  }

  void resetPassword() async {
    if (forgetPasswordPassteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_pwd_input_tip".tr);
      return;
    }
    if (forgetPasswordPassteCtrl.text.length < 8 || forgetPasswordPassteCtrl.text.length > 16) {
      EasyLoading.showToast("login_pwd_input_tip".tr);
      return;
    }

    String regex = "^(?:(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).{6,12}";
    RegExp regExp = RegExp(regex);
    if (!regExp.hasMatch(forgetPasswordPassteCtrl.text)) {
      EasyLoading.showToast("login_pwd_correct_tip".tr);
      return;
    }

    final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.resetPassUrl, arguments: {
      "token": msgCode,
      "password": forgetPasswordPassteCtrl.text,
      "password_confirmation": forgetPasswordConfirmPassteCtrl.text,
    });

    if (data == null) return;
    if (data.errorCode != null) return;
    Get.offAllNamed(Routes.login);
  }

  void handleSubmit() {
    if (step.value == 1) {
      step(2);
      getSms();
    } else if (step.value == 2) {
      step(3);
    } else {
      resetPassword();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    cdTimer?.cancel();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
