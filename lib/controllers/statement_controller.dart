import 'package:Heals/config/config.dart';
import 'package:Heals/utils/utils.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class StatementController extends BaseController {
  final String tag = "STATEMENT_CONTROLLER";
  var linkUrl = ''.obs;
  var isErr = false.obs;
  var xment = ''.obs;

  @override
  void onInit() {
    // exchangeURL  peaceURL
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black, dismissOnTap: true);
    String? localStr = box.read<String>('local');
    String countryCode = 'TW';
    if (localStr != null && localStr.contains("_")) {
      String str2 = localStr.split("_")[1];
      countryCode = str2.toUpperCase();
    } else {
      countryCode = getCountryCode();
    }

    if (Get.arguments != null) {
      switch (Get.arguments) {
        case 'privacy':
          xment("page_privacy_title".tr);
          linkUrl(Config.privacyUrl[countryCode]);
          break;
        case 'statement':
          xment("page_statement_title".tr);
          linkUrl(Config.termUrl[countryCode]);
          break;
        case 'payment':
          xment("page_payment_title".tr);
          linkUrl(Config.paymentUrl[countryCode]);
          break;
        case 'help':
          xment("page_help_title".tr);
          linkUrl(Config.helpUrl[countryCode]);
          break;
        case 'delivery':
          xment("page_statement_title".tr);
          // linkUrl(Config.deliveryUrl);
          linkUrl(Config.termUrl[countryCode]);
          break;
        case 'commercial':
          xment("page_commercial_title".tr);
          // linkUrl(Config.deliveryUrl);
          linkUrl(Config.commercialUrl[countryCode]);
          break;
        default:
          xment("");
          // linkUrl(Config.termUrl[Get.locale!.countryCode]);
          break;
      }
    }
    // linkUrl("https://heals-nc1.s3.ap-east-1.amazonaws.com/cmhk-app/tctc.html");
    // EasyLoading.showToast(linkUrl.value);
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
