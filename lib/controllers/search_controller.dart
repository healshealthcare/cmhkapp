import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/district_data_model.dart';
import 'package:Heals/model/district_model.dart';
import 'package:Heals/model/doctor_data_model.dart';
import 'package:Heals/model/service_model.dart';
import 'package:Heals/model/specialty_data_model.dart';
import 'package:Heals/model/specialty_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class SearchController extends BaseController {
  @override
  final String tag = "SEARCH_CONTROLLER";
  var teCtrl = TextEditingController().obs;
  AppController appSrv = Get.find<AppController>();
  ScrollController scrollController = ScrollController();
  final FocusNode focusNode = FocusNode();
  var doctorList = [].obs;
  var specialtyValue = 0.obs;
  var sevicesValue = 0.obs;
  var districtValue = 0.obs;
  var filterValue = 0.obs;
  var specityList = [].obs;
  var districtList = [].obs;
  var serviceList = [].obs;
  var filterList = [
    ServiceModel(id: 1, name: "filter_distance".tr),
    ServiceModel(id: 2, name: "filter_favorite".tr),
    ServiceModel(id: 3, name: "filter_open".tr)
  ].obs;
  var displaySelect = 0.obs;
  int page = 1;
  var isLoadAll = false.obs;
  var isLoading = true.obs;
  var isLoadingMore = false.obs;
  var specialtyIndex = 0.obs;
  var serviceIndex = 0.obs;
  var districtIndex = 0.obs;
  var shiftIndex = 0.obs;
  var selectVisitFix = false.obs;

  @override
  void onReady() async {
    super.onReady();
    if (Get.arguments != null) {
      if (Get.arguments == 'visit') {
        selectVisitFix(true);
      }
    }
    scrollController.addListener(() {
      if (isLoadingMore.isTrue || isLoadAll.isTrue) return;
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
        isLoadingMore(true);
        getData();
      }
    });
    getData();
    teCtrl.value.addListener(() {
      page = 1;
      if (teCtrl.value.text.isEmpty) {
        getData();
      }
    });
    serviceList([
      ServiceModel(id: -1, name: "filter_service".tr),
      ServiceModel(name: 'search_booking'.tr),
      ServiceModel(name: 'search_eTicket'.tr),
      ServiceModel(name: "search_tele".tr)
    ]);
    // await getCurPosi();
    getSpecity();
    getDistricts();
  }

  Future<void> refreshData() async {
    page = 1;
    getData();
  }

  void changeSelectValue(index) {
    int selectValue = displaySelect.value;
    displaySelect(0);
    switch (selectValue) {
      case 1:
        specialtyIndex(index);
        break;
      case 2:
        serviceIndex(index);
        break;
      case 3:
        districtIndex(index);
        break;
      case 4:
        shiftIndex(index + 1);
        break;
      default:
    }
    page = 1;
    getData();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final DoctorDataModel? data = await apiService.toFetch<DoctorDataModel>(ApiCfg.doctorListUrl, arguments: {
        "page": page,
        "doctor_name": teCtrl().text.isEmpty ? null : teCtrl().text,
        "specialty_id": specialtyIndex.value != 0 ? specityList[specialtyIndex.value].id : null,
        "district_id": districtIndex.value != 0 ? districtList[districtIndex.value].id : null,
        "isAppointment": serviceIndex.value == 1 ? 1 : null,
        "isRemote": serviceIndex.value == 2 ? 1 : null,
        "hasVideoVisit": serviceIndex.value == 3
            ? 1
            : selectVisitFix.isTrue
                ? 1
                : null,
        "only_favorite": shiftIndex.value == 2 ? 1 : null,
        "is_open": shiftIndex.value == 3 ? true : null,
        "currentLat": shiftIndex.value == 1 && appSrv.curPosi != null ? appSrv.curPosi!.latitude : null,
        "currentLng": shiftIndex.value == 1 && appSrv.curPosi != null ? appSrv.curPosi!.longitude : null,
      });
      if (EasyLoading.isShow) EasyLoading.dismiss();

      if (data == null) return;
      if (data.errorCode != null) return;

      isLoadingMore(false);
      isLoading(false);

      if (page == 1) {
        doctorList(data.data);
      } else {
        doctorList.addAll(data.data!);
      }

      if (data.data!.length < 20) {
        isLoadAll(true);
      } else {
        page++;
      }
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  void getSpecity() async {
    final SpecialtyDataModel? data = await apiService.toFetch<SpecialtyDataModel>(ApiCfg.specialtiesUrl);

    if (data == null) return;
    if (data.errorCode != null) return;
    specityList(data.data);
    specityList.insert(0, SpecialtyModel(id: -1, name: "filter_specialty".tr));
  }

  void getDistricts() async {
    final DistrictDataModel? data = await apiService.toFetch<DistrictDataModel>(ApiCfg.districtsUrl);

    if (data == null) return;
    if (data.errorCode != null) return;
    districtList(data.data);
    districtList.insert(0, DistrictModel(id: -1, name: "filter_district".tr));
  }

  void cleanSearch() {
    teCtrl.update((data) {
      data!.text = '';
    });
    displaySelect(0);
    getData();
  }

  void handleSearch() {
    getData();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
