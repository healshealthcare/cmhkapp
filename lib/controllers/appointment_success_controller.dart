import 'dart:io';

import 'package:Heals/routes/app_pages.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class AppointmentSuccessController extends BaseController {
  final String tag = "CHECK_IN_CONTROLLER";
  int appointmentId = 0;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    appointmentId = Get.arguments;
  }

  void toDetail() {
    Get.offNamed(Routes.appointmentDetail, arguments: appointmentId);
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
