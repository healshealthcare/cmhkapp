import 'package:Heals/services/api_service.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

abstract class BaseController extends SuperController {
  String tag = "BASE";
  final ApiService apiService = Get.find<ApiService>();
  final GetStorage box = GetStorage();
}
