import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/check_in_data_model.dart';
import 'package:Heals/model/form_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/select_address_widget.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import 'base_controller.dart';

class CheckInFormController extends BaseController {
  final String tag = "CheckInForm_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  // BookingModel model = BookingModel();
  var appointmentId = 0;
  var teCtrl = TextEditingController();
  var formList = [].obs;
  var addrList = [].obs;
  var step = 0.obs;
  final List<SelectItemModel> areaNum = [
    SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 3, title: '853'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 5, title: '86'.tr, cancel: false, isSelected: false),
  ];
  var result = {}.obs;
  var phoneResult = {}.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    appointmentId = Get.arguments;
    getData();
    getAddressTree();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final CheckInDataModel? data = await apiService.toFetch<CheckInDataModel>(ApiCfg.visitFormUrl);
      if (data == null) return;
      if (data.errorCode != null) return;
      formList(data.data);
    } catch (e) {}
    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void getAddressTree() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final CheckInDataModel? data = await apiService.toFetch<CheckInDataModel>(ApiCfg.formTreeUrl);
      if (data == null) return;
      if (data.errorCode != null) return;
      addrList(data.addTree);
    } catch (e) {}
    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void handleSubmit() {
    showCuzDialog("Are you sure to submit the registration form? ", onConfirm: () {
      postCheckIn();
    });
  }

  void selectAddress() {
    Get.bottomSheet(SelectAddressWidget(addrList));
  }

  void postCheckIn() async {
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.visitCheckInFormUrl,
          arguments: {"appointment_id": appointmentId, "form_data": Map<String, dynamic>.from(result)});
      if (data == null) return;
      if (data.errorCode != null) return;
      Get.offNamed(Routes.checkInSuccess);
    } catch (e) {
      print(e);
    }
  }

  void handleContinue() {
    if (step.value != formList.length - 1) {
      step(step.value + 1);
    }
  }

  void handleBack() {
    if (step.value != 0) {
      step(step.value - 1);
    } else {
      showCuzDialog(
          "If you cancel now, your check-in will not be notified to the clinic. You can come back to check in when you are ready again. \nAre you sure to cancel?",
          textCancel: "No", onConfirm: () {
        Get.back();
      });
    }
  }

  void showSelect(index) {
    FormModel model = formList[step.value][index];
    Get.bottomSheet(SelectDialog(
        contentList: areaNum,
        isBlackBg: true,
        callback: (item) {
          Get.back();
          if (item.id == -1) return;
          if (phoneResult[model.itemId] != null) {
            phoneResult.update(model.itemId, (value) => item.title + "_");
          } else {
            phoneResult.addAll({model.itemId: item.title});
          }
        }));
  }

  void handleInput(index, text) {
    FormModel model = formList[step.value][index];
    if (result[model.itemId] != null) {
      result.update(model.itemId, (value) => text);
    } else {
      result.addAll({model.itemId: text});
    }
  }

  void handleAnswer(index, answer) {
    FormModel model = formList[step.value][index];
    if (model.itemType == Constants.formRadio) {
      if (result[model.itemId] != null) {
        result.update(model.itemId, (value) => answer);
      } else {
        result.addAll({model.itemId: answer});
      }
    } else {
      if (result[model.itemId] != null) {
        List<String> newResult = result[model.itemId];
        if (newResult.contains(answer)) {
          newResult.remove(answer);
        } else {
          newResult.add(answer);
        }
        result.update(model.itemId, (value) => newResult);
      } else {
        List<String> newResult = [];
        newResult.add(answer);
        result.addAll({model.itemId: newResult});
      }
    }
  }

  void showCancelder(index) {
    FormModel model = formList[step.value][index];
    Get.bottomSheet(SfDateRangePicker(
      backgroundColor: Colors.white,
      confirmText: "submit",
      selectionColor: HColors.mainColor,
      selectionMode: DateRangePickerSelectionMode.single,
      showActionButtons: true,
      onSubmit: (args) {
        DateRangePickerSelectionChangedArgs a = DateRangePickerSelectionChangedArgs(args);
        Get.back();
        if (result[model.itemId] != null) {
          result.update(model.itemId, (value) => formatDate(a.value, [yyyy, '-', mm, '-', dd]));
        } else {
          result.addAll({
            model.itemId: formatDate(a.value, [yyyy, '-', mm, '-', dd])
          });
        }
      },
      onCancel: () {
        Get.back();
      },
    ));
  }

  void handleTapInput(index, selectModelList) {
    FormModel model = formList[step.value][index];
    Get.bottomSheet(
      SelectDialog(
        contentList: selectModelList,
        isBlackBg: true,
        callback: (item) {
          Get.back();

          if (item.id == -1) return;
          if (result[model.itemId] != null) {
            result.update(model.itemId, (value) => item.title);
          } else {
            result.addAll({model.itemId: item.title});
          }
        },
      ),
    );
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
