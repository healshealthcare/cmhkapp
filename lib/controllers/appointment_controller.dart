import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/home_controller.dart';
import 'package:Heals/model/address_model.dart';
import 'package:Heals/model/appointment_data_model.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/clinic_model.dart';
import 'package:Heals/model/coupon_model.dart';
import 'package:Heals/model/doctor_data_model.dart';
import 'package:Heals/model/doctor_info_model.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:Heals/model/family_model.dart';
import 'package:Heals/model/favorite_result_model.dart';
import 'package:Heals/model/queue_data_model.dart';
import 'package:Heals/model/relationship_data_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/custom_exception.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/avatar_widget.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/nickname_edit_widget.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:Heals/widgets/text_widget.dart';
import 'package:Heals/widgets/ui_widget.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import 'base_controller.dart';

class AppointmentController extends BaseController {
  final String tag = "DoctorDetail_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var doctorInfo = DoctorInfoModel().obs;
  var doctorData = DoctorModel().obs;
  var isLoading = true.obs;
  var clinic = ClinicModel().obs;
  final List<SelectItemModel> areaNum = [
    SelectItemModel(id: 1, title: '852', cancel: false, isSelected: false),
    SelectItemModel(id: 3, title: '853', cancel: false, isSelected: false),
    SelectItemModel(id: 5, title: '86', cancel: false, isSelected: false),
  ];
  var diaType = 2.obs; // 1 诊所就诊，2 视频就诊
  var bookWay = 1.obs; // 1 Earliest Available Date ，2 Choose Your Own Date
  // List<SelectItemModel> relationshipList = [];
  List<FamilyModel> familyList = [];
  List<SelectItemModel> apmList = [SelectItemModel(id: 1, title: "AM"), SelectItemModel(id: 2, title: "PM")];
  var selectItemModel = SelectItemModel(id: -1).obs;
  var selectPhoneModel = SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false).obs;
  var selectItem1 = SelectItemModel(id: -1, title: "AM").obs;
  var selectItem2 = SelectItemModel(id: -1, title: "AM").obs;
  var selectItem3 = SelectItemModel(id: -1, title: "AM").obs;
  var date1 = DateTime.now().obs;
  var date2 = DateTime.now().obs;
  var date3 = DateTime.now().obs;
  var choice2Select = false.obs;
  var choice3Select = false.obs;
  var inputText = "".obs;
  var homeDelivery = "HOME".obs;
  var typeDelivery = "STANDARD".obs;
  var teCtrl = TextEditingController().obs;
  FocusNode focusNode = FocusNode();
  int appointmentId = 0;
  var doctorId = '';
  var clinicId = '';
  var addressModel = AddressModel().obs;
  var couponModel = CouponModel().obs;
  var familyModel = FamilyModel().obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      doctorId = Get.arguments["doctorId"];
      clinicId = Get.arguments["clinicId"];
    }
    getRelationShip();
    getData();
  }

  void getDoctorDetail() async {
    final DoctorDataModel? data = await apiService
        .toFetch<DoctorDataModel>(ApiCfg.doctorDetailUrl, arguments: {"doctor_id": doctorId, "clinic_id": clinicId});
    if (data == null) return;
    if (data.errorCode != null) return;
    doctorData.update((doctor) => {doctor!.favoriteId = (data.doctorStatus!.favoriteId ?? 0)});
  }

  void showSelect() {
    areaNum.firstWhere((item) => item.id == selectPhoneModel().id).isSelected = true;
    Get.bottomSheet(SelectDialog(
        contentList: areaNum,
        isBlackBg: true,
        callback: (item) {
          Get.back();
          if (item.id == -1) return;
          selectPhoneModel(item);
        }));
  }

  Future<void> showNicknameEdit() async {
    Get.bottomSheet(
      NicknameEditWidget(
        textEditingController: teCtrl,
        inputText: inputText,
        title: "contact_input_title".tr,
        leading: Obx(() => areaNumWidget(selectPhoneModel().title, onTap: () => showSelect())),
        hintText: inputText.value,
        toggleEditNickname: () => {},
        onSubmit: () {
          inputText(teCtrl().text);
        },
      ),
    );
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final DoctorDataModel? data = await apiService.toFetch<DoctorDataModel>(ApiCfg.doctorListUrl,
          arguments: {"page": 1, "doctor_id": doctorId, "clinic_id": clinicId});
      if (EasyLoading.isShow) EasyLoading.dismiss();

      if (data == null) return;
      if (data.errorCode != null || data.data!.isEmpty) {
        return;
      }
      DoctorModel doctorModel = data.data![0];
      doctorData(doctorModel);

      getDoctorDetail();
      doctorInfo(doctorModel.doctor);
      clinic(doctorModel.clinic);
      if (doctorData().videoVisitEnabled != 1 &&
          (doctorData().remoteEnabled == 1 || doctorData().appointmentEnabled == 1)) {
        diaType(1);
      }
      var phone = appSrv.userInfoModel().telephone;
      if (phone != null &&
          phone.isNotEmpty &&
          (phone.startsWith('86_') || phone.startsWith('853_') || phone.startsWith('852_'))) {
        inputText(phone.split("_")[1]);

        areaNum.firstWhere((item) => item.title == phone.split("_")[0]).isSelected = true;
      }
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  void handleFavorite() async {
    if (doctorInfo().isFavoriteDoctor == null) return;
    if (doctorInfo().isFavoriteDoctor! > 0) {
      final BaseModel? data =
          await apiService.toFetch<BaseModel>(ApiCfg.delFavoriteUrl, arguments: {"id": doctorData().favoriteId});
      if (data == null) return;
      if (data.errorCode != null) return;
      doctorInfo.update((data) {
        data!.isFavoriteDoctor = 0;
      });
    } else {
      final FavoriteResultModel? data = await apiService
          .toFetch<FavoriteResultModel>(ApiCfg.favoriteDoctorUrl, arguments: {"doctor_id": doctorInfo().id});
      if (data == null) return;
      if (data.errorCode != null) return;
      doctorInfo.update((data) {
        data!.isFavoriteDoctor = 1;
      });
      doctorData.update((doctor) => {doctor!.favoriteId = (data.favoriteId ?? 0)});
    }
  }

  // 获取自己地址
  void showAddress() {
    if (!appSrv.isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    Get.toNamed(Routes.address, arguments: true);
  }

  void pickDate(index) {
    DateTime initDate = DateTime.now();
    if (index == 1) {
      initDate = date1.value;
    }
    if (index == 2) {
      initDate = date2.value;
    }
    if (index == 3) {
      initDate = date3.value;
    }
    Get.bottomSheet(
      SfDateRangePicker(
        backgroundColor: Colors.white,
        confirmText: "submit",
        enablePastDates: false,
        initialSelectedDate: initDate,
        selectionColor: HColors.mainColor,
        selectionMode: DateRangePickerSelectionMode.single,
        showActionButtons: true,
        onSubmit: (args) {
          DateRangePickerSelectionChangedArgs a = DateRangePickerSelectionChangedArgs(args);
          Get.back();
          switch (index) {
            case 1:
              date1(a.value);
              break;
            case 2:
              choice2Select(true);
              date2(a.value);
              break;
            case 3:
              choice3Select(true);
              date3(a.value);
              break;
            default:
          }
        },
        onCancel: () {
          Get.back();
          switch (index) {
            case 2:
              choice2Select(false);
              break;
            case 3:
              choice2Select(false);
              break;
            default:
          }
        },
      ),
    );
  }

  void getRelationShip() async {
    final RelationshipDataModel? data = await apiService.toFetch<RelationshipDataModel>(ApiCfg.relationshipUrl);
    if (data == null) return;
    if (data.errorCode != null) return;
    familyList = data.data!;
    // for (var i = 0; i < data.data!.length; i++) {
    //   FamilyModel familyModel = data.data![i];
    //   // relationshipList.add(SelectItemModel(id: familyModel.id, title: familyModel.name));
    // }
    isLoading(false);
  }

  void showSelectRel() async {
    if (!appSrv.isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    if (!appSrv.isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    Get.toNamed(Routes.family, arguments: true);
    // Get.bottomSheet(familyWidget(), isScrollControlled: true);
  }

  void showSelectCoupon() async {
    if (!appSrv.isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    if (!appSrv.isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    Get.toNamed(Routes.couponList, arguments: {'mode': 'isSelect'});
  }

  Widget familyWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 36.0),
      decoration: const BoxDecoration(color: Colors.white),
      child: ListView.builder(
        itemBuilder: ((context, index) {
          FamilyModel model = familyList[index];
          return shadowCard(
              child: Row(
            children: [
              AvatarWidget(
                avatar: model.icon ?? '',
              ),
              titleNormalTxt(model.name)
            ],
          ));
        }),
        itemCount: familyList.length,
      ),
    );
  }

  void selectDay(index) {
    focusNode.unfocus();
    Get.bottomSheet(
      SelectDialog(
          contentList: apmList,
          isBlackBg: true,
          callback: (item) {
            Get.back();
            if (item.id == -1) return;
            switch (index) {
              case 1:
                selectItem1(item);
                break;
              case 2:
                selectItem2(item);
                break;
              case 3:
                selectItem3(item);
                break;
              default:
            }
          }),
    );
  }

  void bookAppoingment() async {
    if (!appSrv.isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    if (userinfoIsFull(appSrv.userInfoModel())) {
      Get.toNamed(Routes.profile);
      return;
    }
    // if (isLoading.isTrue) {
    //   EasyLoading.showToast("loading".tr);
    //   return;
    // }
    var phone = inputText.value;
    // if (inputText.startsWith("852")) {
    //   phone.replaceFirst(RegExp(r"852"), "852_", 1);
    // } else if (inputText.startsWith("853")) {
    //   phone.replaceFirst(RegExp(r"852"), "853_", 1);
    // } else if (inputText.startsWith("86")) {
    //   phone.replaceFirst(RegExp(r"852"), "86_", 1);
    // } else {
    //   EasyLoading.showToast("phone_input".tr);
    //   return;
    // }
    if (phone.length < 8) {
      EasyLoading.showToast("contact_input".tr);
      return;
    }
    // if (selectItemModel().id == -1) {
    //   EasyLoading.showToast("Please choose patient");
    //   return;
    // }
    try {
      final AppointmentDataModel? data =
          await apiService.toFetch<AppointmentDataModel>(ApiCfg.appointmentBookUrl, arguments: {
        "clinic_id": clinic().id,
        "doctor_id": doctorInfo().id,
        "visit_type": diaType.value == 1 ? "IN_CLINIC" : "VIDEO_VISIT",
        "contact_phone": '${selectPhoneModel().title!}_$phone',
        // "user_id": appSrv.userInfoModel().id,
        "slotDate": bookWay.value == 1 ? null : formatDate(date1.value, [yyyy, '-', mm, '-', dd]),
        "slotPeriod": bookWay.value == 1 ? null : selectItem1().title!.toLowerCase(),
        "slotOneDate": bookWay.value == 1
            ? null
            : choice2Select.isFalse
                ? null
                : formatDate(date2.value, [yyyy, '-', mm, '-', dd]),
        "slotOnePeriod": bookWay.value == 1
            ? null
            : choice2Select.isFalse
                ? null
                : selectItem2().title!.toLowerCase(),
        "slotTwoDate": bookWay.value == 1
            ? null
            : choice3Select.isFalse
                ? null
                : formatDate(date3.value, [yyyy, '-', mm, '-', dd]),
        "slotTwoPeriod": bookWay.value == 1
            ? null
            : choice3Select.isFalse
                ? null
                : selectItem3().title!.toLowerCase(),
      });

      if (data == null) return;
      if (data.errorCode != null) return;
      // EasyLoading.showToast("预约成功");
      Get.find<HomeController>().getHome();
      if (data.cancelFine != null && data.cancelFine!.transactionId != null) {
        Get.toNamed(Routes.pay, arguments: {
          "transactionId": data.cancelFine!.transactionId,
          "appointmentId": appointmentId,
          "type": "appointment"
        });
      } else {
        Get.offAndToNamed(Routes.appointmentQuestion, arguments: data.data);
      }
    } catch (e) {}
  }

  void queueNow() async {
    if (!appSrv.isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    if (doctorData().clinic!.connected == 0) return;
    // if (isLoading.isTrue) {
    //   EasyLoading.showToast("loading".tr);
    //   return;
    // }
    if (addressModel().id == null && diaType.value == 2) {
      EasyLoading.showToast("appointment_choose_address_tip".tr);
      return;
    }

    // EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final QueueDataModel? data = await apiService.toFetch<QueueDataModel>(ApiCfg.visitRemoteUrl, arguments: {
        "clinic_id": clinic().id,
        "doctor_id": doctorInfo().id,
        "is_video_visit": diaType.value == 2 ? 1 : null, // 视频预约加此参数
        "delivery_method": homeDelivery.value,
        "delivery_service": typeDelivery.value,
        "delivery_address_id": addressModel().id,
        "cmhk_coupon_code": couponModel().id,
        // "payment_method": "UNIONPAY",
        // "contact_phone": phone,
        "user_id": familyModel().id,
        // "card_id": '111',
      });
      if (EasyLoading.isShow) EasyLoading.dismiss();
      if (data == null) return;
      if (data.errorCode != null) return;
      if (data.transactionId != null) {
        Get.offAndToNamed(Routes.pay, arguments: {
          "transactionId": data.transactionId,
          "appointmentId": appointmentId,
          "type": "queue",
          "doctorId": doctorInfo().id,
          "clinicId": clinic().id,
          "visitToken": data.visitToken,
          "userId": familyModel().id
        });
      } else {
        Get.offAndToNamed(Routes.queueDetail, arguments: {"doctorId": doctorId, "clinicId": clinicId});
      }
      Get.find<HomeController>().updateData();
    } on ApiException catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
      if (e.code == 2200) {
        EasyLoading.showToast("appointment_again_tip".tr);
      }
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
