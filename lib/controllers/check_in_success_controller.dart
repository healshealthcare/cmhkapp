import 'dart:io';

import 'package:Heals/routes/app_pages.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class CheckInSuccessController extends BaseController {
  final String tag = "CHECK_IN_CONTROLLER";
  int CheckInId = 0;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    CheckInId = Get.arguments;
  }

  void toDetail() {
    // Get.toNamed(Routes.CheckInDetail, arguments: CheckInId);
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
