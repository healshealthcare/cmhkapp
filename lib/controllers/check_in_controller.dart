import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/booking_data_model.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/note_data_model.dart';
import 'package:Heals/model/note_key_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/custom_exception.dart';
import 'package:Heals/utils/utils.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class CheckInController extends BaseController {
  final String tag = "CHECK_IN_CONTROLLER";
  var appointmentList = [].obs;
  var noteKeyModel = NoteKeyModel().obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getData();
    getNotes();
  }

  // void getData() async {
  //   final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.formTreeUrl);
  //   if (data == null) return;
  //   if (data.errorCode != null) return;
  // }

  Future<void> getData({String? doctorName}) async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final BookingDataModel? data =
          await apiService.toFetch<BookingDataModel>(ApiCfg.myBookingUrl, arguments: {"state": "SUCCESS"});
      if (data == null) return;
      if (data.errorCode != null) return;
      for (var i = 0; i < data.data!.length; i++) {
        BookingModel model = data.data![i];
        if (!isExpried(model.appointmentDate ?? '', model.appointmentTime)) {
          appointmentList.add(model);
        }
      }
    } catch (e) {}

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void getNotes() async {
    final NoteDataModel? data = await apiService.toFetch<NoteDataModel>(ApiCfg.appointmentNotesUrl);
    if (data == null) return;
    if (data.errorCode != null) return;
    noteKeyModel(data.data);
  }

  void toCheckInForm(BookingModel model) async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.visitCheckinUrl,
          arguments: {"clinic_id": model.clinic!.id, "doctor_id": model.doctorId});
      if (data == null) return;
      if (data.errorCode != null) return;
      Get.offAndToNamed(Routes.queueSuccess, arguments: model.doctorId);
      getData();
    } on ApiException catch (e) {
      if (e.code == 2200) {
        EasyLoading.showToast("eticket_again_tip".tr);
      }
    }
    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  // void toCheckInForm(model) {
  //   Get.toNamed(Routes.checkInForm, arguments: model.id);
  // }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
