import 'dart:convert';
import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/constants/style.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/family_controller.dart';
import 'package:Heals/model/credential_data_model.dart';
import 'package:Heals/model/credential_model.dart';
import 'package:Heals/model/relationship_data_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/model/user_data_model.dart';
import 'package:Heals/model/user_info_model.dart';
import 'package:Heals/services/media_service.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:Heals/widgets/nickname_edit_widget.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

import 'base_controller.dart';

class ProfileController extends BaseController {
  final String tag = "PROFILE_CONTROLLER";
  final AppController appSrv = Get.find<AppController>();

  var textEditingController = TextEditingController().obs;
  var inputText = "".obs;
  List<SelectItemModel> relativeList = [];
  var relativeModel = SelectItemModel().obs;

  final List<SelectItemModel> areaNum = [
    SelectItemModel(id: 1, title: '852', cancel: false, isSelected: false),
    SelectItemModel(id: 3, title: '853', cancel: false, isSelected: false),
    SelectItemModel(id: 5, title: '86', cancel: false, isSelected: false),
  ];
  var selectItemModel = SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false).obs;
  Map<String, dynamic> phoneMap = {};
  var userInfoModel = UserInfoModel().obs;
  var profileEditType = 0.obs; // 0 修改自己  1 ：修改家人 2： 增加家人

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    textEditingController.value.addListener(() {
      inputText(textEditingController.value.text);
    });
    if (Get.arguments != null) {
      if (Get.arguments.runtimeType == String && Get.arguments == "add") {
        profileEditType(2);
      } else if (Get.arguments["dependentId"] != null) {
        userInfoModel(Get.arguments["profile"]);
        userInfoModel.update((v) {
          v!.dependentId = Get.arguments["dependentId"];
        });
        profileEditType(1);
      } else {
        updateUserInfo();
      }
    } else {
      updateUserInfo();
    }
    getRelative();
  }

  void chooseAlbum() async {
    MediaService mediaService = MediaService();
    AssetEntity? pickedFile = await mediaService.pickImage();
    List<String> resourcePathList = await getImagePathList([pickedFile!]);
    if (resourcePathList.isEmpty) return;
    final CredentialDataModel? data = await apiService.toFetch<CredentialDataModel>(ApiCfg.uploadTokenUrl);
    if (data == null) return;
    if (data.errorCode != null) return;
    CredentialModel model = data.data!;
    String resourcePath = resourcePathList[0];

    String fileType = getFileType(resourcePath);

    await uploadImg(resourcePath);
  }

  Future<void> uploadImg(path) async {
    List<int> bytes = await File.fromUri(Uri.file(path)).readAsBytes();

    String bs64 = base64Encode(bytes);
    Map<String, dynamic> args = {
      'avatar': bs64,
    };
    if (profileEditType.value == 1) {
      args["dependent_id"] = userInfoModel().dependentId;
    }

    userInfoModel.update((val) {
      val!.avatar = bs64;
    });
    try {
      await apiService.toFetch<String>(ApiCfg.changeIconUrl, arguments: args);
    } catch (e) {}

    refreshFamily();
  }

  Future<void> updateUserInfo() async {
    final UserDataModel? data = await apiService.toFetch<UserDataModel>(ApiCfg.userInfo);
    if (data == null) return;
    if (data.success == null || !data.success!) return;
    appSrv.userInfoModel(data.userProfile);
    userInfoModel(data.userProfile);
  }

  Future<void> showNicknameEdit(name) async {
    Map<String, dynamic> profile = {
      'id_num': "id_num".tr,
      'chi_name': "chi_name".tr,
      'eng_name': "eng_name".tr,
      'relationship': "relationship".tr,
      'telephone': "telephone".tr,
      // 'gender': 'May',
      'email': "email".tr,
      'address': "address".tr,
      'emerg_name': "emerg_name".tr,
      'emerg_phone': "emerg_phone".tr,
      'nickname': "nickname".tr,
    };
    String title = profile[name];
    bool isPhone = false;
    if (name == 'telephone' || name == 'emerg_phone') {
      isPhone = true;
    }
    String hintText = '';
    if (name == 'id_num') {
      hintText = 'XX999999(X)';
    }
    Get.bottomSheet(
      NicknameEditWidget(
        textEditingController: textEditingController,
        inputText: inputText,
        title: title,
        leading: !isPhone
            ? const SizedBox()
            : Obx(() => areaNumWidget(selectItemModel().title, onTap: () => showSelect(name))),
        hintText: hintText,
        toggleEditNickname: () => {},
        onSubmit: () {
          if (textEditingController().text.trim().isEmpty) return;
          if (name == "id_num") {
            RegExp idReg = RegExp(Constants.idRegex);
            if (!idReg.hasMatch(textEditingController().text.trim())) {
              EasyLoading.showToast("profile_hkid_tip".tr);
              return;
            }
          }
          if (name == "telephone") {
            RegExp idReg = RegExp(Constants.phoneRegex);
            if (!idReg.hasMatch(phoneMap[name] ?? '852${textEditingController().text.trim()}')) {
              EasyLoading.showToast("login_phone_correct_tip".tr);
              return;
            }
          }
          userEdit(name);
          textEditingController().text = '';
        },
      ),
    );
  }

  void showSelect(name) {
    areaNum.firstWhere((item) => item.id == selectItemModel().id).isSelected = true;
    Get.bottomSheet(SelectDialog(
        contentList: areaNum,
        isBlackBg: true,
        callback: (item) {
          Get.back();
          if (item.id == -1) return;
          selectItemModel(item);
          phoneMap[name] = item.title;
        }));
  }

  void changeGender() {
    List<SelectItemModel> genderList = [
      SelectItemModel(id: 1, title: 'gender_male'.tr, cancel: false, isSelected: userInfoModel().gender == "M"),
      SelectItemModel(id: 3, title: 'gender_female'.tr, cancel: false, isSelected: userInfoModel().gender == "F"),
      SelectItemModel(id: 5, title: 'gender_other'.tr, cancel: false, isSelected: userInfoModel().gender == "other"),
    ];
    showDialog(
        context: Get.context!,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SelectDialog(
              contentList: genderList,
              isBlackBg: true,
              callback: (item) {
                Get.back();
                userEdit("gender",
                    value: item.id == 1
                        ? "M"
                        : item.id == 3
                            ? "F"
                            : "other");
              });
        });
  }

  void changeRelation() {
    showDialog(
        context: Get.context!,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SelectDialog(
              contentList: relativeList,
              isBlackBg: true,
              callback: (item) {
                Get.back();
                relativeModel(item);
                userEdit("emerg_relation_id", value: item.id.toString());
              });
        });
  }

  void showCancelder() {
    Get.bottomSheet(SfDateRangePicker(
      backgroundColor: Colors.white,
      selectionColor: HColors.mainColor,
      selectionMode: DateRangePickerSelectionMode.single,
      showActionButtons: true,
      // initialDisplayDate: DateTime(userInfoModel().dob ?? ''),
      cancelText: "check_in_cancel".tr,
      confirmText: "check_in_submit".tr,
      onSubmit: (args) {
        DateRangePickerSelectionChangedArgs a = DateRangePickerSelectionChangedArgs(args);
        Get.back();
        userEdit("dob", value: formatDate(a.value, [yyyy, '-', mm, '-', dd]));
      },
      onCancel: () {
        Get.back();
      },
    ));
  }

  void userEdit(name, {value}) async {
    Map<String, dynamic> args = {
      "nickname": userInfoModel().nickname,
      "eng_name": userInfoModel().engName,
      "dob": userInfoModel().dob ?? DateTime.now().toString(),
      "telephone": userInfoModel().telephone,
      "chi_name": userInfoModel().chiName,
      "emerg_name": userInfoModel().emergName,
      "gender": userInfoModel().gender == null || userInfoModel().gender!.isEmpty ? 'Other' : userInfoModel().gender,
      "id_num": userInfoModel().id,
      "emerg_relation_id": userInfoModel().emergRelationId == null ? null : userInfoModel().emergRelationId.toString(),
      "email": userInfoModel().email,
      "emerg_phone": userInfoModel().emergPhone
    };
    if (value != null) {
      args[name] = value;
    } else if (name == 'phone' || name == 'telephone' || name == 'emerg_phone') {
      args[name] = (phoneMap[name] ?? '852') + '_' + textEditingController().text.trim();
    } else {
      args[name] = textEditingController().text.trim();
    }
    if (profileEditType.value == 2) {
      userInfoModel(UserInfoModel.fromJson(args, (value) {}));
      return;
    }

    if (profileEditType.value == 1) {
      args["dependent_id"] = userInfoModel().dependentId;
    }

    saveProfile(args);
  }

  void saveProfile(args) async {
    try {
      final UserDataModel? data = await apiService.toFetch<UserDataModel>(
          profileEditType.value == 1
              ? ApiCfg.relationshipUpdateUrl
              : profileEditType.value == 2
                  ? ApiCfg.relationshipAddUrl
                  : ApiCfg.editUserUrl,
          arguments: args);

      if (data == null) return;

      if (profileEditType.value == 0 || userInfoModel().dependentId == appSrv.userInfoModel().dependentId) {
        appSrv.userInfoModel(data.userProfile);
      }
      if (profileEditType.value == 2) {
        Get.back();
      }
      refreshFamily();
      userInfoModel(data.userProfile);
    } catch (e) {}
  }

  void refreshFamily() {
    if (Get.isRegistered<FamilyController>()) {
      Get.find<FamilyController>().getMyfamily();
    }
  }

  void getRelative() async {
    try {
      final RelationshipDataModel? data = await apiService.toFetch<RelationshipDataModel>(ApiCfg.relativeUrl);
      if (data == null) return;
      if (data.errorCode != null) return;
      for (var item in data.relationship!) {
        relativeList.add(
          SelectItemModel(
            id: item.id,
            title: item.name,
            isSelected: item.id == userInfoModel().emergRelationId,
          ),
        );
      }
      SelectItemModel? model = relativeList.firstWhere((item) => userInfoModel().emergRelationId == item.id);
      relativeModel(model);
    } catch (e) {}
  }

  void backJudge() {
    // if (!userinfoIsFull(appSrv.userInfoModel.value)) {
    //   showCuzDialog("finish_profile_tip".tr, onlySubmitBtn: true);
    // } else {
    Get.back();
    // }
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
