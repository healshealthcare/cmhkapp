import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/message_controller.dart';
import 'package:Heals/pages/account_page.dart';
import 'package:Heals/pages/message_page.dart';
import 'package:Heals/pages/home_page.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/services/notify_point_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class TabbarController extends BaseController {
  final String tag = "TABBAR_CONTROLLER";
  final NotifyPointService notifyPointService = Get.find<NotifyPointService>();

  var tabIndex = 0.obs;
  List<Widget> list = [];

  @override
  void onInit() {
    list
      ..add(HomePage())
      // ..add(CheckInPage())
      ..add(MeassagePage())
      ..add(AccountPage());
    super.onInit();
  }

  void onPressedTab(int index) {
    if (index != 0 && !Get.find<AppController>().isLogin) {
      Get.toNamed(Routes.login);
      return;
    }
    tabIndex(index);
    if (index == 1) {
      Get.find<MeassageController>().initData();
    }
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
