import 'dart:async';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/login_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class ResetPasswordController extends BaseController with SingleGetTickerProviderMixin {
  @override
  final String tag = "resetPassword_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  ScrollController tabLScrollController = ScrollController();
  ScrollController tabRScrollController = ScrollController();
  TextEditingController teCtrl = TextEditingController();
  TextEditingController resetPasswordOldteCtrl = TextEditingController();
  TextEditingController resetPasswordteCtrl = TextEditingController();
  TextEditingController resetRepPasswordteCtrl = TextEditingController();
  FocusNode focusNodePass = FocusNode();
  FocusNode focusNodePassOld = FocusNode();
  FocusNode focusNodePassRep = FocusNode();

  var countDown = 0.obs;
  Timer? cdTimer;
  var isAgree = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  void resetPassword() async {
    if (resetPasswordteCtrl.text.isEmpty) {
      focusNodePass.requestFocus();
      EasyLoading.showToast("login_pwd_input_tip".tr);
      return;
    }
    if (resetPasswordteCtrl.text.length < 8 || resetPasswordteCtrl.text.length > 16) {
      focusNodePass.requestFocus();
      EasyLoading.showToast("login_pwd_input_tip".tr);
      return;
    }

    String regex = "^(?:(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).{6,12}";
    RegExp regExp = RegExp(regex);
    if (!regExp.hasMatch(resetPasswordteCtrl.text)) {
      EasyLoading.showToast("login_pwd_correct_tip".tr);
      return;
    }

    final LoginModel? data = await apiService.toFetch<LoginModel>(ApiCfg.changePassUrl, arguments: {
      "old_password": resetPasswordOldteCtrl.text,
      "password": resetPasswordteCtrl.text,
      "password_confirmation": resetRepPasswordteCtrl.text,
    });

    if (data == null) return;
    if (data.errorCode != null) return;
    Get.find<AppController>().logout();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    cdTimer?.cancel();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
