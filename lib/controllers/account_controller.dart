import 'dart:io';

import 'package:Heals/controllers/app_controller.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class AccountController extends BaseController {
  final String tag = "ACCOUNT_CONTROLLER";
  AppController appSrv = Get.find<AppController>();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
