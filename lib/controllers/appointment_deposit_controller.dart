import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/card_model.dart';
import 'package:Heals/model/deposit_data_model.dart';
import 'package:Heals/model/deposit_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'base_controller.dart';
import 'package:get/get.dart';

class AppointmentDepositController extends BaseController {
  final String tag = "CHECK_IN_CONTROLLER";
  var teHolderCtrl = TextEditingController().obs;
  var depositModel = DepositModel().obs;
  var cardList = [].obs;
  var cardModel = CardModel().obs;
  int appointmentId = 0;
  String transactionId = '';

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    appointmentId = Get.arguments["appointmentId"];
    transactionId = Get.arguments["transactionId"];

    // teCvvCtrl.update((data) {
    //   data!.text = "915";
    // });
    getData();
    getCardInfo();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final DepositDataModel? data = await apiService.toFetch<DepositDataModel>(ApiCfg.depositSettingUrl);
      if (data == null) return;
      if (data.errorCode != null) return;

      depositModel(data.data);
    } catch (e) {}

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void getCardInfo() async {
    final DepositDataModel? data = await apiService.toFetch<DepositDataModel>(ApiCfg.unionpayUrl);
    if (data == null) return;
    if (data.errorCode != null) return;

    cardList(data.cardData);
    CardModel model = cardList[0];
    cardModel(model);
    // teCtrl.update((data) {
    //   data!.text = model.cardNo!.replaceAll("X", "");
    // });
    teHolderCtrl.update((data) {
      data!.text = model.name!;
    });
  }

  void handleDeposit() async {
    final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.unionpayPayUrl,
        arguments: {"card_id": cardModel().id, "transaction_id": transactionId});
    if (data == null) return;
    if (data.errorCode != null) return;
    Get.offAndToNamed(Routes.appointmentSuccess, arguments: appointmentId);
    EasyLoading.showToast("success");
  }

  void showCardDialog() {
    Get.bottomSheet(
      Obx(
        () => cardSelectWidget(cardList, selectId: cardModel().id, onSelect: (index) {
          cardModel(cardList[index]);
        }, onAdd: () {
          Get.back();
          Get.toNamed(
            Routes.bindDeposit,
          );
        }),
      ),
    );
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
