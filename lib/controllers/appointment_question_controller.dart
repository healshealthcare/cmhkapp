import 'dart:convert';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/model/appointment_answer_data_model.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/question_model.dart';
import 'package:Heals/model/questions_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class AppointmentQuestionController extends BaseController {
  final String tag = "CHECK_IN_CONTROLLER";
  var isLoading = true.obs;
  var questions = [].obs;
  var answer = [].obs;
  var getAnswer = 0;
  int appointmentId = 0;
  TextEditingController teCtrl = TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    appointmentId = Get.arguments;
    getQuestionnaire();
    teCtrl.addListener(() {
      if (teCtrl.text.isNotEmpty) {
        answerQues(2, teCtrl.text, isSelect: false);
      }
    });
  }

  void getQuestionnaire() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final QuestionsDataModel? data = await apiService.toFetch<QuestionsDataModel>(ApiCfg.questionnaireUrl);
      if (data == null) return;
      if (data.errorCode != null) return;
      questions(data.data);
      answer(data.data);
      isLoading(false);
      if (EasyLoading.isShow) EasyLoading.dismiss();
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  void submitQues() async {
    if (getAnswer < answer.length) {
      EasyLoading.showToast("answer_all_tip".tr);
      return;
    }
    Map<String, dynamic> params = {"appointment_id": appointmentId};
    List<Map<String, dynamic>> ansList = [];
    for (var i = 0; i < answer.length; i++) {
      QuestionModel model = answer[i];
      Map<String, dynamic> ansMap = model.toJson((value) => null);
      ansMap.remove("has_user_txt");
      ansList.add(ansMap);
    }
    params["details"] = ansList;
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final AppointmentAnswerDataModel? data = await apiService.toFetch<AppointmentAnswerDataModel>(
          ApiCfg.questionnaireAnswerUrl,
          arguments: {"appointment_id": appointmentId, "details": ansList});
      if (data == null) return;
      if (data.errorCode != null) return;
      Get.offNamed(Routes.appointmentSuccess, arguments: appointmentId);
    } catch (e) {}
    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void answerQues(index, ans, {isSelect = true}) {
    getAnswer = getAnswer + 1;
    if (answer[index].hasUserTxt == 1 && !isSelect) {
      answer[index].userTxt = ans;
    } else {
      answer[index].answer = ans;
    }

    answer.refresh();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
