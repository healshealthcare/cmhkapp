import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/controllers/home_controller.dart';
import 'package:Heals/controllers/tabbar_controller.dart';
import 'package:Heals/model/user_info_model.dart';
import 'package:Heals/model/user_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/services/notify_point_service.dart';
import 'package:Heals/services/rtc_service.dart';
import 'package:Heals/utils/geolocator.dart';
import 'package:Heals/utils/utils.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

import 'base_controller.dart';
import 'package:fl_umeng/fl_umeng.dart';

class AppController extends BaseController {
  final String tag = "APP_CONTROLLER";
  bool isLogin = false;
  late RtcService rtcs;

  var userInfoModel = UserInfoModel().obs;
  Position? curPosi;

  Future<void> getUserInfo() async {
    String? uToken = box.read<String>("uToken");
    if (uToken != null) {
      apiService.setLoginHeader({"Authorization": "Bearer $uToken"});
      updateUserInfo();
      loginInit();
      isLogin = true;
    } else {
      isLogin = false;
      // Get.offAllNamed(Routes.login);
    }
  }

  void loginInit() {
    Get.find<NotifyPointService>().run();
  }

  Future<void> updateUserInfo() async {
    try {
      final UserDataModel? data = await apiService.toFetch<UserDataModel>(ApiCfg.userInfo);
      if (data == null) return;
      if (data.success == null || !data.success!) return;
      userInfoModel(data.userProfile);
      // box.write('local', Constants.lang[data.userProfile!.receiveLang ?? Get.deviceLocale?.countryCode]);
      // Locale local = const Locale("zh", "EN");
      // if (data.userProfile!.receiveLang != 'en') {
      //   if (data.userProfile!.receiveLang!.contains("Hans")) {
      //     local = const Locale('en', 'EN');
      //   } else {
      //     local = const Locale("zh", "TW");
      //   }
      // }
      // Get.updateLocale(local);
      if (!Get.isRegistered<RtcService>()) rtcs = Get.put(RtcService());
      // judgeFullInfo();
      loginInit();
    } catch (e) {}
  }

  void judgeFullInfo() {
    bool? isFullInfo = box.read<bool>("isFullInfo");
    if (isFullInfo == null || !isFullInfo) {
      if (!userinfoIsFull(userInfoModel.value)) {
        toProfile();
      }
    }
  }

  void toProfile() {
    showCuzDialog(
      "profile_tip".tr,
      barrierDismissible: false,
      onlySubmitBtn: true,
      onConfirm: () {
        Get.toNamed(Routes.profile);
      },
    );
  }

  Future<void> getCurPosi() async {
    curPosi = await determinePosition();
  }

  void logout() {
    isLogin = false;
    apiService.setLoginHeader({});
    box.remove("uToken");
    FlUMeng().signOff();
    Get.find<HomeController>().feeds([]);
    Get.offNamed(Routes.root);
    if (Get.isRegistered<TabbarController>()) {
      Get.find<TabbarController>().tabIndex(0);
    }
    Get.toNamed(Routes.login);
    if (Get.isRegistered<NotifyPointService>()) {
      Get.find<NotifyPointService>().timer?.cancel();
    }
  }

  void umInit() async {
    await FlUMeng().init(androidAppKey: Config.androidAppKey, iosAppKey: Config.iosAppKey, channel: Config.channel);
    // logService.d('Umeng 初始化成功 = $data');
    // final bool? crash = await FlUMeng().setConfigWithCrash();
    // logService.d('UmengCrash 初始化成功 = $crash');
    // await FlUMeng().setConfigWithCrash();
    await FlUMeng().setLogEnabled(true);
  }

  Future<AppController> init() async {
    await getUserInfo();

    return this;
  }

  @override
  void onReady() async {
    super.onReady();
    umInit();
    await getCurPosi();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
