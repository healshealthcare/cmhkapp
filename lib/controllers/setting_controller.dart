import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/utils/utils.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:launch_review/launch_review.dart';

import 'base_controller.dart';

class SettingController extends BaseController {
  @override
  final String tag = "SETTING_CONTROLLER";
  var cache = ''.obs;

  void logout() {
    showCuzDialog("logout".tr, onConfirm: () async {
      try {
        await apiService.toFetch<String>(ApiCfg.logoutURL);
        // ignore: empty_catches
      } catch (e) {}
      Get.find<AppController>().logout();
    });
    // });
  }

  void deleteAccount() {
    showCuzDialog("delete_account_dialog".tr, onConfirm: () async {
      showCuzDialog("delete_account_again_dialog".tr, onConfirm: () async {
        try {
          await apiService.toFetch<String>(ApiCfg.deleteAccURL);
        } catch (e) {}
        Get.find<AppController>().logout();
      });
    });
    // });
  }

  void changeLanguage() {
    List<SelectItemModel> languageList = [
      SelectItemModel(id: 1, title: 'language_en'.tr, cancel: false, isSelected: false),
      SelectItemModel(id: 3, title: 'language_cn'.tr, cancel: false, isSelected: false),
      SelectItemModel(id: 5, title: 'language_tw'.tr, cancel: false, isSelected: false),
    ];
    showDialog(
        context: Get.context!,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SelectDialog(
              contentList: languageList,
              isBlackBg: true,
              callback: (item) {
                Get.back();

                postLanguage(item.id);
              });
        });
  }

  void postLanguage(id) async {
    Locale local = const Locale("en", "EN");
    String languageParams = 'en';
    switch (id) {
      case 1:
        local = const Locale('en', 'EN');
        // box.write('local', 'zh_EN');
        break;
      case 3:
        local = const Locale("zh", "CN");
        // box.write('local', 'zh_CN');
        languageParams = Constants.lang['cn'];
        break;
      case 5:
        local = const Locale("zh", "TW");
        // box.write('local', 'zh_TW');
        languageParams = Constants.lang['tw'];
        break;
      default:
    }
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.languageUrl, arguments: {
        "language": languageParams,
      });
      if (data == null) return;
      apiService.setLoginHeader({"Accept-Language": languageParams});
      String localStr = '${local.languageCode}_${local.countryCode!}';
      box.write('local', localStr);
      // Get.updateLocale(local);
      showCuzDialog("local_change_tip".tr, onlySubmitBtn: true);
    } catch (e) {}
  }

  void cleanCacheDia() {
    showCuzDialog("clean_cache".tr, onConfirm: () {
      cleanCache();
    });
  }

  void cleanCache() async {
    if (cache.value == '0.00' || cache.value == '0') return;
    Directory tempDir = Platform.isIOS ? await getApplicationDocumentsDirectory() : await getTemporaryDirectory();
    //删除缓存目录
    await delDir(tempDir);
    await getTotalSize();
    cache('0.00');
  }

  Future getTotalSize() async {
    FileSystemEntity tempDir =
        Platform.isIOS ? await getApplicationDocumentsDirectory() : await getTemporaryDirectory();
    double cacheTemp = await getTotalSizeOfFilesInDir(tempDir);
    cache((cacheTemp / 1024 / 1024).toStringAsFixed(2));
  }

  void onTapVersion() async {
    LaunchReview.launch(androidAppId: "com.heals.cmhkapp", iOSAppId: "1616454814");
  }

  @override
  void onReady() {
    super.onReady();

    getTotalSize();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
