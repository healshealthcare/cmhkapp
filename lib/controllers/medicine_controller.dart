import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/user_data_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class MedicineController extends BaseController {
  final String tag = "Medicine_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var medicineList = [].obs;
  var isLoading = true.obs;
  var isMedicine = false.obs;

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null && Get.arguments is bool) {
      isMedicine(Get.arguments);
    }
    getData();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final UserDataModel? data = await apiService.toFetch<UserDataModel>(ApiCfg.medicalHisUrl);
      if (data == null) return;
      if (data.errorCode != null) return;

      medicineList(data.medicineModel);
    } catch (e) {}
    isLoading(false);
    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
