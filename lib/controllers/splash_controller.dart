import 'package:get/get.dart';

import 'base_controller.dart';

class SplashController extends BaseController {
  final String tag = "HOME_CONTROLLER";
  var splashIndex = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
