import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/doctor_data_model.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class DoctorDetailController extends BaseController {
  final String tag = "DOCTOR_DETAIL_CONTROLLER";
  var doctorData = DoctorModel().obs;
  var clinicList = [].obs;
  var isShowOpen = false.obs;
  var isMedecinePage = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    if (Get.arguments != null) {
      if (Get.arguments["medicine"] != null) {
        isMedecinePage(true);
        Future.delayed(const Duration(seconds: 5), () => Get.offAndToNamed(Routes.statement, arguments: "commercial"));
      }
      doctorData(Get.arguments["doctor"]);
      // doctorData.update((data) => {data!.favoriteId = Get.arguments["doctor"]["id"]});
      clinicList([doctorData().clinic]);
    }
    super.onReady();
    getDoctorDetail();
  }

  void getDoctorDetail() async {
    final DoctorDataModel? data = await apiService.toFetch<DoctorDataModel>(ApiCfg.doctorDetailUrl,
        arguments: {"doctor_id": doctorData().doctorId, "clinic_id": doctorData().clinicId});
    if (data == null) return;
    if (data.errorCode != null) return;
    doctorData.update((doctor) => {doctor!.favoriteId = (data.doctorStatus!.favoriteId ?? 0)});
  }

  void postFavoriteDoctor() async {
    if (doctorData().doctor!.isFavoriteDoctor! > 0) {
      final BaseModel? data =
          await apiService.toFetch<BaseModel>(ApiCfg.delFavoriteUrl, arguments: {"id": doctorData().favoriteId});
      if (data == null) return;
      if (data.errorCode != null) return;
      doctorData.update((data) {
        data!.doctor!.isFavoriteDoctor = 0;
      });
    } else {
      final BaseModel? data = await apiService
          .toFetch<BaseModel>(ApiCfg.favoriteDoctorUrl, arguments: {"doctor_id": doctorData().doctor!.id});
      if (data == null) return;
      if (data.errorCode != null) return;
      doctorData.update((data) {
        data!.doctor!.isFavoriteDoctor = 1;
      });
    }
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
