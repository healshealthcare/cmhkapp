import 'dart:async';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/config/assets.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/home_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/clinic_model.dart';
import 'package:Heals/model/doctor_data_model.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:Heals/model/family_model.dart';
import 'package:Heals/model/visit_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/services/notify_point_service.dart';
import 'package:Heals/utils/custom_exception.dart';
import 'package:Heals/utils/utils.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:rive/rive.dart';

import 'base_controller.dart';

class QueueDetailController extends BaseController {
  final String tag = "ACCOUNT_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  final NotifyPointService notifyPointService = Get.find<NotifyPointService>();
  var doctorData = DoctorModel().obs;
  String? doctorId = '';
  String? visitToken = '';
  Timer? timer;
  Artboard? artboard;
  var animaIsReady = false.obs;
  var isFail = false.obs;
  var visitStatusModel = VisitDataModel().obs;
  var clinic = ClinicModel().obs;
  var clinicId = '';
  var appointmentModel = BookingModel().obs;
  var visitDetailModel = VisitDataModel().obs;
  int? userId;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      if (Get.arguments.containsKey("doctorId")) {
        doctorId = Get.arguments["doctorId"];
      }
      if (Get.arguments.containsKey("clinicId")) {
        clinicId = Get.arguments["clinicId"];
      }
      if (Get.arguments.containsKey("visitToken")) {
        visitToken = Get.arguments["visitToken"];
      }
      if (doctorId == null) {
        Get.back();
      }
      if (visitToken != null && visitToken!.length > 0) {
        List<String> visitList = visitToken!.split("|");
        userId = int.parse(visitList[visitList.length - 1]);
      } else if (Get.arguments["user_id"] != null) {
        userId = Get.arguments["user_id"];
      }
    }
    rootBundle.load(AssetsInfo.loadingRiv).then((data) async {
      final file = RiveFile.import(data);

      if (file.mainArtboard.hasAnimations) {
        artboard = file.mainArtboard;
        artboard?.addController(SimpleAnimation('Animation 1'));
        animaIsReady.toggle();
      }
    });
    timer = Timer.periodic(const Duration(seconds: Config.queuetimer), (_timer) {
      getVisitStatus();
    });
    getVisitStatus();
    getData();
  }

  void getData() async {
    try {
      final DoctorDataModel? data = await apiService.toFetch<DoctorDataModel>(ApiCfg.doctorListUrl, arguments: {
        "page": 1,
        "doctor_id": doctorId,
        "clinic_id": clinicId,
      });

      if (data == null) return;
      if (data.errorCode != null || data.data!.isEmpty) {
        return;
      }
      DoctorModel doctorModel = data.data![0];
      doctorData(doctorModel);
      // doctorInfo(doctorModel.doctor);
      clinic(doctorModel.clinic);
      if (appointmentModel().doctor == null) {
        appointmentModel(BookingModel(
          doctor: doctorModel,
          clinic: doctorModel.clinic,
          user: FamilyModel.fromJson(appSrv.userInfoModel.toJson(), (json) => null),
        ));
      }
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  void getVisitStatus() async {
    try {
      final VisitDataModel? data =
          await apiService.toFetch<VisitDataModel>(ApiCfg.visitStatusUrl, arguments: {"user_id": userId});

      if (data == null) return;
      if (data.errorCode != null) {
        return;
      }
      visitStatusModel(data);
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  // void getVisitDetail() async {
  //   try {
  //     final VisitDataModel? data =
  //         await apiService.toFetch<VisitDataModel>(ApiCfg.visitDetailUrl, arguments: {"visit_token": visitToken});

  //     if (data == null) return;
  //     if (data.errorCode != null) {
  //       return;
  //     }
  //     visitStatusModel(data);
  //   } catch (e) {
  //     if (EasyLoading.isShow) EasyLoading.dismiss();
  //   }
  // }

  void checkQueue() async {
    try {
      final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.visitCheckinUrl,
          arguments: {"clinic_id": clinic().id, "doctor_id": doctorId, "user_id": userId});
      if (data == null) return;
      if (data.errorCode != null) return;
      // Get.offAndToNamed(Routes.queueSuccess, arguments: doctorId);
    } on ApiException catch (e) {
      if (e.code == 2200) {
        // EasyLoading.showToast("eticket_again_tip".tr);
      }
    }
  }

  void handleJoinRoom() async {
    if (visitStatusModel().data == null) return;
    timer?.cancel();
    checkQueue();
    Get.offAndToNamed(
      Routes.chat,
      arguments: {"visitDetailModel": visitStatusModel().data!, "doctorModel": doctorData(), "user_id": userId},
    );
  }

  void cancelQueue() async {
    showCuzDialog("leave_queue_sure".tr, onConfirm: () async {
      try {
        final BaseModel? data =
            await apiService.toFetch<BaseModel>(ApiCfg.visitRemoteCancelUrl, arguments: {"user_id": userId});
        if (data == null) return;
        if (data.errorCode != null) {
          return;
        }
        EasyLoading.showToast("leave_tip".tr);
        Get.find<HomeController>().getHome();
        Get.back();
        Get.offAllNamed(Routes.root);
      } catch (e) {
        if (EasyLoading.isShow) EasyLoading.dismiss();
      }
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
