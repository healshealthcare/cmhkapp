import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/appointment_controller.dart';
import 'package:Heals/model/address_data_model.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/utils.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class AddressController extends BaseController {
  final String tag = "Address_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var addressList = [].obs;
  bool isSelectAddr = false;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      isSelectAddr = Get.arguments;
    }
    getMyAddress();
  }

  void getMyAddress() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final AddressDataModel? data = await apiService.toFetch<AddressDataModel>(ApiCfg.myAddressUrl);
      if (data == null) return;
      if (data.errorCode != null) return;
      addressList(data.data);
    } catch (e) {}

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void handleAddress(index) async {
    if (isSelectAddr) {
      Get.find<AppointmentController>().addressModel(addressList[index]);
      Get.back();
    } else {
      Get.toNamed(Routes.address, arguments: addressList[index]);
    }
  }

  void deleteAddress(index) async {
    showCuzDialog("delete_address_tip".tr, onConfirm: () async {
      EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
      try {
        final BaseModel? data =
            await apiService.toFetch<BaseModel>(ApiCfg.deleteAddressUrl, arguments: {"id": addressList[index].id});
        if (data == null) return;
        if (data.errorCode != null) return;
        addressList.removeAt(index);
      } catch (e) {}
      if (EasyLoading.isShow) EasyLoading.dismiss();
    });
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
