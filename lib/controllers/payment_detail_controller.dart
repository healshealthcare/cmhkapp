import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/clinic_model.dart';
import 'package:Heals/model/invoice_data_model.dart';
import 'package:Heals/model/invoice_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class PaymentDetailController extends BaseController {
  final String tag = "PAYMENT_DETAIL_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var invoiceData = InvoiceModel().obs;
  int invoiceId = 0;
  var clinic = ClinicModel().obs;
  var appointmentModel = BookingModel().obs;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      invoiceId = Get.arguments["invoice_id"];
    }
    getData();
  }

  void getData() async {
    try {
      final InvoiceDataModel? data =
          await apiService.toFetch<InvoiceDataModel>(ApiCfg.invoiceUrl, arguments: {"id": invoiceId});

      if (data == null) return;
      if (data.errorCode != null || data.data!.isEmpty) {
        return;
      }
      invoiceData(data.data![0]);
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  void toPay() {
    if (invoiceData().ticketDeposit!.amount == null) return;
    if (invoiceData().ticketDeposit!.amount! <= 0) {
      Get.offAndToNamed(Routes.queuePaySuccess);
    } else {
      Get.offAndToNamed(Routes.pay, arguments: {"transactionId": invoiceData().transactions![0].id});
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
