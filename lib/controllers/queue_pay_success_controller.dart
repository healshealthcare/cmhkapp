import 'dart:async';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/config/assets.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/model/visit_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'base_controller.dart';
import 'package:rive/rive.dart';

class QueuePaySuccessController extends BaseController {
  final String tag = "QueuePaySuccess_CONTROLLER";
  String? doctorId = '';
  String? clinicId = '';
  Artboard? artboard;
  var animaIsReady = false.obs;
  var isFail = false.obs;
  Timer? timer;
  var visitStatusModel = VisitDataModel().obs;
  int? userId = 0;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (Get.arguments != null) {
      doctorId = Get.arguments["doctorId"];
      userId = Get.arguments["userId"];
      if (Get.arguments.containsKey("clinicId")) {
        clinicId = Get.arguments["clinicId"];
      }
    }
    timer = Timer.periodic(const Duration(seconds: Config.queuetimer), (_timer) {
      getVisitStatus();
    });
    getVisitStatus();
    rootBundle.load(AssetsInfo.loadingRiv).then((data) async {
      final file = RiveFile.import(data);

      if (file.mainArtboard.hasAnimations) {
        artboard = file.mainArtboard;
        artboard?.addController(SimpleAnimation('Animation 1'));
        animaIsReady.toggle();
      }
    });
    Future.delayed(const Duration(seconds: 15), () {
      isFail(true);
      timer?.cancel();
    });
  }

  void getVisitStatus() async {
    try {
      final VisitDataModel? data =
          await apiService.toFetch<VisitDataModel>(ApiCfg.visitStatusUrl, arguments: {"user_id": userId});

      if (data == null) return;
      if (data.errorCode != null) {
        return;
      }
      visitStatusModel(data);
      if (visitStatusModel().data!.queuePos != null) {
        Get.offAndToNamed(Routes.queueDetail,
            arguments: {"doctorId": doctorId, "user_id": userId, "clinicId": clinicId});
      }
    } catch (e) {}
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
