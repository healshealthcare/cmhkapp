import 'dart:io';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/doctor_data_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class FavoriteDoctorController extends BaseController {
  final String tag = "FavoriteDoctor_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var doctorList = [].obs;

  var teCtrl = TextEditingController().obs;
  final FocusNode focusNode = FocusNode();

  @override
  void onReady() {
    super.onReady();
    getData();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    // try {
    //   final DoctorDataModel? data = await apiService.toFetch<DoctorDataModel>(ApiCfg.favoriteDoctorUrl, arguments: {
    //     // "doctor_name": teCtrl().text.isEmpty ? null : teCtrl().text,
    //   });
    //   if (EasyLoading.isShow) EasyLoading.dismiss();

    //   if (data == null) return;
    //   if (data.errorCode != null) return;
    //   doctorList(data.favoriteDoctors);
    // } catch (e) {
    //   if (EasyLoading.isShow) EasyLoading.dismiss();
    // }
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final DoctorDataModel? data = await apiService.toFetch<DoctorDataModel>(ApiCfg.doctorListUrl, arguments: {
        "page": 1,
        "doctor_name": teCtrl().text.isEmpty ? null : teCtrl().text,
        "only_favorite": 1,
      });
      if (EasyLoading.isShow) EasyLoading.dismiss();

      if (data == null) return;
      if (data.errorCode != null) return;
      doctorList(data.data);
    } catch (e) {
      if (EasyLoading.isShow) EasyLoading.dismiss();
    }
  }

  void cleanSearch() {
    teCtrl.update((data) {
      data!.text = '';
    });
    // getData(-1);
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
