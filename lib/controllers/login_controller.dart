import 'dart:async';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/constants/constant.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/login_model.dart';
import 'package:Heals/model/select_item_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/widgets/select_dialog.dart';
import 'package:fl_umeng/fl_umeng.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class LoginController extends BaseController with SingleGetTickerProviderMixin {
  @override
  final String tag = "LOGIN_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  ScrollController tabLScrollController = ScrollController();
  ScrollController tabRScrollController = ScrollController();
  TextEditingController teCtrl = TextEditingController();
  TextEditingController loginPhoneteCtrl = TextEditingController();
  TextEditingController loginCodeteCtrl = TextEditingController();
  TextEditingController loginPassteCtrl = TextEditingController();
  TextEditingController registerPhoneteCtrl = TextEditingController();
  TextEditingController registerCodeteCtrl = TextEditingController();
  TextEditingController registerPassteCtrl = TextEditingController();
  TextEditingController registerRePassteCtrl = TextEditingController();
  FocusNode focusNodePhone = FocusNode();
  FocusNode focusNodeSms = FocusNode();
  FocusNode focusNodePass = FocusNode();
  FocusNode focusNodeRegPhone = FocusNode();
  FocusNode focusNodeRegSms = FocusNode();
  FocusNode focusNodeRegPass = FocusNode();

  var isPassLoginWay = true.obs;
  var isRequesting = false;

  var selectItemModel = SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false).obs;

  final List<SelectItemModel> areaNum = [
    SelectItemModel(id: 1, title: '852'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 3, title: '853'.tr, cancel: false, isSelected: false),
    SelectItemModel(id: 5, title: '86'.tr, cancel: false, isSelected: false),
  ];

  var countDown = 0.obs;
  Timer? cdTimer;
  var isAgree = false.obs;

  late TabController tabController;

  @override
  void onInit() {
    super.onInit();
    tabController = TabController(length: 2, vsync: this);

    bool? isFirstInstall = box.read<bool>("isFirstInstall");
    if (isFirstInstall == null) {
      box.write("isFirstInstall", true);
      isFirstInstall = true;
    } else {
      isAgree(true);
    }
  }

  void changeLoginWay() {
    isPassLoginWay.toggle();
  }

  void showSelect() {
    areaNum.firstWhere((item) => item.id == selectItemModel().id).isSelected = true;
    Get.bottomSheet(SelectDialog(
        contentList: areaNum,
        isBlackBg: true,
        callback: (item) {
          Get.back();
          if (item.id == -1) return;
          selectItemModel(item);
        }));
  }

  bool isAgreeState() {
    if (isAgree.isFalse) {
      EasyLoading.showToast("statement_tip".tr);
      return false;
    }
    return true;
  }

  void getSms() async {
    if (countDown > 0) return;
    if (loginPhoneteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_phone_input_tip".tr);
      return;
    }
    countDown(59);
    cdTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (countDown.value <= 0) {
        countDown(0);
        timer.cancel();
      } else {
        countDown(countDown.value - 1);
      }
    });
    final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.userSmsReq, arguments: {
      "phone": '${selectItemModel().title}_' + loginPhoneteCtrl.text,
    });
    if (data == null) return;
    if (data.errorCode != null) return;

    EasyLoading.showToast("login_msg_sent_tip".tr);
  }

  void getRegSms() async {
    if (countDown > 0) return;
    if (registerPhoneteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_phone_input_tip".tr);
      return;
    }
    countDown(59);
    cdTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (countDown.value <= 0) {
        countDown(0);
        timer.cancel();
      } else {
        countDown(countDown.value - 1);
      }
    });
    final BaseModel? data = await apiService.toFetch<BaseModel>(ApiCfg.userSmsReq, arguments: {
      "phone": '${selectItemModel().title}_' + registerPhoneteCtrl.text,
    });
    if (data == null) return;
    if (data.errorCode != null) return;

    EasyLoading.showToast("login_msg_sent_tip".tr);
  }

  void postRegister() async {
    if (registerPhoneteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_phone_input_tip".tr);
      return;
    }
    RegExp regExpPhone = RegExp(Constants.phoneRegex);
    if (!regExpPhone.hasMatch('${selectItemModel().title}' + registerPhoneteCtrl.text)) {
      EasyLoading.showToast("login_phone_correct_tip".tr);
      return;
    }

    if (registerCodeteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_msg_input_tip".tr);
      return;
    }
    if (registerPassteCtrl.text.length < 8 || registerPassteCtrl.text.length > 16) {
      EasyLoading.showToast("login_pwd_input_tip".tr);
      return;
    }

    RegExp regExp = RegExp(Constants.passRegex);
    if (!regExp.hasMatch(registerPassteCtrl.text)) {
      EasyLoading.showToast("login_pwd_correct_tip".tr);
      return;
    }
    if (!isAgreeState()) return;
    final LoginModel? data = await apiService.toFetch<LoginModel>(ApiCfg.userRegister, arguments: {
      "phone": '${selectItemModel().title}_' + registerPhoneteCtrl.text,
      "verification": registerCodeteCtrl.text,
      "password": registerPassteCtrl.text,
    });

    if (data == null) return;
    if (data.errorCode != null) return;
    await box.write("uToken", data.token);
    if (data.token != null && data.token!.isNotEmpty) {
      await apiService.setLoginHeader({"Authorization": "Bearer ${data.token}"});
      appSrv.updateUserInfo();
      appSrv.isLogin = true;
      // Get.offAllNamed(Routes.root);
      Get.back();
    }
    EasyLoading.showToast("register_success_tip".tr);
  }

  void loginIn() async {
    if (loginPhoneteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_phone_input_tip".tr);
      return;
    }
    RegExp regExpPhone = RegExp(Constants.phoneRegex);
    if (!regExpPhone.hasMatch('${selectItemModel().title}' + loginPhoneteCtrl.text)) {
      EasyLoading.showToast("login_phone_correct_tip".tr);
      return;
    }

    if (loginPassteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_pwd_input_tip".tr);
      return;
    }
    if (loginPassteCtrl.text.length < 8 || loginPassteCtrl.text.length > 16) {
      EasyLoading.showToast("login_pwd_input_tip".tr);
      return;
    }

    if (!isAgreeState()) return;
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final LoginModel? data = await apiService.toFetch<LoginModel>(ApiCfg.userAuth, arguments: {
        "phone": '${selectItemModel().title}_' + loginPhoneteCtrl.text,
        "password": loginPassteCtrl.text,
      });

      if (data == null) return;
      if (data.errorCode != null) return;
      await box.write("uToken", data.token);
      if (data.token != null && data.token!.isNotEmpty) {
        await apiService.setLoginHeader({"Authorization": "Bearer ${data.token}"});
        appSrv.updateUserInfo();
        appSrv.isLogin = true;
        // Get.offAllNamed(Routes.root);
        Get.back();
      }
    } catch (e) {
      print(e);
    }
    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  void loginInByCode() async {
    if (loginPhoneteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_phone_input_tip".tr);
      return;
    }
    if (loginCodeteCtrl.text.isEmpty) {
      EasyLoading.showToast("login_msg_input_tip".tr);
      return;
    }

    if (!isAgreeState()) return;

    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final LoginModel? data = await apiService.toFetch<LoginModel>(ApiCfg.userAuthOtp, arguments: {
        "phone": '${selectItemModel().title}_' + loginPhoneteCtrl.text,
        "verification": loginCodeteCtrl.text,
      });

      if (data == null) return;
      if (data.errorCode != null) return;
      await box.write("uToken", data.token);

      if (data.userProfile != null && data.userProfile!.id != null) {
        FlUMeng().signIn(data.userProfile!.id!);
      }

      if (data.token != null && data.token!.isNotEmpty) {
        await apiService.setLoginHeader({"Authorization": "Bearer ${data.token}"});
        appSrv.updateUserInfo();
        Get.offAllNamed(Routes.root);
      }
    } catch (e) {}

    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    cdTimer?.cancel();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
