import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/doctor_model.dart';
import 'package:Heals/model/video_info_model.dart';
import 'package:Heals/model/visit_data_model.dart';
import 'package:Heals/model/visit_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/services/rtc_service.dart';
import 'package:Heals/utils/utils.dart';
import 'package:get/get.dart';
import 'package:wakelock/wakelock.dart';

import 'base_controller.dart';

class ChatController extends BaseController {
  final String tag = "Chat_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  final RtcService rtcService = Get.find<RtcService>();
  var channelId = ''.obs;
  var visitDetailModel = VisitModel().obs;
  var doctorModel = DoctorModel().obs;
  var videoInfo = VideoInfoModel().obs;
  var visitStatusModel = VisitDataModel().obs;
  var isCompleteDio = false.obs;
  var isEnableSpeaker = true.obs;
  var currentUid = 0.obs; // 当前占最大屏幕的id
  int? userId;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() async {
    super.onReady();
    Get.closeAllSnackbars();
    Wakelock.enable();
    if (Get.arguments != null) {
      visitDetailModel(Get.arguments["visitDetailModel"]);
      doctorModel(Get.arguments["doctorModel"]);
      userId = Get.arguments["user_id"];
      videoInfo(visitDetailModel().videoInfo);
      rtcService.onJoinChannel = () {
        getVisitStatus();
        rtcService.setEnableSpeakerphone(true);
      };
      rtcService.onRemoteJoin = (uid) {
        getVisitStatus();
      };
      await rtcService.initEngine(videoInfo().appId);
      rtcService.joinChannel(videoInfo().rtcToken, videoInfo().meetId, videoInfo().uid);
    }
  }

  void setEnableSpeakerphone() {
    rtcService.setEnableSpeakerphone(!isEnableSpeaker.isTrue);
    isEnableSpeaker.toggle();
  }

  void getVisitStatus() async {
    try {
      final VisitDataModel? data =
          await apiService.toFetch<VisitDataModel>(ApiCfg.visitStatusUrl, arguments: {"user_id": userId});

      if (data == null) return;
      if (data.errorCode != null) {
        return;
      }
      visitStatusModel(data);
      if (data.data!.doctorArrivalTime != null) {
        isCompleteDio(true);
      }
      // ignore: empty_catches
    } catch (e) {}
  }

  void onTapWin(uid) {
    currentUid(uid);
  }

  void leaveRoom() {
    showCuzDialog("chat_quit".tr, onConfirm: () {
      if (isCompleteDio.isFalse) {
        Get.back();
      } else {
        Get.offAndToNamed(Routes.doctorDetail, arguments: {"medicine": true, "doctor": doctorModel()});
      }
    });
  }

  @override
  void onClose() {
    rtcService.leaveChannel();
    Wakelock.disable();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
