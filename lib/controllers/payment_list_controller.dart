import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/booking_model.dart';
import 'package:Heals/model/clinic_model.dart';
import 'package:Heals/model/invoice_data_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'base_controller.dart';

class PaymentListController extends BaseController {
  final String tag = "PAYMENT_DETAIL_CONTROLLER";
  AppController appSrv = Get.find<AppController>();
  var invoiceData = [].obs;
  int invoiceId = 0;
  var transactionId = '';
  var clinic = ClinicModel().obs;
  var appointmentModel = BookingModel().obs;

  @override
  void onInit() {
    super.onInit();
    getData();
  }

  void getData() async {
    EasyLoading.show(status: "loading".tr, maskType: EasyLoadingMaskType.black);
    try {
      final InvoiceDataModel? data = await apiService.toFetch<InvoiceDataModel>(ApiCfg.invoiceUrl, arguments: {});

      if (data == null) return;
      if (data.errorCode != null) {
        return;
      }
      invoiceData(data.data!);
    } catch (e) {}
    if (EasyLoading.isShow) EasyLoading.dismiss();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}
}
