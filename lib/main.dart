import 'dart:async';
import 'dart:io';

import 'package:Heals/constants/constant.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/services/api_service.dart';
import 'package:Heals/services/notify_point_service.dart';
import 'package:Heals/utils/chinese_cupertino_localizations.dart';
import 'package:fl_umeng/fl_umeng.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/i18n/i18n.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:Heals/utils/utils.dart';
import 'package:get_storage/get_storage.dart';
import 'package:package_info/package_info.dart';

void main() {
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();

    await initService();
    try {
      // await Firebase.initializeApp(
      //   options: const FirebaseOptions(
      //     apiKey: 'AIzaSyB_TNCBoh9c9R-4_csJ0Ar5sTVJNTc5C8g',
      //     appId: '1:181353583158:android:e61c68242481eedc5a7e97',
      //     messagingSenderId: '181353583158',
      //     projectId: 'cmhk-app',
      //   ),
      // );
      // String? token = await FirebaseMessaging.instance.getToken();
      // EasyLoading.showToast(token ?? 'no token');
    } catch (e) {
      // EasyLoading.showToast(e.toString());
      print(e);
    }
    if (Platform.isAndroid) {
      SystemUiOverlayStyle style = const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
      );

      SystemChrome.setSystemUIOverlayStyle(style);
    } else {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    }

    I18N i18n = I18N();

    GetStorage box = GetStorage();
    String? localStr = box.read<String>('local');
    Locale? local;
    if (localStr != null && localStr.contains("_")) {
      local = Locale(localStr.split("_")[0], localStr.split("_")[1]);
    }
    bool? isFirstInstall = box.read<bool>("isFirstInstall");
    if (isFirstInstall == null) {
      box.write("isFirstInstall", true);
    }
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
      runApp(GetMaterialApp(
        theme: ThemeData.light(),
        themeMode: ThemeMode.light,
        debugShowCheckedModeBanner: false,
        defaultTransition: Transition.cupertino,
        localizationsDelegates: const [
          ChineseCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        routingCallback: (Routing? route) {
          if (route == null) return;
          if (route.current.isNotEmpty && route.current != Routes.splash && route.current != route.previous) {
            if (route.current != Routes.home) {
              // 其他页面开始统计
              FlUMeng().onPageEnd(route.previous);
              FlUMeng().onPageStart(route.current);
            } else {
              // 默认匹配页面开始统计
              FlUMeng().onPageStart('home');
            }
          }
        },
        enableLog: !Config.productMode,
        translations: i18n,
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          Get.updateLocale(deviceLocale!);
        },
        scrollBehavior: const CupertinoScrollBehavior(),
        locale: local ?? (i18n.keys.containsKey(Get.locale) ? Get.locale : null),
        supportedLocales: const <Locale>[Locale('en', 'US'), Locale('zh', 'CN'), Locale('zh', 'TW')],
        // initialRoute: AppPages.splash,
        initialRoute: isFirstInstall != null ? AppPages.initial : AppPages.splash,
        builder: EasyLoading.init(builder: (context, widget) {
          return GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
                FocusManager.instance.primaryFocus!.unfocus();
              }
              Get.closeAllSnackbars();
            },
            child: MediaQuery(
                //设置文字大小不随系统设置改变
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                child: widget!),
          );
        }),
        getPages: AppPages.routes,
        // builder: EasyLoading.init(),
        onReady: beautyToast, // toast 全局修改样式
      ));
    });
  }, (error, stackTrace) {
    // print(error);
    // reportError(error, stackTrace, TAG_NAME);
  });
}

Future<void> initService() async {
  Get.testMode = true;
  GetStorage box = GetStorage();
  await GetStorage.init();

  String? localStr = box.read<String>('local');

  if (localStr != null && localStr.contains("_")) {
    String str2 = localStr.split("_")[1];
    Config.headers["Accept-Language"] = Constants.lang[str2.toLowerCase()];
  } else {
    String countryCode = getCountryCode();
    Config.headers["Accept-Language"] = Constants.lang[countryCode.toLowerCase()];
  }
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  Config.headers[HttpHeaders.userAgentHeader] = "heals.CMHK/<${packageInfo.version}>";

  Get.put(ApiService());
  Get.put(NotifyPointService());
  await Get.putAsync<AppController>(() => AppController().init(), permanent: true);
}
