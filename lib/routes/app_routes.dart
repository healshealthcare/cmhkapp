part of 'app_pages.dart';

abstract class Routes {
  static const splash = "/splash";
  static const root = "/root";
  static const login = "/login";
  static const home = "/home";
  static const search = "/search";
  static const setting = "/setting";
  static const profile = "/profile";
  static const family = "/family";
  static const doctorDetail = "/doctor_detail";
  static const appointment = "/appointment";
  static const appointmentDetail = "/appointment_detail";
  static const appointmentSuccess = "/appointment_success";
  static const appointmentDeposit = "/appointment_deposit";
  static const appointmentQuestion = "/appointment_question";
  static const appointmentList = "/appointment_list";
  static const bindDeposit = "/bind_deposit";
  static const checkInForm = "/check_in_form";
  static const checkInSuccess = "/check_in_success";
  static const medicineBox = "/medicine_box";
  static const favoriteDoctor = "/favorite_doctor";
  static const queueDetail = "/queue_detail";
  static const queueSuccess = "/queue_success";
  static const statement = "/statement";
  static const pay = "/pay";
  static const resetPass = "/reset_pass";
  static const forgetPass = "/forget_pass";
  static const paymentList = "/payment_list";
  static const paymentDetail = "/payment_detail";
  static const chat = "/chat";
  static const address = "/address";
  static const editAddress = "/edit_address";
  static const queuePaySuccess = "/queue_pay_success";
  static const couponList = "/coupon_list";
  static const couponDetail = "/coupon_detail";
}
