import 'package:Heals/bindings/address_binding.dart';
import 'package:Heals/bindings/appointment_deposit_binding.dart';
import 'package:Heals/bindings/appointment_detail_binding.dart';
import 'package:Heals/bindings/appointment_question_binding.dart';
import 'package:Heals/bindings/appointment_success_binding.dart';
import 'package:Heals/bindings/appointment_binding.dart';
import 'package:Heals/bindings/bind_deposit_binding.dart';
import 'package:Heals/bindings/chat_binding.dart';
import 'package:Heals/bindings/check_in_binding.dart';
import 'package:Heals/bindings/coupon_binding.dart';
import 'package:Heals/bindings/coupon_detail_binding.dart';
import 'package:Heals/bindings/doctor_detail_binding.dart';
import 'package:Heals/bindings/edit_address_binding.dart';
import 'package:Heals/bindings/family_binding.dart';
import 'package:Heals/bindings/favorite_doctor_binding.dart';
import 'package:Heals/bindings/forget_password_binding.dart';
import 'package:Heals/bindings/login_binding.dart';
import 'package:Heals/bindings/appointment_list_binding.dart';
import 'package:Heals/bindings/medicine_binding.dart';
import 'package:Heals/bindings/pay_binding.dart';
import 'package:Heals/bindings/payment_detail_binding.dart';
import 'package:Heals/bindings/payment_list_binding.dart';
import 'package:Heals/bindings/profile_binding.dart';
import 'package:Heals/bindings/queue_detail_binding.dart';
import 'package:Heals/bindings/queue_pay_success_binding.dart';
import 'package:Heals/bindings/queue_success_binding.dart';
import 'package:Heals/bindings/reset_password_binding.dart';
import 'package:Heals/bindings/search_binding.dart';
import 'package:Heals/bindings/setting_binding.dart';
import 'package:Heals/bindings/splash_binding.dart';
import 'package:Heals/bindings/statement_binding.dart';
import 'package:Heals/bindings/tabbar_binding.dart';
import 'package:Heals/controllers/forger_password_controller.dart';
import 'package:Heals/pages/address_page.dart';
import 'package:Heals/pages/appointment_detail_page.dart';
import 'package:Heals/pages/appointment_question_page.dart';
import 'package:Heals/pages/appointment_deposit_page.dart';
import 'package:Heals/pages/appointment_page.dart';
import 'package:Heals/pages/appointment_success_page.dart';
import 'package:Heals/pages/bind_deposit_page.dart';
import 'package:Heals/pages/chat_page.dart';
import 'package:Heals/pages/check_in_form_page.dart';
import 'package:Heals/pages/check_in_success_page.dart';
import 'package:Heals/pages/coupon_detail_page.dart';
import 'package:Heals/pages/coupon_page.dart';
import 'package:Heals/pages/doctor_detail_page.dart';
import 'package:Heals/pages/edit_address_page.dart';
import 'package:Heals/pages/family_page.dart';
import 'package:Heals/pages/favorite_doctor_page.dart';
import 'package:Heals/pages/forger_password_page.dart';
import 'package:Heals/pages/login_page.dart';
import 'package:Heals/pages/appointment_list_page.dart';
import 'package:Heals/pages/medicine_page.dart';
import 'package:Heals/pages/pay_page.dart';
import 'package:Heals/pages/payment_detail_page.dart';
import 'package:Heals/pages/payment_list_page.dart';
import 'package:Heals/pages/profile_page.dart';
import 'package:Heals/pages/queue_detail_page.dart';
import 'package:Heals/pages/queue_pay_success_page.dart';
import 'package:Heals/pages/queue_success_page.dart';
import 'package:Heals/pages/reset_password_page.dart';
import 'package:Heals/pages/search_page.dart';
import 'package:Heals/pages/setting_page.dart';
import 'package:Heals/pages/splash_page.dart';
import 'package:Heals/pages/statement_page.dart';
import 'package:Heals/pages/tabbar_page.dart';
import 'package:get/route_manager.dart';

part 'app_routes.dart';

class AppPages {
  static const initial = Routes.root;
  static const splash = Routes.splash;

  static final routes = [
    GetPage(
      name: Routes.splash,
      page: () => SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: Routes.root,
      page: () => const TabbarPage(),
      binding: TabbarBinding(),
    ),
    GetPage(
      name: Routes.search,
      page: () => const SearchPage(),
      binding: SearchBinding(),
    ),
    GetPage(
      name: Routes.setting,
      page: () => const SettingPage(),
      binding: SettingBinding(),
    ),
    GetPage(
      name: Routes.profile,
      page: () => const ProfilePage(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: Routes.doctorDetail,
      page: () => const DoctorDetailPage(),
      binding: DoctorDetailBinding(),
    ),
    GetPage(
      name: Routes.login,
      page: () => LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.appointment,
      page: () => const AppointmentPage(),
      binding: AppointmentBinding(),
    ),
    GetPage(
      name: Routes.appointmentDetail,
      page: () => const AppointmentDetailPage(),
      binding: AppointmentDetailBinding(),
    ),
    GetPage(
      name: Routes.appointmentDeposit,
      page: () => const AppointmentDepositPage(),
      binding: AppointmentDepositBinding(),
    ),
    GetPage(
      name: Routes.appointmentSuccess,
      page: () => const AppointmentSuccessPage(),
      binding: AppointmentSuccessBinding(),
    ),
    GetPage(
      name: Routes.appointmentQuestion,
      page: () => const AppointmentQuestionPage(),
      binding: AppointmentQuestionBinding(),
    ),
    GetPage(
      name: Routes.appointmentList,
      page: () => const AppointmentListPage(),
      binding: AppointmentListBinding(),
    ),
    GetPage(
      name: Routes.bindDeposit,
      page: () => const BindDepositPage(),
      binding: BindDepositBinding(),
    ),
    GetPage(
      name: Routes.checkInForm,
      page: () => CheckInFormPage(),
      binding: CheckInBinding(),
    ),
    GetPage(
      name: Routes.checkInSuccess,
      page: () => const CheckInSuccessPage(),
      binding: CheckInBinding(),
    ),
    GetPage(
      name: Routes.medicineBox,
      page: () => MedicinePage(),
      binding: MedicineBinding(),
    ),
    GetPage(
      name: Routes.favoriteDoctor,
      page: () => const FavoriteDoctorPage(),
      binding: FavoriteDoctorBinding(),
    ),
    GetPage(
      name: Routes.queueDetail,
      page: () => QueueDetailPage(),
      binding: QueueDetailBinding(),
    ),
    GetPage(
      name: Routes.queueSuccess,
      page: () => const QueueSuccessPage(),
      binding: QueueSuccessBinding(),
    ),
    GetPage(
      name: Routes.statement,
      page: () => StatementPage(),
      binding: StatementBinding(),
    ),
    GetPage(
      name: Routes.pay,
      page: () => PayPage(),
      binding: PayBinding(),
    ),
    GetPage(
      name: Routes.resetPass,
      page: () => const ResetPasswordPage(),
      binding: ResetPasswordBinding(),
    ),
    GetPage(
      name: Routes.forgetPass,
      page: () => const ForgerPasswordPage(),
      binding: ForgetPasswordBinding(),
    ),
    GetPage(
      name: Routes.paymentDetail,
      page: () => const PaymentDetailPage(),
      binding: PaymentDetailBinding(),
    ),
    GetPage(
      name: Routes.paymentList,
      page: () => const PaymentListPage(),
      binding: PaymentListBinding(),
    ),
    GetPage(
      name: Routes.chat,
      page: () => ChatPage(),
      binding: ChatBinding(),
    ),
    GetPage(
      name: Routes.address,
      page: () => AddressPage(),
      binding: AddressBinding(),
    ),
    GetPage(
      name: Routes.editAddress,
      page: () => EditAddressPae(),
      binding: EditAddressBinding(),
    ),
    GetPage(
      name: Routes.queuePaySuccess,
      page: () => const QueuePaySuccessPage(),
      binding: QueuePaySuccessBinding(),
    ),
    GetPage(
      name: Routes.family,
      page: () => const FamilyPage(),
      binding: FamilyBinding(),
    ),
    GetPage(
      name: Routes.couponList,
      page: () => CouponPage(),
      binding: CouponBinding(),
    ),
    GetPage(
      name: Routes.couponDetail,
      page: () => CouponDetailPage(),
      binding: CouponDetailBinding(),
    ),
  ];
}
