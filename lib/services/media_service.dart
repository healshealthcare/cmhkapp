import 'dart:async';
import 'package:Heals/constants/style.dart';
import 'package:get/get.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

class MediaService extends GetxService {
  StreamSubscription? recorderSubscription;
  StreamSubscription? streamSub;

  //从相册选照片，成功则返回照片的本地路径，注：Android 必须要加 file:// 头
  Future<AssetEntity?> pickImage() async {
    final List<AssetEntity>? pickedFileList = await AssetPicker.pickAssets(
      Get.context!,
      pickerConfig: AssetPickerConfig(
        themeColor: HColors.mainColor,
        maxAssets: 1,
        filterOptions: FilterOptionGroup(
          videoOption: const FilterOption(
            durationConstraint: DurationConstraint(
              max: Duration(minutes: 1),
            ),
          ),
          imageOption: const FilterOption(
            sizeConstraint: SizeConstraint(minWidth: 1, minHeight: 1, ignoreSize: false),
          ),
        ),
      ),
    );

    if (pickedFileList == null) {
      return Future.value();
    }
    return pickedFileList[0];
  }

  //从相册多选照片，成功则返回照片的本地路径，注：Android 必须要加 file:// 头
}
