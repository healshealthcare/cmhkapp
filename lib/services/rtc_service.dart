import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';

class RtcService extends GetxService {
  late final RtcEngine _engine;
  var switchCamera = true.obs;
  var switchRender = true.obs;
  var cameraOpen = false.obs;
  var voiceOpen = true.obs;
  var remoteUid = [].obs;
  var isJoined = false.obs;
  var duringTime = 0.obs;
  var isStarted = false;
  Function? onJoinChannel;
  Function? onRemoteJoin;
  Timer? timer;

  Future<void> initEngine(appId) async {
    if (isStarted) return;
    isStarted = true;
    _engine = await RtcEngine.createWithContext(RtcEngineContext(appId));
    _addListeners();
    // _engine = await RtcEngine.createWithContext(RtcEngineContext(config.appId));
  }

  Future<void> startView() async {
    _engine.enableLocalVideo(false);
    await _engine.enableVideo();
    await _engine.startPreview();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(ClientRole.Broadcaster);
    cameraOpen(true);
    _engine.enableLocalVideo(true);
  }

  _addListeners() {
    _engine.setEventHandler(RtcEngineEventHandler(
      warning: (warningCode) {
        debugPrint('warning $warningCode');
      },
      error: (errorCode) {
        timer?.cancel();
        debugPrint('error ${errorCode}');
      },
      joinChannelSuccess: (channel, uid, elapsed) {
        debugPrint('joinChannelSuccess ${channel} ${uid} ${elapsed}');
        isJoined(true);
        timer = Timer.periodic(const Duration(seconds: 1), (timer) {
          duringTime(duringTime.value + 1);
        });
        if (onJoinChannel != null) {
          onJoinChannel!();
        }
      },
      userJoined: (uid, elapsed) {
        debugPrint('userJoined  ${uid} ${elapsed}');
        remoteUid.add(uid);
        if (onRemoteJoin != null) {
          onRemoteJoin!(uid);
        }
      },
      userOffline: (uid, reason) {
        debugPrint('userOffline  ${uid} ${reason}');
        remoteUid.removeWhere((element) => element == uid);
      },
      leaveChannel: (stats) {
        timer?.cancel();
        debugPrint('leaveChannel ${stats.toJson()}');
        isJoined(false);
        remoteUid.clear();
      },
    ));
  }

  joinChannel(token, channelName, uid) async {
    if (Platform.isAndroid) {
      Map<Permission, PermissionStatus> status = await [Permission.microphone, Permission.camera].request();
      if (!status[Permission.microphone]!.isGranted || !status[Permission.camera]!.isGranted) {
        EasyLoading.showToast('permission_ungrant'.tr);
        return;
      }
    }
    await _engine.joinChannelWithUserAccount(token, channelName, uid);
    await startView();
  }

  leaveChannel() async {
    await _engine.leaveChannel();

    await _engine.disableVideo();
  }

  rtcSwitchCamera() {
    _engine.switchCamera().then((value) {
      switchCamera.toggle();
    }).catchError((err) {
      debugPrint('switchCamera $err');
    });
  }

  rtcToggleCamera() async {
    _engine.enableLocalVideo(!cameraOpen.isTrue).then((value) => cameraOpen.toggle()).catchError((err) {
      debugPrint('toggleCamera $err');
    });
  }

  rtcToggleVoice() async {
    await _engine.enableLocalAudio(!voiceOpen.isTrue).then((value) => voiceOpen.toggle()).catchError((err) {
      debugPrint('toggleVoice $err');
    });
  }

  rtcSwitchRender() {
    switchRender.toggle();
    remoteUid(List.of(remoteUid.reversed));
  }

  void dispose() {
    timer?.cancel();
  }

  void setEnableSpeakerphone(isEnableSpeaker) {
    _engine.setEnableSpeakerphone(isEnableSpeaker);
  }
}
