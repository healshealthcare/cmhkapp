import 'dart:async';

import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/controllers/tabbar_controller.dart';
import 'package:Heals/model/message_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/services/api_service.dart';
import 'package:Heals/widgets/bility_ui_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NotifyPointService extends GetxService {
  Timer? timer;
  ApiService apiService = Get.find<ApiService>();
  var messageCount = 0.obs;

  void run() {
    if (timer != null) timer?.cancel();
    getMessageUnread();
    timer = Timer.periodic(const Duration(seconds: Config.notifyPeriod), (timer) {
      if (Get.isRegistered<AppController>()) {
        AppController appCtrl = Get.find<AppController>();
        if (appCtrl.isLogin) {
          getMessageUnread();
        }
      }
    });
  }

  void getMessageUnread() async {
    apiService.toFetch<MessageDataModel>(ApiCfg.messageUnreadUrl).then((data) {
      messageCount(data!.unread);
      if (messageCount.value > 0 &&
          Get.currentRoute != Routes.pay &&
          !Get.isSnackbarOpen &&
          !Get.isBottomSheetOpen! &&
          Get.currentRoute != Routes.chat) {
        Get.snackbar(
          '',
          '',
          titleText: notificationTip(onTap: () {
            if (Get.currentRoute != Routes.root) {
              Get.offAllNamed(Routes.root);
            }
            Future.delayed(Duration(milliseconds: Get.currentRoute != Routes.root ? 10 : 500), () {
              Get.find<TabbarController>().tabIndex(1);
            });
          }),
          messageText: const SizedBox(),
          backgroundColor: Colors.transparent,
          duration: const Duration(seconds: 15),
        );
      }
    });
  }

  void cleanCache() {
    messageCount(0);
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }
}
