import 'dart:io';

import 'package:Heals/Interceptors/auth_interceptor.dart';
import 'package:Heals/Interceptors/error_interceptor.dart';
import 'package:Heals/config/api_cfg.dart';
import 'package:Heals/config/config.dart';
import 'package:Heals/controllers/app_controller.dart';
import 'package:Heals/model/address_data_model.dart';
import 'package:Heals/model/appointment_answer_data_model.dart';
import 'package:Heals/model/appointment_data_model.dart';
import 'package:Heals/model/base_model.dart';
import 'package:Heals/model/booking_data_model.dart';
import 'package:Heals/model/cancel_data_model.dart';
import 'package:Heals/model/check_in_data_model.dart';
import 'package:Heals/model/coupon_data_model.dart';
import 'package:Heals/model/coupon_detail_data_model.dart';
import 'package:Heals/model/coupon_model.dart';
import 'package:Heals/model/credential_data_model.dart';
import 'package:Heals/model/deposit_data_model.dart';
import 'package:Heals/model/district_data_model.dart';
import 'package:Heals/model/doctor_data_model.dart';
import 'package:Heals/model/favorite_result_model.dart';
import 'package:Heals/model/home_data_model.dart';
import 'package:Heals/model/invoice_data_model.dart';
import 'package:Heals/model/login_model.dart';
import 'package:Heals/model/message_data_model.dart';
import 'package:Heals/model/note_data_model.dart';
import 'package:Heals/model/payment_data_model.dart';
import 'package:Heals/model/questions_data_model.dart';
import 'package:Heals/model/queue_data_model.dart';
import 'package:Heals/model/relationship_data_model.dart';
import 'package:Heals/model/specialty_data_model.dart';
import 'package:Heals/model/user_data_model.dart';
import 'package:Heals/model/visit_data_model.dart';
import 'package:Heals/routes/app_pages.dart';
import 'package:Heals/utils/custom_exception.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart' hide MultipartFile, FormData;
import 'package:retrofit/retrofit.dart';

part "api_service.g.dart";

@RestApi()
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @GET(ApiCfg.userInfo)
  Future<UserDataModel?> getUserInfo();

  @GET(ApiCfg.logoutURL)
  Future<BaseModel?> getLogout();

  @POST(ApiCfg.deleteAccURL)
  @FormUrlEncoded()
  Future<BaseModel?> postDelete();

  @POST(ApiCfg.userSmsReq)
  @FormUrlEncoded()
  Future<BaseModel?> postUserSmsReq(@Field("phone") String? phone, @Field("password") String? password);

  @POST(ApiCfg.forgetPswUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postForgetPsd(@Field("phone") String? phone);

  @POST(ApiCfg.userRegister)
  @FormUrlEncoded()
  Future<LoginModel?> postRegister(
    @Field("phone") String? phone,
    @Field("verification_token") String? verification,
    @Field("password") String? password,
    @Field("app_version") String? appVersion,
    @Field("source") String? source,
  );

  @POST(ApiCfg.userAuth)
  @FormUrlEncoded()
  Future<LoginModel?> postLogin(
    @Field("phone") String? phone,
    @Field("password") String? password,
    @Field("app_version") String? appVersion,
    @Field("source") String? source,
  );

  @POST(ApiCfg.changePassUrl)
  @FormUrlEncoded()
  Future<LoginModel?> postChangePass(
    @Field("old_password") String? oldPassword,
    @Field("password") String? password,
    @Field("password_confirmation") String? passwordCon,
  );

  @POST(ApiCfg.changeIconUrl)
  @FormUrlEncoded()
  Future<LoginModel?> postchangeAvatar(
    @Field("icon_base64") String? avatar,
    @Field("user_id") int? dependentId,
  );

  @POST(ApiCfg.resetPassUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postResetPass(
    @Field("token") String? token,
    @Field("password") String? password,
    @Field("password_confirmation") String? passwordCon,
  );

  @POST(ApiCfg.userAuthOtp)
  @FormUrlEncoded()
  Future<BaseModel?> postLoginOtp(
    @Field("phone") String? phone,
    @Field("verification_token") String? verification,
    @Field("app_version") String? appVersion,
  );

  @GET(ApiCfg.homeUrl)
  Future<HomeDataModel?> getHome();

  @GET(ApiCfg.specialtiesUrl)
  Future<SpecialtyDataModel?> getSpecialty();

  @GET(ApiCfg.districtsUrl)
  Future<DistrictDataModel?> getDistricts();

  @GET(ApiCfg.relationshipUrl)
  Future<RelationshipDataModel?> getRelationship();

  @POST(ApiCfg.relationshipAddUrl)
  Future<UserDataModel?> postAddFamily(
    @Field("id_num") String? idNum,
    @Field("chi_name") String? chiNam,
    @Field("eng_name") String? engName,
    @Field("dob") String? dob,
    @Field("telephone") String? telephone,
    @Field("gender") String? gender,
    @Field("email") String? email,
    @Field("address") String? address,
    @Field("emerg_name") String? emergName,
    @Field("emerg_relation_id") int? emergRelationId,
    @Field("emerg_phone") String? emergPhone,
    @Field("nickname") String? nickname,
  );

  @POST(ApiCfg.relationshipUpdateUrl)
  Future<UserDataModel?> postEditFamily(
    @Field("id_num") String? idNum,
    @Field("chi_name") String? chiNam,
    @Field("eng_name") String? engName,
    @Field("dob") String? dob,
    @Field("telephone") String? telephone,
    @Field("gender") String? gender,
    @Field("email") String? email,
    @Field("address") String? address,
    @Field("emerg_name") String? emergName,
    @Field("emerg_relation_id") int? emergRelationId,
    @Field("emerg_phone") String? emergPhone,
    @Field("nickname") String? nickname,
    @Field("dependent_id") int dependentId,
  );

  @GET(ApiCfg.relativeUrl)
  Future<RelationshipDataModel?> getRelativeship();

  @GET(ApiCfg.doctorDetailUrl)
  Future<DoctorDataModel?> getDoctorDetail(
    @Query("doctor_id") String doctorId,
    @Query("clinic_id") String clinicId,
  );

  @GET(ApiCfg.doctorListUrl)
  Future<DoctorDataModel?> getDoctorList(
    @Query("page") int page,
    @Query("doctor_id") String? doctorId,
    @Query("clinic_id") String? clinicId,
    @Query("doctor_name") String? doctorName,
    @Query("specialty_id") int? specialtyId,
    @Query("district_id") int? districtId,
    @Query("isAppointment") int? isAppointment,
    @Query("isRemote") int? isRemote,
    @Query("hasVideoVisit") int? hasVideoVisit,
    @Query("only_favorite") int? onlyFavorite,
    @Query("is_open") bool? isOpen,
    @Query("currentLat") double? currentLat,
    @Query("currentLng") double? currentLng,
  );

  @GET(ApiCfg.questionnaireUrl)
  Future<QuestionsDataModel?> getQuestionnaire();

  @POST(ApiCfg.questionnaireAnswerUrl)
  @Headers({"Content-Type": "application/json"})
  Future<AppointmentAnswerDataModel?> postQuestionnaireAns(
      @Field("appointment_id") int appoingmentId, @Field("details") List details);

  @GET(ApiCfg.myBookingUrl)
  Future<BookingDataModel?> getMyBooking(
    @Query("query") String? query,
    @Query("id") int? id,
    @Query("state") String? state,
  );

  @GET(ApiCfg.invoiceUrl)
  Future<InvoiceDataModel?> getInvoice(@Query("invoice_id") int? id);

  @GET(ApiCfg.visitStatusUrl)
  Future<VisitDataModel?> getVisitStatus(
    @Query("user_id") int? userId,
  );

  @GET(ApiCfg.messageUrl)
  Future<MessageDataModel?> getMessage(
    @Query("category") String? category,
    @Query("page") int? page,
  );

  @GET(ApiCfg.messageUnreadUrl)
  Future<MessageDataModel?> getMessageUnread();

  @GET(ApiCfg.appointmentNotesUrl)
  Future<NoteDataModel?> getAppNotes();

  @GET(ApiCfg.depositSettingUrl)
  Future<DepositDataModel?> getDepositSetting();

  @POST(ApiCfg.appointmentBookUrl)
  @FormUrlEncoded()
  Future<AppointmentDataModel?> postBookAppointment(
    @Field("clinic_id") String? clinicId,
    @Field("doctor_id") String? doctorId,
    @Field("visit_type") String? visitType,
    @Field("contact_phone") String? contactPhone,
    // @Field("user_id") int? userId,
    @Field("desired_date") String? slotsDate,
    @Field("desired_period") String? slotsPeriod,
    @Field("desired_time_slots[0][date]") String? slotsOneDate,
    @Field("desired_time_slots[0][period]") String? slotsOnePeriod,
    @Field("desired_time_slots[1][date]") String? slotsTwoDate,
    @Field("desired_time_slots[1][period]") String? slotsTwoPeriod,
  );

  @POST(ApiCfg.visitRemoteUrl)
  @FormUrlEncoded()
  Future<QueueDataModel?> postVisitRemote(
    @Field("clinic_id") String? clinicId,
    @Field("doctor_id") String? doctorId,
    @Field("is_video_visit") int? isVideoVisit,
    @Field("delivery_methoe") String? deliveryMethod,
    @Field("delivery_service") String? deliveryService,
    @Field("delivery_address_id") int? deliveryAddressId,
    @Field("cmhk_coupon_code") String? cmhkCouponId,
    @Field("user_id") int? userId,
    @Field("source") String? source,
    // @Field("payment_method") String? paymentMethod,
    // @Field("contact_phone") String? contactPhone,
  );

  @POST(ApiCfg.visitCheckinUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postVisitCheckin(
    @Field("clinic_id") String? clinicId,
    @Field("doctor_id") String? doctorId,
    @Field("user_id") int? userId,
  );

  @POST(ApiCfg.visitRemoteCancelUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postCancelVisitRemote(
    @Field("user_id") int? userId,
  );

  @POST(ApiCfg.favoriteDoctorUrl)
  @FormUrlEncoded()
  Future<FavoriteResultModel?> postFavoriteDoctor(@Field("doctor_id") String clinicId);

  @GET(ApiCfg.favoriteDoctorUrl)
  Future<DoctorDataModel?> getFavoriteDoctor();

  @POST(ApiCfg.delFavoriteUrl)
  @FormUrlEncoded()
  Future<BaseModel?> delFavoriteDoctor(@Field("favorite_id") int id);

  @POST(ApiCfg.replyAppointUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postReplyAppoint(
    @Field("appointment_id") int appointmentId,
    @Field("accept") int accept,
  );

  @POST(ApiCfg.cancelAppointUrl)
  @FormUrlEncoded()
  Future<CancelDataModel?> postCancelAppoint(
    @Field("appointment_id") int appointmentId,
  );

  @POST(ApiCfg.referralLetterUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postReferralImage(
    @Field("appointment_id") int appointmentId,
    @Field("image_url") String imageUrl,
  );

  @POST(ApiCfg.uploadTokenUrl)
  @FormUrlEncoded()
  Future<CredentialDataModel?> getUploadToken();

  @POST(ApiCfg.updateAddressUrl)
  @FormUrlEncoded()
  Future<CredentialDataModel?> postUpdateAddress(
    @Field("id") int id,
    @Field("city") String city,
    @Field("area") String area,
    @Field("region") String region,
    @Field("district") String district,
    @Field("street_addr_1") String streetAddr1,
    @Field("street_addr_2") String? streetAddr2,
    @Field("recipient_name") String recipientName,
    @Field("recipient_phone") String recipientPhone,
    @Field("is_default") int? isDefault,
  );

  @GET(ApiCfg.addressUrl)
  Future<AddressDataModel?> getAddressTree();

  @GET(ApiCfg.myAddressUrl)
  Future<AddressDataModel?> getMyAddressTree();

  @POST(ApiCfg.addAddressUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postAddAddress(
    @Field("city") String city,
    @Field("area") String area,
    @Field("region") String region,
    @Field("district") String district,
    @Field("street_addr_1") String streetAddr1,
    @Field("street_addr_2") String? streetAddr2,
    @Field("recipient_name") String recipientName,
    @Field("recipient_phone") String recipientPhone,
    @Field("is_default") int? isDefault,
  );
  @POST(ApiCfg.deleteAddressUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postDeleteAddress(@Field("id") int id);

  @POST(ApiCfg.deleteFamilyUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postDeleteFamily(@Field("dependent_id") int id);

  @GET(ApiCfg.formTreeUrl)
  Future<CheckInDataModel?> getFormTree();

  @GET(ApiCfg.visitFormUrl)
  Future<CheckInDataModel?> getVisitForm();

  @GET(ApiCfg.visitDetailUrl)
  Future<VisitDataModel?> getVisitDetail(@Queries() Map<String, dynamic>? queries);

  @POST(ApiCfg.visitCheckInFormUrl)
  @Headers({"Content-Type": "application/json"})
  Future<BaseModel?> postCheckIn(
      @Field("appointment_id") int appoingmentId, @Field("form_data") Map<String, dynamic> formData);

  @GET(ApiCfg.medicalHisUrl)
  Future<UserDataModel?> getMedicalHis();

  @GET(ApiCfg.unionpayUrl)
  Future<DepositDataModel?> getUnion();

  @GET(ApiCfg.couponGroupListUrl)
  Future<CouponDataModel?> getCouponList();

  @GET(ApiCfg.couponDetailUrl)
  Future<CouponDetailDataModel?> getCouponDetail(
    @Query("coupon_code") String? couponCode,
  );

  @POST(ApiCfg.unionpayAddUrl)
  @FormUrlEncoded()
  Future<BaseModel?> postUnionAdd(
    @Field("name") String name,
    @Field("account_no") String accountNo,
    @Field("expire_date") String expireDate,
    @Field("cvv") String cvv,
  );

  @POST(ApiCfg.unionpayPayUrl)
  Future<BaseModel?> postUnionPay(
    @Field("card_id") int cardId,
    @Field("transaction_id") String transactionId,
  );

  @POST(ApiCfg.markReadUrl)
  Future<BaseModel?> postMarkRead(
    @Field("message_id") int cardId,
  );

  @POST(ApiCfg.payUrl)
  Future<PaymentDataModel?> postPayUrl(
    @Field("transaction_id") String? transactionId,
  );

  @POST(ApiCfg.languageUrl)
  Future<PaymentDataModel?> postLanguageUrl(
    @Field("language") String? language,
  );

  @POST(ApiCfg.editUserUrl)
  Future<UserDataModel?> postEditUser(
    @Field("id_num") String? idNum,
    @Field("chi_name") String? chiNam,
    @Field("eng_name") String? engName,
    @Field("dob") String? dob,
    @Field("telephone") String? telephone,
    @Field("gender") String? gender,
    @Field("email") String? email,
    @Field("address") String? address,
    @Field("emerg_name") String? emergName,
    @Field("emerg_relation_id") int? emergRelationId,
    @Field("emerg_phone") String? emergPhone,
    @Field("nickname") String? nickname,
  );

  @GET(Config.uploadUrl)
  @Headers({})
  @MultiPart()
  Future<String?> postUpload(
    @Part(name: "key") String key,
    @Part(name: "acl") String acl,
    @Part(name: "X-Amz-Credential") String credential,
    @Part(name: "X-Amz-Algorithm") String algorithm,
    @Part(name: "X-Amz-Date") String dateTime,
    @Part(name: "Policy") String policy,
    @Part(name: "X-Amz-Signature") String signature,
    @Part(name: "x-amz-security-token") String token,
    @Part(name: "file") File file,
  );
}

class ApiService extends GetxService {
  late Dio _dio;
  late ApiClient apiClient;

  @override
  void onInit() async {
    super.onInit();

    // (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
    //   client.userAgent = null;
    // };

    BaseOptions options = BaseOptions(
      baseUrl: Config.baseURL,
      connectTimeout: Config.connTO,
      receiveTimeout: Config.receiveTO,
      responseType: Config.resp,
      headers: Config.headers,
    );

    _dio = Dio(options);

    _dio.interceptors.add(LogInterceptor(
      request: !Config.productMode,
      requestHeader: !Config.productMode,
      requestBody: !Config.productMode,
      responseHeader: !Config.productMode,
      responseBody: !Config.productMode,
      error: !Config.productMode,
    ));

    _dio.interceptors.add(AuthInterceptor());
    _dio.interceptors.add(ErrorInterceptor());

    apiClient = ApiClient(_dio);
  }

  setLoginHeader(Map<String, String> headers) {
    _dio.options.headers.addAll(headers);
  }

  resetLoginHeader() {
    _dio.options.headers = {};
  }

  Map<String, dynamic> getHeader() {
    return _dio.options.headers;
  }

  Future<T?> toFetch<T>(String uri, {Map<String, dynamic>? arguments}) async {
    late final BaseModel? model;
    try {
      switch (uri) {
        case ApiCfg.editUserUrl:
          if (arguments != null) {
            model = await apiClient.postEditUser(
              arguments["id_num"],
              arguments["chi_name"],
              arguments["eng_name"],
              arguments["dob"],
              arguments["telephone"],
              arguments["gender"],
              arguments["email"],
              arguments["address"],
              arguments["emerg_name"],
              arguments["emerg_relation_id"] == null ? null : int.parse(arguments["emerg_relation_id"]),
              arguments["emerg_phone"],
              arguments["nickname"],
            );
          } else {
            model = await apiClient.getUserInfo();
            break;
          }
          break;
        case ApiCfg.logoutURL:
          model = await apiClient.getLogout();
          break;
        case ApiCfg.deleteAccURL:
          model = await apiClient.postDelete();
          break;
        case ApiCfg.languageUrl:
          model = await apiClient.postLanguageUrl(arguments!["language"]);
          break;
        case ApiCfg.userSmsReq:
          model = await apiClient.postUserSmsReq(arguments!["phone"], arguments["password"]);
          break;
        case ApiCfg.forgetPswUrl:
          model = await apiClient.postForgetPsd(arguments!["phone"]);
          break;
        case ApiCfg.userRegister:
          model = await apiClient.postRegister(
            arguments!["phone"],
            arguments["verification"],
            arguments["password"],
            '1.0.0',
            'CMHK',
          );
          break;
        case ApiCfg.userAuth:
          model = await apiClient.postLogin(
            arguments!["phone"],
            arguments["password"],
            '1.0.0',
            'CMHK',
          );
          break;
        case ApiCfg.changePassUrl:
          model = await apiClient.postChangePass(
            arguments!["old_password"],
            arguments["password"],
            arguments["password_confirmation"],
          );
          break;
        case ApiCfg.changeIconUrl:
          model = await apiClient.postchangeAvatar(arguments!["avatar"], arguments["dependent_id"]);
          break;
        case ApiCfg.resetPassUrl:
          model = await apiClient.postResetPass(
            arguments!["token"],
            arguments["password"],
            arguments["password_confirmation"],
          );
          break;
        case ApiCfg.userAuthOtp:
          model = await apiClient.postLoginOtp(
            arguments!["phone"],
            arguments["verification"],
            '1.0.0',
          );
          break;
        case ApiCfg.homeUrl:
          model = await apiClient.getHome();
          break;
        case ApiCfg.specialtiesUrl:
          model = await apiClient.getSpecialty();
          break;
        case ApiCfg.districtsUrl:
          model = await apiClient.getDistricts();
          break;
        case ApiCfg.relationshipUrl:
          if (arguments != null) {
            model = await apiClient.postAddFamily(
              arguments["id_num"],
              arguments["chi_name"],
              arguments["eng_name"],
              arguments["dob"],
              arguments["telephone"],
              arguments["gender"],
              arguments["email"],
              arguments["address"],
              arguments["emerg_name"],
              arguments["emerg_relation_id"] == null ? null : int.parse(arguments["emerg_relation_id"]),
              arguments["emerg_phone"],
              arguments["nickname"],
            );
          } else {
            model = await apiClient.getRelationship();
          }
          break;
        case ApiCfg.relationshipUpdateUrl:
          model = await apiClient.postEditFamily(
            arguments!["id_num"],
            arguments["chi_name"],
            arguments["eng_name"],
            arguments["dob"],
            arguments["telephone"],
            arguments["gender"],
            arguments["email"],
            arguments["address"],
            arguments["emerg_name"],
            arguments["emerg_relation_id"] == null ? null : int.parse(arguments["emerg_relation_id"]),
            arguments["emerg_phone"],
            arguments["nickname"],
            arguments["dependent_id"],
          );
          break;
        case ApiCfg.relativeUrl:
          model = await apiClient.getRelativeship();
          break;
        case ApiCfg.doctorDetailUrl:
          model = await apiClient.getDoctorDetail(arguments!["doctor_id"], arguments["clinic_id"]);
          break;
        case ApiCfg.doctorListUrl:
          model = await apiClient.getDoctorList(
            arguments!["page"],
            arguments["doctor_id"],
            arguments["clinic_id"],
            arguments["doctor_name"],
            arguments["specialty_id"],
            arguments["district_id"],
            arguments["isAppointment"],
            arguments["isRemote"],
            arguments["hasVideoVisit"],
            arguments["only_favorite"],
            arguments["is_open"],
            arguments["currentLat"],
            arguments["currentLng"],
          );
          break;
        case ApiCfg.questionnaireUrl:
          model = await apiClient.getQuestionnaire();
          break;
        case ApiCfg.questionnaireAnswerUrl:
          model = await apiClient.postQuestionnaireAns(arguments!["appointment_id"], arguments["details"]);
          break;
        case ApiCfg.appointmentBookUrl:
          model = await apiClient.postBookAppointment(
            arguments!["clinic_id"],
            arguments["doctor_id"],
            arguments["visit_type"],
            arguments["contact_phone"],
            // arguments["user_id"],
            arguments["slotDate"],
            arguments["slotPeriod"],
            arguments["slotOneDate"],
            arguments["slotOnePeriod"],
            arguments["slotTwoDate"],
            arguments["slotTwoPeriod"],
          );
          break;
        case ApiCfg.visitRemoteUrl:
          model = await apiClient.postVisitRemote(
            arguments!["clinic_id"],
            arguments["doctor_id"],
            arguments["is_video_visit"],
            arguments["delivery_method"],
            arguments["delivery_service"],
            arguments["delivery_address_id"],
            arguments["cmhk_coupon_code"],
            arguments["user_id"],
            'CMHK',
            // arguments["payment_method"],
            // arguments["contact_phone"],
          );
          break;
        case ApiCfg.visitCheckinUrl:
          model = await apiClient.postVisitCheckin(
            arguments!["clinic_id"],
            arguments["doctor_id"],
            arguments["user_id"],
          );
          break;
        case ApiCfg.visitRemoteCancelUrl:
          model = await apiClient.postCancelVisitRemote(
            arguments!["user_id"],
          );
          break;
        case ApiCfg.favoriteDoctorUrl:
          if (arguments!["doctor_id"] != null) {
            model = await apiClient.postFavoriteDoctor(arguments["doctor_id"]);
          } else {
            model = await apiClient.getFavoriteDoctor();
          }
          break;
        case ApiCfg.delFavoriteUrl:
          model = await apiClient.delFavoriteDoctor(arguments!["id"]);
          break;
        case ApiCfg.cancelAppointUrl:
          model = await apiClient.postCancelAppoint(arguments!["appointment_id"]);
          break;
        case ApiCfg.referralLetterUrl:
          model = await apiClient.postReferralImage(arguments!["appointment_id"], arguments["image_url"]);
          break;
        case ApiCfg.replyAppointUrl:
          model = await apiClient.postReplyAppoint(
            arguments!["appointment_id"],
            arguments["accept"],
          );
          break;
        case ApiCfg.myBookingUrl:
          model = await apiClient.getMyBooking(arguments!["query"], arguments["id"], arguments["state"]);
          break;
        case ApiCfg.messageUrl:
          model = await apiClient.getMessage(arguments!["category"], arguments["page"]);
          break;
        case ApiCfg.invoiceUrl:
          model = await apiClient.getInvoice(arguments!["id"]);
          break;
        case ApiCfg.visitStatusUrl:
          model = await apiClient.getVisitStatus(arguments!["user_id"]);
          break;
        case ApiCfg.messageUnreadUrl:
          model = await apiClient.getMessageUnread();
          break;
        case ApiCfg.appointmentNotesUrl:
          model = await apiClient.getAppNotes();
          break;
        case ApiCfg.depositSettingUrl:
          model = await apiClient.getDepositSetting();
          break;
        case ApiCfg.formTreeUrl:
          model = await apiClient.getFormTree();
          break;
        case ApiCfg.addressUrl:
          model = await apiClient.getAddressTree();
          break;
        case ApiCfg.myAddressUrl:
          model = await apiClient.getMyAddressTree();
          break;
        case ApiCfg.addAddressUrl:
          model = await apiClient.postAddAddress(
            arguments!["city"],
            arguments["area"],
            arguments["region"],
            arguments["district"],
            arguments["street_addr_1"],
            arguments["street_addr_2"],
            arguments["recipient_name"],
            arguments["recipient_phone"],
            arguments["is_default"],
          );
          break;
        case ApiCfg.deleteAddressUrl:
          model = await apiClient.postDeleteAddress(arguments!["id"]);
          break;
        case ApiCfg.updateAddressUrl:
          model = await apiClient.postUpdateAddress(
            arguments!["id"],
            arguments["city"],
            arguments["area"],
            arguments["region"],
            arguments["district"],
            arguments["street_addr_1"],
            arguments["street_addr_2"],
            arguments["recipient_name"],
            arguments["recipient_phone"],
            arguments["is_default"],
          );
          break;
        case ApiCfg.deleteFamilyUrl:
          model = await apiClient.postDeleteFamily(arguments!["dependent_id"]);
          break;
        case ApiCfg.visitFormUrl:
          model = await apiClient.getVisitForm();
          break;
        case ApiCfg.visitDetailUrl:
          model = await apiClient.getVisitDetail(
            {"visit_token": arguments!["visit_token"]},
          );
          break;
        case ApiCfg.visitCheckInFormUrl:
          model = await apiClient.postCheckIn(arguments!["appointment_id"], arguments["form_data"]);
          break;
        case ApiCfg.medicalHisUrl:
          model = await apiClient.getMedicalHis();
          break;
        case ApiCfg.unionpayUrl:
          model = await apiClient.getUnion();
          break;
        case ApiCfg.couponGroupListUrl:
          model = await apiClient.getCouponList();
          break;
        case ApiCfg.couponDetailUrl:
          model = await apiClient.getCouponDetail(arguments!["coupon_code"]);
          break;
        case ApiCfg.unionpayAddUrl:
          model = await apiClient.postUnionAdd(
            arguments!["account_no"],
            arguments["name"],
            arguments["expire_date"],
            arguments["cvv"],
          );
          break;
        case ApiCfg.unionpayPayUrl:
          model = await apiClient.postUnionPay(arguments!["card_id"], arguments["transaction_id"]);
          break;
        case ApiCfg.markReadUrl:
          model = await apiClient.postMarkRead(arguments!["message_id"]);
          break;
        case ApiCfg.payUrl:
          model = await apiClient.postPayUrl(arguments!["transaction_id"]);
          break;
        case ApiCfg.uploadTokenUrl:
          model = await apiClient.getUploadToken();
          break;
        case Config.uploadUrl:
          model = null;
          try {
            await apiClient.postUpload(
              arguments!["key"],
              arguments["acl"],
              arguments["X-Amz-Credential"],
              arguments["X-Amz-Algorithm"],
              arguments["X-Amz-Date"],
              arguments["Policy"],
              arguments["X-Amz-Signature"],
              arguments["x-amz-security-token"],
              arguments["file"],
            );
          } catch (e) {}
          break;
      }
    } on DioError catch (e) {
      model = BaseModel.fromJson(
        e.response!.data,
        (json) => json as dynamic,
      );
    } catch (e) {
      print("DioError: $e");
    }

    if (model == null) {
      return Future.value(null);
    }

    if (model.errorCode != null && model.errorCode!.isNotEmpty) {
      switch (model.errorCode) {
        case '1000':
          if (model.errorMessages != null &&
              model.errorMessages![0].isNotEmpty &&
              model.errorMessages![0] != 'Unauthenticated') {
            EasyLoading.showToast(model.errorMessages![0]);
            Get.find<AppController>().isLogin = false;
          }
          throw ApiException([int.parse(model.errorCode!), model.errorMessages![0]]);
        case '2200':
          if (Get.currentRoute == Routes.profile || Get.currentRoute == Routes.login) {
            EasyLoading.showToast(model.errorMessages![0]);
          }
          throw ApiException([int.parse(model.errorCode!), model.errorMessages![0]]);
        default:
          if (model.errorMessages != null && model.errorMessages![0].isNotEmpty) {
            EasyLoading.showToast(model.errorMessages![0]);
          }
          throw Exception(model.errorCode);
      }
    }

    return model as T;
  }
}
